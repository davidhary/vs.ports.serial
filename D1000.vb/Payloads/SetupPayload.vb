Imports isr.Ports.Serial
Imports isr.Ports.Teleport

''' <summary> A payload consisting of the setup information. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Class SetupPayload
    Implements IPayload

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " SIMULATE "

    ''' <summary> Simulate payload. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress"> The module address. </param>
    ''' <returns> A SetupPayload. </returns>
    Public Shared Function SimulatePayload(ByVal moduleAddress As Byte) As SetupPayload
        Dim result As New SetupPayload() With {.ModuleAddress = moduleAddress,
                                               .LinefeedEnabled = False, .ParityEnabled = False, .Parity = IO.Ports.Parity.Even, .BaudRate = 9600}
        Return result
    End Function

#End Region

#Region " VALUES "

    ''' <summary> Gets or sets the module address. </summary>
    ''' <value> The module address. </value>
    Public Property ModuleAddress As Byte

    ''' <summary> Gets or sets the module address. </summary>
    ''' <value> The module address. </value>
    Public Property ModuleAddressPayload As String
        Get
            Return Me.ModuleAddress.ToHex()
        End Get
        Set(value As String)
            Me.ModuleAddress = Byte.Parse(value, Globalization.NumberStyles.HexNumber)
        End Set
    End Property

    ''' <summary> Gets or sets the module address packed nibbles. </summary>
    ''' <value> The module address packed nibbles. </value>
    Public Property ModuleAddressNibbles As PackedNibblesInt8
        Get
            Dim result As New PackedNibblesInt8(Me.ModuleAddress)
            Return result
        End Get
        Set(value As PackedNibblesInt8)
            If value IsNot Nothing Then Me.ModuleAddress = value.PackedValue.Value
        End Set
    End Property

    ''' <summary> Gets or sets the line feeds bit. </summary>
    ''' <value> The line feeds bit. </value>
    Public Property LinefeedsBit As Byte

    ''' <summary> Gets or sets the linefeed enabled. </summary>
    ''' <value> The linefeed enabled. </value>
    Public Property LinefeedEnabled As Boolean
        Get
            Return Me.LinefeedsBit = 1
        End Get
        Set(value As Boolean)
            Me.LinefeedsBit = CByte(If(value, 1, 0))
        End Set
    End Property

    ''' <summary> Gets or sets the parity enabled bit. </summary>
    ''' <value> The parity enabled bit. </value>
    Public Property ParityEnabledBit As Byte

    ''' <summary> Gets or sets the parity enabled. </summary>
    ''' <value> The parity enabled. </value>
    Public Property ParityEnabled As Boolean
        Get
            Return Me.ParityEnabledBit = 1
        End Get
        Set(value As Boolean)
            Me.ParityEnabledBit = CByte(If(value, 1, 0))
        End Set
    End Property

    ''' <summary> Gets or sets the parity bit. </summary>
    ''' <value> The parity bit. </value>
    Public Property ParityBit As Byte

    ''' <summary> Gets or sets the parity. </summary>
    ''' <value> The parity. </value>
    Public Property Parity As System.IO.Ports.Parity
        Get
            Return If(Me.ParityBit = 1, IO.Ports.Parity.Odd, IO.Ports.Parity.Even)
        End Get
        Set(value As System.IO.Ports.Parity)
            Me.ParityBit = CByte(If(value = IO.Ports.Parity.Even, 0, 1))
        End Set
    End Property

    ''' <summary> Gets or sets the zero-based index of the baud rate. </summary>
    ''' <value> The baud rate index. </value>
    Public Property BaudRateIndex As Byte

    ''' <summary> Gets or sets the supported baud rates. </summary>
    ''' <value> The supported baud rates. </value>
    Private ReadOnly Property SupportedBaudRates As Integer() = New Integer() {300, 600, 1200, 2400, 4800, 9600, 19200, 38400}

    ''' <summary> Gets or sets the baud rate. </summary>
    ''' <value> The baud rate. </value>
    Public Property BaudRate As Integer
        Get
            Return Me.SupportedBaudRates(Me.BaudRateIndex - 7)
        End Get
        Set(value As Integer)
            Dim i As Byte = 0
            For Each rate As Integer In Me.SupportedBaudRates
                If value <= rate Then Exit For
                i += CByte(1)
            Next
            Me.BaudRateIndex = CByte(i + 7)
        End Set
    End Property

    ''' <summary> Gets or sets the communication nibbles. </summary>
    ''' <value> The communication nibbles. </value>
    Public Property CommunicationNibbles As PackedNibblesInt8
        Get
            Dim result As New PackedNibblesInt8((Me.LinefeedsBit << 7) Or (Me.ParityBit << 6) Or (Me.ParityEnabledBit << 5) Or Me.BaudRateIndex)
            Return result
        End Get
        Set(value As PackedNibblesInt8)
            If value IsNot Nothing Then
                Me.LinefeedsBit = CByte(1 And (value.PackedValue.Value >> 7))
                Me.ParityBit = CByte(1 And (value.PackedValue.Value >> 6))
                Me.ParityEnabledBit = CByte(1 And (value.PackedValue.Value >> 5))
                Me.BaudRateIndex = CByte(7 And value.PackedValue.Value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the options nibbles. </summary>
    ''' <value> The options nibbles. </value>
    Public Property OptionsNibbles As PackedNibblesInt8

    ''' <summary> Gets or sets the display, filtering and time constant nibbles. </summary>
    ''' <value> The display nibbles. </value>
    Public Property DisplayNibbles As PackedNibblesInt8 = New PackedNibblesInt8(&H82)

#End Region

#Region " PAYLOAD "

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary> Gets or sets the reading. </summary>
    ''' <value> The reading. </value>
    Public Property Reading As String
        Get
            Return Me._Reading
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Reading) Then
                Me._Reading = value
                Me.Payload = value.ToHexBytes
            End If
        End Set
    End Property

    ''' <summary> The payload. </summary>
    Private _Payload As List(Of Byte)

    ''' <summary> Gets or sets the payload. </summary>
    ''' <value> The payload. </value>
    Public Property Payload As IEnumerable(Of Byte) Implements IPayload.Payload
        Get
            Me._Payload = New List(Of Byte) From {
                Me.ModuleAddressNibbles.PackedValue.HighNibble,
                Me.ModuleAddressNibbles.PackedValue.LowNibble,
                Me.CommunicationNibbles.PackedValue.HighNibble,
                Me.CommunicationNibbles.PackedValue.LowNibble,
                Me.OptionsNibbles.PackedValue.HighNibble,
                Me.OptionsNibbles.PackedValue.LowNibble,
                Me.DisplayNibbles.PackedValue.HighNibble,
                Me.DisplayNibbles.PackedValue.LowNibble
            }
            Return Me._Payload
        End Get
        Set(value As IEnumerable(Of Byte))
            Me._Payload = New List(Of Byte)(value)
            If value?.Any Then
                Me.Reading = value.ToHex
                Dim values As New Queue(Of Byte)(value)
                Me.ModuleAddressNibbles = New PackedNibblesInt8(values.Dequeue, values.Dequeue)
                Me.CommunicationNibbles = New PackedNibblesInt8(values.Dequeue, values.Dequeue)
                Me.OptionsNibbles = New PackedNibblesInt8(values.Dequeue, values.Dequeue)
                Me.DisplayNibbles = New PackedNibblesInt8(values.Dequeue, values.Dequeue)
            Else
                Me.Reading = String.Empty
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Public Property StatusCode As StatusCode Implements IPayload.StatusCode

#End Region

#Region " I PAYLOAD IMPLEMENTATION "

    ''' <summary> Populates the value from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> A StatusCode. </returns>
    Public Function Parse(ByVal values As IEnumerable(Of Byte)) As StatusCode Implements IPayload.Parse
        Me.Payload = values.ToHex.ToHexBytes
        Return Me.StatusCode
    End Function

    ''' <summary> Returns the payload value in bytes. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> This object as an IEnumerable(Of Byte) </returns>
    Public Function Build() As IEnumerable(Of Byte) Implements IPayload.Build
        Return Me.Payload.ToHex.ToHexBytes
    End Function

#End Region

End Class

