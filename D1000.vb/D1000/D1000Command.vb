Imports System.ComponentModel

Imports isr.Core.EnumExtensions
Imports isr.Ports.Teleport

''' <summary> Defines the D1000 family command. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-16 </para>
''' </remarks>
Public Class D1000Command
    Inherits ProtocolCommand

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The command code. </param>
    Public Sub New(ByVal commandCode As D1000CommandCode)
        MyBase.New(commandCode)
    End Sub

    ''' <summary>
    ''' Gets or sets the time it takes from the receipt of the command to when the module starts to
    ''' transmit a response.
    ''' </summary>
    ''' <value> The command timeout. </value>
    Public Overrides Property TurnaroundTime As TimeSpan
        Get
            Return D1000Command.SelectTurnaroundTime(Me.CommandAscii)
        End Get
        Set(value As TimeSpan)
            MyBase.TurnaroundTime = value
        End Set
    End Property

    ''' <summary> The ten millisecond commands. </summary>
    Private Shared ReadOnly TenMillisecondCommands As String() = New String() {"DI", "DO", "RD", "WE"}

    ''' <summary>
    ''' Selects the time it takes from the receipt of the command to when the module starts to
    ''' transmit a response.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function SelectTurnaroundTime(ByVal commandAscii As String) As TimeSpan
        Return If(D1000Command.TenMillisecondCommands.Contains(commandAscii, StringComparer.OrdinalIgnoreCase),
                    TimeSpan.FromMilliseconds(10),
                    TimeSpan.FromMilliseconds(100))
    End Function

End Class

''' <summary> Dictionary of D1000 commands. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-16 </para>
''' </remarks>
Public NotInheritable Class D1000CommandCollection
    Inherits ProtocolCommandCollection

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub New()
        MyBase.New(GetType(D1000CommandCode))
    End Sub

#End Region

#Region " SINGLETON "

    ''' <summary>
    ''' Gets or sets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property SyncLocker As New Object

    ''' <summary> Gets or sets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance As D1000CommandCollection

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As D1000CommandCollection
        If D1000CommandCollection.Instance Is Nothing Then
            SyncLock SyncLocker
                D1000CommandCollection.Instance = New D1000CommandCollection
            End SyncLock
        End If
        Return D1000CommandCollection.Instance
    End Function

#End Region

End Class

''' <summary> Values that represent the D1000 commands. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum D1000CommandCode

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("(None)")>
    None

    ''' <summary> An enum constant representing the read digital inputs option. </summary>
    <Description("Read Digital Inputs (DI)")>
    ReadDigitalInputs

    ''' <summary> An enum constant representing the set digital outputs option. </summary>
    <Description("Set Digital Outputs (DO)")>
    SetDigitalOutputs

    ''' <summary> An enum constant representing the new data option. </summary>
    <Description("New Data (ND)")>
    NewData

    ''' <summary> An enum constant representing the read data option. </summary>
    <Description("Read Data (RD)")>
    ReadData

    ''' <summary> An enum constant representing the read events option. </summary>
    <Description("Read Events (RE)")>
    ReadEvents

    ''' <summary> An enum constant representing the read low alarm option. </summary>
    <Description("Read Low Alarm (RL)")>
    ReadLowAlarm

    ''' <summary> An enum constant representing the read high alarm option. </summary>
    <Description("Read High Alarm (RH)")>
    ReadHighAlarm

    ''' <summary> An enum constant representing the read setup option. </summary>
    <Description("Read Setup (RS)")>
    ReadSetup

    ''' <summary> An enum constant representing the read zero register option. </summary>
    <Description("Read Zero Register (RZ)")>
    ReadZeroRegister

    ''' <summary> An enum constant representing the write enable option. </summary>
    <Description("Write Enable (WE)")>
    WriteEnable

    ''' <summary> An enum constant representing the clear alarms option. Write protected Command. </summary>
    <Description("Clear Alarms (CA)")>
    ClearAlarms

    ''' <summary> An enum constant representing the clear events option. Write protected Command. </summary>
    <Description("Clear Events (CE)")>
    ClearEvents

    ''' <summary> An enum constant representing the clear zero register option. Write protected Command. </summary>
    <Description("Clear Zero Register (CZ)")>
    ClearZeroRegister

    ''' <summary> An enum constant representing the disable alarms option. Write protected Command. </summary>
    <Description("Disable Alarms (DA)")>
    DisableAlarms

    ''' <summary> An enum constant representing the enable alarms option. Write protected Command. </summary>
    <Description("Enable Alarms (EA)")>
    EnableAlarms

    <Description("Events Clear (EC)")>
    EventsClear

    ''' <summary> An enum constant representing the set high alarm option. Write protected Command. </summary>
    <Description("Set High Alarm (HI)")>
    SetHighAlarm

    ''' <summary> An enum constant representing the set low alarm option. Write protected Command. </summary>
    <Description("Set Low Alarm (LO)")>
    SetLowAlarm

    ''' <summary> An enum constant representing the remote reset option. Write protected Command. </summary>
    <Description("Remote Reset (RR)")>
    RemoteReset

    ''' <summary> An enum constant representing the setup module option. Write protected Command. </summary>
    <Description("Setup Module (SU)")>
    SetupModule

    ''' <summary> An enum constant representing the set setpoint option. Write protected Command. </summary>
    <Description("Set Setpoint (SP)")>
    SetSetpoint

    ''' <summary> An enum constant representing the trim span option. Write protected Command. </summary>
    <Description("Trim Span (TS)")>
    TrimSpan

    ''' <summary> An enum constant representing the trim zero option. Write protected Command. </summary>
    <Description("Trim Zero (TZ)")>
    TrimZero
End Enum

