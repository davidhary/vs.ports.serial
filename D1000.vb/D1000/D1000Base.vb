Imports System.ComponentModel
Imports System.IO.Ports
Imports Arebis.TypedUnits
Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Core.ExceptionExtensions
Imports isr.Core.EventHandlerExtensions
Imports isr.Ports.Teleport

''' <summary> A base class for the D1000 modules. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-10 </para>
''' </remarks>
Public MustInherit Class D1000Base
    Inherits Ports.Teleport.Scribe

#Region " CONSTRUCTION "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub New()
        MyBase.New()
        MyBase.Messenger.Transport.PopulateCommands(D1000CommandCollection.Get)
        MyBase.Messenger.OutputMessage = ProtocolMessage.Create
        MyBase.Messenger.InputMessage = ProtocolMessage.Create
        MyBase.Messenger.Transport.TransportProtocolMessage = ProtocolMessage.Create
        MyBase.Messenger.Transport.SentProtocolMessage = ProtocolMessage.Create
        MyBase.Messenger.Transport.ReceivedProtocolMessage = ProtocolMessage.Create
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PRESETTABLE "

    ''' <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    Public Overridable Sub InitKnownState()
    End Sub

    ''' <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    ''' <param name="moduleName"> The name of the module. </param>
    Public Sub InitKnownState(ByVal moduleName As String)
        Me.ModuleName = moduleName
        Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"{Me.ModuleName}.{Me.Messenger.ModuleAddress} initializing known state")
        Me._AnalogInputPayload = New RealValuePayload() With {.Unit = Arebis.StandardUnits.ElectricUnits.Volt}
        Me._ReadSetupPayload = New SetupPayload()
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Turnaround time. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> A timespan. </returns>
    Public Function TurnaroundTime(ByVal commandAscii As String) As TimeSpan
        Return Me.Messenger.Transport.SelectCommand(commandAscii).TurnaroundTime
    End Function

    ''' <summary> Converts a commandAscii to a command code. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> CommandAscii as a D1000CommandCode. </returns>
    Public Function ToCommandCode(ByVal commandAscii As String) As D1000CommandCode
        Return D1000Base.ToCommandCode(Me.Messenger.Transport.SelectCommand(commandAscii).CommandCode)
    End Function

    ''' <summary> Converts a commandAscii to a command code. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> CommandAscii as a D1000CommandCode. </returns>
    Public Shared Function ToCommandCode(ByVal value As Integer) As D1000CommandCode
        Return CType(value, D1000CommandCode)
    End Function


#End Region

#Region " MODULE INFO "

    ''' <summary> Name of the module. </summary>
    Private _ModuleName As String

    ''' <summary> Gets or sets the name of the module. </summary>
    ''' <value> The name of the module. </value>
    Public Property ModuleName As String
        Get
            Return Me._ModuleName
        End Get
        Set(value As String)
            If Not String.Equals(Me.ModuleName, value) Then
                Me._ModuleName = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " ANALOG INPUT "

    ''' <summary> The analog input read. </summary>
    Private _AnalogInputRead As Amount

    ''' <summary> Gets or sets the Analog Input. </summary>
    ''' <value> The Input Error read. </value>
    Public Property AnalogInputRead As Amount
        Get
            Return Me._AnalogInputRead
        End Get
        Protected Set(value As Amount)
            If value <> Me.AnalogInputRead Then
                Me._AnalogInputRead = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the analog input payload. </summary>
    ''' <value> The analog input payload. </value>
    Public ReadOnly Property AnalogInputPayload As RealValuePayload

    ''' <summary> Reads Analog Input. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The Analog Input. </returns>
    Public Function ReadAnalogInput() As Amount
        Dim activity As String = "building read analog input message"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.CommandPrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.ReadData).CommandAscii
            activity = "reading analog input"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
            Me.AnalogInputPayload.Parse(Me.Transport.ReceivedProtocolMessage.Payload)
            Me.AnalogInputRead = Me.AnalogInputPayload.Amount
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.AnalogInputPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.AnalogInputPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.AnalogInputPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.AnalogInputPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.AnalogInputRead
    End Function

    ''' <summary> Reply analog input. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyAnalogInput() As StatusCode
        Dim activity As String = "building analog input reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.ResponsePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.ReadData).CommandAscii
            message.Payload = Me.AnalogInputPayload.SimulatedPayload
            activity = "replying analog input"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.AnalogInputPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.AnalogInputPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.AnalogInputPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.AnalogInputPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " WRITE ENABLE "

    ''' <summary> Enables the write. </summary>
    ''' <remarks> David, 2020-10-20. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function EnableWrite() As StatusCode
        Dim activity As String = "building read analog input message"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.CommandPrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.WriteEnable).CommandAscii
            activity = "applying and reading back write enable"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

    ''' <summary> Reply enable write. </summary>
    ''' <remarks> David, 2020-10-20. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyEnableWrite() As StatusCode
        Dim activity As String = "building write enable reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.ResponsePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.WriteEnable).CommandAscii
            activity = "replying write enable"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " MODULE SETUP: READ "

    ''' <summary> Gets or sets the module setup payload that was read. </summary>
    ''' <value> The module setup payload. </value>
    Public ReadOnly Property ReadSetupPayload As SetupPayload

    ''' <summary> Reads module setup. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The status. </returns>
    Public Function ReadModuleSetup() As StatusCode
        Dim activity As String = "building module setup read message"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.CommandPrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.ReadSetup).CommandAscii
            activity = "reading setup"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
            Me.ReadSetupPayload.Parse(Me.Transport.ReceivedProtocolMessage.Payload)
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReadSetupPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReadSetupPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReadSetupPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReadSetupPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

    ''' <summary> Reply read module setup. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyReadModuleSetup() As StatusCode
        Dim activity As String = "building read module setup read reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.ResponsePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.ReadSetup).CommandAscii
            message.Payload = SetupPayload.SimulatePayload(Me.Messenger.ModuleAddress(0)).Payload
            activity = "replying read module setup"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReadSetupPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReadSetupPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReadSetupPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReadSetupPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " MODULE SETUP: WRITE "

    ''' <summary> Gets or sets the module setup payload that was written. </summary>
    ''' <value> The module setup payload. </value>
    Public ReadOnly Property WrittenSetupPayload As SetupPayload

    ''' <summary> Writes module setup. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The status. </returns>
    Public Function WriteModuleSetup() As StatusCode
        Dim activity As String = "building module setup write message"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.CommandPrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.SetupModule).CommandAscii
            activity = "Writing setup"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
            Me.WrittenSetupPayload.Parse(Me.Transport.ReceivedProtocolMessage.Payload)
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.WrittenSetupPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.WrittenSetupPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.WrittenSetupPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.WrittenSetupPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

    ''' <summary> Reply Write module setup. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyWriteModuleSetup() As StatusCode
        Dim activity As String = "building write module setup reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.ResponsePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(D1000CommandCode.SetupModule).CommandAscii
            message.Payload = SetupPayload.SimulatePayload(Me.Messenger.ModuleAddress(0)).Payload
            activity = "replying write module setup"
            Me.Query(message, TimeSpan.FromSeconds(0.01))
        End Using
        If Me.Transport.IsSuccess Then
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.WrittenSetupPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.WrittenSetupPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.WrittenSetupPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.WrittenSetupPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " COLLECTOR (listener) PROCESSING "

    ''' <summary> Listener message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Protocol event information. </param>
    Protected Sub ListenerMessageReceived(e As ProtocolEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Select Case Me.ToCommandCode(e.ProtocolMessage.CommandAscii)
            Case D1000CommandCode.ReadData
                Me.ReplyAnalogInput()
            Case D1000CommandCode.WriteEnable
                Me.ReplyEnableWrite()
            Case D1000CommandCode.ReadSetup
                Me.ReplyReadModuleSetup()
            Case D1000CommandCode.SetupModule
                Me.ReplyWriteModuleSetup()
        End Select
    End Sub

    ''' <summary> Notifies a message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Protocol event information. </param>
    Protected Overrides Sub NotifyMessageReceived(e As ProtocolEventArgs)
        MyBase.NotifyMessageReceived(e)
        If Me.Messenger.MessengerRole = MessengerRole.Collector AndAlso Me.Transport.IsSuccess Then
            Me.ListenerMessageReceived(e)
        End If
    End Sub

#End Region

End Class

