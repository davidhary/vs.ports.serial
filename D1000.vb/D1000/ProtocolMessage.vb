Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.EnumExtensions
Imports isr.Ports.Serial
Imports isr.Ports.Teleport

''' <summary> A protocol message for the D1000 modules. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-06-19 </para>
''' </remarks>
Public Class ProtocolMessage
    Inherits ProtocolMessageBase
    Implements IProtocolMessage

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-20. </remarks>
    Public Sub New()
        MyBase.New(New Text.ASCIIEncoding)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> The message. </param>
    Public Sub New(ByVal protocolMessage As IProtocolMessage)
        MyBase.New(protocolMessage)
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object
        Return ProtocolMessage.Clone(Me)
    End Function

    ''' <summary>
    ''' Creates a new <see cref="ProtocolMessageBase">serial protocol message</see> that is a copy of
    ''' the specified instance.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overloads Shared Function Clone(ByVal value As IProtocolMessage) As IProtocolMessage
        Return New ProtocolMessage(value)
    End Function

    ''' <summary> Creates a new IProtocolMessage. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> An IProtocolMessage. </returns>
    Public Shared Function Create() As IProtocolMessage
        Dim result As IProtocolMessage = Nothing
        Try
            result = New ProtocolMessage()
        Catch
            If result IsNot Nothing Then result.Dispose()
            Throw
        End Try
        Return result
    End Function

#End Region

#Region " CONSTANTS AND SHARED "

    ''' <summary> The maximum length of the message. </summary>
    Public Const MaximumMessageLength As Integer = 135

    ''' <summary> The stream termination value. </summary>
    Public Const StreamTerminationValue As Byte = 13

    ''' <summary> The command prompt. </summary>
    Public Const CommandPrompt As Byte = CByte(Asc("#"c) And &HFF)

    ''' <summary> The response prompt. </summary>
    Public Const ResponsePrompt As Byte = CByte(Asc("*"c) And &HFF)

    ''' <summary> The error prompt. </summary>
    Public Const ErrorPrompt As Byte = CByte(Asc("?"c) And &HFF)

    ''' <summary> Length of the command prompt. </summary>
    Public Const CommandPromptLength As Integer = 1

    ''' <summary> Length of the checksum. </summary>
    Public Const ChecksumLength As Integer = 1

    ''' <summary> Length of the command. </summary>
    Public Const CommandLength As Integer = 2

    ''' <summary> The minimum length of the message. </summary>
    Public Const MinimumMessageLength As Integer = CommandPromptLength + CommandLength + ChecksumLength

    ''' <summary> The positive overload. </summary>
    Public Const PositiveOverload As Double = 99999.99

    ''' <summary> The negative overload. </summary>
    Public Const NegativeOverload As Double = -99999.99

#End Region

#Region " CONTENTS "

    ''' <summary> Gets or sets the command prompt. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The command prompt. </value>
    Public Overrides Property Prefix As Byte
        Get
            Return MyBase.Prefix
        End Get
        Set(value As Byte)
            If Me.Prefix <> value Then
                MyBase.Prefix = value
            End If
            Select Case value
                Case ProtocolMessage.CommandPrompt
                    Me.MessageType = MessageType.Command
                Case ProtocolMessage.ResponsePrompt
                    Me.MessageType = MessageType.Response
                Case ProtocolMessage.ErrorPrompt
                    Me.MessageType = MessageType.Failure
                Case Else
                    Throw New InvalidOperationException($"Unhandled prompt {value}.{Me.PrefixAscii}")
            End Select
        End Set
    End Property

    ''' <summary> Gets or sets the message type. </summary>
    ''' <value> The message type. </value>
    Public Overrides Property MessageType As MessageType Implements IProtocolMessage.MessageType
        Get
            Return MyBase.MessageType
        End Get
        Set(value As MessageType)
            If Me.MessageType <> value Then
                MyBase.MessageType = value
                Me.Prefix = ProtocolMessage.FromMessageType(value)
            End If
        End Set
    End Property

#End Region

#Region " CONTENT MANAGEMENT "

    ''' <summary> Clears the contents of an existing <see cref="ProtocolMessageBase" />. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
    End Sub

#End Region

#Region " PARSERS "

    ''' <summary> Initializes this object from the given from message type. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Byte. </returns>
    Public Shared Function FromMessageType(ByVal value As MessageType) As Byte
        Select Case value
            Case Teleport.MessageType.Command
                Return ProtocolMessage.CommandPrompt
            Case Teleport.MessageType.Failure
                Return ProtocolMessage.ErrorPrompt
            Case Teleport.MessageType.Response
                Return ProtocolMessage.ResponsePrompt
            Case Else
                Return 0
        End Select
    End Function

    ''' <summary> Parse message type. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A MessageType. </returns>
    Public Function ParseMessageType(ByVal value As Byte) As MessageType
        Me.Prefix = value
        Return Me.MessageType
    End Function

    ''' <summary> Parse hexadecimal message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexMessage"> A message describing the hexadecimal. </param>
    ''' <returns> A StatusCode. </returns>
    Public Overrides Function ParseHexMessage(ByVal hexMessage As String) As StatusCode
        Dim result As StatusCode = StatusCode.ValueNotSet
        Return result
    End Function

    ''' <summary> Parses the given values. </summary>
    ''' <remarks> David, 2020-10-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Serial.MessageParserOutcome. </returns>
    Public Overrides Function Parse(ByVal values As IEnumerable(Of Byte)) As Serial.MessageParserOutcome
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As MessageParserOutcome
        ' check if value start with initiation characters.
        If Not values.Any Then
            result = MessageParserOutcome.Incomplete
            ' TO_DO: Needs fixing
        ElseIf True Then ' Me.IsTerminated(values) Then
            Me.Status = Me.ParseStream(Me.MessageType, values)
            result = If(Me.Status = StatusCode.Okay, MessageParserOutcome.Complete, MessageParserOutcome.Invalid)
        Else
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to parse the <paramref name="data">byte data</paramref> into a valid message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="messageType"> Type of the message. </param>
    ''' <param name="data">        The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Public Overrides Function ParseStream(ByVal messageType As MessageType, ByVal data As IEnumerable(Of Byte)) As StatusCode
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        Dim result As StatusCode = StatusCode.Okay
        Me.Clear()
        If Not data.Any OrElse data.Count < ProtocolMessage.MinimumMessageLength Then
            result = StatusCode.MessageTooShort
        Else
            Me.MessageType = messageType
            Me.Stream = data
            Dim dataSet As New Queue(Of Byte)(data)
            Me.ModuleAddress = New Byte() {dataSet.Dequeue}
            If Me.ModuleAddress(0) = 0 Then
                result = StatusCode.InvalidModuleAddress
            Else
                ' get the prompt.
                Me.Prefix = dataSet.Dequeue
                ' check if error prompt
                If Me.Prefix = ProtocolMessage.ErrorPrompt Then
                    ' if error, the command is left empty and the rest of the input is the error message
                Else
                    Me.Command = New Byte() {dataSet.Dequeue, dataSet.Dequeue}
                    dataSet = New Queue(Of Byte)(dataSet.Reverse)
                    Dim checksumCache As Byte = dataSet.Dequeue
                    If data.Count > 0 Then
                        Me.Payload = dataSet.Reverse
                        Me.AsyncNotifyPropertyChanged(NameOf(ProtocolMessageBase.Payload))
                    End If
                    Me.Checksum = New Byte() {checksumCache}
                    If Me.Checksum(0) <> Me.CalculateChecksum Then
                        result = StatusCode.ChecksumInvalid
                    End If
                End If
            End If
        End If
        Me.Status = result
        Return result
    End Function

    ''' <summary> Validates the protocol message relative to this protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> The validated protocol message. </param>
    ''' <returns> A StatusCode. </returns>
    Public Overrides Function Validate(ByVal protocolMessage As IProtocolMessage) As StatusCode
        If protocolMessage Is Nothing Then
            Return StatusCode.Okay
        ElseIf Me.ModuleAddress(0) <> protocolMessage.ModuleAddress(0) Then
            Return StatusCode.IncorrectModuleAddressReceived
        ElseIf Me.CommandAscii <> protocolMessage.CommandAscii Then
            Return StatusCode.IncorrectCommandReceived
        Else
            Return StatusCode.Okay
        End If
    End Function

#End Region

#Region " BUILDERS "

    ''' <summary> The checksum mask. </summary>
    Public Const ChecksumMask As Integer = &HFF

    ''' <summary> Calculates the checksum hexadecimal. </summary>
    ''' <remarks> David, 2020-10-20. </remarks>
    ''' <returns> The calculated checksum hexadecimal. </returns>
    Public Overrides Function CalculateChecksumHex() As String
        Dim result As Long = 0
        ' the prefix counts only for messages received from the controller.
        If Me.MessageType = Teleport.MessageType.Response Then result += Me.Prefix
        result += Me.ModuleAddress.Sum
        If Me.MessageType = Teleport.MessageType.Command Then result += Me.Command.Sum
        result += Me.Payload.Sum
        result = result And ProtocolMessage.ChecksumMask
        Return result.ToString("X2")
    End Function

    ''' <summary> Calculates the checksum value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The calculated checksum. </returns>
    Public Overloads Function CalculateChecksum() As Byte
        Dim result As Long = 0
        result += Me.Prefix
        For Each value As Byte In Me.Command
            result += value
        Next
        For Each value As Byte In Me.Payload
            result += value
        Next
        result = result And ProtocolMessage.ChecksumMask
        Return result.ToString("X2").ToBytes(0)
    End Function

    ''' <summary> Enumerates build message in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Public Overrides Function BuildStream(ByVal value As MessageType) As IEnumerable(Of Byte)
        Return Me.BuildMessage(ProtocolMessage.FromMessageType(value))
    End Function

    ''' <summary> Builds the message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="prompt"> The command, response or error prompt to use for building the message,
    '''                       which allows emulating a module for sending an error or response
    '''                       message. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Public Overloads Function BuildMessage(ByVal prompt As Byte) As IEnumerable(Of Byte)
        Me.Prefix = prompt
        Return Me.BuildMessage()
    End Function

    ''' <summary> Builds the message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Private Overloads Function BuildMessage() As IEnumerable(Of Byte)
        Dim values As New List(Of Byte)
        If Me.IsIncomplete Then
        ElseIf Me.Status = StatusCode.InvalidMessageLength Then
        Else
            values.Add(Me.Prefix)
            values.Add(Me.ModuleAddress(0))
            values.AddRange(Me.Command)
            If Me.Payload.Any Then values.AddRange(Me.Payload)
            Me.UpdateChecksum()
            values.Add(Me.Checksum(0))
            values.Add(ProtocolMessage.StreamTerminationValue)
        End If
        Return values
    End Function

    ''' <summary> Builds the hex message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="prompt"> The command, response or error prompt to use for building the message,
    '''                       which allows emulating a module for sending an error or response
    '''                       message. </param>
    ''' <returns> A String. </returns>
    Public Overloads Function BuildHexMessage(ByVal prompt As Byte) As String
        Dim sb As New System.Text.StringBuilder
        For Each b As Byte In Me.BuildMessage(prompt)
            sb.Append(b.ToHex)
        Next
        Return sb.ToString
    End Function

    ''' <summary> Builds hexadecimal message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Overloads Function BuildHexMessage(ByVal value As MessageType) As String
        Return Me.BuildHexMessage(ProtocolMessage.FromMessageType(value))
    End Function

#End Region

End Class
