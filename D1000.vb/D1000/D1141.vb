''' <summary> A D1141 Module class. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-17 </para>
''' </remarks>
Public Class D1141
    Inherits D1000Base

#Region " PRESETTABLE "

    ''' <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState("D1141")
        Me.AnalogInputPayload.Range = New isr.Core.Constructs.RangeR(-10, 10)
        Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Done initializing {Me.ModuleName}.{Me.Messenger.ModuleAddress} known state")
    End Sub

#End Region

End Class
