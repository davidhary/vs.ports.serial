Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = 1001

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Win32 Serial Port Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Win32 Serial Port Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Ports.Serial.Win32"

    End Class

End Namespace
