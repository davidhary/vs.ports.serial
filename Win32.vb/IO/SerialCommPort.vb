Option Strict On
Imports System.Text
Imports System.Threading

''' <summary>
''' A serial communications port. This class provides all the necessary support for communicating
''' with the Comm Port (otherwise known as the Serial Port, or RS232 port).
''' </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Class SerialCommPort

#Region " EVENTS "

    ''' <summary> Event queue for all listeners interested in DataReceived events. 
    '''           These events allow the program using this class to react to Comm Port  events. </summary>
    Public Event DataReceived(ByVal source As SerialCommPort, ByVal dataBuffer() As Byte)

    ''' <summary> Event queue for all listeners interested in TxCompleted events. </summary>
    Public Event TxCompleted(ByVal source As SerialCommPort)

    ''' <summary> Event queue for all listeners interested in comm events. </summary>
    Public Event CommEvent(ByVal source As SerialCommPort, ByVal mask As EventMasks)
#End Region

#Region " CONSTANTS "

    ''' <summary> The purge receive abort. 
    ''' These constants are used to make the code clearer.</summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const PURGE_RXABORT As Integer = &H2

    ''' <summary> The purge RX Clear. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const PURGE_RXCLEAR As Integer = &H8

    ''' <summary> The purge TX Abort. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const PURGE_TXABORT As Integer = &H1

    ''' <summary> The purge TX Clear. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const PURGE_TXCLEAR As Integer = &H4

    ''' <summary> The generic read. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const GENERIC_READ As Integer = &H80000000

    ''' <summary> The generic write. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const GENERIC_WRITE As Integer = &H40000000

    ''' <summary> The open existing. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const OPEN_EXISTING As Integer = 3

    ''' <summary> The create always. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const CREATE_ALWAYS As Integer = 2

    ''' <summary> The invalid handle value. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const INVALID_HANDLE_VALUE As Integer = -1

    ''' <summary> Size of the i/o buffer. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const IO_BUFFER_SIZE As Integer = 1024

    ''' <summary> The file flag overlapped. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const FILE_FLAG_OVERLAPPED As Integer = &H40000000

    ''' <summary> The error i/o pending. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const ERROR_IO_PENDING As Integer = 997

    ''' <summary> The wait object 0. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const WAIT_OBJECT_0 As Integer = 0

    ''' <summary> The error i/o incomplete. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const ERROR_IO_INCOMPLETE As Integer = 996

    ''' <summary> The wait timeout. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const WAIT_TIMEOUT As Integer = &H102&

    ''' <summary> The infinite. </summary>
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Private Const INFINITE As Integer = &HFFFFFFFF


#End Region

#Region " PROPERTIES "

    ''' <summary> Sets the baud rate. </summary>
    ''' <value> The baud rate. </value>
    Public Property BaudRate() As Integer = 19200

    ''' <summary> Sets the buffer size. </summary>
    ''' <value> The size of the buffer. </value>
    Public Property BufferSize() As Integer = 512

    ''' <summary> Sets the data bit. </summary>
    ''' <value> The data bit. </value>
    Public Property DataBit() As Integer = 8

    ''' <summary>  Handle to Com Port </summary>
    Private _PortHandle As Integer = -1

    ''' <summary> Sets the DTR Line. </summary>
    ''' <value> The DTR Line. </value>
    Public WriteOnly Property Dtr() As Boolean
        Set(ByVal value As Boolean)
            If Not Me._PortHandle = -1 Then
                If value Then
                    UnsafeNativeMethods.EscapeCommFunction(Me._PortHandle, Lines.SetDtr)
                Else
                    UnsafeNativeMethods.EscapeCommFunction(Me._PortHandle, Lines.ClearDtr)
                End If
            End If
        End Set
    End Property

    ''' <summary> Stream to read data from. </summary>
    Private _InputStream As Byte()

    ''' <summary>
    ''' Gets the input stream: an array of bytes that represents the input coming into the Comm Port.
    ''' </summary>
    ''' <value> The input stream. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Public Overridable ReadOnly Property InputStream() As Byte()
        Get
            Return Me._InputStream
        End Get
    End Property

    ''' <summary>
    ''' Gets the input stream string: represents the data coming into to the Comm Port.
    ''' </summary>
    ''' <value> The input stream string. </value>
    Public Overridable ReadOnly Property InputStreamString() As String
        Get
            Dim oEncoder As New System.Text.ASCIIEncoding()
            Return oEncoder.GetString(Me.InputStream)
        End Get
    End Property

    ''' <summary> Gets the open status of the Comm Port. </summary>
    ''' <value> The is open. </value>
    Public ReadOnly Property IsOpen() As Boolean
        Get
            Return CBool(Me._PortHandle <> -1)
        End Get
    End Property

    ''' <summary> Gets the serial communications port error. </summary>
    ''' <exception cref="IOChannelException"> Thrown when an I/O Channel error condition occurs. </exception>
    ''' <value> The serial communications port error. </value>
    Public ReadOnly Property SerialCommPortError() As String

    ''' <summary> Gets the modem status. </summary>
    ''' <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error 
    '''                                                        condition occurs. </exception>
    ''' <value> The modem status. </value>
    Public ReadOnly Property ModemStatus() As ModemStatusBits
        Get
            If Me._PortHandle = -1 Then
                Throw New IOChannelException("Please initialize and open port before using this method")
            Else
                ' Retrieve modem status
                Dim lpModemStatus As Integer
                If Not UnsafeNativeMethods.GetCommModemStatus(Me._PortHandle, lpModemStatus) Then
                    Throw New IOChannelException("Unable to get modem status")
                Else
                    Return CType(lpModemStatus, ModemStatusBits)
                End If
            End If
        End Get
    End Property

    ''' <summary> Sets the parity. </summary>
    ''' <value> The parity. </value>
    Public Property Parity() As DataParity = DataParity.None

    ''' <summary> Sets the port. </summary>
    ''' <value> The port. </value>
    Public Property Port() As Integer = 1

    ''' <summary> Sets the RTS line. </summary>
    ''' <value> The RTS line. </value>
    Public WriteOnly Property Rts() As Boolean
        Set(ByVal value As Boolean)
            If Not Me._PortHandle = -1 Then
                If value Then
                    UnsafeNativeMethods.EscapeCommFunction(Me._PortHandle, Lines.SetRts)
                Else
                    UnsafeNativeMethods.EscapeCommFunction(Me._PortHandle, Lines.ClearRts)
                End If
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the stop bit. </summary>
    ''' <value> The stop bit. </value>
    Public Property StopBit() As DataStopBit = DataStopBit.StopBit1

    ''' <summary> The  timeout in ms. </summary>
    Private _Timeout As Integer = 70

    ''' <summary> Gets or sets the timeout in ms. </summary>
    ''' <value> The timeout in ms. </value>
    Public Overridable Property Timeout() As Integer
        Get
            Return Me._Timeout
        End Get
        Set(ByVal value As Integer)
            Me._Timeout = If(value = 0, 500, value)
            ' If Port is open updates it on the fly
            Me.PSetTimeout()
        End Set
    End Property

    ''' <summary> Gets or sets the working mode to overlapped or non-overlapped. </summary>
    ''' <value> The working mode. </value>
    Public Property WorkingMode() As WorkingMode

#End Region

#Region " METHODS "

    ''' <summary>
    ''' This subroutine invokes a thread to perform an asynchronous read. This routine should not be
    ''' called directly, but is used by the class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ReadAsync()
        Me.Read(Me._TempBytesToRead)
    End Sub

    ''' <summary> Buffer for temporary transmit data. </summary>
    Private _TempTransmitBuffer As Byte()

    ''' <summary>
    ''' This subroutine invokes a thread to perform an asynchronous write.   This routine should not
    ''' be called directly, but is used   by the class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub WriteAsync()
        Me.Write(Me._TempTransmitBuffer)
    End Sub

    ''' <summary> The temporary bytes to read. </summary>
    Private _TempBytesToRead As Integer

    ''' <summary> The transmit receive thread. </summary>
    Private _TransmitReceiveThread As Thread

    ''' <summary>
    ''' Asynchronous read. This subroutine uses another thread to read from the Comm Port. It raises
    ''' RxCompleted when done. It reads an integer.
    ''' </summary>
    ''' <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error 
    '''                                                        condition occurs. </exception>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="bytes2Read"> The bytes 2 read. </param>
    Public Overloads Sub AsyncRead(ByVal bytes2Read As Integer)
        If Me.WorkingMode <> WorkingMode.Overlapped Then Throw New IOChannelException($"Asynchronous Methods allowed only when {NameOf(Serial.WorkingMode)} is {WorkingMode.Overlapped}")
        Me._TempBytesToRead = bytes2Read
        Me._TransmitReceiveThread = New Thread(AddressOf Me.ReadAsync)
        Me._TransmitReceiveThread.Start()
    End Sub

    ''' <summary> True to wait on write. </summary>
    Private ReadOnly _WaitOnWrite As Boolean

    ''' <summary>
    ''' Asynchronous write. This subroutine uses another thread to write to the Comm Port. It  raises
    ''' TxCompleted when done. It writes an array of bytes.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error 
    '''                                                        condition occurs. </exception>
    ''' <param name="buffer"> The buffer. </param>
    Public Overloads Sub AsyncWrite(ByVal buffer() As Byte)
        If Me.WorkingMode <> WorkingMode.Overlapped Then Throw New IOChannelException($"Async Methods allowed only when {NameOf(Serial.WorkingMode)} is {WorkingMode.Overlapped}")
        If Me._WaitOnWrite Then Throw New IOChannelException("Unable to send message because of pending transmission.")
        Me._TempTransmitBuffer = buffer
        Me._TransmitReceiveThread = New Thread(AddressOf Me.WriteAsync)
        Me._TransmitReceiveThread.Start()
    End Sub

    ''' <summary>
    ''' Asynchronous write. This subroutine uses another thread to write to the Comm Port. It  raises
    ''' TxCompleted when done. It writes an array of bytes.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    Public Overloads Sub AsyncWrite(ByVal buffer As String)
        Dim oEncoder As New System.Text.ASCIIEncoding()
        Dim aByte() As Byte = oEncoder.GetBytes(buffer)
        Me.AsyncWrite(aByte)
    End Sub

    ''' <summary>
    ''' Check line status. This function takes the ModemStatusBits and returns a boolean value
    ''' signifying whether the Modem is active.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="line"> The line. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function CheckLineStatus(ByVal line As ModemStatusBits) As Boolean
        Return Convert.ToBoolean(Me.ModemStatus And line)
    End Function

    ''' <summary> Clears the input buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ClearInputBuffer()
        If Not Me._PortHandle = -1 Then
            UnsafeNativeMethods.PurgeComm(Me._PortHandle, PURGE_RXCLEAR)
        End If
    End Sub

    ''' <summary> Closes the Comm Port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Close()
        If Me._PortHandle <> -1 Then
            UnsafeNativeMethods.CloseHandle(Me._PortHandle)
            Me._PortHandle = -1
        End If
    End Sub

    ''' <summary> Opens and initializes the Comm Port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when a IO Channel error 
    '''                                                        condition occurs. </exception>
    Public Overloads Sub Open()

        ' Get DCB block,Update with current data
        Dim uDcb As UnsafeNativeMethods.DCB, iRc As Integer

        ' Set working mode
        Dim iMode As Integer = Convert.ToInt32(IIf(Me.WorkingMode = WorkingMode.Overlapped, FILE_FLAG_OVERLAPPED, 0))

        ' Initializes Com Port
        If Me.Port > 0 Then
            Try
                ' Creates a COM Port stream handle 
                Me._PortHandle = UnsafeNativeMethods.CreateFile($"COM{Me.Port}", GENERIC_READ Or GENERIC_WRITE, 0, 0, OPEN_EXISTING, iMode, 0)
                If Me._PortHandle <> -1 Then
                    ' Clear all communication errors
                    Dim lpErrCode As Integer
                    iRc = UnsafeNativeMethods.ClearCommError(Me._PortHandle, lpErrCode, 0&)
                    ' Clears I/O buffers
                    iRc = UnsafeNativeMethods.PurgeComm(Me._PortHandle, PurgeBuffers.RXClear Or
                        PurgeBuffers.TxClear)
                    ' Gets COM Settings
                    iRc = UnsafeNativeMethods.GetCommState(Me._PortHandle, uDcb)
                    ' Updates COM Settings
                    Dim sParity As String = "NOEM"
                    sParity = sParity.Substring(Me.Parity, 1)
                    ' Set DCB State
                    Dim sDCBState As String = $"baud={Me.BaudRate} parity={sParity} data={Me.DataBit} stop={CInt(Me.StopBit)}"
                    iRc = UnsafeNativeMethods.BuildCommDCB(sDCBState, uDcb)
                    If iRc = 0 Then
                        Dim sErrTxt As String = Me.PErr2Text(UnsafeNativeMethods.GetLastError())
                        Throw New IOChannelException($"Unable to set COM state {sErrTxt}")
                    End If
                    iRc = UnsafeNativeMethods.SetCommState(Me._PortHandle, uDcb)
                    If iRc = 0 Then
                        Dim sErrTxt As String = Me.PErr2Text(UnsafeNativeMethods.GetLastError())
                        Throw New IOChannelException($"Unable to set COM state{sErrTxt}")
                    End If
                    ' Setup Buffers (Rx,Tx)
                    iRc = UnsafeNativeMethods.SetupComm(Me._PortHandle, Me.BufferSize, Me.BufferSize)
                    ' Set Timeouts
                    Me.PSetTimeout()
                Else
                    ' Raise Initialization problems 
                    Me._SerialCommPortError = $"Unable to open COM{Me.Port}"
                    Throw New IOChannelException(Me._SerialCommPortError)
                End If
            Catch Ex As Exception
                Me._SerialCommPortError = $"Exception opening pert;. {Ex}"
                Throw New IOChannelException(Me._SerialCommPortError, Ex)
            End Try
        Else
            ' Port not defined, cannot open
            Me._SerialCommPortError = "COM Port not defined, use Port property to set it before invoking InitPort"
            Throw New isr.Ports.Serial.IOChannelException(Me._SerialCommPortError)
        End If
    End Sub

    ''' <summary> Opens and initializes the Comm Port (overloaded to support parameters). </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port">       The port. </param>
    ''' <param name="baudRate">   The baud rate. </param>
    ''' <param name="dataBit">    The data bit. </param>
    ''' <param name="parity">     The parity. </param>
    ''' <param name="stopBit">    The stop bit. </param>
    ''' <param name="bufferSize"> Size of the buffer. </param>
    Public Overloads Sub Open(ByVal port As Integer, ByVal baudRate As Integer, ByVal dataBit As Integer,
                              ByVal parity As DataParity, ByVal stopBit As DataStopBit, ByVal bufferSize As Integer)
        Me.Port = port
        Me.BaudRate = baudRate
        Me.DataBit = dataBit
        Me.Parity = parity
        Me.StopBit = stopBit
        Me.BufferSize = bufferSize
        Me.Open()
    End Sub

    ''' <summary> Translates an API error code to text. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="code"> The code. </param>
    ''' <returns> A String. </returns>
    Private Function PErr2Text(ByVal code As Integer) As String
        Dim builder As New StringBuilder(256)
        Dim result As Integer = UnsafeNativeMethods.FormatMessage(&H1000, 0, code, 0, builder, 256, 0)
        Return If(result > 0, builder.ToString, "Error not found.")
    End Function

    ''' <summary> The overlapped. </summary>
    Private _Overlapped As UnsafeNativeMethods.OVERLAPPED

    ''' <summary> True to wait on read. </summary>
    Private _WaitOnRead As Boolean

    ''' <summary> Handles the overlapped read described by bytes2Read. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException">  Thrown when one or more arguments have unsupported or
    '''                                       illegal values. </exception>
    ''' <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
    '''                                                        condition occurs. </exception>
    ''' <param name="bytes2Read"> The bytes 2 read. </param>
    Private Sub PHandleOverlappedRead(ByVal bytes2Read As Integer)
        Dim iReadChars, iRc, iRes, iLastErr As Integer
        Me._Overlapped.hEvent = UnsafeNativeMethods.CreateEvent(Nothing, 1, 0, Nothing)
        If Me._Overlapped.hEvent = 0 Then
            ' Can't create event
            Throw New isr.Ports.Serial.IOChannelException("Error creating event for overlapped read.")
        Else
            ' Overlapped reading
            If Me._WaitOnRead = False Then
                ReDim Me._InputStream(bytes2Read - 1)
                iRc = UnsafeNativeMethods.ReadFile(Me._PortHandle, Me._InputStream, bytes2Read,
                    iReadChars, Me._Overlapped)
                If iRc = 0 Then
                    iLastErr = UnsafeNativeMethods.GetLastError()
                    If iLastErr <> ERROR_IO_PENDING Then
                        Throw New ArgumentException("Overlapped Read Error: " &
                            Me.PErr2Text(iLastErr))
                    Else
                        ' Set Flag
                        Me._WaitOnRead = True
                    End If
                Else
                    ' Read completed successfully
                    RaiseEvent DataReceived(Me, Me._InputStream)
                End If
            End If
        End If
        ' Wait for operation to be completed
        If Me._WaitOnRead Then
            iRes = UnsafeNativeMethods.WaitForSingleObject(Me._Overlapped.hEvent, Me._Timeout)
            Select Case iRes
                Case WAIT_OBJECT_0
                    ' Object signaled,operation completed
                    If UnsafeNativeMethods.GetOverlappedResult(Me._PortHandle, Me._Overlapped,
                        iReadChars, 0) = 0 Then

                        ' Operation error
                        iLastErr = UnsafeNativeMethods.GetLastError()
                        If iLastErr = ERROR_IO_INCOMPLETE Then
                            Throw New isr.Ports.Serial.IOChannelException("Read operation incomplete")
                        Else
                            Throw New isr.Ports.Serial.IOChannelException($"Read operation error {iLastErr}")
                        End If
                    Else
                        ' Operation completed
                        RaiseEvent DataReceived(Me, Me._InputStream)
                        Me._WaitOnRead = False
                    End If
                Case WAIT_TIMEOUT
                    Throw New isr.Ports.Serial.IOTimeoutException("Timeout error")
                Case Else
                    Throw New isr.Ports.Serial.IOChannelException("Overlapped read error")
            End Select
        End If
    End Sub

    ''' <summary> The overlapped write. </summary>
    Private _OverlappedWrite As UnsafeNativeMethods.OVERLAPPED

    ''' <summary> Handles the overlapped write described by buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException">  Thrown when one or more arguments have unsupported or
    '''                                       illegal values. </exception>
    ''' <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
    '''                                                        condition occurs. </exception>
    ''' <param name="buffer"> The buffer. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    '''
    Private Function PHandleOverlappedWrite(ByVal buffer() As Byte) As Boolean
        Dim iBytesWritten, iRc, iLastErr, iRes As Integer, bErr As Boolean
        Me._OverlappedWrite.hEvent = UnsafeNativeMethods.CreateEvent(Nothing, 1, 0, Nothing)
        If Me._OverlappedWrite.hEvent = 0 Then
            ' Can't create event
            Throw New isr.Ports.Serial.IOChannelException("Error creating event for overlapped write.")
        Else
            ' Overlapped write
            UnsafeNativeMethods.PurgeComm(Me._PortHandle, PURGE_RXCLEAR Or PURGE_TXCLEAR)
            Me._WaitOnRead = True
            iRc = UnsafeNativeMethods.WriteFile(Me._PortHandle, buffer, buffer.Length,
                iBytesWritten, Me._OverlappedWrite)
            If iRc = 0 Then
                iLastErr = UnsafeNativeMethods.GetLastError()
                If iLastErr <> ERROR_IO_PENDING Then
                    Throw New ArgumentException("Overlapped Read Error: " &
                        Me.PErr2Text(iLastErr))
                Else
                    ' Write is pending
                    iRes = UnsafeNativeMethods.WaitForSingleObject(Me._OverlappedWrite.hEvent, INFINITE)
                    Select Case iRes
                        Case WAIT_OBJECT_0
                            ' Object signaled,operation completed
                            If UnsafeNativeMethods.GetOverlappedResult(Me._PortHandle, Me._OverlappedWrite,
                                iBytesWritten, 0) = 0 Then

                                bErr = True
                            Else
                                ' Notifies Asynchronous transmit completion,stops thread
                                Me._WaitOnRead = False
                                RaiseEvent TxCompleted(Me)
                            End If
                    End Select
                End If
            Else
                ' Wait operation completed immediately
                bErr = False
            End If
        End If
        UnsafeNativeMethods.CloseHandle(Me._OverlappedWrite.hEvent)
        Return bErr
    End Function

    ''' <summary> Sets the Comm Port timeouts. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub PSetTimeout()
        Dim uCtm As UnsafeNativeMethods.COMMTIMEOUTS
        ' Set ComTimeout
        If Me._PortHandle = -1 Then
            Exit Sub
        Else
            ' Changes setup on the fly
            uCtm.ReadIntervalTimeout = &HFFFFFFFF  '*********************   '0
            uCtm.ReadTotalTimeoutMultiplier = 0    '*settings to disable*
            uCtm.ReadTotalTimeoutConstant = 0      '* built-in Kernel32 *   'miTimeout       
            uCtm.WriteTotalTimeoutMultiplier = 0   '*   comm timeout    *   '10
            uCtm.WriteTotalTimeoutConstant = 0     '*********************   '100
            UnsafeNativeMethods.SetCommTimeouts(Me._PortHandle, uCtm)
        End If
    End Sub

    ''' <summary>
    ''' Reads the given bytes 2 read. Returns an integer specifying the number of bytes read from the
    ''' Comm Port. It accepts a parameter specifying the number of desired bytes to read.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
    ''' <param name="bytes2Read"> The bytes 2 read. </param>
    ''' <returns> An Integer. </returns>
    Public Function Read(ByVal bytes2Read As Integer) As Integer
        Dim iReadChars, iRc As Integer

        ' If Bytes2Read not specified uses Buffer size
        If bytes2Read = 0 Then bytes2Read = Me.BufferSize
        If Me._PortHandle = -1 Then
            Throw New InvalidOperationException("Please initialize and open port before using this method")
        Else
            ' Get bytes from port
            Try
                ' Purge buffers
                'PurgeComm(mhRS, PURGE_RXCLEAR Or PURGE_TXCLEAR)
                ' Creates an event for overlapped operations
                If Me.WorkingMode = WorkingMode.Overlapped Then
                    Me.PHandleOverlappedRead(bytes2Read)
                Else
                    ' Non overlapped mode
                    ReDim Me._InputStream(bytes2Read - 1)
                    iRc = UnsafeNativeMethods.ReadFile(Me._PortHandle, Me._InputStream, bytes2Read, iReadChars, Nothing)
                    If iRc = 0 Then
                        ' Read Error
                        Throw New System.IO.IOException("ReadFile error " & iRc.ToString)
                    Else
                        ' Handles timeout or returns input chars
                        If iReadChars < bytes2Read Then
                            'changed PLS 7-17-2003
                            'Throw New IOTimeoutException("Timeout error")
                            Return (iReadChars)
                        Else
                            Me._WaitOnRead = True
                            Return (iReadChars)
                        End If
                    End If
                End If
            Catch Ex As Exception
                ' Others generic errors
                Throw New System.IO.IOException("Read Error: " & Ex.Message, Ex)
            End Try
        End If
        Return (iReadChars)
    End Function

    ''' <summary> Writes an array of bytes to the Comm Port to be written. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
    ''' <param name="buffer"> The buffer. </param>
    Public Overloads Sub Write(ByVal buffer As Byte())
        Dim iBytesWritten, iRc As Integer

        If Me._PortHandle = -1 Then
            Throw New InvalidOperationException("Please initialize and open port before using this method")
        Else
            ' Transmit data to COM Port
            Try
                If Me.WorkingMode = WorkingMode.Overlapped Then
                    ' Overlapped write
                    If Me.PHandleOverlappedWrite(buffer) Then
                        Throw New System.IO.IOException("Error in overlapped write")
                    End If
                Else
                    ' Clears IO buffers
                    UnsafeNativeMethods.PurgeComm(Me._PortHandle, PURGE_RXCLEAR Or PURGE_TXCLEAR)
                    iRc = UnsafeNativeMethods.WriteFile(Me._PortHandle, buffer, buffer.Length, iBytesWritten, Nothing)
                    If iRc = 0 Then
                        Throw New System.IO.IOException($"Write Error - Bytes Written {iBytesWritten} of {buffer.Length}")
                    End If
                End If
            Catch Ex As Exception
                Throw
            End Try
        End If
    End Sub

    ''' <summary> Writes a string to the Comm Port to be written. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    Public Overloads Sub Write(ByVal buffer As String)
        Dim oEncoder As New System.Text.ASCIIEncoding()
        Dim aByte() As Byte = oEncoder.GetBytes(buffer)
        Me.Write(aByte)
    End Sub

#End Region

#Region " PRIVATE ENUMS "

    ''' <summary> Values that represent values used to purge the various buffers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Enum PurgeBuffers

        ''' <summary> An enum constant representing the Receive abort option. </summary>
        RXAbort = &H2

        ''' <summary> An enum constant representing the Receive clear option. </summary>
        RXClear = &H8

        ''' <summary> An enum constant representing the Transmit abort option. </summary>
        TxAbort = &H1

        ''' <summary> An enum constant representing the Transmit clear option. </summary>
        TxClear = &H4
    End Enum

    ''' <summary> Values for the lines sent to the Comm Port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Enum Lines

        ''' <summary> An enum constant representing the set RTS option. </summary>
        SetRts = 3

        ''' <summary> An enum constant representing the clear RTS option. </summary>
        ClearRts = 4

        ''' <summary> An enum constant representing the set dtr option. </summary>
        SetDtr = 5

        ''' <summary> An enum constant representing the clear dtr option. </summary>
        ClearDtr = 6

        ''' <summary> An enum constant representing the reset Development option. 
        '''           Reset device if possible </summary>
        ResetDev = 7

        ''' <summary> An enum constant representing option for setting break line. </summary>
        SetBreak = 8

        ''' <summary> An enum constant representing the clear break option. </summary>
        ClearBreak = 9
    End Enum
#End Region

End Class

#Region " EXCEPTIONS "

''' <summary>
''' This class defines a customized channel exception. This exception is raised when a NACK is
''' raised.
''' </summary>
''' <remarks> David, 2020-10-22. </remarks>
<CodeAnalysis.SuppressMessage("Design", "CA1032:Implement standard exception constructors", Justification:="<Pending>")>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class IOChannelException : Inherits Exception

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:IOChannelException" /> class with a specified
    ''' error message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message"> A message that describes the error. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:IOChannelException" /> class with a specified
    ''' error message and a reference to the inner exception that is the cause of this exception.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message">        The error message that explains the reason for the exception. </param>
    ''' <param name="innerException"> The exception that is the cause of the current exception. If the
    '''                               <paramref name="innerException" /> parameter is not a null
    '''                               reference, the current exception is raised in a
    '''                               <see langword="catch" /> block that handles the inner
    '''                               exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub
End Class

''' <summary> Exception for signaling i/o timeout errors. </summary>
''' <remarks> David, 2020-10-22. </remarks>
<CodeAnalysis.SuppressMessage("Design", "CA1032:Implement standard exception constructors", Justification:="<Pending>")>
Public Class IOTimeoutException : Inherits IOChannelException

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:IOTimeoutException" /> class with a specified
    ''' error message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message"> A message that describes the error. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:IOTimeoutException" /> class with a specified
    ''' error message and a reference to the inner exception that is the cause of this exception.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message">        The error message that explains the reason for the exception. </param>
    ''' <param name="innerException"> The exception that is the cause of the current exception. If the
    '''                               <paramref name="innerException" /> parameter is not a null
    '''                               reference, the current exception is raised in a
    '''                               <see langword="catch" /> block that handles the inner
    '''                               exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub
End Class

#End Region

''' <summary> Values that represent data parities. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum DataParity

    ''' <summary> An enum constant representing the none option. </summary>
    None = 0

    ''' <summary> An enum constant representing the odd option. </summary>
    Odd

    ''' <summary> An enum constant representing the even option. </summary>
    Even

    ''' <summary> An enum constant representing the parity mark option. </summary>
    Parity_Mark
End Enum

''' <summary>
''' Values that represent data stop bits. This enumeration provides Data Stop Bit values. It is
''' set to begin with a one, so that the enumeration values match the actual values.
''' </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum DataStopBit

    ''' <summary> An enum constant representing the stop bit 1 option. </summary>
    StopBit1 = 1

    ''' <summary> An enum constant representing the stop bit 2 option. </summary>
    StopBit2
End Enum

''' <summary>
''' Values that represent modem status bits. This enumeration provides values for the Modem
''' Status, since we'll be communicating primarily with a modem. Note that the Flags() attribute
''' is set to allow for a bitwise combination of values.
''' </summary>
''' <remarks> David, 2020-10-22. </remarks>
<Flags()> Public Enum ModemStatusBits
    ClearToSendOn = &H10

    ''' <summary> An enum constant representing the data set ready on option. </summary>
    DataSetReadyOn = &H20

    ''' <summary> An enum constant representing the ring indicator on option. </summary>
    RingIndicatorOn = &H40

    ''' <summary> An enum constant representing the carrier detect option. </summary>
    CarrierDetect = &H80
End Enum

''' <summary> Values that represent values for the Working mode. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum WorkingMode

    ''' <summary> An enum constant representing the non overlapped option. </summary>
    NonOverlapped

    ''' <summary> An enum constant representing the overlapped option. </summary>
    Overlapped
End Enum

''' <summary>
''' Values for the Comm Masks used. Note that the Flags() attribute is set to allow for a bitwise
''' combination of values.
''' </summary>
''' <remarks> David, 2020-10-22. </remarks>
<Flags()> Public Enum EventMasks
    RxChar = &H1

    ''' <summary> An enum constant representing the Receive flag option. </summary>
    RxFlag = &H2

    ''' <summary> An enum constant representing the Transmit buffer empty option. </summary>
    TxBufferEmpty = &H4

    ''' <summary> An enum constant representing the clear to send option. </summary>
    ClearToSend = &H8

    ''' <summary> An enum constant representing the data set ready option. </summary>
    DataSetReady = &H10

    ''' <summary> An enum constant representing the receive line option. </summary>
    ReceiveLine = &H20

    ''' <summary> An enum constant representing the break option. </summary>
    Break = &H40

    ''' <summary> An enum constant representing the status error option. </summary>
    StatusError = &H80

    ''' <summary> An enum constant representing the ring option. </summary>
    Ring = &H100
End Enum




