Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading

''' <summary>
''' Gets or sets safe application programming interface calls. This class suppresses stack walks
''' for unmanaged code permission.  This class is for methods that are safe for anyone to call.
''' Callers of these methods are not required to do a full security review to ensure that the
''' usage is secure because the methods are harmless for any caller.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2004-11-22, 1.0.1787. Created </para>
''' </remarks>
<AttributeUsage(AttributeTargets.Class Or AttributeTargets.Method Or AttributeTargets.Interface)>
Friend NotInheritable Class UnsafeNativeMethods

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Prevents construction of this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Friend Sub New()
    End Sub

#End Region

#Region " DLL Imports "

    ''' <summary> Gets time in ms since windows started. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The tick count. </returns>
    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetTickCount")>
    Public Shared Function GetTickCount() As Int32
    End Function

    ''' <summary> Gets system task time in milliseconds. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> An Int32. </returns>
    <System.Runtime.InteropServices.DllImport("winmm.dll", EntryPoint:="timeGetTime")>
    Public Shared Function TimeGetTime() As Int32
    End Function

    ''' <summary> Gets system performance counter value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="x"> [in,out] Returns the performance counter counts. </param>
    ''' <returns> The performance counter int 64. </returns>
    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="QueryPerformanceCounter")>
    Public Shared Function QueryPerformanceCounterInt64(ByRef x As Int64) _
      As <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Gets system performance counter frequency. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="x"> [in,out] Returns the performance counter frequency. </param>
    ''' <returns> The performance frequency int 64. </returns>
    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="QueryPerformanceFrequency")>
    Public Shared Function QueryPerformanceFrequencyInt64(ByRef x As Int64) _
      As <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)> Boolean
    End Function

#End Region

#Region " STRUCTURES "

    ''' <summary> This is the DCB structure used by the calls to the Windows API. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <StructLayout(LayoutKind.Sequential, Pack:=1)>
    Friend Structure DCB
#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The device-context blength. </summary>
        Public DCBlength As Integer

        ''' <summary> The baud rate. </summary>
        Public BaudRate As Integer

        ''' <summary> The first bits. </summary>
        Public Bits1 As Integer

        ''' <summary> The reserved. </summary>
        Public wReserved As Int16

        ''' <summary> The XON limit. </summary>
        Public XonLim As Int16

        ''' <summary> The XOFF limit. </summary>
        Public XoffLim As Int16

        ''' <summary> Size of the byte. </summary>
        Public ByteSize As Byte

        ''' <summary> The parity. </summary>
        Public Parity As Byte

        ''' <summary> The stop bits. </summary>
        Public StopBits As Byte

        ''' <summary> The XON character. </summary>
        Public XonChar As Byte

        ''' <summary> The XOFF character. </summary>
        Public XoffChar As Byte

        ''' <summary> The error character. </summary>
        Public ErrorChar As Byte

        ''' <summary> The EOF character. </summary>
        Public EofChar As Byte

        ''' <summary> The event character. </summary>
        Public EvtChar As Byte

        ''' <summary> The second reserved. </summary>
        Public wReserved2 As Int16
#Enable Warning IDE1006 ' Naming Styles
    End Structure

    ''' <summary> This is the CommTimeOuts structure used by the calls to the Windows API. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <StructLayout(LayoutKind.Sequential, Pack:=1)>
    Friend Structure COMMTIMEOUTS
#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The read interval timeout. </summary>
        Public ReadIntervalTimeout As Integer

        ''' <summary> The read total timeout multiplier. </summary>
        Public ReadTotalTimeoutMultiplier As Integer

        ''' <summary> The read total timeout constant. </summary>
        Public ReadTotalTimeoutConstant As Integer

        ''' <summary> The write total timeout multiplier. </summary>
        Public WriteTotalTimeoutMultiplier As Integer

        ''' <summary> The write total timeout constant. </summary>
        Public WriteTotalTimeoutConstant As Integer
#Enable Warning IDE1006 ' Naming Styles
    End Structure

    ''' <summary> This is the CommConfig structure used by the calls to the Windows API. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <StructLayout(LayoutKind.Sequential, Pack:=1)>
    Friend Structure COMMCONFIG
#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The size. </summary>
        Public dwSize As Integer

        ''' <summary> The version. </summary>
        Public wVersion As Int16

        ''' <summary> The reserved. </summary>
        Public wReserved As Int16

        ''' <summary> The dcbx. </summary>
        Public dcbx As DCB

        ''' <summary> Type of the provider sub. </summary>
        Public dwProviderSubType As Integer

        ''' <summary> The provider offset. </summary>
        Public dwProviderOffset As Integer

        ''' <summary> Size of the provider. </summary>
        Public dwProviderSize As Integer

        ''' <summary> Information describing the wc provider. </summary>
        Public wcProviderData As Byte
#Enable Warning IDE1006 ' Naming Styles
    End Structure

    ''' <summary> This is the OverLapped structure used by the calls to the Windows API. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <StructLayout(LayoutKind.Sequential, Pack:=1)>
    Friend Structure OVERLAPPED
#Disable Warning IDE1006 ' Naming Styles
    Public Internal As Integer

        ''' <summary> The internal high. </summary>
        Public InternalHigh As Integer

        ''' <summary> The offset. </summary>
        Public Offset As Integer

        ''' <summary> The offset high. </summary>
        Public OffsetHigh As Integer

        ''' <summary> The event. </summary>
        Public hEvent As Integer
#Enable Warning IDE1006 ' Naming Styles
    End Structure

#End Region

#Region " COM PORT Win32API "

    ''' <summary> Builds communications dcb. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="lpDef"> The definition. </param>
    ''' <param name="lpDCB"> [in,out] The dcb. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Shared Function BuildCommDCB(
        ByVal lpDef As String, ByRef lpDCB As DCB) As Integer
    End Function

    ''' <summary> Clears the communications error. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">    The file. </param>
    ''' <param name="lpErrors"> The errors. </param>
    ''' <param name="l">        An Integer to process. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function ClearCommError(
        ByVal hFile As Integer, ByVal lpErrors As Integer,
        ByVal l As Integer) As Integer
    End Function

    ''' <summary> Closes a handle. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hObject"> The object. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function CloseHandle(
        ByVal hObject As Integer) As Integer
    End Function

    ''' <summary> Creates an event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="lpEventAttributes"> The event attributes. </param>
    ''' <param name="bManualReset">      The manual reset. </param>
    ''' <param name="bInitialState">     State of the initial. </param>
    ''' <param name="lpName">            The name. </param>
    ''' <returns> The new event. </returns>
    <DllImport("kernel32.dll")>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Shared Function CreateEvent(ByVal lpEventAttributes As Integer, ByVal bManualReset As Integer, ByVal bInitialState As Integer,
                                       <MarshalAs(UnmanagedType.LPStr)> ByVal lpName As String) As Integer
    End Function

    ''' <summary> Creates a file. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="lpFileName">            The file name. </param>
    ''' <param name="dwDesiredAccess">       The desired access. </param>
    ''' <param name="dwShareMode">           The share mode. </param>
    ''' <param name="lpSecurityAttributes">  The security attributes. </param>
    ''' <param name="dwCreationDisposition"> The creation disposition. </param>
    ''' <param name="dwFlagsAndAttributes">  The flags and attributes. </param>
    ''' <param name="hTemplateFile">         The template file. </param>
    ''' <returns> The new file. </returns>
    <DllImport("kernel32.dll")>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Shared Function CreateFile(<MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String,
        ByVal dwDesiredAccess As Integer, ByVal dwShareMode As Integer,
        ByVal lpSecurityAttributes As Integer,
        ByVal dwCreationDisposition As Integer,
        ByVal dwFlagsAndAttributes As Integer,
        ByVal hTemplateFile As Integer) As Integer
    End Function

    ''' <summary> Escape communications function. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile"> The file. </param>
    ''' <param name="ifunc"> The ifunc. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function EscapeCommFunction(ByVal hFile As Integer, ByVal ifunc As Long) As Boolean
    End Function

    ''' <summary> Format message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="dwFlags">      The flags. </param>
    ''' <param name="lpSource">     Source for the. </param>
    ''' <param name="dwMessageId">  Identifier for the message. </param>
    ''' <param name="dwLanguageId"> Identifier for the language. </param>
    ''' <param name="lpBuffer">     The buffer. </param>
    ''' <param name="nSize">        The size. </param>
    ''' <param name="arguments">    The arguments. </param>
    ''' <returns> The formatted message. </returns>
    <DllImport("kernel32.dll")>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Shared Function FormatMessage(ByVal dwFlags As Integer, ByVal lpSource As Integer, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer,
                                         <MarshalAs(UnmanagedType.LPStr)> ByVal lpBuffer As String, ByVal nSize As Integer, ByVal arguments As Integer) As Integer
    End Function

    ''' <summary> The format message. </summary>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Declare Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Integer, ByVal lpSource As Integer,
                                                                                 ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer,
                                                                                 ByVal lpBuffer As StringBuilder, ByVal nSize As Integer,
                                                                                 ByVal arguments As Integer) As Integer

    ''' <summary> Gets communications modem status. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">         The file. </param>
    ''' <param name="lpModemStatus"> [in,out] The modem status. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <DllImport("kernel32.dll")>
    Public Shared Function GetCommModemStatus(ByVal hFile As Integer, ByRef lpModemStatus As Integer) As Boolean
    End Function

    ''' <summary> Gets communications state. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hCommDev"> The communications development. </param>
    ''' <param name="lpDCB">    [in,out] TheDCB. </param>
    ''' <returns> The communications state. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function GetCommState(ByVal hCommDev As Integer, ByRef lpDCB As DCB) As Integer
    End Function

    ''' <summary> Gets communications timeouts. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">          The file. </param>
    ''' <param name="lpCommTimeouts"> [in,out] The communications timeouts. </param>
    ''' <returns> The communications timeouts. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function GetCommTimeouts(ByVal hFile As Integer, ByRef lpCommTimeouts As COMMTIMEOUTS) As Integer
    End Function

    ''' <summary> Gets the last error. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The last error. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function GetLastError() As Integer
    End Function

    ''' <summary> Gets overlapped result. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">                      The file. </param>
    ''' <param name="lpOverlapped">               [in,out] The overlapped. </param>
    ''' <param name="lpNumberOfBytesTransferred"> [in,out] Number of bytes transferred. </param>
    ''' <param name="bWait">                      The wait. </param>
    ''' <returns> The overlapped result. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function GetOverlappedResult(ByVal hFile As Integer, ByRef lpOverlapped As OVERLAPPED, ByRef lpNumberOfBytesTransferred As Integer,
                                               ByVal bWait As Integer) As Integer
    End Function

    ''' <summary> Purge communications. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">   The file. </param>
    ''' <param name="dwFlags"> The flags. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function PurgeComm(ByVal hFile As Integer, ByVal dwFlags As Integer) As Integer
    End Function

    ''' <summary> Reads a file. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">                The file. </param>
    ''' <param name="buffer">               The buffer. </param>
    ''' <param name="nNumberOfBytesToRead"> Number of bytes to reads. </param>
    ''' <param name="lpNumberOfBytesRead">  [in,out] Number of bytes reads. </param>
    ''' <param name="lpOverlapped">         [in,out] The overlapped. </param>
    ''' <returns> The file. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function ReadFile(ByVal hFile As Integer, ByVal buffer As Byte(), ByVal nNumberOfBytesToRead As Integer,
                                    ByRef lpNumberOfBytesRead As Integer, ByRef lpOverlapped As OVERLAPPED) As Integer
    End Function

    ''' <summary> Sets communications timeouts. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">          The file. </param>
    ''' <param name="lpCommTimeouts"> [in,out] The communications timeouts. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function SetCommTimeouts(ByVal hFile As Integer, ByRef lpCommTimeouts As COMMTIMEOUTS) As Integer
    End Function

    ''' <summary> Sets communications state. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hCommDev"> The communications development. </param>
    ''' <param name="lpDCB">    [in,out] The dcb. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function SetCommState(ByVal hCommDev As Integer, ByRef lpDCB As DCB) As Integer
    End Function

    ''' <summary> Sets up the communications. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">      The file. </param>
    ''' <param name="dwInQueue">  Queue of INS. </param>
    ''' <param name="dwOutQueue"> Queue of outs. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function SetupComm(ByVal hFile As Integer, ByVal dwInQueue As Integer, ByVal dwOutQueue As Integer) As Integer
    End Function

    ''' <summary> Sets communications mask. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">     The file. </param>
    ''' <param name="lpEvtMask"> The event mask. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function SetCommMask(ByVal hFile As Integer, ByVal lpEvtMask As Integer) As Integer
    End Function

    ''' <summary> Wait communications event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">     The file. </param>
    ''' <param name="mask">      [in,out] The mask. </param>
    ''' <param name="lpOverlap"> [in,out] The overlap. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function WaitCommEvent(ByVal hFile As Integer, ByRef mask As EventMasks, ByRef lpOverlap As OVERLAPPED) As Integer
    End Function

    ''' <summary> Wait for single object. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hHandle">        The handle. </param>
    ''' <param name="dwMilliseconds"> The milliseconds. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function WaitForSingleObject(ByVal hHandle As Integer, ByVal dwMilliseconds As Integer) As Integer
    End Function

    ''' <summary> Writes a file. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hFile">                  The file. </param>
    ''' <param name="buffer">                 The buffer. </param>
    ''' <param name="nNumberOfBytesToWrite">  Number of bytes to writes. </param>
    ''' <param name="lpNumberOfBytesWritten"> [in,out] Number of bytes writtens. </param>
    ''' <param name="lpOverlapped">           [in,out] The overlapped. </param>
    ''' <returns> An Integer. </returns>
    <DllImport("kernel32.dll")>
    Friend Shared Function WriteFile(ByVal hFile As Integer, ByVal buffer As Byte(), ByVal nNumberOfBytesToWrite As Integer,
                                                                 ByRef lpNumberOfBytesWritten As Integer, ByRef lpOverlapped As OVERLAPPED) As Integer
    End Function

#End Region

End Class

