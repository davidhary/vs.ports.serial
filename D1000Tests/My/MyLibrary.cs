﻿
namespace isr.Ports.D1000Tests.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Serial Ports D1000 Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the D1000 Modules Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.Core.Ports.D1000.Tests";
    }
}