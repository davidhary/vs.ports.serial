using System;

using isr.Ports.Serial;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.D1000Tests
{

    /// <summary> Tests the D1141 Module. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 1/26/2018 </para></remarks>
    [TestClass()]
    public class D1141Tests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Serial.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( D1141Settings.Get().Exists, $"{typeof( D1141Settings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " CHECK SUM TESTS "

        /// <summary> (Unit Test Method) tests checksum. </summary>
        /// <remarks> David, 2020-11-16. </remarks>
        [TestMethod()]
        public void ChecksumTest()
        {
            string payload = "#1DOFF";
            string expected = "73";
            string actual = payload.Checksum();
            Assert.AreEqual( expected, actual, $"Check sum of {payload}" );
        }

        /// <summary> (Unit Test Method) validates the checksum test. </summary>
        /// <remarks> David, 2020-11-16. </remarks>
        [TestMethod()]
        public void ValidateChecksumTest()
        {
            string payload = "*1RD+00072.10A4";
            string expected = "A4";
            string message = payload.Remove( payload.Length - 2, 2 );
            string expectedMessage = "*1RD+00072.10";
            Assert.AreEqual( expectedMessage, message, $"Stripped message" );
            string actual = message.Checksum();
            Assert.AreEqual( expected, actual, $"Validated check sum of {payload}" );
        }

        #endregion

    }
}
