using System;
using System.Runtime.InteropServices;
using System.Text;

namespace isr.Ports.Serial
{

    /// <summary>
    /// Gets or sets safe application programming interface calls. This class suppresses stack walks
    /// for unmanaged code permission.  This class is for methods that are safe for anyone to call.
    /// Callers of these methods are not required to do a full security review to ensure that the
    /// usage is secure because the methods are harmless for any caller.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2004-11-22, 1.0.1787. Created </para>
    /// </remarks>
    internal sealed class UnsafeNativeMethods
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Prevents construction of this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        internal UnsafeNativeMethods()
        {
        }

        #endregion

        #region " DLL Imports "

        /// <summary> Gets time in ms since windows started. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The tick count. </returns>
        [DllImport( "kernel32.dll", EntryPoint = "GetTickCount" )]
        public static extern int GetTickCount();

        /// <summary> Gets system task time in milliseconds. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An Int32. </returns>
        [DllImport( "winmm.dll", EntryPoint = "timeGetTime" )]
        public static extern int TimeGetTime();

        /// <summary> Gets system performance counter value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="x"> [in,out] Returns the performance counter counts. </param>
        /// <returns> The performance counter int 64. </returns>
        [DllImport( "kernel32.dll", EntryPoint = "QueryPerformanceCounter" )]
        public static extern bool QueryPerformanceCounterInt64( ref long x );

        /// <summary> Gets system performance counter frequency. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="x"> [in,out] Returns the performance counter frequency. </param>
        /// <returns> The performance frequency int 64. </returns>
        [DllImport( "kernel32.dll", EntryPoint = "QueryPerformanceFrequency" )]
        public static extern bool QueryPerformanceFrequencyInt64( ref long x );

        #endregion

        #region " STRUCTURES "

        /// <summary> This is the DCB structure used by the calls to the Windows API. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        internal struct DCB
        {
#pragma warning disable IDE1006 // Naming Styles

            /// <summary> The device-context blength. </summary>
            public int DCBlength;

            /// <summary> The baud rate. </summary>
            public int BaudRate;

            /// <summary> The first bits. </summary>
            public int Bits1;

            /// <summary> The reserved. </summary>
            public short wReserved;

            /// <summary> The XON limit. </summary>
            public short XonLim;

            /// <summary> The XOFF limit. </summary>
            public short XoffLim;

            /// <summary> Size of the byte. </summary>
            public byte ByteSize;

            /// <summary> The parity. </summary>
            public byte Parity;

            /// <summary> The stop bits. </summary>
            public byte StopBits;

            /// <summary> The XON character. </summary>
            public byte XonChar;

            /// <summary> The XOFF character. </summary>
            public byte XoffChar;

            /// <summary> The error character. </summary>
            public byte ErrorChar;

            /// <summary> The EOF character. </summary>
            public byte EofChar;

            /// <summary> The event character. </summary>
            public byte EvtChar;

            /// <summary> The second reserved. </summary>
            public short wReserved2;
#pragma warning restore IDE1006 // Naming Styles
        }

        /// <summary> This is the CommTimeOuts structure used by the calls to the Windows API. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        internal struct COMMTIMEOUTS
        {
#pragma warning disable IDE1006 // Naming Styles

            /// <summary> The read interval timeout. </summary>
            public int ReadIntervalTimeout;

            /// <summary> The read total timeout multiplier. </summary>
            public int ReadTotalTimeoutMultiplier;

            /// <summary> The read total timeout constant. </summary>
            public int ReadTotalTimeoutConstant;

            /// <summary> The write total timeout multiplier. </summary>
            public int WriteTotalTimeoutMultiplier;

            /// <summary> The write total timeout constant. </summary>
            public int WriteTotalTimeoutConstant;
#pragma warning restore IDE1006 // Naming Styles
        }

        /// <summary> This is the CommConfig structure used by the calls to the Windows API. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        internal struct COMMCONFIG
        {
#pragma warning disable IDE1006 // Naming Styles

            /// <summary> The size. </summary>
            public int dwSize;

            /// <summary> The version. </summary>
            public short wVersion;

            /// <summary> The reserved. </summary>
            public short wReserved;

            /// <summary> The dcbx. </summary>
            public DCB dcbx;

            /// <summary> Type of the provider sub. </summary>
            public int dwProviderSubType;

            /// <summary> The provider offset. </summary>
            public int dwProviderOffset;

            /// <summary> Size of the provider. </summary>
            public int dwProviderSize;

            /// <summary> Information describing the wc provider. </summary>
            public byte wcProviderData;
#pragma warning restore IDE1006 // Naming Styles
        }

        /// <summary> This is the OverLapped structure used by the calls to the Windows API. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        internal struct OVERLAPPED
        {
#pragma warning disable IDE1006 // Naming Styles

            public int Internal;

            /// <summary> The internal high. </summary>
            public int InternalHigh;

            /// <summary> The offset. </summary>
            public int Offset;

            /// <summary> The offset high. </summary>
            public int OffsetHigh;

            /// <summary> The event. </summary>
            public int hEvent;
#pragma warning restore IDE1006 // Naming Styles
        }

        #endregion

        #region " COM PORT Win32API "

        /// <summary> Builds communications dcb. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="lpDef"> The definition. </param>
        /// <param name="lpDCB"> [in,out] The dcb. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        internal static extern int BuildCommDCB( string lpDef, ref DCB lpDCB );

        /// <summary> Clears the communications error. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">    The file. </param>
        /// <param name="lpErrors"> The errors. </param>
        /// <param name="l">        An Integer to process. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int ClearCommError( int hFile, int lpErrors, int l );

        /// <summary> Closes a handle. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hObject"> The object. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int CloseHandle( int hObject );

        /// <summary> Creates an event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="lpEventAttributes"> The event attributes. </param>
        /// <param name="bManualReset">      The manual reset. </param>
        /// <param name="bInitialState">     State of the initial. </param>
        /// <param name="lpName">            The name. </param>
        /// <returns> The new event. </returns>
        [DllImport( "kernel32.dll" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        internal static extern int CreateEvent( int lpEventAttributes, int bManualReset, int bInitialState, [MarshalAs( UnmanagedType.LPStr )] string lpName );

        /// <summary> Escape communications function. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile"> The file. </param>
        /// <param name="ifunc"> The ifunc. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern bool EscapeCommFunction( int hFile, long ifunc );

        /// <summary> Format message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="dwFlags">      The flags. </param>
        /// <param name="lpSource">     Source for the. </param>
        /// <param name="dwMessageId">  Identifier for the message. </param>
        /// <param name="dwLanguageId"> Identifier for the language. </param>
        /// <param name="lpBuffer">     The buffer. </param>
        /// <param name="nSize">        The size. </param>
        /// <param name="arguments">    The arguments. </param>
        /// <returns> The formatted message. </returns>
        [DllImport( "kernel32.dll" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        internal static extern int FormatMessage( int dwFlags, int lpSource, int dwMessageId, int dwLanguageId, [MarshalAs( UnmanagedType.LPStr )] string lpBuffer, int nSize, int arguments );

        /// <summary> The format message. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        [DllImport( "kernel32", EntryPoint = "FormatMessageA" )]
        internal static extern int FormatMessage( int dwFlags, int lpSource, int dwMessageId, int dwLanguageId, StringBuilder lpBuffer, int nSize, int arguments );

        /// <summary> Gets communications modem status. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">         The file. </param>
        /// <param name="lpModemStatus"> [in,out] The modem status. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [DllImport( "kernel32.dll" )]
        public static extern bool GetCommModemStatus( int hFile, ref int lpModemStatus );

        /// <summary> Gets communications state. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hCommDev"> The communications development. </param>
        /// <param name="lpDCB">    [in,out] TheDCB. </param>
        /// <returns> The communications state. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int GetCommState( int hCommDev, ref DCB lpDCB );

        /// <summary> Gets communications timeouts. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">          The file. </param>
        /// <param name="lpCommTimeouts"> [in,out] The communications timeouts. </param>
        /// <returns> The communications timeouts. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int GetCommTimeouts( int hFile, ref COMMTIMEOUTS lpCommTimeouts );

        /// <summary> Gets the last error. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The last error. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int GetLastError();

        /// <summary> Gets overlapped result. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">                      The file. </param>
        /// <param name="lpOverlapped">               [in,out] The overlapped. </param>
        /// <param name="lpNumberOfBytesTransferred"> [in,out] Number of bytes transferred. </param>
        /// <param name="bWait">                      The wait. </param>
        /// <returns> The overlapped result. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int GetOverlappedResult( int hFile, ref OVERLAPPED lpOverlapped, ref int lpNumberOfBytesTransferred, int bWait );

        /// <summary> Purge communications. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">   The file. </param>
        /// <param name="dwFlags"> The flags. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int PurgeComm( int hFile, int dwFlags );

        /// <summary> Reads a file. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">                The file. </param>
        /// <param name="buffer">               The buffer. </param>
        /// <param name="nNumberOfBytesToRead"> Number of bytes to reads. </param>
        /// <param name="lpNumberOfBytesRead">  [in,out] Number of bytes reads. </param>
        /// <param name="lpOverlapped">         [in,out] The overlapped. </param>
        /// <returns> The file. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int ReadFile( int hFile, byte[] buffer, int nNumberOfBytesToRead, ref int lpNumberOfBytesRead, ref OVERLAPPED lpOverlapped );

        /// <summary> Sets communications timeouts. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">          The file. </param>
        /// <param name="lpCommTimeouts"> [in,out] The communications timeouts. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int SetCommTimeouts( int hFile, ref COMMTIMEOUTS lpCommTimeouts );

        /// <summary> Sets communications state. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hCommDev"> The communications development. </param>
        /// <param name="lpDCB">    [in,out] The dcb. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int SetCommState( int hCommDev, ref DCB lpDCB );

        /// <summary> Sets up the communications. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">      The file. </param>
        /// <param name="dwInQueue">  Queue of INS. </param>
        /// <param name="dwOutQueue"> Queue of outs. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int SetupComm( int hFile, int dwInQueue, int dwOutQueue );

        /// <summary> Sets communications mask. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">     The file. </param>
        /// <param name="lpEvtMask"> The event mask. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int SetCommMask( int hFile, int lpEvtMask );

        /// <summary> Wait communications event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">     The file. </param>
        /// <param name="mask">      [in,out] The mask. </param>
        /// <param name="lpOverlap"> [in,out] The overlap. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int WaitCommEvent( int hFile, ref EventMasks mask, ref OVERLAPPED lpOverlap );

        /// <summary> Wait for single object. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hHandle">        The handle. </param>
        /// <param name="dwMilliseconds"> The milliseconds. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int WaitForSingleObject( int hHandle, int dwMilliseconds );

        /// <summary> Writes a file. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hFile">                  The file. </param>
        /// <param name="buffer">                 The buffer. </param>
        /// <param name="nNumberOfBytesToWrite">  Number of bytes to writes. </param>
        /// <param name="lpNumberOfBytesWritten"> [in,out] Number of bytes writtens. </param>
        /// <param name="lpOverlapped">           [in,out] The overlapped. </param>
        /// <returns> An Integer. </returns>
        [DllImport( "kernel32.dll" )]
        internal static extern int WriteFile( int hFile, byte[] buffer, int nNumberOfBytesToWrite, ref int lpNumberOfBytesWritten, ref OVERLAPPED lpOverlapped );

        #endregion

        #region " DERECATED "

        /// <summary> Creates a file. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="lpFileName">            The file name. </param>
        /// <param name="dwDesiredAccess">       The desired access. </param>
        /// <param name="dwShareMode">           The share mode. </param>
        /// <param name="lpSecurityAttributes">  The security attributes. </param>
        /// <param name="dwCreationDisposition"> The creation disposition. </param>
        /// <param name="dwFlagsAndAttributes">  The flags and attributes. </param>
        /// <param name="hTemplateFile">         The template file. </param>
        /// <returns> The new file. </returns>
        [Obsolete( " Use PINVOKE.NET Declarations " )]
        [DllImport( "kernel32.dll" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        internal static extern int CreateFile( [MarshalAs( UnmanagedType.LPStr )] string lpFileName, int dwDesiredAccess, int dwShareMode,
                                               int lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, int hTemplateFile );

        #endregion

        #region " https://www.pinvoke.net/default.aspx/kernel32.CreateFile "

        /// <summary>   Allocates a file object in the kernel, then return a handle to it. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <param name="filename">             Filename of the file. </param>
        /// <param name="access">               The access. </param>
        /// <param name="share">                The share. </param>
        /// <param name="securityAttributes">   The optional SECURITY_ATTRIBUTES Structure or IntPtr.Zero. </param>
        /// <param name="fileMode">             The file mode. </param>
        /// <param name="flagsAndAttributes">   The flags and attributes. </param>
        /// <param name="templateFile">         The template file. </param>
        /// <returns>   The new file. </returns>
        [DllImport( "kernel32.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        internal static extern IntPtr CreateFile(
             [MarshalAs( UnmanagedType.LPTStr )] string filename,
             [MarshalAs( UnmanagedType.U4 )] FileAccess access,
             [MarshalAs( UnmanagedType.U4 )] FileShare share,
             IntPtr securityAttributes, 
             [MarshalAs( UnmanagedType.U4 )] CreationDisposition fileMode,
             [MarshalAs( UnmanagedType.U4 )] FileAttributes flagsAndAttributes,
             IntPtr templateFile );

        /// <summary>   Allocates a file object in the kernel, then return a handle to it. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <param name="filename">             Filename of the file. </param>
        /// <param name="access">               The access. </param>
        /// <param name="share">                The share. </param>
        /// <param name="securityAttributes">   The optional SECURITY_ATTRIBUTES Structure or
        ///                                     IntPtr.Zero. </param>
        /// <param name="fileMode">             The file mode. </param>
        /// <param name="flagsAndAttributes">   The flags and attributes. </param>
        /// <param name="templateFile">         The template file. Per Safe Handle Example, must be zero. </param>
        /// <returns>   The new file a. </returns>
        [DllImport( "kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        internal static extern IntPtr CreateFileA(
             [MarshalAs( UnmanagedType.LPStr )] string filename,
             [MarshalAs( UnmanagedType.U4 )] FileAccess access,
             [MarshalAs( UnmanagedType.U4 )] FileShare share,
             IntPtr securityAttributes,
             [MarshalAs( UnmanagedType.U4 )] CreationDisposition fileMode,
             [MarshalAs( UnmanagedType.U4 )] FileAttributes flagsAndAttributes,
             IntPtr templateFile );

        /// <summary>   Allocates a file object in the kernel, then return a handle to it. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <param name="filename">             Filename of the file. </param>
        /// <param name="access">               The access. </param>
        /// <param name="share">                The share. </param>
        /// <param name="securityAttributes">   The optional SECURITY_ATTRIBUTES Structure or
        ///                                     IntPtr.Zero. </param>
        /// <param name="fileMode">             The file mode. </param>
        /// <param name="flagsAndAttributes">   The flags and attributes. </param>
        /// <param name="templateFile">         The template file. </param>
        /// <returns>   The new file w. </returns>
        [DllImport( "kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true )]
        internal static extern IntPtr CreateFileW(
             [MarshalAs( UnmanagedType.LPWStr )] string filename,
             [MarshalAs( UnmanagedType.U4 )] FileAccess access,
             [MarshalAs( UnmanagedType.U4 )] FileShare share,
             IntPtr securityAttributes,
             [MarshalAs( UnmanagedType.U4 )] CreationDisposition fileMode,
             [MarshalAs( UnmanagedType.U4 )] FileAttributes flagsAndAttributes,
             IntPtr templateFile );

        #endregion
    }

}
