using System;
using System.Text;
using System.Threading;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports.Serial
{

    /// <summary>
    /// A serial communications port. This class provides all the necessary support for communicating
    /// with the Comm Port (otherwise known as the Serial Port, or RS232 port).
    /// </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public class SerialCommPort
    {

        #region " EVENTS "

        /// <summary> Event queue for all listeners interested in DataReceived events.
        /// These events allow the program using this class to react to Comm Port  events. </summary>
        public event DataReceivedEventHandler DataReceived;

        /// <summary>   Delegate for handling DataReceived events. </summary>
        /// <remarks>   David, 2020-11-18. </remarks>
        /// <param name="source">       Source for the. </param>
        /// <param name="dataBuffer">   Buffer for data data. </param>
        public delegate void DataReceivedEventHandler( SerialCommPort source, byte[] dataBuffer );

        /// <summary> Event queue for all listeners interested in TxCompleted events. </summary>
        public event TxCompletedEventHandler TxCompleted;

        /// <summary>   Delegate for handling TxCompleted events. </summary>
        /// <remarks>   David, 2020-11-18. </remarks>
        /// <param name="source">   Source for the. </param>
        public delegate void TxCompletedEventHandler( SerialCommPort source );

        /// <summary>   Event queue for all listeners interested in comm events. </summary>
        public event CommEventEventHandler CommEvent;

        /// <summary>   Delegate for handling CommEvent events. </summary>
        /// <remarks>   David, 2020-11-18. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="mask">     The mask. </param>
        public delegate void CommEventEventHandler( SerialCommPort source, EventMasks mask );

        #endregion

        #region " CONSTANTS "

        /// <summary> The purge receive abort.
        /// These constants are used to make the code clearer.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int PURGE_RXABORT = 0x2;

        /// <summary> The purge RX Clear. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int PURGE_RXCLEAR = 0x8;

        /// <summary> The purge TX Abort. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int PURGE_TXABORT = 0x1;

        /// <summary> The purge TX Clear. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int PURGE_TXCLEAR = 0x4;

        /// <summary> The generic read. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const uint GENERIC_READ = 0x80000000;

        /// <summary> The generic write. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int GENERIC_WRITE = 0x40000000;

        /// <summary> The open existing. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int OPEN_EXISTING = 3;

        /// <summary> The create always. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int CREATE_ALWAYS = 2;

        /// <summary> The invalid handle value. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int INVALID_HANDLE_VALUE = -1;

        /// <summary> Size of the i/o buffer. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int IO_BUFFER_SIZE = 1024;

        /// <summary> The file flag overlapped. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int FILE_FLAG_OVERLAPPED = 0x40000000;

        /// <summary> The error i/o pending. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int ERROR_IO_PENDING = 997;

        /// <summary> The wait object 0. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int WAIT_OBJECT_0 = 0;

        /// <summary> The error i/o incomplete. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int ERROR_IO_INCOMPLETE = 996;

        /// <summary> The wait timeout. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int WAIT_TIMEOUT = 0x102;

        /// <summary> The infinite. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        private const int INFINITE = 0xFFFFFFF; // was 0xFFFFFFFF


        #endregion

        #region " PROPERTIES "

        /// <summary> Sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        public int BaudRate { get; set; } = 19200;

        /// <summary> Sets the buffer size. </summary>
        /// <value> The size of the buffer. </value>
        public int BufferSize { get; set; } = 512;

        /// <summary> Sets the data bit. </summary>
        /// <value> The data bit. </value>
        public int DataBit { get; set; } = 8;

        /// <summary>  Handle to Com Port </summary>
        private int _PortHandle = -1;

        /// <summary> Sets the DTR Line. </summary>
        /// <value> The DTR Line. </value>
        public bool Dtr
        {
            set {
                if ( !(this._PortHandle == -1) )
                {
                    _ = value
                        ? UnsafeNativeMethods.EscapeCommFunction( this._PortHandle, ( long ) Lines.SetDtr )
                        : UnsafeNativeMethods.EscapeCommFunction( this._PortHandle, ( long ) Lines.ClearDtr );
                }
            }
        }

        /// <summary> Stream to read data from. </summary>
        private byte[] _InputStream;

        /// <summary>
        /// Gets the input stream: an array of bytes that represents the input coming into the Comm Port.
        /// </summary>
        /// <value> The input stream. </value>
        public virtual byte[] InputStream => this._InputStream;

        /// <summary>
        /// Gets the input stream string: represents the data coming into to the Comm Port.
        /// </summary>
        /// <value> The input stream string. </value>
        public virtual string InputStreamString
        {
            get {
                var oEncoder = new ASCIIEncoding();
                return oEncoder.GetString( this.InputStream );
            }
        }

        /// <summary> Gets the open status of the Comm Port. </summary>
        /// <value> The is open. </value>
        public bool IsOpen => this._PortHandle != -1;

        /// <summary> Gets the serial communications port error. </summary>
        /// <exception cref="IOChannelException"> Thrown when an I/O Channel error condition occurs. </exception>
        /// <value> The serial communications port error. </value>
        public string SerialCommPortError { get; private set; }

        /// <summary> Gets the modem status. </summary>
        /// <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
        /// condition occurs. </exception>
        /// <value> The modem status. </value>
        public ModemStatusBits ModemStatus
        {
            get {
                if ( this._PortHandle == -1 )
                {
                    throw new IOChannelException( "Please initialize and open port before using this method" );
                }
                else
                {
                    // Retrieve modem status
                    var lpModemStatus = default( int );
                    return !UnsafeNativeMethods.GetCommModemStatus( this._PortHandle, ref lpModemStatus )
                        ? throw new IOChannelException( "Unable to get modem status" )
                        : ( ModemStatusBits ) Conversions.ToInteger( lpModemStatus );
                }
            }
        }

        /// <summary> Sets the parity. </summary>
        /// <value> The parity. </value>
        public DataParity Parity { get; set; } = DataParity.None;

        /// <summary> Sets the port. </summary>
        /// <value> The port. </value>
        public int Port { get; set; } = 1;

        /// <summary> Sets the RTS line. </summary>
        /// <value> The RTS line. </value>
        public bool Rts
        {
            set {
                if ( !(this._PortHandle == -1) )
                {
                    _ = value
                        ? UnsafeNativeMethods.EscapeCommFunction( this._PortHandle, ( long ) Lines.SetRts )
                        : UnsafeNativeMethods.EscapeCommFunction( this._PortHandle, ( long ) Lines.ClearRts );
                }
            }
        }

        /// <summary> Gets or sets the stop bit. </summary>
        /// <value> The stop bit. </value>
        public DataStopBit StopBit { get; set; } = DataStopBit.StopBit1;

        /// <summary> The  timeout in ms. </summary>
        private int _Timeout = 70;

        /// <summary> Gets or sets the timeout in ms. </summary>
        /// <value> The timeout in ms. </value>
        public virtual int Timeout
        {
            get => this._Timeout;

            set {
                this._Timeout = value == 0 ? 500 : value;
                // If Port is open updates it on the fly
                this.PSetTimeout();
            }
        }

        /// <summary> Gets or sets the working mode to overlapped or non-overlapped. </summary>
        /// <value> The working mode. </value>
        public WorkingMode WorkingMode { get; set; }

        #endregion

        #region " METHODS "

        /// <summary>
        /// This subroutine invokes a thread to perform an asynchronous read. This routine should not be
        /// called directly, but is used by the class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ReadAsync()
        {
            _ = this.Read( this._TempBytesToRead );
        }

        /// <summary> Buffer for temporary transmit data. </summary>
        private byte[] _TempTransmitBuffer;

        /// <summary>
        /// This subroutine invokes a thread to perform an asynchronous write.   This routine should not
        /// be called directly, but is used   by the class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void WriteAsync()
        {
            this.Write( this._TempTransmitBuffer );
        }

        /// <summary> The temporary bytes to read. </summary>
        private int _TempBytesToRead;

        /// <summary> The transmit receive thread. </summary>
        private Thread _TransmitReceiveThread;

        /// <summary>
        /// Asynchronous read. This subroutine uses another thread to read from the Comm Port. It raises
        /// RxCompleted when done. It reads an integer.
        /// </summary>
        /// <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
        /// condition occurs. </exception>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="bytes2Read"> The bytes 2 read. </param>
        public void AsyncRead( int bytes2Read )
        {
            if ( this.WorkingMode != WorkingMode.Overlapped )
                throw new IOChannelException( $"Asynchronous Methods allowed only when {nameof( Serial.WorkingMode )} is {WorkingMode.Overlapped}" );
            this._TempBytesToRead = bytes2Read;
            this._TransmitReceiveThread = new Thread( this.ReadAsync );
            this._TransmitReceiveThread.Start();
        }

        private readonly bool _WaitOnWrite;

        /// <summary>
        /// Asynchronous write. This subroutine uses another thread to write to the Comm Port. It  raises
        /// TxCompleted when done. It writes an array of bytes.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
        /// condition occurs. </exception>
        /// <param name="buffer"> The buffer. </param>
        public void AsyncWrite( byte[] buffer )
        {
            if ( this.WorkingMode != WorkingMode.Overlapped )
                throw new IOChannelException( $"Async Methods allowed only when {nameof( Serial.WorkingMode )} is {WorkingMode.Overlapped}" );
            if ( this._WaitOnWrite )
                throw new IOChannelException( "Unable to send message because of pending transmission." );
            this._TempTransmitBuffer = buffer;
            this._TransmitReceiveThread = new Thread( this.WriteAsync );
            this._TransmitReceiveThread.Start();
        }

        /// <summary>
        /// Asynchronous write. This subroutine uses another thread to write to the Comm Port. It  raises
        /// TxCompleted when done. It writes an array of bytes.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="buffer"> The buffer. </param>
        public void AsyncWrite( string buffer )
        {
            var oEncoder = new ASCIIEncoding();
            var aByte = oEncoder.GetBytes( buffer );
            this.AsyncWrite( aByte );
        }

        /// <summary>
        /// Check line status. This function takes the ModemStatusBits and returns a boolean value
        /// signifying whether the Modem is active.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="line"> The line. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool CheckLineStatus( ModemStatusBits line )
        {
            return Convert.ToBoolean( ( int ) (this.ModemStatus & line) );
        }

        /// <summary> Clears the input buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ClearInputBuffer()
        {
            if ( !(this._PortHandle == -1) )
            {
                _ = UnsafeNativeMethods.PurgeComm( this._PortHandle, PURGE_RXCLEAR );
            }
        }

        /// <summary> Closes the Comm Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Close()
        {
            if ( this._PortHandle != -1 )
            {
                _ = UnsafeNativeMethods.CloseHandle( this._PortHandle );
                this._PortHandle = -1;
            }
        }

        /// <summary> Opens and initializes the Comm Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when a IO Channel error
        /// condition occurs. </exception>
        public void Open()
        {

            // Get DCB block,Update with current data
            var uDcb = default( UnsafeNativeMethods.DCB );
            int iRc;

            // Set working mode
            // int iMode = Convert.ToInt32(Interaction.IIf(WorkingMode == WorkingMode.Overlapped, FILE_FLAG_OVERLAPPED, 0));
            FileAttributes iMode = this.WorkingMode == WorkingMode.Overlapped ? FileAttributes.Overlapped : FileAttributes.Normal;

            // Initializes Com Port
            if ( this.Port > 0 )
            {
                try
                {
                    // Creates a COM Port stream handle 
                    // _PortHandle = UnsafeNativeMethods.CreateFile($"COM{Port}", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, iMode, IntPtr.Zero);
                    IntPtr handle = UnsafeNativeMethods.CreateFile( $"COM{this.Port}", FileAccess.GenericRead | FileAccess.GenericWrite,
                                                                    FileShare.None, IntPtr.Zero,
                                                                    CreationDisposition.OpenExisting, iMode, IntPtr.Zero );
                    this._PortHandle = ( int ) handle;
                    if ( this._PortHandle != -1 )
                    {
                        // Clear all communication errors
                        var lpErrCode = default( int );
                        iRc = UnsafeNativeMethods.ClearCommError( this._PortHandle, lpErrCode, 0 );
                        // Clears I/O buffers
                        iRc = UnsafeNativeMethods.PurgeComm( this._PortHandle, ( int ) (PurgeBuffers.RXClear | PurgeBuffers.TxClear) );
                        // Gets COM Settings
                        iRc = UnsafeNativeMethods.GetCommState( this._PortHandle, ref uDcb );
                        // Updates COM Settings
                        string sParity = "NOEM";
                        sParity = sParity.Substring( ( int ) this.Parity, 1 );
                        // Set DCB State
                        string sDCBState = $"baud={this.BaudRate} parity={sParity} data={this.DataBit} stop={( int ) this.StopBit}";
                        iRc = UnsafeNativeMethods.BuildCommDCB( sDCBState, ref uDcb );
                        if ( iRc == 0 )
                        {
                            string sErrTxt = this.PErr2Text( UnsafeNativeMethods.GetLastError() );
                            throw new IOChannelException( $"Unable to set COM state {sErrTxt}" );
                        }

                        iRc = UnsafeNativeMethods.SetCommState( this._PortHandle, ref uDcb );
                        if ( iRc == 0 )
                        {
                            string sErrTxt = this.PErr2Text( UnsafeNativeMethods.GetLastError() );
                            throw new IOChannelException( $"Unable to set COM state{sErrTxt}" );
                        }
                        // Setup Buffers (Rx,Tx)
                        iRc = UnsafeNativeMethods.SetupComm( this._PortHandle, this.BufferSize, this.BufferSize );
                        // Set Timeouts
                        this.PSetTimeout();
                    }
                    else
                    {
                        // Raise Initialization problems 
                        this.SerialCommPortError = $"Unable to open COM{this.Port}";
                        throw new IOChannelException( this.SerialCommPortError );
                    }
                }
                catch ( Exception Ex )
                {
                    this.SerialCommPortError = $"Exception opening pert;. {Ex}";
                    throw new IOChannelException( this.SerialCommPortError, Ex );
                }
            }
            else
            {
                // Port not defined, cannot open
                this.SerialCommPortError = "COM Port not defined, use Port property to set it before invoking InitPort";
                throw new IOChannelException( this.SerialCommPortError );
            }
        }

        /// <summary> Opens and initializes the Comm Port (overloaded to support parameters). </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port">       The port. </param>
        /// <param name="baudRate">   The baud rate. </param>
        /// <param name="dataBit">    The data bit. </param>
        /// <param name="parity">     The parity. </param>
        /// <param name="stopBit">    The stop bit. </param>
        /// <param name="bufferSize"> Size of the buffer. </param>
        public void Open( int port, int baudRate, int dataBit, DataParity parity, DataStopBit stopBit, int bufferSize )
        {
            this.Port = port;
            this.BaudRate = baudRate;
            this.DataBit = dataBit;
            this.Parity = parity;
            this.StopBit = stopBit;
            this.BufferSize = bufferSize;
            this.Open();
        }

        /// <summary> Translates an API error code to text. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="code"> The code. </param>
        /// <returns> A String. </returns>
        private string PErr2Text( int code )
        {
            var builder = new StringBuilder( 256 );
            int result = UnsafeNativeMethods.FormatMessage( 0x1000, 0, code, 0, builder, 256, 0 );
            return result > 0 ? builder.ToString() : "Error not found.";
        }

        /// <summary> The overlapped. </summary>
        private UnsafeNativeMethods.OVERLAPPED _Overlapped;

        /// <summary> True to wait on read. </summary>
        private bool _WaitOnRead;

        /// <summary> Handles the overlapped read described by bytes2Read. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException">  Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
        /// condition occurs. </exception>
        /// <param name="bytes2Read"> The bytes 2 read. </param>
        private void PHandleOverlappedRead( int bytes2Read )
        {
            int iReadChars = default, iRc, iRes, iLastErr;
            this._Overlapped.hEvent = UnsafeNativeMethods.CreateEvent( default, 1, 0, null );
            if ( this._Overlapped.hEvent == 0 )
            {
                // Can't create event
                throw new IOChannelException( "Error creating event for overlapped read." );
            }
            // Overlapped reading
            else if ( this._WaitOnRead == false )
            {
                this._InputStream = new byte[bytes2Read];
                iRc = UnsafeNativeMethods.ReadFile( this._PortHandle, this._InputStream, bytes2Read, ref iReadChars, ref this._Overlapped );
                if ( iRc == 0 )
                {
                    iLastErr = UnsafeNativeMethods.GetLastError();
                    if ( iLastErr != ERROR_IO_PENDING )
                    {
                        throw new ArgumentException( "Overlapped Read Error: " + this.PErr2Text( iLastErr ) );
                    }
                    else
                    {
                        // Set Flag
                        this._WaitOnRead = true;
                    }
                }
                else
                {
                    // Read completed successfully
                    DataReceived?.Invoke( this, this._InputStream );
                }
            }
            // Wait for operation to be completed
            if ( this._WaitOnRead )
            {
                iRes = UnsafeNativeMethods.WaitForSingleObject( this._Overlapped.hEvent, this._Timeout );
                switch ( iRes )
                {
                    case WAIT_OBJECT_0:
                        {
                            // Object signaled,operation completed
                            if ( UnsafeNativeMethods.GetOverlappedResult( this._PortHandle, ref this._Overlapped, ref iReadChars, 0 ) == 0 )
                            {

                                // Operation error
                                iLastErr = UnsafeNativeMethods.GetLastError();
                                if ( iLastErr == ERROR_IO_INCOMPLETE )
                                {
                                    throw new IOChannelException( "Read operation incomplete" );
                                }
                                else
                                {
                                    throw new IOChannelException( $"Read operation error {iLastErr}" );
                                }
                            }
                            else
                            {
                                // Operation completed
                                DataReceived?.Invoke( this, this._InputStream );
                                this._WaitOnRead = false;
                            }

                            break;
                        }

                    case WAIT_TIMEOUT:
                        {
                            throw new IOTimeoutException( "Timeout error" );
                        }

                    default:
                        {
                            throw new IOChannelException( "Overlapped read error" );
                        }
                }
            }
        }

        /// <summary> The overlapped write. </summary>
        private UnsafeNativeMethods.OVERLAPPED _OverlappedWrite;

        /// <summary> Handles the overlapped write described by buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException">  Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <exception cref="isr.Ports.Serial.IOChannelException"> Thrown when an Application error
        /// condition occurs. </exception>
        /// <param name="buffer"> The buffer. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool PHandleOverlappedWrite( byte[] buffer )
        {
            int iBytesWritten = default, iRc, iLastErr, iRes;
            var bErr = default( bool );
            this._OverlappedWrite.hEvent = UnsafeNativeMethods.CreateEvent( default, 1, 0, null );
            if ( this._OverlappedWrite.hEvent == 0 )
            {
                // Can't create event
                throw new IOChannelException( "Error creating event for overlapped write." );
            }
            else
            {
                // Overlapped write
                _ = UnsafeNativeMethods.PurgeComm( this._PortHandle, PURGE_RXCLEAR | PURGE_TXCLEAR );
                this._WaitOnRead = true;
                iRc = UnsafeNativeMethods.WriteFile( this._PortHandle, buffer, buffer.Length, ref iBytesWritten, ref this._OverlappedWrite );
                if ( iRc == 0 )
                {
                    iLastErr = UnsafeNativeMethods.GetLastError();
                    if ( iLastErr != ERROR_IO_PENDING )
                    {
                        throw new ArgumentException( "Overlapped Read Error: " + this.PErr2Text( iLastErr ) );
                    }
                    else
                    {
                        // Write is pending
                        iRes = UnsafeNativeMethods.WaitForSingleObject( this._OverlappedWrite.hEvent, INFINITE );
                        switch ( iRes )
                        {
                            case WAIT_OBJECT_0:
                                {
                                    // Object signaled,operation completed
                                    if ( UnsafeNativeMethods.GetOverlappedResult( this._PortHandle, ref this._OverlappedWrite, ref iBytesWritten, 0 ) == 0 )
                                    {
                                        bErr = true;
                                    }
                                    else
                                    {
                                        // Notifies Asynchronous transmit completion,stops thread
                                        this._WaitOnRead = false;
                                        TxCompleted?.Invoke( this );
                                    }

                                    break;
                                }
                        }
                    }
                }
                else
                {
                    // Wait operation completed immediately
                    bErr = false;
                }
            }

            _ = UnsafeNativeMethods.CloseHandle( this._OverlappedWrite.hEvent );
            return bErr;
        }

        /// <summary> Sets the Comm Port timeouts. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void PSetTimeout()
        {
            UnsafeNativeMethods.COMMTIMEOUTS uCtm;
            // Set ComTimeout
            if ( this._PortHandle == -1 )
            {
                return;
            }
            else
            {
                // Changes setup on the fly
                uCtm.ReadIntervalTimeout = 0xFFFFFFF;  // *********************   '0 0xFFFFFFFF;  
                uCtm.ReadTotalTimeoutMultiplier = 0;    // *settings to disable*
                uCtm.ReadTotalTimeoutConstant = 0;      // * built-in Kernel32 *   'miTimeout       
                uCtm.WriteTotalTimeoutMultiplier = 0;   // *   comm timeout    *   '10
                uCtm.WriteTotalTimeoutConstant = 0;     // *********************   '100
                _ = UnsafeNativeMethods.SetCommTimeouts( this._PortHandle, ref uCtm );
            }
        }

        /// <summary>
        /// Reads the given bytes 2 read. Returns an integer specifying the number of bytes read from the
        /// Comm Port. It accepts a parameter specifying the number of desired bytes to read.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="bytes2Read"> The bytes 2 read. </param>
        /// <returns> An Integer. </returns>
        public int Read( int bytes2Read )
        {
            int iReadChars = default, iRc;

            // If Bytes2Read not specified uses Buffer size
            if ( bytes2Read == 0 )
                bytes2Read = this.BufferSize;
            if ( this._PortHandle == -1 )
            {
                throw new InvalidOperationException( "Please initialize and open port before using this method" );
            }
            else
            {
                // Get bytes from port
                try
                {
                    // Purge buffers
                    // PurgeComm(mhRS, PURGE_RXCLEAR Or PURGE_TXCLEAR)
                    // Creates an event for overlapped operations
                    if ( this.WorkingMode == WorkingMode.Overlapped )
                    {
                        this.PHandleOverlappedRead( bytes2Read );
                    }
                    else
                    {
                        // Non overlapped mode
                        this._InputStream = new byte[bytes2Read];
                        UnsafeNativeMethods.OVERLAPPED arglpOverlapped = default;
                        iRc = UnsafeNativeMethods.ReadFile( this._PortHandle, this._InputStream, bytes2Read, ref iReadChars, ref arglpOverlapped );
                        if ( iRc == 0 )
                        {
                            // Read Error
                            throw new System.IO.IOException( "ReadFile error " + iRc.ToString() );
                        }
                        // Handles timeout or returns input chars
                        else if ( iReadChars < bytes2Read )
                        {
                            // changed PLS 7-17-2003
                            // Throw New IOTimeoutException("Timeout error")
                            return iReadChars;
                        }
                        else
                        {
                            this._WaitOnRead = true;
                            return iReadChars;
                        }
                    }
                }
                catch ( Exception Ex )
                {
                    // Others generic errors
                    throw new System.IO.IOException( "Read Error: " + Ex.Message, Ex );
                }
            }

            return iReadChars;
        }

        /// <summary> Writes an array of bytes to the Comm Port to be written. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="buffer"> The buffer. </param>
        public void Write( byte[] buffer )
        {
            int iBytesWritten = default, iRc;
            if ( this._PortHandle == -1 )
            {
                throw new InvalidOperationException( "Please initialize and open port before using this method" );
            }
            else
            {
                // Transmit data to COM Port
                try
                {
                    if ( this.WorkingMode == WorkingMode.Overlapped )
                    {
                        // Overlapped write
                        if ( this.PHandleOverlappedWrite( buffer ) )
                        {
                            throw new System.IO.IOException( "Error in overlapped write" );
                        }
                    }
                    else
                    {
                        // Clears IO buffers
                        _ = UnsafeNativeMethods.PurgeComm( this._PortHandle, PURGE_RXCLEAR | PURGE_TXCLEAR );
                        UnsafeNativeMethods.OVERLAPPED arglpOverlapped = default;
                        iRc = UnsafeNativeMethods.WriteFile( this._PortHandle, buffer, buffer.Length, ref iBytesWritten, ref arglpOverlapped );
                        if ( iRc == 0 )
                        {
                            throw new System.IO.IOException( $"Write Error - Bytes Written {iBytesWritten} of {buffer.Length}" );
                        }
                    }
                }
                catch ( Exception )
                {
                    throw;
                }
            }
        }

        /// <summary> Writes a string to the Comm Port to be written. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="buffer"> The buffer. </param>
        public void Write( string buffer )
        {
            var oEncoder = new ASCIIEncoding();
            var aByte = oEncoder.GetBytes( buffer );
            this.Write( aByte );
        }

        #endregion

        #region " PRIVATE ENUMS "

        /// <summary> Values that represent values used to purge the various buffers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private enum PurgeBuffers
        {

            /// <summary> An enum constant representing the Receive abort option. </summary>
            RXAbort = 0x2,

            /// <summary> An enum constant representing the Receive clear option. </summary>
            RXClear = 0x8,

            /// <summary> An enum constant representing the Transmit abort option. </summary>
            TxAbort = 0x1,

            /// <summary> An enum constant representing the Transmit clear option. </summary>
            TxClear = 0x4
        }

        /// <summary> Values for the lines sent to the Comm Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private enum Lines
        {

            /// <summary> An enum constant representing the set RTS option. </summary>
            SetRts = 3,

            /// <summary> An enum constant representing the clear RTS option. </summary>
            ClearRts = 4,

            /// <summary> An enum constant representing the set dtr option. </summary>
            SetDtr = 5,

            /// <summary> An enum constant representing the clear dtr option. </summary>
            ClearDtr = 6,

            /// <summary> An enum constant representing the reset Development option.
            /// Reset device if possible </summary>
            ResetDev = 7,

            /// <summary> An enum constant representing option for setting break line. </summary>
            SetBreak = 8,

            /// <summary> An enum constant representing the clear break option. </summary>
            ClearBreak = 9
        }
        #endregion

    }

}
