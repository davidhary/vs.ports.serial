using System;

using isr.Ports.Serial;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.Win32Tests
{

    /// <summary> Tests the <see cref="isr.Ports.Serial.SerialCommPort"/> port using a two port null model. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "NullModemTwoPorts" )]
    public class NullModemTwoPortTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( NullModemSettings.Get().Exists, $"{typeof( NullModemSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " NULL MODEM TESTS "

        /// <summary> Assets the port open condition. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        private static void AssertPortOpen( SerialCommPort port )
        {
            Assert.AreEqual( true, port.IsOpen, $"Serial Port #{port.Port} should be open" );
        }

        /// <summary> (Unit Test Method) tests null modem using two ports. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void TwoPortNullModemTest()
        {
            if ( !NullModemSettings.Get().CheckPort1Exists() )
                Assert.Inconclusive( $"Serial Port #{NullModemSettings.Get().Port1Number} not found" );
            if ( !NullModemSettings.Get().CheckPort1Exists() )
                Assert.Inconclusive( $"Serial Port #{NullModemSettings.Get().Port2Number} not found" );
            var port1 = new SerialCommPort() { Port = NullModemSettings.Get().Port1Number };
            try
            {
                port1.Open();
                Assert.AreEqual( NullModemSettings.Get().Port1Number, port1.Port, $"Serial port number should match" );
                AssertPortOpen( port1 );
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            var port2 = new SerialCommPort() { Port = NullModemSettings.Get().Port2Number };
            try
            {
                port2.Open();
                Assert.AreEqual( NullModemSettings.Get().Port1Number, port2.Port, $"Serial port number should match" );
                AssertPortOpen( port2 );
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            string message = "ABC";
            port1.Write( message );
            Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 10d ) );
            _ = port2.Read( message.Length );
            string receivedMessage = port2.InputStreamString;
            Assert.AreEqual( message, receivedMessage, $"Serial Port #{port2.Port} received message should match" );
        }

        #endregion

    }
}
