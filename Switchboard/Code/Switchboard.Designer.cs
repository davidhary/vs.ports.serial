﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports
{
    [DesignerGenerated()]
    public partial class Switchboard
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(Switchboard));
            _ApplicationsListBoxLabel = new Label();
            __OpenButton = new Button();
            __OpenButton.Click += new EventHandler(OpenButton_Click);
            __ExitButton = new Button();
            __ExitButton.Click += new EventHandler(ExitButton_Click);
            _ApplicationsListBox = new ListBox();
            _Tooltip = new ToolTip(components);
            SuspendLayout();
            // 
            // _ApplicationsListBoxLabel
            // 
            _ApplicationsListBoxLabel.Location = new Point(15, 15);
            _ApplicationsListBoxLabel.Name = "_ApplicationsListBoxLabel";
            _ApplicationsListBoxLabel.Size = new Size(345, 21);
            _ApplicationsListBoxLabel.TabIndex = 15;
            _ApplicationsListBoxLabel.Text = "Select an item from the list and click Open";
            // 
            // _OpenButton
            // 
            __OpenButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __OpenButton.Location = new Point(414, 38);
            __OpenButton.Margin = new Padding(3, 4, 3, 4);
            __OpenButton.Name = "__OpenButton";
            __OpenButton.Size = new Size(87, 30);
            __OpenButton.TabIndex = 13;
            __OpenButton.Text = "&Open...";
            // 
            // _ExitButton
            // 
            __ExitButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ExitButton.Location = new Point(414, 165);
            __ExitButton.Margin = new Padding(3, 4, 3, 4);
            __ExitButton.Name = "__ExitButton";
            __ExitButton.Size = new Size(87, 30);
            __ExitButton.TabIndex = 12;
            __ExitButton.Text = "E&xit";
            // 
            // _ApplicationsListBox
            // 
            _ApplicationsListBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ApplicationsListBox.ItemHeight = 17;
            _ApplicationsListBox.Location = new Point(13, 38);
            _ApplicationsListBox.Margin = new Padding(3, 4, 3, 4);
            _ApplicationsListBox.Name = "_ApplicationsListBox";
            _ApplicationsListBox.Size = new Size(382, 157);
            _ApplicationsListBox.TabIndex = 14;
            _Tooltip.SetToolTip(_ApplicationsListBox, "Select an application to test");
            // 
            // Switchboard
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            ClientSize = new Size(513, 217);
            Controls.Add(_ApplicationsListBoxLabel);
            Controls.Add(__OpenButton);
            Controls.Add(__ExitButton);
            Controls.Add(_ApplicationsListBox);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Switchboard";
            Text = "Switchboard";
            Load += new EventHandler(Form_Load);
            Shown += new EventHandler(Form_Shown);
            ResumeLayout(false);
        }

        private Label _ApplicationsListBoxLabel;
        private Button __OpenButton;

        private Button _OpenButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenButton != null)
                {
                    __OpenButton.Click -= OpenButton_Click;
                }

                __OpenButton = value;
                if (__OpenButton != null)
                {
                    __OpenButton.Click += OpenButton_Click;
                }
            }
        }

        private Button __ExitButton;

        private Button _ExitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitButton != null)
                {
                    __ExitButton.Click -= ExitButton_Click;
                }

                __ExitButton = value;
                if (__ExitButton != null)
                {
                    __ExitButton.Click += ExitButton_Click;
                }
            }
        }

        private ListBox _ApplicationsListBox;
        private ToolTip _Tooltip;
    }
}