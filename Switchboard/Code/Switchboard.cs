using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports
{

    /// <summary>
    /// Includes code for Switchboard Form, which serves as a switchboard for this program.
    /// </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2095-10-11, 1.0.2110.x. </para>
    /// </remarks>
    public partial class Switchboard : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public Switchboard() : base()
        {
            this.InitializeComponent();
            // turn off saving to prevent exception reported due to access from two programs
            this.SaveSettingsOnClosing = false;
            this._OpenForms = new Core.Forma.ConsoleFormCollection();
            this.__OpenButton.Name = "_OpenButton";
            this.__ExitButton.Name = "_ExitButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    // this disposes of any form that did not close.
                    if ( this._OpenForms is object )
                        this._OpenForms.ClearDispose();
                    Application.DoEvents();
                    if ( this.components is object )
                        this.components.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Event handler. Called by form for load events. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // set the form caption
                this.Text = ApplicationInfo.BuildApplicationTitleCaption( nameof( Switchboard ) );

                // center the form
                this.CenterToScreen();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by form for shown events. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void Form_Shown( object sender, EventArgs e )
        {
            try
            {
                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // populate the action list
                this.PopulateActiveList();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary> Populates the list of options in the action combo box. </summary>
        /// <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
        private void PopulateActiveList()
        {

            // set the action list
            this._ApplicationsListBox.DataSource = null;
            this._ApplicationsListBox.Items.Clear();
            this._ApplicationsListBox.DataSource = typeof( ActionOption ).ValueDescriptionPairs().ToList();
            this._ApplicationsListBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._ApplicationsListBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
        }

        /// <summary> Gets the selected action. </summary>
        /// <value> The selected action. </value>
        private ActionOption SelectedAction => ( ActionOption ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ApplicationsListBox.SelectedItem).Key );

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Closes the form and exits the application. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void ExitButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary> Enumerates the action options. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private enum ActionOption
        {

            /// <summary> An enum constant representing the serial terminal option. </summary>
            [System.ComponentModel.Description( "COM Terminal" )]
            SerialTerminal,

            /// <summary> An enum constant representing the console form option. </summary>
            [System.ComponentModel.Description( "Serial Console Dialog" )]
            ConsoleForm
        }

        /// <summary> The terminal form. </summary>
        private ComTerm.TerminalPanel _TerminalForm;

        /// <summary> The open forms. </summary>
        private readonly Core.Forma.ConsoleFormCollection _OpenForms;

        /// <summary> Open selected items. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void OpenButton_Click( object sender, EventArgs e )
        {
            this.Cursor = Cursors.WaitCursor;
            switch ( this.SelectedAction )
            {
                case ActionOption.SerialTerminal:
                    {
                        if ( this._TerminalForm is null )
                            this._TerminalForm = new ComTerm.TerminalPanel();
                        this._TerminalForm.Show();
                        break;
                    }

                case ActionOption.ConsoleForm:
                    {
                        this._OpenForms.ShowNew( $"Term #{this._OpenForms.Count + 1}", new Serial.Forms.PortConsole(), My.MyProject.Application.Logger );
                        break;
                    }
            }

            this.Cursor = Cursors.Default;
        }

        #endregion

    }
}
