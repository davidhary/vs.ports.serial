﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Ports.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public Switchboard m_Switchboard;

            public Switchboard Switchboard
            {
                [DebuggerHidden]
                get
                {
                    m_Switchboard = Create__Instance__(m_Switchboard);
                    return m_Switchboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Switchboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Switchboard);
                }
            }
        }
    }
}