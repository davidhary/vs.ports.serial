﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Ports.My.MyApplication.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Ports.My.MyApplication.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Ports.My.MyApplication.AssemblyProduct )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: CLSCompliant( true )]
