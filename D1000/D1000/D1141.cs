using System.Diagnostics;

namespace isr.Ports.D1000
{

    /// <summary> A D1141 Module class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-17 </para>
    /// </remarks>
    public class D1141 : D1000Base
    {

        #region " PRESETTABLE "

        /// <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
        /// <remarks> Use this to customize the reset. </remarks>
        public override void InitKnownState()
        {
            this.InitKnownState( "D1141" );
            this.AnalogInputPayload.Range = new Core.Primitives.RangeR( -10, 10d );
            _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Done initializing {this.ModuleName}.{this.Messenger.ModuleAddress} known state" );
        }

        #endregion

    }
}
