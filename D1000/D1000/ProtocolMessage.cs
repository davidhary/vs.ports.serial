using System;
using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial;
using isr.Ports.Teleport;

namespace isr.Ports.D1000
{

    /// <summary> A protocol message for the D1000 modules. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-19 </para>
    /// </remarks>
    public class ProtocolMessage : ProtocolMessageBase, IProtocolMessage
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public ProtocolMessage() : base( new System.Text.ASCIIEncoding() )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> The message. </param>
        public ProtocolMessage( IProtocolMessage protocolMessage ) : base( protocolMessage )
        {
        }

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public override object Clone()
        {
            return Clone( this );
        }

        /// <summary>
        /// Creates a new <see cref="ProtocolMessageBase">serial protocol message</see> that is a copy of
        /// the specified instance.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A new object that is a copy of this instance. </returns>
        public static IProtocolMessage Clone( IProtocolMessage value )
        {
            return new ProtocolMessage( value );
        }

        /// <summary> Creates a new IProtocolMessage. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An IProtocolMessage. </returns>
        public static IProtocolMessage Create()
        {
            IProtocolMessage result = null;
            try
            {
                result = new ProtocolMessage();
            }
            catch
            {
                if ( result is object )
                    result.Dispose();
                throw;
            }

            return result;
        }

        #endregion

        #region " CONSTANTS AND SHARED "

        /// <summary> The maximum length of the message. </summary>
        public const int MaximumMessageLength = 135;

        /// <summary> The stream termination value. </summary>
        public const byte StreamTerminationValue = 13;

        /// <summary> The command prompt. </summary>
        public const byte CommandPrompt = 35;

        /// <summary> The response prompt. </summary>
        public const byte ResponsePrompt = 42;

        /// <summary> The error prompt. </summary>
        public const byte ErrorPrompt = 63;

        /// <summary> Length of the command prompt. </summary>
        public const int CommandPromptLength = 1;

        /// <summary> Length of the checksum. </summary>
        public const int ChecksumLength = 1;

        /// <summary> Length of the command. </summary>
        public const int CommandLength = 2;

        /// <summary> The minimum length of the message. </summary>
        public const int MinimumMessageLength = CommandPromptLength + CommandLength + ChecksumLength;

        /// <summary> The positive overload. </summary>
        public const double PositiveOverload = 99999.99d;

        /// <summary> The negative overload. </summary>
        public const double NegativeOverload = -99999.99d;

        #endregion

        #region " CONTENTS "

        /// <summary> Gets or sets the command prompt. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The command prompt. </value>
        public override byte Prefix
        {
            get => base.Prefix;

            set {
                if ( this.Prefix != value )
                {
                    base.Prefix = value;
                }

                switch ( value )
                {
                    case CommandPrompt:
                        {
                            this.MessageType = MessageType.Command;
                            break;
                        }

                    case ResponsePrompt:
                        {
                            this.MessageType = MessageType.Response;
                            break;
                        }

                    case ErrorPrompt:
                        {
                            this.MessageType = MessageType.Failure;
                            break;
                        }

                    default:
                        {
                            throw new InvalidOperationException( $"Unhandled prompt {value}.{this.PrefixAscii}" );
                        }
                }
            }
        }

        /// <summary> Gets or sets the message type. </summary>
        /// <value> The message type. </value>
        public override MessageType MessageType
        {
            get => base.MessageType;

            set {
                if ( this.MessageType != value )
                {
                    base.MessageType = value;
                    this.Prefix = FromMessageType( value );
                }
            }
        }

        #endregion

        #region " CONTENT MANAGEMENT "

        /// <summary> Clears the contents of an existing <see cref="ProtocolMessageBase" />. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        #endregion

        #region " PARSERS "

        /// <summary> Initializes this object from the given from message type. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Byte. </returns>
        public static byte FromMessageType( MessageType value )
        {
            switch ( value )
            {
                case MessageType.Command:
                    {
                        return CommandPrompt;
                    }

                case MessageType.Failure:
                    {
                        return ErrorPrompt;
                    }

                case MessageType.Response:
                    {
                        return ResponsePrompt;
                    }

                default:
                    {
                        return 0;
                    }
            }
        }

        /// <summary> Parse message type. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A MessageType. </returns>
        public MessageType ParseMessageType( byte value )
        {
            this.Prefix = value;
            return this.MessageType;
        }

        /// <summary> Parse hexadecimal message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hexMessage"> A message describing the hexadecimal. </param>
        /// <returns> A StatusCode. </returns>
        public override StatusCode ParseHexMessage( string hexMessage )
        {
            var result = StatusCode.ValueNotSet;
            return result;
        }

        /// <summary> Parses the given values. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Serial.MessageParserOutcome. </returns>
        public override MessageParserOutcome Parse( IEnumerable<byte> values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            MessageParserOutcome result;
            // check if value start with initiation characters.
            if ( !values.Any() )
            {
                result = MessageParserOutcome.Incomplete;
            }
            // TO_DO: Needs fixing
            else if ( true ) // Me.IsTerminated(values) Then
            {
                this.Status = this.ParseStream( this.MessageType, values );
                result = this.Status == StatusCode.Okay ? MessageParserOutcome.Complete : MessageParserOutcome.Invalid;
            }
            else
            {
            }

            return result;
        }

        /// <summary>
        /// Tries to parse the <paramref name="data">byte data</paramref> into a valid message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="messageType"> Type of the message. </param>
        /// <param name="data">        The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        public override StatusCode ParseStream( MessageType messageType, IEnumerable<byte> data )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            var result = StatusCode.Okay;
            this.Clear();
            if ( !data.Any() || data.Count() < MinimumMessageLength )
            {
                result = StatusCode.MessageTooShort;
            }
            else
            {
                this.MessageType = messageType;
                this.Stream = data;
                var dataSet = new Queue<byte>( data );
                this.ModuleAddress = new byte[] { dataSet.Dequeue() };
                if ( this.ModuleAddress.ElementAtOrDefault( 0 ) == 0 )
                {
                    result = StatusCode.InvalidModuleAddress;
                }
                else
                {
                    // get the prompt.
                    this.Prefix = dataSet.Dequeue();
                    // check if error prompt
                    if ( this.Prefix == ErrorPrompt )
                    {
                    }
                    // if error, the command is left empty and the rest of the input is the error message
                    else
                    {
                        this.Command = new byte[] { dataSet.Dequeue(), dataSet.Dequeue() };
                        dataSet = new Queue<byte>( dataSet.Reverse() );
                        byte checksumCache = dataSet.Dequeue();
                        if ( data.Count() > 0 )
                        {
                            this.Payload = dataSet.Reverse();
                            this.AsyncNotifyPropertyChanged( nameof( this.Payload ) );
                        }

                        this.Checksum = new byte[] { checksumCache };
                        if ( this.Checksum.ElementAtOrDefault( 0 ) != this.CalculateChecksum() )
                        {
                            result = StatusCode.ChecksumInvalid;
                        }
                    }
                }
            }

            this.Status = result;
            return result;
        }

        /// <summary> Validates the protocol message relative to this protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> The validated protocol message. </param>
        /// <returns> A StatusCode. </returns>
        public override StatusCode Validate( IProtocolMessage protocolMessage )
        {
            return protocolMessage is null
                ? StatusCode.Okay
                : this.ModuleAddress.ElementAtOrDefault( 0 ) != protocolMessage.ModuleAddress.ElementAtOrDefault( 0 )
                    ? StatusCode.IncorrectModuleAddressReceived
                    : (this.CommandAscii ?? "") != (protocolMessage.CommandAscii ?? "") ? StatusCode.IncorrectCommandReceived : StatusCode.Okay;
        }

        #endregion

        #region " BUILDERS "

        /// <summary> The checksum mask. </summary>
        public const int ChecksumMask = 0xFF;

        /// <summary> Calculates the checksum hexadecimal. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> The calculated checksum hexadecimal. </returns>
        public override string CalculateChecksumHex()
        {
            long result = 0L;
            // the prefix counts only for messages received from the controller.
            if ( this.MessageType == MessageType.Response )
                result += this.Prefix;
            result += this.ModuleAddress.Sum();
            if ( this.MessageType == MessageType.Command )
                result += this.Command.Sum();
            result += this.Payload.Sum();
            result &= ChecksumMask;
            return result.ToString( "X2" );
        }

        /// <summary> Calculates the checksum value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The calculated checksum. </returns>
        public new byte CalculateChecksum()
        {
            long result = 0L;
            result += this.Prefix;
            foreach ( byte value in this.Command )
                result += value;
            foreach ( byte value in this.Payload )
                result += value;
            result &= ChecksumMask;
            return result.ToString( "X2" ).ToBytes()[0];
        }

        /// <summary> Enumerates build message in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        public override IEnumerable<byte> BuildStream( MessageType value )
        {
            return this.BuildMessage( FromMessageType( value ) );
        }

        /// <summary> Builds the message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="prompt"> The command, response or error prompt to use for building the message,
        /// which allows emulating a module for sending an error or response
        /// message. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        public IEnumerable<byte> BuildMessage( byte prompt )
        {
            this.Prefix = prompt;
            return this.BuildMessage();
        }

        /// <summary> Builds the message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        private IEnumerable<byte> BuildMessage()
        {
            var values = new List<byte>();
            if ( this.IsIncomplete() )
            {
            }
            else if ( this.Status == StatusCode.InvalidMessageLength )
            {
            }
            else
            {
                values.Add( this.Prefix );
                values.Add( this.ModuleAddress.ElementAtOrDefault( 0 ) );
                values.AddRange( this.Command );
                if ( this.Payload.Any() )
                    values.AddRange( this.Payload );
                this.UpdateChecksum();
                values.Add( this.Checksum.ElementAtOrDefault( 0 ) );
                values.Add( StreamTerminationValue );
            }

            return values;
        }

        /// <summary> Builds the hex message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="prompt"> The command, response or error prompt to use for building the message,
        /// which allows emulating a module for sending an error or response
        /// message. </param>
        /// <returns> A String. </returns>
        public string BuildHexMessage( byte prompt )
        {
            var sb = new System.Text.StringBuilder();
            foreach ( byte b in this.BuildMessage( prompt ) )
                _ = sb.Append( b.ToHex() );
            return sb.ToString();
        }

        /// <summary> Builds hexadecimal message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public string BuildHexMessage( MessageType value )
        {
            return this.BuildHexMessage( FromMessageType( value ) );
        }

        #endregion

    }
}
