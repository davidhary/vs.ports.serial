﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Ports.Love.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Ports.Love.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Ports.Love.My.MyLibrary.AssemblyProduct )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: CLSCompliant( true )]
