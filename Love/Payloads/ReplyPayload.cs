using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial;
using isr.Ports.Teleport;

namespace isr.Ports.Love
{

    /// <summary> A reply payload. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-05 </para>
    /// </remarks>
    public class ReplyPayload : IPayload
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ReplyPayload() : base()
        {
        }

        #endregion

        #region " SIMULATE A REPLY "

        /// <summary> Simulate payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A SetupPayload. </returns>
        public static ReplyPayload SimulatePayload()
        {
            var result = new ReplyPayload() { ReplyStatus = 0 };
            return result;
        }

        #endregion

        #region " FIELDS "

        /// <summary> The success reply. </summary>
        public const int SuccessReply = 0;

        /// <summary> The reply status. </summary>
        private int _ReplyStatus;

        /// <summary> Gets or sets the reply status. </summary>
        /// <value> The reply status. </value>
        public int ReplyStatus
        {
            get => this._ReplyStatus;

            set {
                if ( this.ReplyStatus != value )
                {
                    this._ReplyStatus = value;
                    this.ReplyAscii = value.ToString( "X2" );
                }
            }
        }

        /// <summary> The reply ASCII. </summary>
        private string _ReplyAscii;

        /// <summary> Gets or sets the reply ASCII. </summary>
        /// <value> The reply ASCII. </value>
        public string ReplyAscii
        {
            get => this._ReplyAscii;

            set {
                if ( !string.Equals( this.ReplyAscii, value ) )
                {
                    this._ReplyAscii = value;
                    this.ReplyStatus = int.Parse( value );
                }
            }
        }

        /// <summary> The reading. </summary>
        private string _Reading;

        /// <summary> Gets or sets the reading. </summary>
        /// <value> The reading. </value>
        public string Reading
        {
            get => this._Reading;

            set {
                if ( !string.Equals( value, this.Reading ) )
                {
                    this._Reading = value;
                }
            }
        }

        #endregion

        #region " I PAYLOAD IMPLEMENTATION "

        /// <summary> Gets or sets a list of payloads. </summary>
        /// <value> A list of payloads. </value>
        private List<byte> PayloadList { get; set; }

        /// <summary> Gets or sets the payload. </summary>
        /// <value> The payload. </value>
        public IEnumerable<byte> Payload
        {
            get => this.PayloadList;

            set => this.PayloadList = new List<byte>( value );
        }

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        public StatusCode StatusCode { get; set; }

        /// <summary> Populates the payload from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="payload"> The payload. </param>
        /// <returns> A StatusCode. </returns>
        public StatusCode Parse( IEnumerable<byte> payload )
        {
            this.StatusCode = StatusCode.Okay;
            this.Payload = payload;
            var values = new Queue<byte>( payload );
            System.Text.Encoding encoding = new System.Text.ASCIIEncoding();
            if ( this.StatusCode == StatusCode.Okay )
            {
                if ( values.Any() && values.Count >= 2 )
                {
                    this.ReplyAscii = encoding.GetString( new byte[] { values.Dequeue(), values.Dequeue() } );
                }
                else
                {
                    this.StatusCode = StatusCode.MessageIncomplete;
                }
            }

            this._Reading = $"{this.ReplyAscii}";
            return this.StatusCode;
        }

        /// <summary> Converts the payload to bytes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> This object as an IEnumerable(Of Byte) </returns>
        public IEnumerable<byte> Build()
        {
            this.StatusCode = StatusCode.Okay;
            var result = new List<byte>();
            result.AddRange( this.ReplyAscii.ToBytes() );
            this.Payload = result;
            return this.Payload;
        }

        #endregion

    }
}
