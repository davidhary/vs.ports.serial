using System;
using System.Collections.Generic;
using System.ComponentModel;

using isr.Ports.Teleport;

using Microsoft.VisualBasic.CompilerServices;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Ports.Love
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Defines the Love protocol command. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-16 </para>
    /// </remarks>
    public class LoveCommand : ProtocolCommand
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The command code. </param>
        public LoveCommand( LoveCommandCode commandCode ) : base( commandCode )
        {
        }

        /// <summary>
        /// Gets or sets the time it takes from the receipt of the command to when the module starts to
        /// transmit a response.
        /// </summary>
        /// <value> The command timeout. </value>
        public override TimeSpan TurnaroundTime
        {
            get => SelectTurnaroundTime( ( LoveCommandCode ) Conversions.ToInteger( this.CommandCode ) );

            set => base.TurnaroundTime = value;
        }

        /// <summary> List of times of the turnarounds. </summary>
        private static Dictionary<LoveCommandCode, TimeSpan> _TurnaroundTimes;

        /// <summary> Gets a list of times of the turnarounds. </summary>
        /// <value> A list of times of the turnarounds. </value>
        public static Dictionary<LoveCommandCode, TimeSpan> TurnaroundTimes
        {
            get {
                if ( _TurnaroundTimes is null )
                {
                    _TurnaroundTimes = new Dictionary<LoveCommandCode, TimeSpan>() { { LoveCommandCode.ReadSetpoint, TimeSpan.FromMilliseconds( 10d ) }, { LoveCommandCode.ReadTemperature, TimeSpan.FromMilliseconds( 10d ) }, { LoveCommandCode.WriteSetpoint, TimeSpan.FromMilliseconds( 10d ) }, { LoveCommandCode.WriteSetpointProcess2, TimeSpan.FromMilliseconds( 10d ) } };
                }

                return _TurnaroundTimes;
            }
        }

        /// <summary> The default turnaround time. </summary>
        /// <value> The default turnaround time. </value>
        public static TimeSpan DefaultTurnaroundTime { get; set; } = TimeSpan.FromMilliseconds( 10d );

        /// <summary> Select turnaround time. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The command code. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan SelectTurnaroundTime( LoveCommandCode commandCode )
        {
            return TurnaroundTimes.ContainsKey( commandCode ) ? TurnaroundTimes[commandCode] : DefaultTurnaroundTime;
        }

        /// <summary> The refractory periods. </summary>
        private static Dictionary<LoveCommandCode, TimeSpan> _RefractoryPeriods;

        /// <summary> Gets a list of Refractory periods. </summary>
        /// <value> A list of Refractory periods. </value>
        public static Dictionary<LoveCommandCode, TimeSpan> RefractoryPeriods
        {
            get {
                if ( _RefractoryPeriods is null )
                {
                    _RefractoryPeriods = new Dictionary<LoveCommandCode, TimeSpan>() { { LoveCommandCode.WriteSetpoint, TimeSpan.FromMilliseconds( 750d ) }, { LoveCommandCode.WriteSetpointProcess2, TimeSpan.FromMilliseconds( 750d ) } };
                }

                return _RefractoryPeriods;
            }
        }

        /// <summary> The default refractory period. </summary>
        /// <value> The default refractory period. </value>
        public static TimeSpan DefaultRefractoryPeriod { get; set; } = TimeSpan.Zero;

        /// <summary> Select Refractory Period. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The command code. </param>
        /// <returns> A Timespan. </returns>
        public static TimeSpan SelectRefractoryPeriod( LoveCommandCode commandCode )
        {
            return RefractoryPeriods.ContainsKey( commandCode ) ? RefractoryPeriods[commandCode] : DefaultRefractoryPeriod;
        }
    }

    /// <summary> Dictionary of Love protocol commands. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-02 </para>
    /// </remarks>
    public sealed class LoveCommandCollection : ProtocolCommandCollection
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private LoveCommandCollection() : base( typeof( LoveCommandCode ) )
        {
        }

        #endregion

        #region " SINGLETON "

        /// <summary>
        /// Gets or sets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets or sets the instance. </summary>
        /// <value> The instance. </value>
        private static LoveCommandCollection Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static LoveCommandCollection Get()
        {
            if ( Instance is null )
            {
                lock ( SyncLocker )
                    Instance = new LoveCommandCollection();
            }

            return Instance;
        }

        #endregion

        #region " LOVE COMMAND "

        /// <summary> Love command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> A LoveCommand. </returns>
        public LoveCommand LoveCommand( string commandAscii )
        {
            return new LoveCommand( ( LoveCommandCode ) Conversions.ToInteger( this.Command( commandAscii ).CommandCode ) );
        }

        #endregion

    }

    /// <summary> Defines the Love protocol response. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-16 </para>
    /// </remarks>
    public class LoveResponse : ProtocolCommand
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The response code. </param>
        public LoveResponse( LoveResponseCode commandCode ) : base( commandCode )
        {
        }

        /// <summary>
        /// Gets or sets the time it takes from the receipt of the command to when the module starts to
        /// transmit a response.
        /// </summary>
        /// <value> The command timeout. </value>
        public override TimeSpan TurnaroundTime
        {
            get => SelectTurnaroundTime( ( LoveResponseCode ) Conversions.ToInteger( this.CommandCode ) );

            set => base.TurnaroundTime = value;
        }

        /// <summary> List of times of the turnarounds. </summary>
        private static Dictionary<LoveResponseCode, TimeSpan> _TurnaroundTimes;

        /// <summary> Gets a list of times of the turnarounds. </summary>
        /// <value> A list of times of the turnarounds. </value>
        public static Dictionary<LoveResponseCode, TimeSpan> TurnaroundTimes
        {
            get {
                if ( _TurnaroundTimes is null )
                {
                    _TurnaroundTimes = new Dictionary<LoveResponseCode, TimeSpan>() { { LoveResponseCode.Success, TimeSpan.FromMilliseconds( 10d ) } };
                }

                return _TurnaroundTimes;
            }
        }

        /// <summary> Gets or sets the default turnaround time. </summary>
        /// <value> The default turnaround time. </value>
        public static TimeSpan DefaultTurnaroundTime { get; set; } = TimeSpan.FromMilliseconds( 10d );

        /// <summary> Select turnaround time. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The response code. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan SelectTurnaroundTime( LoveResponseCode commandCode )
        {
            return TurnaroundTimes.ContainsKey( commandCode ) ? TurnaroundTimes[commandCode] : DefaultTurnaroundTime;
        }
    }

    /// <summary> Dictionary of Love protocol responses. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-02 </para>
    /// </remarks>
    public sealed class LoveResponseCollection : ProtocolCommandCollection
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private LoveResponseCollection() : base( typeof( LoveResponseCode ) )
        {
        }

        #endregion

        #region " SINGLETON "

        /// <summary>
        /// Gets or sets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets or sets the instance. </summary>
        /// <value> The instance. </value>
        private static LoveResponseCollection Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static LoveResponseCollection Get()
        {
            if ( Instance is null )
            {
                lock ( SyncLocker )
                    Instance = new LoveResponseCollection();
            }

            return Instance;
        }

        #endregion

        #region " LOVE COMMAND "

        /// <summary> Love Response. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="responseAscii"> The response ASCII. </param>
        /// <returns> A LoveCommand. </returns>
        public LoveResponse LoveResponse( string responseAscii )
        {
            return new LoveResponse( ( LoveResponseCode ) Conversions.ToInteger( this.Command( responseAscii ).CommandCode ) );
        }

        #endregion

    }

    /// <summary> Values that represent the Love protocol commands. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public enum LoveCommandCode
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "(None)" )]
        None,

        /// <summary> An enum constant representing the read temperature option. </summary>
        [Description( "Read Temperature (00)" )]
        ReadTemperature,

        /// <summary> An enum constant representing the read setpoint option. </summary>
        [Description( "Read Setpoint (0100)" )]
        ReadSetpoint,

        /// <summary> An enum constant representing the write setpoint option. </summary>
        [Description( "Write Setpoint (0200)" )]
        WriteSetpoint,

        /// <summary> An enum constant representing the write setpoint process 2 option. </summary>
        [Description( "Write Setpoint Process 2 (0201)" )]
        WriteSetpointProcess2
    }

    /// <summary> Values that represent the Love protocol responses. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public enum LoveResponseCode
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "(None)" )]
        None,

        /// <summary> An enum constant representing the success option. </summary>
        [Description( "Success (00)" )]
        Success
    }
}
