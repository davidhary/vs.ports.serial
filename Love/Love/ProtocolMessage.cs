using System;
using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial;
using isr.Ports.Teleport;

namespace isr.Ports.Love
{

    /// <summary> A protocol message for the Love modules. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-02 </para>
    /// </remarks>
    public class ProtocolMessage : ProtocolMessageBase, IProtocolMessage
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ProtocolMessage() : base( new System.Text.ASCIIEncoding() )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> The message. </param>
        public ProtocolMessage( IProtocolMessage protocolMessage ) : base( protocolMessage )
        {
        }

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public override object Clone()
        {
            return Clone( this );
        }

        /// <summary>
        /// Creates a new <see cref="ProtocolMessageBase">serial protocol message</see> that is a copy of
        /// the specified instance.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A new object that is a copy of this instance. </returns>
        public static IProtocolMessage Clone( IProtocolMessage value )
        {
            return new ProtocolMessage( value );
        }

        /// <summary> Creates a new IProtocolMessage. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An IProtocolMessage. </returns>
        public static IProtocolMessage Create()
        {
            IProtocolMessage result = null;
            try
            {
                result = new ProtocolMessage();
            }
            catch
            {
                if ( result is object )
                {
                    result.Dispose();
                }

                throw;
            }

            return result;
        }

        #endregion

        #region " CONSTANTS AND SHARED "

        /// <summary> The stream start value. </summary>
        public const byte StreamStartValue = 2;

        /// <summary> The command termination value. </summary>
        public const byte CommandTerminationValue = 3;

        /// <summary> The response termination value. </summary>
        public const byte ResponseTerminationValue = 6;

        /// <summary> The love prompt. </summary>
        public const byte LovePrompt = 76;

        /// <summary> Length of the prompt. </summary>
        public const int PromptLength = 1;

        /// <summary> Length of the address. </summary>
        public const int AddressLength = 2;

        /// <summary> Length of the checksum. </summary>
        public const int ChecksumLength = 2;

        /// <summary> Length of the command. </summary>
        public const int CommandLength = 2;

        /// <summary> The minimum length of the message. </summary>
        public const int MinimumMessageLength = PromptLength + AddressLength + CommandLength + ChecksumLength;

        #endregion

        #region " CONTENTS "

        /// <summary> Gets or sets the message type. </summary>
        /// <remarks> Also sets the stream start and end values. </remarks>
        /// <value> The message type. </value>
        public override MessageType MessageType
        {
            get => base.MessageType;

            set {
                if ( this.MessageType != value )
                {
                    this.Prefix = FromMessageType( value );
                    base.MessageType = value;
                    this.StreamInitiation = new byte[] { StreamStartValue };
                    this.StreamTermination = value == MessageType.Command ? (new byte[] { CommandTerminationValue }) : (new byte[] { ResponseTerminationValue });
                }
            }
        }

        #endregion

        #region " I MESSAGE PARSER "

        /// <summary> Query if 'values' is terminated. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> <c>true</c> if terminated; otherwise <c>false</c> </returns>
        private bool IsInitiated( IEnumerable<byte> values )
        {
            bool result = false;
            if ( values.Count() > this.StreamInitiation.Count() )
            {
                int i = -1;
                result = true;
                foreach ( byte b in this.StreamInitiation )
                {
                    i += 1;
                    result = result && b == values.ElementAtOrDefault( i );
                }
            }

            return result;
        }

        /// <summary> Query if 'values' is terminated. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> <c>true</c> if terminated; otherwise <c>false</c> </returns>
        private bool IsTerminated( IEnumerable<byte> values )
        {
            bool result = false;
            if ( values.Count() > this.StreamTermination.Count() )
            {
                int i = values.Count();
                result = true;
                foreach ( byte b in this.StreamTermination.Reverse() )
                {
                    i -= 1;
                    result = result && b == values.ElementAtOrDefault( i );
                }
            }

            return result;
        }

        /// <summary> Parses the given values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A MessageParserOutcome. </returns>
        public override MessageParserOutcome Parse( IEnumerable<byte> values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            var result = MessageParserOutcome.None;
            // check if value start with initiation characters.
            if ( !values.Any() )
            {
                result = MessageParserOutcome.Incomplete;
            }
            else if ( this.IsTerminated( values ) )
            {
                this.Status = this.ParseStream( this.MessageType, values );
                result = this.Status == StatusCode.Okay ? MessageParserOutcome.Complete : MessageParserOutcome.Invalid;
            }
            else
            {
            }

            return result;
        }

        #endregion

        #region " PARSERS "

        /// <summary> Initializes this object from the given from message type. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Byte. </returns>
        public static byte FromMessageType( MessageType value )
        {
            switch ( value )
            {
                case MessageType.Command:
                    {
                        return LovePrompt;
                    }

                case MessageType.Failure:
                    {
                        return LovePrompt;
                    }

                case MessageType.Response:
                    {
                        return LovePrompt;
                    }

                default:
                    {
                        return LovePrompt;
                    }
            }
        }

        /// <summary> Enumerates trim start in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data">  The byte data. </param>
        /// <param name="count"> Number of. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process trim start in this collection.
        /// </returns>
        private static IEnumerable<byte> TrimStart( IEnumerable<byte> data, int count )
        {
            var dataSet = new Queue<byte>( data );
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
                _ = dataSet.Dequeue();
            return dataSet;
        }

        /// <summary> Enumerates trim end in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data">  The byte data. </param>
        /// <param name="count"> Number of. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process trim end in this collection.
        /// </returns>
        private static IEnumerable<byte> TrimEnd( IEnumerable<byte> data, int count )
        {
            var dataSet = new Queue<byte>( data.Reverse() );
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
                _ = dataSet.Dequeue();
            return dataSet.Reverse();
        }

        /// <summary> Parse internal stream. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="data"> The byte data. </param>
        /// <returns> A StatusCode. </returns>
        private StatusCode ParseInternalStream( IEnumerable<byte> data )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            var result = StatusCode.Okay;
            var dataSet = new Queue<byte>( data );
            // get the prefix
            this.Prefix = dataSet.Dequeue();
            if ( dataSet.Count > 1 )
            {
                this.ModuleAddress = new byte[] { dataSet.Dequeue(), dataSet.Dequeue() };
                if ( this.ModuleAddressAscii == "00" )
                {
                    result = StatusCode.InvalidModuleAddress;
                }
                else if ( dataSet.Count > 2 )
                {
                    dataSet = new Queue<byte>( dataSet.Reverse() );
                    var checksumCache = new byte[] { dataSet.Dequeue(), dataSet.Dequeue() };
                    if ( data.Count() > 0 )
                    {
                        this.Payload = dataSet.Reverse();
                        this.NotifyPropertyChanged( nameof( this.Payload ) );
                    }

                    this.Checksum = checksumCache.Reverse().ToArray();
                    if ( !Methods.NullableSequenceEquals( this.Checksum, this.CalculateChecksum() ) )
                    {
                        result = StatusCode.ChecksumInvalid;
                    }
                }
                else
                {
                    result = StatusCode.MessageTooShort;
                }
            }
            else
            {
                result = StatusCode.InvalidMessageLength;
            }

            return result;
        }

        /// <summary> Parse hexadecimal message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hexMessage"> A message describing the hexadecimal. </param>
        /// <returns> A StatusCode. </returns>
        public override StatusCode ParseHexMessage( string hexMessage )
        {
            var result = StatusCode.ValueNotSet;
            return result;
        }

        /// <summary>
        /// Tries to parse the <paramref name="data">byte data</paramref> into a valid message.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The Protocol <see cref="isr.Ports.Love.LoveCommand"/> of the collector (listener) must be set by the
        /// emitter</para>
        /// <para>
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="messageType"> The message type. </param>
        /// <param name="data">        The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        public override StatusCode ParseStream( MessageType messageType, IEnumerable<byte> data )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            this.Clear();
            StatusCode result;
            if ( !data.Any() || data.Count() < MinimumMessageLength )
            {
                result = StatusCode.MessageTooShort;
            }
            else if ( !this.IsInitiated( data ) )
            {
                result = StatusCode.InvalidStreamStart;
            }
            else if ( !this.IsTerminated( data ) )
            {
                result = StatusCode.InvalidStreamEnd;
            }
            else
            {
                this.MessageType = messageType;
                this.Stream = data;
                data = TrimStart( data, this.StreamInitiation.Count() );
                data = TrimEnd( data, this.StreamInitiation.Count() );
                this.InternalStream = data;
                result = this.ParseInternalStream( data );
            }

            this.Status = result;
            return result;
        }

        /// <summary> Validates the protocol message relative to this protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> The validated protocol message. </param>
        /// <returns> A StatusCode. </returns>
        public override StatusCode Validate( IProtocolMessage protocolMessage )
        {
            return protocolMessage is null
                ? StatusCode.Okay
                : !Methods.NullableSequenceEquals( this.ModuleAddress, protocolMessage.ModuleAddress )
                    ? StatusCode.IncorrectModuleAddressReceived
                    : (this.CommandAscii ?? "") != (protocolMessage.CommandAscii ?? "") ? StatusCode.IncorrectCommandReceived : StatusCode.Okay;
        }

        #endregion

        #region " BUILDERS "

        /// <summary> The checksum mask. </summary>
        public const int ChecksumMask = 0xFF;

        /// <summary> Calculates the checksum value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The calculated checksum hexadecimal. </returns>
        public override string CalculateChecksumHex()
        {
            long result = 0L;
            // the prefix counts only for messages received from the controller.
            if ( this.MessageType == MessageType.Response )
                result += this.Prefix;
            result += this.ModuleAddress.Sum();
            if ( this.MessageType == MessageType.Command )
                result += this.Command.Sum();
            result += this.Payload.Sum();
            result &= ChecksumMask;
            return result.ToString( "X2" );
        }

        /// <summary> Enumerates build message in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messageType"> The message type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        public override IEnumerable<byte> BuildStream( MessageType messageType )
        {
            this.MessageType = messageType;
            return this.BuildStream();
        }

        /// <summary> Enumerates build internal stream in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process build internal stream in this
        /// collection.
        /// </returns>
        private IEnumerable<byte> BuildInternalStream()
        {
            this.InternalStreamValues.Clear();
            if ( this.IsIncomplete() )
            {
            }
            else if ( this.Status == StatusCode.InvalidMessageLength )
            {
            }
            else
            {
                this.InternalStreamValues.Add( this.Prefix );
                this.InternalStreamValues.AddRange( this.ModuleAddress );
                this.InternalStreamValues.AddRange( this.Command );
                if ( this.Payload.Any() )
                    this.InternalStreamValues.AddRange( this.Payload );
                this.UpdateChecksum();
                this.InternalStreamValues.AddRange( this.Checksum );
            }

            return this.InternalStreamValues;
        }

        /// <summary> Builds the stream. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        private IEnumerable<byte> BuildStream()
        {
            var values = new List<byte>();
            if ( this.IsIncomplete() )
            {
            }
            else if ( this.Status == StatusCode.InvalidMessageLength )
            {
            }
            else
            {
                values.AddRange( this.StreamInitiation );
                values.AddRange( this.BuildInternalStream() );
                values.AddRange( this.StreamTermination );
            }

            return values;
        }

        #endregion

    }
}
