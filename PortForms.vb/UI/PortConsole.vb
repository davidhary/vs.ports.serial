Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.IO.Ports
Imports System.Text
Imports System.Windows.Forms
Imports isr.Core
Imports isr.Ports.Serial.Forms.ExceptionExtensions
Imports isr.Core.EscapeSequencesExtensions

''' <summary> Hosts the <see cref="IPort">port</see>. </summary>
''' <remarks>
''' Based on Extended Serial Port Windows Forms Sample
''' http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37 <para>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class PortConsole
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PortConsole" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(New Port)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PortConsole" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
    Public Sub New(ByVal port As IPort)
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me._ErrorProvider = New ErrorProvider
        Me.InitializingComponents = False
        Me.BindPortThis(port)
        Me._ToolStrip.Renderer = New CustomProfessionalRenderer
        Me.ToggleTransmissionRepeatControlEnabled(False)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose()
                If Me._Port IsNot Nothing Then
                    If Me.IsPortOwner Then Me._Port.Dispose()
                    Me._Port = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SHOW IN FORM "

    ''' <summary> Shows the control in a modeless form. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="formCaption">    The form caption. </param>
    ''' <param name="controlCaption"> The control caption. </param>
    ''' <param name="listener">       The listener. </param>
    Public Shared Sub ShowModeless(ByVal formCaption As String, ByVal controlCaption As String, ByVal listener As IMessageListener)
        Dim control As PortConsole = Nothing
        Dim f As Core.Forma.ConsoleForm = Nothing
        Try
            f = New Core.Forma.ConsoleForm
            control = New PortConsole
            f.Text = formCaption
            f.AddTalkerControl(controlCaption, control, True)
            f.AddListener(listener)
            f.Show()
        Catch
            If control IsNot Nothing Then control.Dispose() : 
            If f IsNot Nothing Then f.Dispose() : 
            Throw
        End Try
    End Sub

    ''' <summary> Shows the control in a modal form. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="formCaption">    The form caption. </param>
    ''' <param name="controlCaption"> The control caption. </param>
    ''' <param name="listener">       The listener. </param>
    Public Shared Sub ShowModal(ByVal formCaption As String, ByVal controlCaption As String, ByVal listener As IMessageListener)
        Using f As New Core.Forma.ConsoleForm
            Using control As New PortConsole
                f.Text = formCaption
                f.AddTalkerControl(controlCaption, control, True)
                f.AddListener(listener)
                f.ShowDialog()
            End Using
        End Using
    End Sub

    ''' <summary> Shows the simple read write console. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Shared Sub ShowSimpleReadWriteDialog(ByVal listener As IMessageListener)
        PortConsole.ShowModal("Serial Port Dialog", "Serial", listener)
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the Load event of the form control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.OnFormShown()
        Catch ex As Exception
            isr.Core.WindowsForms.ShowDialog(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the ResizeEnd event of the form control. Adjusts the character counts.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Form_ResizeEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            activity = "displaying the receive text box ruler"
            Me.ReceiveTextBoxCharactersPerLine = Me._ReceiveRulerLabel.DisplayRuler(Me._ReceiveTextBox.Width, Me.ReceiveTextBoxCharacterWidth, Me.ReceiveDisplayOption)
            activity = "displaying the transmit text box ruler"
            Me.TransmitTextBoxCharactersPerLine = Me._TransmitRulerLabel.DisplayRuler(Me._TransmitTextBox.Width, Me.TransmitTextBoxCharacterWidth, Me.TransmitDisplayOption)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Clears the selections. A workaround to prevent highlights. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ClearSelections()
        Me._PortNamesCombo.ComboBox.Select(0, 0)
        Me._BaudRateCombo.ComboBox.Select(0, 0)
        Me._DataBitsCombo.ComboBox.Select(0, 0)
        Me._ParityCombo.ComboBox.Select(0, 0)
        Me._StopBitsCombo.ComboBox.Select(0, 0)
        Me._ReceiveDelayCombo.ComboBox.Select(0, 0)
        Me._ReceiveThresholdCombo.ComboBox.Select(0, 0)
        Me._TransmitDisplayOptionComboBox.ComboBox.Select(0, 0)
        Me._TransmitEnterOptionComboBox.ComboBox.Select(0, 0)
        Me._ReceiveDisplayOptionComboBox.ComboBox.Select(0, 0)
    End Sub

    ''' <summary> Messenger console visible changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MessengerConsole_VisibleChanged(sender As Object, e As System.EventArgs) Handles Me.VisibleChanged
        Me.ClearSelections()
    End Sub

    ''' <summary> Handles the display of the controls. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub OnFormShown()

        ' set the form caption
        Me.Text = Core.ApplicationInfo.BuildApplicationDescriptionCaption("")

        Me.Text &= String.Format(Globalization.CultureInfo.CurrentCulture,
                                 " Version {0}.{1:00}", My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        Me._StatusLabel.Text = "closed"
        Me._TransmitTerminationComboBox.Text = Serial.My.MySettings.Default.Termination
        Me.TransmitEnterOption = CType(Serial.My.MySettings.Default.EnterOption, DisplayOptions)
        Me.TransmitDisplayOption = CType(Serial.My.MySettings.Default.TransmitDisplayOption, DisplayOptions)
        Me.ClearTransmitBox()
        Me.ReceiveDisplayOption = CType(Serial.My.MySettings.Default.ReceiveDisplayOption, DisplayOptions)
        Me.ClearReceiveBox()
        Me.SelectFontSize(Serial.My.MySettings.Default.FontSize)

        'list the port names.
        Me.ListPortNames()

        ' clear the selections
        Me.ClearSelections()

        Me.OnConnectionChanged(Me._Port.IsOpen)

    End Sub

    #End Region

    #Region " TRANSMIT MANAGEMENT "

    ''' <summary> The delimiter. </summary>
    Private Const _Delimiter As String = " "

    ''' <summary>
    ''' Handles the Click event of the _TransmitCopyMenuItem control. Copies a transmit box selection.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub TransmitCopyMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitCopyMenuItem.Click
        Me._TransmitTextBox.Copy()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _TransmitPasteMenuItem control. Pastes a transmit box
    ''' selection.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub TransmitPasteMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitPasteMenuItem.Click

        Me._TransmitTextBox.Paste()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _TransmitCutMenuItem control. Cuts the transmit text box item.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub TransmitCutMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitCutMenuItem.Click
        Me._TransmitTextBox.Cut()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _TransmitSendMenuItem control. Sends position of caret line
    ''' from transmit text box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TransmitSendMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitSendMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = "setting transmission data from text line"
            Dim loc As Integer = Me._TransmitTextBox.GetFirstCharIndexOfCurrentLine
            Dim ln As Integer = Me._TransmitTextBox.GetLineFromCharIndex(loc)
            If 0 <= ln AndAlso ln < Me._TransmitTextBox.Lines.Count Then
                activity = "transmitting text line"
                Me.SendData(Me._TransmitTextBox.Lines(ln), Me.EnterTransmitHexData)

                activity = "all text sent"
                Me._TransmitMessageCombo.Text = String.Empty
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Sends only selection in transmit box to com. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TransmitSendSelectionMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitSendSelectionMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = "setting transmission data from selected text"
            If Me._TransmitTextBox.SelectionLength > 0 Then
                activity = "sending selected text"
                Me.SendData(Me._TransmitTextBox.SelectedText, Me.EnterTransmitHexData)
                activity = "select text sent"

                Me._TransmitMessageCombo.Text = String.Empty
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> fetches a line from the transmit box to the transmit message box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TransmitTextBox_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _TransmitTextBox.MouseClick
        Dim activity As String = String.Empty
        Try
            activity = "fetching transmission data from text box"
            Dim rb As RichTextBox = TryCast(sender, RichTextBox)
            If rb IsNot Nothing Then
                Dim loc As Integer = rb.GetFirstCharIndexOfCurrentLine

                Dim ln As Integer = rb.GetLineFromCharIndex(loc)
                If 0 <= ln AndAlso ln < rb.Lines.Count Then
                    activity = "updating transmission combo box"
                    Me._TransmitMessageCombo.Text = rb.Lines(ln)
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Clears the transmit box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearTransmitBox()
        Dim activity As String = String.Empty
        Try
            activity = "clearing the Transmit text box"
            Me._TransmitTextBox.Clear()
            activity = "displaying the Transmit text box ruler"
            Me.TransmitTextBoxCharactersPerLine = Me._TransmitRulerLabel.DisplayRuler(Me._TransmitTextBox.Width, Me.TransmitTextBoxCharacterWidth, Me.TransmitDisplayOption)
            Me.TransmitCount = 0
            activity = "setting Transmit status image"
            Me._TransmitStatusLabel.Image = If(Me.IsPortOpen, My.Resources.LedCornerOrange, My.Resources.LedCornerGray)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _ClearTransmitBoxButton control. Clears the transmit box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearTransmitBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearTransmitBoxButton.Click
        Dim activity As String = String.Empty
        Try
            activity = "clearing transmission text box"
            Me.ClearTransmitBox()
        Catch ex As Exception

            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Build hex string in the transmit box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TransmitMessageCombo_TextUpdate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitMessageCombo.TextUpdate
        If Me.HasNewHexCharacter AndAlso Me.EnterTransmitHexData Then
            Dim cb As ToolStripComboBox = TryCast(sender, ToolStripComboBox)
            If cb IsNot Nothing Then
                cb.Text = cb.Text.ToByteFormatted(_Delimiter)

                cb.SelectionStart = cb.Text.Length
            End If
        End If

    End Sub

    ''' <summary> Gets or sets the transmit enter option. </summary>
    ''' <value> The transmit enter option. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property TransmitEnterOption As DisplayOptions
        Get
            Return CType(Me._TransmitEnterOptionComboBox.SelectedIndex + 1, DisplayOptions)
        End Get
        Set(value As DisplayOptions)
            If value <> Me.TransmitEnterOption Then
                Me._TransmitEnterOptionComboBox.SelectedIndex = value - 1
            End If
        End Set
    End Property

    ''' <summary> Gets information describing the enter transmit hexadecimal. </summary>
    ''' <value> Information describing the enter transmit hexadecimal. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property EnterTransmitHexData As Boolean
        Get
            Return DisplayOptions.Hex = (Me.TransmitEnterOption And DisplayOptions.Hex)
        End Get
    End Property

    ''' <summary> Gets or sets the has new hexadecimal character. </summary>
    ''' <value> The has new hexadecimal character. </value>
    Private Property HasNewHexCharacter As Boolean

    ''' <summary>
    ''' Event handler to suppress ding sound on enter. Called by _LotNumberTextBox for key down
    ''' events.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SuppressDingHandler(sender As Object, e As KeyEventArgs) Handles _TransmitMessageCombo.KeyDown, _TransmitMessageCombo.KeyUp
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            activity = "handling transmit message combo down"
            If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Return Then
                ' this suppresses the bell.
                e.Handled = True
                ' do not suppress the key press, just the key bell.
                e.SuppressKeyPress = False
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the KeyPress event of the cboEnterMessage control. Enter only allowed keys in hex
    ''' mode.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="KeyPressEventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TransmitMessageCombo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _TransmitMessageCombo.KeyPress
        Dim activity As String = String.Empty
        Try
            activity = "building data"

            Me.HasNewHexCharacter = False
            If e.KeyChar <> vbCr Then
                If Me.EnterTransmitHexData Then
                    Me.HasNewHexCharacter = e.KeyChar.IsHexadecimal
                    If Char.IsControl(e.KeyChar) Then
                    ElseIf Char.IsLetterOrDigit(e.KeyChar) AndAlso Not e.KeyChar.IsHexadecimal Then
                        e.Handled = True
                    ElseIf Char.IsPunctuation(e.KeyChar) Then
                        e.Handled = True
                    End If
                End If
            Else
                activity = "sending data"
                Me.SendData(Me._TransmitMessageCombo.Text, Me.EnterTransmitHexData)
                Me._TransmitMessageCombo.Text = String.Empty
                e.Handled = True
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _TransmitShowHEXToolStripMenuItem control. Toggles
    ''' showing the transmit data in hex.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TransmitDisplayOptionComboBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitDisplayOptionComboBox.SelectedIndexChanged
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty

        Try
            activity = "displaying the transmit text box ruler"
            Me.TransmitTextBoxCharactersPerLine = Me._TransmitRulerLabel.DisplayRuler(Me._TransmitTextBox.Width, Me.TransmitTextBoxCharacterWidth, Me.TransmitDisplayOption)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Gets or sets the transmit display option. </summary>
    ''' <value> The transmit display option. </value>
    Public Property TransmitDisplayOption As DisplayOptions
        Get
            Return CType(Me._TransmitDisplayOptionComboBox.SelectedIndex + 1, DisplayOptions)
        End Get
        Set(value As DisplayOptions)
            Me._TransmitDisplayOptionComboBox.SelectedIndex = value - 1
        End Set
    End Property

    ''' <summary> Gets information describing the display transmit hexadecimal. </summary>
    ''' <value> Information describing the display transmit hexadecimal. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property DisplayTransmitHexData As Boolean
        Get
            Return DisplayOptions.Hex = (Me.TransmitDisplayOption And DisplayOptions.Hex)
        End Get
    End Property

    ''' <summary> Number of transmits. </summary>
    Private _TransmitCount As Integer

    ''' <summary> Gets or sets the transmit count. </summary>
    ''' <value> The transmit count. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property TransmitCount As Integer
        Get
            Return Me._TransmitCount
        End Get
        Set(value As Integer)
            Me._TransmitCount = value
            Me._TransmitCountLabel.Text = $"{Me.TransmitCount:D6}"
        End Set
    End Property

    ''' <summary> Sends the data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="textData">  The text data. </param>
    ''' <param name="isHexData"> True if is hexadecimal data, false if not. </param>
    Public Sub SendData(ByVal textData As String, ByVal isHexData As Boolean)
        If String.IsNullOrWhiteSpace(textData) Then
        ElseIf Not Me.IsPortOpen Then
            Throw New InvalidOperationException("Port not connected")
        Else
            Dim data As New List(Of Byte)
            If isHexData Then
                data.AddRange(textData.RemoveDelimiter(_Delimiter).ToHexBytes)
                If data(0) = Serial.Methods.ParseErrorValue Then Me.PublishWarning($"failed converting '{textData}'; '{Chr(data(1))}' is not a valid HEX character")
            Else
                data.AddRange(Encoding.ASCII.GetBytes(textData))
            End If
            data.AddRange(Me._TransmitTerminationComboBox.Text.CommonEscapeValues)

            Me._Port.SendData(data)
        End If
        ' Store the data in the transmit combo box.
        Me._TransmitMessageCombo.Items.Add(textData)
    End Sub

    #End Region

    #Region " RECEIVE HANDLERS "

    ''' <summary> Number of receives. </summary>
    Private _ReceiveCount As Integer

    ''' <summary> Gets or sets the Receive count. </summary>
    ''' <value> The Receive count. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ReceiveCount As Integer
        Get
            Return Me._ReceiveCount
        End Get
        Set(value As Integer)
            Me._ReceiveCount = value
            Me._ReceiveCountLabel.Text = $"{Me.ReceiveCount:D6}"
        End Set
    End Property

    ''' <summary> Clears the transmit box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearReceiveBox()
        Dim activity As String = String.Empty
        Try
            activity = "clearing the receive text box"
            Me._ReceiveTextBox.Clear()
            activity = "displaying the receive text box ruler"
            Me.ReceiveTextBoxCharactersPerLine = Me._ReceiveRulerLabel.DisplayRuler(Me._ReceiveTextBox.Width, Me.ReceiveTextBoxCharacterWidth, Me.ReceiveDisplayOption)
            Me.ReceiveCount = 0
            activity = "setting receive status image"
            Me._ReceiveStatusLabel.Image = If(Me.IsPortOpen, My.Resources.LedCornerOrange, My.Resources.LedCornerGray)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _ClearReceiveBoxButton control. clear receive box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ClearReceiveBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearReceiveBoxButton.Click
        Me.ClearReceiveBox()

    End Sub

    ''' <summary> Gets or sets the receive display option. </summary>
    ''' <value> The receive display option. </value>
    Public Property ReceiveDisplayOption As DisplayOptions
        Get
            Return CType(Me._ReceiveDisplayOptionComboBox.SelectedIndex + 1, DisplayOptions)
        End Get
        Set(value As DisplayOptions)
            Me._ReceiveDisplayOptionComboBox.SelectedIndex = value - 1
        End Set
    End Property

    ''' <summary> Gets information describing the display receive hexadecimal. </summary>
    ''' <value> Information describing the display receive hexadecimal. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property DisplayReceiveHexData As Boolean
        Get
            Return DisplayOptions.Hex = (Me.ReceiveDisplayOption And DisplayOptions.Hex)
        End Get
    End Property

    ''' <summary> Receive display option combo box index changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReceiveDisplayOptionComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReceiveDisplayOptionComboBox.SelectedIndexChanged

        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            activity = "displaying the receive text box ruler"
            Me.ReceiveTextBoxCharactersPerLine = Me._ReceiveRulerLabel.DisplayRuler(Me._ReceiveTextBox.Width, Me.ReceiveTextBoxCharacterWidth, Me.ReceiveDisplayOption)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _SaveReceiveTextBoxButton control. Save receive box to file.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveReceiveTextBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveReceiveTextBoxButton.Click
        Dim activity As String = String.Empty
        Try
            activity = "instantiating the save received data dialog"
            Using saveFileDialog As New System.Windows.Forms.SaveFileDialog()
                saveFileDialog.DefaultExt = "*.TXT"
                saveFileDialog.Filter = "Text files (*.txt)|*.txt"
                activity = "showing the save received data dialog"
                If saveFileDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fullpath As String = saveFileDialog.FileName()
                    activity = $"saving received data to {fullpath}"
                    Me._ReceiveTextBox.SaveFile(fullpath, RichTextBoxStreamType.PlainText)
                    Me._StatusLabel.Text = IO.Path.GetFileName(fullpath) & " written"
                Else
                    Me._StatusLabel.Text = "no data chosen"
                End If
            End Using
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _LoadTransmitFileButton control. load file into transmit box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LoadTransmitFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadTransmitFileButton.Click
        Dim activity As String = String.Empty
        Try
            activity = "instantiating open dialog for transmit data"
            Using openFileDialog As New System.Windows.Forms.OpenFileDialog()
                openFileDialog.DefaultExt = "*.TXT"
                openFileDialog.Filter = "Text files (*.txt)|*.txt"
                activity = "opening transmit data file dialog"
                If openFileDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fullpath As String = openFileDialog.FileName()
                    Me._TransmitTextBox.Clear()
                    activity = $"loading transmit data from {fullpath}"
                    Me._TransmitTextBox.LoadFile(fullpath, RichTextBoxStreamType.PlainText)
                Else
                    Me._StatusLabel.Text = "no data chosen"
                End If
            End Using
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    #End Region

    #Region " CONNECTION MANAGEMENT "

    ''' <summary> Handles the Click event of the _ConnectButton control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConnectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConnectButton.Click
        Dim activity As String = String.Empty
        Try
            activity = "handing connect button click"
            Me.OnConnecting(Me.IsPortOpen)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Applies the port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A PortParametersDictionary. </returns>
    Private Function BuildPortParamters() As PortParametersDictionary
        Dim result As PortParametersDictionary = Me.Port.ToPortParameters
        result.PortName = Me._PortNamesCombo.Text
        result.BaudRate = CInt(Me._BaudRateCombo.Text)
        result.DataBits = CInt(Me._DataBitsCombo.Text)
        result.Parity = PortParametersDictionary.ParseParity(Me._ParityCombo.Text)
        result.StopBits = PortParametersDictionary.ParseStopBits(Me._StopBitsCombo.Text)
        result.ReceiveDelay = CInt(Me._ReceiveDelayCombo.Text)

        result.ReceivedBytesThreshold = CInt(Me._ReceiveThresholdCombo.Text)
        Return result
    End Function

    ''' <summary> Process connecting. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnConnecting(ByVal isConnected As Boolean)
        Dim activity As String = String.Empty
        Me._ErrorProvider.Clear()
        Dim e As New Core.ActionEventArgs
        Try
            If isConnected Then
                If Me.Port IsNot Nothing Then
                    activity = $"disconnecting {Me.Port.SerialPort?.PortName}"
                    Me.Port.TryClose(e)
                End If
            Else
                activity = $"connecting {Me._PortNamesCombo.Text}"
                If Me.Port Is Nothing Then
                    Me.Publish(TraceEventType.Warning, $"Failed {activity}; Port class is nothing")
                Else
                    activity = $"building port parameters for {Me._PortNamesCombo.Text}"
                    activity = $"connecting {Me._PortNamesCombo.Text}"
                    Me.Port.TryOpen(Me.BuildPortParamters, e)
                End If
            End If
            If e.Failed Then Me.Publish(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the change of connection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
    Private Sub OnConnectionChanged(ByVal isConnected As Boolean)
        Me._ErrorProvider.Clear()
        If isConnected Then
            Me._ParityCombo.Enabled = False
            Me._StopBitsCombo.Enabled = False
            Me._PortNamesCombo.Enabled = False
            Me._BaudRateCombo.Enabled = False
            Me._DataBitsCombo.Enabled = False
            Me._ReceiveDelayCombo.Enabled = False
            Me._ReceiveThresholdCombo.Enabled = False
            Me._ConnectButton.Image = My.Resources.LedCornerGreen
            Me._ReceiveStatusLabel.Image = My.Resources.LedCornerOrange
            Me._TransmitStatusLabel.Image = My.Resources.LedCornerOrange
            If Me.Port IsNot Nothing Then Me.ShowPortParameters(Me.Port.ToPortParameters)
        Else
            Me._ParityCombo.Enabled = True
            Me._StopBitsCombo.Enabled = True
            Me._PortNamesCombo.Enabled = True
            Me._BaudRateCombo.Enabled = True
            Me._DataBitsCombo.Enabled = True
            Me._ReceiveDelayCombo.Enabled = True
            Me._ReceiveThresholdCombo.Enabled = True
            Me._ConnectButton.Image = My.Resources.LedCornerGray
            Me._ReceiveStatusLabel.Image = My.Resources.LedCornerGray
            Me._TransmitStatusLabel.Image = My.Resources.LedCornerGray
            Me._StatusLabel.Text = "Disconnected"
        End If
    End Sub

    #End Region

    #Region " PORT PARAMETERS FILE MANAGEMENT "

    ''' <summary> load configuration. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LoadConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadConfigButton.Click
        Dim activity As String = $"loading port parameters from file"
        Try
            Dim args As New Core.ActionEventArgs
            If Not Me.Port.TryRestorePortParameters(args) Then Me.Publish(args)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> save port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveConfigButton.Click
        Dim activity As String = $"saving port parameters to file"
        Try
            Dim args As New Core.ActionEventArgs
            If Not Me.Port.TryStorePortParameters(args) Then Me.Publish(args)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub


    #End Region

    #Region " COM PORT MANAGEMENT "

    ''' <summary> Gets the port. </summary>
    ''' <value> The port. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property Port As IPort

    ''' <summary> Gets the is port that owns this item. </summary>
    ''' <value> The is port owner. </value>
    Private Property IsPortOwner As Boolean

    ''' <summary> Bind port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
    Public Sub BindPort(ByVal port As IPort)
        Me.BindPortThis(port)
        Me.IsPortOwner = False
    End Sub

    ''' <summary> Bind port this. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
    Private Sub BindPortThis(ByVal port As IPort)
        If Me.Port IsNot Nothing Then
            RemoveHandler Me.Port.PropertyChanged, AddressOf Me.PortPropertyChanged
            RemoveHandler Me.Port.ConnectionChanged, AddressOf Me.Port_ConnectionChanged
            RemoveHandler Me.Port.DataReceived, AddressOf Me.Port_DataReceived
            RemoveHandler Me.Port.DataSent, AddressOf Me.Port_DataSent
            RemoveHandler Me.Port.SerialPortErrorReceived, AddressOf Me.Port_SerialPortErrorReceived
            RemoveHandler Me.Port.Timeout, AddressOf Me.Port_Timeout
            If Me.IsPortOwner Then Me.Port.Dispose()

            Me._Port = Nothing
        End If
        Me._Port = port
        If Me.Port Is Nothing Then
            Me.OnConnectionChanged(Me.IsPortOpen)
        Else
            AddHandler Me.Port.PropertyChanged, AddressOf Me.PortPropertyChanged
            AddHandler Me.Port.ConnectionChanged, AddressOf Me.Port_ConnectionChanged
            AddHandler Me.Port.DataReceived, AddressOf Me.Port_DataReceived
            AddHandler Me.Port.DataSent, AddressOf Me.Port_DataSent
            AddHandler Me.Port.SerialPortErrorReceived, AddressOf Me.Port_SerialPortErrorReceived
            AddHandler Me.Port.Timeout, AddressOf Me.Port_Timeout
            Me.HandlePropertyChanged(Me.Port, NameOf(IPort.IsOpen))
            Me.HandlePropertyChanged(Me.Port, NameOf(IPort.PortParametersFileName))
            Me.HandlePropertyChanged(Me.Port, NameOf(IPort.SupportedBaudRates))
            Me.HandlePropertyChanged(Me.Port, NameOf(IPort.InputBufferingOption))
        End If
        Me.IsPortOwner = True
        Me.BindPortParameters(port)
    End Sub

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As IPort, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(IPort.InputBufferingOption)
            Case NameOf(IPort.IsOpen)
                Me.OnConnectionChanged(sender.IsOpen)
            Case NameOf(IPort.SupportedBaudRates)
                Me._BaudRateCombo.ComboBox.DataSource = Nothing
                Me._BaudRateCombo.Items.Clear()
                Me._BaudRateCombo.ComboBox.DataSource = sender.SupportedBaudRates
        End Select
    End Sub

    ''' <summary> Port property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PortPropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.IPort)}.{NameOf(isr.Ports.Serial.IPort.PropertyChanged)} event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.PortPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, IPort), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Gets the is port open. </summary>
    ''' <value> The is port open. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property IsPortOpen As Boolean
        Get
            Return Me.Port IsNot Nothing AndAlso Me.Port.IsOpen
        End Get
    End Property

    ''' <summary> Gets or sets options for controlling the port. </summary>
    ''' <value> Options that control the port. </value>
    Private ReadOnly Property PortParameters As PortParametersDictionary

    ''' <summary> Bind port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
    Private Sub BindPortParameters(ByVal port As IPort)
        If Me.PortParameters IsNot Nothing Then
            RemoveHandler Me.PortParameters.PropertyChanged, AddressOf Me.PortPropertyChanged
            Me._PortParameters = Nothing
        End If
        If port IsNot Nothing Then
            Me._PortParameters = port.PortParameters
            AddHandler Me.PortParameters.PropertyChanged, AddressOf Me.PortParametersPropertyChanged
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.BaudRate))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.DataBits))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.Handshake))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.Parity))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.PortName))

            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.ReadBufferSize))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.ReadTimeout))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.ReceivedBytesThreshold))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.ReceiveDelay))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.RtsEnable))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.StopBits))
            Me.HandlePropertyChanged(Me.PortParameters, NameOf(PortParametersDictionary.WriteBufferSize))
        End If
        Me.IsPortOwner = True
    End Sub

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As PortParametersDictionary, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(PortParametersDictionary.BaudRate)
                Me._BaudRateCombo.Text = sender.BaudRate.ToString
            Case NameOf(PortParametersDictionary.DataBits)
                Me._DataBitsCombo.Text = sender.DataBits.ToString
            Case NameOf(PortParametersDictionary.Handshake)
            Case NameOf(PortParametersDictionary.Parity)
                Me._ParityCombo.Text = sender.Parity.ToString
            Case NameOf(PortParametersDictionary.PortName)
                Me._PortNamesCombo.Text = sender.PortName
            Case NameOf(PortParametersDictionary.ReadBufferSize)
            Case NameOf(PortParametersDictionary.ReadTimeout)
            Case NameOf(PortParametersDictionary.ReceivedBytesThreshold)
                Me._ReceiveThresholdCombo.Text = sender.ReceivedBytesThreshold.ToString
            Case NameOf(PortParametersDictionary.ReceiveDelay)
                Me._ReceiveDelayCombo.Text = sender.ReceiveDelay.ToString
            Case NameOf(PortParametersDictionary.RtsEnable)
            Case NameOf(PortParametersDictionary.StopBits)
                Me._StopBitsCombo.Text = sender.StopBits.ToString
            Case NameOf(PortParametersDictionary.WriteBufferSize)
        End Select
    End Sub

    ''' <summary> Port parameters property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PortParametersPropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.PortParametersDictionary)}.{NameOf(PortParametersDictionary.PropertyChanged)} event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.PortPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, PortParametersDictionary), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Port event information. </param>
    Private Overloads Sub HandleDataReceived(ByVal sender As IPort, ByVal e As PortEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        If e.StatusOkay Then
            Me._ReceiveStatusLabel.Image = My.Resources.LedCornerGreen
            If e.DataBuffer IsNot Nothing Then
                Me.ReceiveCount += e.DataBuffer.Count
                Me._ReceiveTextBox.AppendBytes(e.DataBuffer, Me.Port.SerialPort.Encoding, Me.ReceiveTextBoxCharactersPerLine, Me.ReceiveDisplayOption)
            End If
        Else
            Me._ReceiveStatusLabel.Image = My.Resources.LedCornerRed
        End If
    End Sub

    ''' <summary>
    ''' Handles the Data Received event of the _serialPort control. Updates the data boxes and the
    ''' received status.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="PortEventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Port_DataReceived(sender As Object, ByVal e As PortEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.IPort)}.{NameOf(isr.Ports.Serial.IPort.DataReceived)} event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PortEventArgs)(AddressOf Me.Port_DataReceived), New Object() {sender, e})
            Else
                Me.HandleDataReceived(TryCast(sender, IPort), e)

            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Port event information. </param>
    Private Overloads Sub HandleDataSent(ByVal sender As IPort, ByVal e As PortEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        If e.StatusOkay Then
            Me._TransmitStatusLabel.Image = My.Resources.LedCornerGreen
            Me.TransmitCount += e.DataBuffer.Count
            Me._TransmitStatusLabel.Image = My.Resources.LedCornerGray
            Me._TransmitTextBox.AppendBytes(e.DataBuffer, Me.Port.SerialPort.Encoding, Me.TransmitTextBoxCharactersPerLine, Me.TransmitDisplayOption)
        Else
            Me._TransmitStatusLabel.Image = My.Resources.LedCornerRed
        End If
    End Sub

    ''' <summary>
    ''' Handles the Data Sent event of the _serialPort control. Updates controls and data.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="PortEventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Port_DataSent(sender As Object, e As PortEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.IPort)}.{NameOf(isr.Ports.Serial.IPort.DataSent)} event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PortEventArgs)(AddressOf Me.Port_DataSent), New Object() {sender, e})
            Else
                Me.HandleDataSent(TryCast(sender, IPort), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the connection changed described by e. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Connection event information. </param>
    Private Overloads Sub HandleConnectionChanged(ByVal e As ConnectionEventArgs)
        If e Is Nothing Then Return
        Me.OnConnectionChanged(e.IsPortOpen)
    End Sub

    ''' <summary> Handles the Connection Changed event of the _serialPort control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="ConnectionEventArgs" /> instance containing the event
    '''                       data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Port_ConnectionChanged(sender As Object, e As ConnectionEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.IPort)}.{NameOf(isr.Ports.Serial.IPort.ConnectionChanged)} event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, ConnectionEventArgs)(AddressOf Me.Port_ConnectionChanged), New Object() {sender, e})
            Else
                Me.HandleConnectionChanged(e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the serial port error received described by e. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Serial error received event information. </param>
    Private Overloads Sub HandleSerialPortErrorReceived(ByVal sender As IPort, ByVal e As System.IO.Ports.SerialErrorReceivedEventArgs)
        If sender Is Nothing OrElse sender.SerialPort Is Nothing OrElse e Is Nothing Then Return
        Dim message As String = $"Serial port error received from port {sender.SerialPort.PortName}: {e}"
        If Me.Talker Is Nothing Then
            My.MyLibrary.Appliance.LogUnpublishedMessage(TraceEventType.Warning, My.MyLibrary.TraceEventId, message)
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, message)
        End If
    End Sub

    ''' <summary> Port serial port error received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Serial error received event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Port_SerialPortErrorReceived(sender As Object, ByVal e As System.IO.Ports.SerialErrorReceivedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.IPort)}.{NameOf(isr.Ports.Serial.Port.SerialPortErrorReceived)} event"
        Try

            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, SerialErrorReceivedEventArgs)(AddressOf Me.Port_SerialPortErrorReceived), New Object() {sender, e})
            Else
                Me.HandleSerialPortErrorReceived(TryCast(sender, IPort), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Overloads Sub HandleTimeout(ByVal sender As IPort, ByVal e As System.EventArgs)
        If sender Is Nothing OrElse sender.SerialPort Is Nothing OrElse e Is Nothing Then Return
        Dim message As String = $"Serial port {sender.SerialPort.PortName} timeout"
        If Me.Talker Is Nothing Then
            My.MyLibrary.Appliance.LogUnpublishedMessage(TraceEventType.Warning, My.MyLibrary.TraceEventId, message)
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, message)
        End If
    End Sub

    ''' <summary> Port timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Port_Timeout(sender As Object, e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(isr.Ports.Serial.IPort)}.{NameOf(isr.Ports.Serial.Port.Timeout)} event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.EventArgs)(AddressOf Me.Port_Timeout), New Object() {sender, e})
            Else
                Me.HandleTimeout(TryCast(sender, IPort), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> List port names. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A String() </returns>
    Private Function ListPortNames() As String()
        Dim portNames As String() = Extensions.ListPortNames(Me._PortNamesCombo.ComboBox, Me.Port.SerialPort.PortName)
        Me._PortNamesCombo.Items.Clear()
        Me._PortNamesCombo.Items.AddRange(portNames)
        ' read available ports on system
        If portNames.Length > 0 Then
            Me._PortNamesCombo.Text = If(portNames.Contains(Serial.My.MySettings.Default.PortName, StringComparer.OrdinalIgnoreCase), Serial.My.MySettings.Default.PortName, portNames(0))
        Else
            Me._StatusLabel.Text = "no ports detected"
            Me._PortNamesCombo.Text = "NO PORTS"
            Me._ConnectButton.Text = "NO PORTS"
            Me._ConnectButton.Enabled = False
        End If
        Return portNames
    End Function

    ''' <summary> Refresh port list button click. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RefreshPortListButton_Click(sender As Object, e As EventArgs) Handles _RefreshPortListButton.Click
        Me.ListPortNames()

    End Sub


    #End Region

    #Region " DISPLAY MANAGEMENT "

    ''' <summary> Width of the receive text box character. </summary>
    Private _ReceiveTextBoxCharacterWidth As Single

    ''' <summary> Gets or sets the average width of the receive text box character. </summary>
    ''' <value> The average width of the receive text box character. </value>
    Private Property ReceiveTextBoxCharacterWidth As Single
        Get
            If Me._ReceiveTextBoxCharacterWidth = 0 Then
                Me._ReceiveTextBoxCharacterWidth = PortConsole.EstimateCharacterWidth(Me._ReceiveTextBox)
            End If
            Return Me._ReceiveTextBoxCharacterWidth
        End Get
        Set(value As Single)
            Me._ReceiveTextBoxCharacterWidth = value
        End Set
    End Property

    ''' <summary> Width of the transmit text box character. </summary>
    Private _TransmitTextBoxCharacterWidth As Single

    ''' <summary> Gets or sets the average width of the Transmit text box character. </summary>
    ''' <value> The average width of the Transmit text box character. </value>
    Private Property TransmitTextBoxCharacterWidth As Single
        Get
            If Me._TransmitTextBoxCharacterWidth = 0 Then
                Me._TransmitTextBoxCharacterWidth = PortConsole.EstimateCharacterWidth(Me._TransmitTextBox)
            End If
            Return Me._TransmitTextBoxCharacterWidth
        End Get
        Set(value As Single)
            Me._TransmitTextBoxCharacterWidth = value
        End Set
    End Property

    ''' <summary> Estimate character width. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="textBox"> The text box control. </param>
    ''' <returns> A Single. </returns>
    Private Shared Function EstimateCharacterWidth(ByVal textBox As RichTextBox) As Single
        Dim result As Single = 10
        Using g As Graphics = textBox.CreateGraphics
            ' measure with a test string
            Dim szF As SizeF = g.MeasureString("0123456789", textBox.Font)
            result = 0.1F * szF.Width
        End Using
        Return result
    End Function

    ''' <summary> Gets or sets the transmit panel characters per line. </summary>
    ''' <value> The transmit panel characters per line. </value>
    Private Property TransmitTextBoxCharactersPerLine As Integer

    ''' <summary> Gets or sets the receive panel characters per line. </summary>
    ''' <value> The receive panel characters per line. </value>
    Private Property ReceiveTextBoxCharactersPerLine As Integer

    ''' <summary> Selected font. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Font. </returns>
    Private Shared Function SelectedFont(ByVal value As FontSize) As Font
        Dim result As Font

        Select Case value
            Case FontSize.Large
                result = New Font(Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.LargeFontSize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Case FontSize.Medium
                result = New Font(Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.MediumFontSize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Case FontSize.Small
                result = New Font(Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.SmallFontSize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Case Else
                result = New Font(Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.MediumFontSize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        End Select
        Return result
    End Function

    ''' <summary> Values that represent font sizes. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Enum FontSize

        ''' <summary> An enum constant representing the small option. </summary>
        Small

        ''' <summary> An enum constant representing the medium option. </summary>
        Medium

        ''' <summary> An enum constant representing the large option. </summary>
        Large
    End Enum

    ''' <summary> Select font size. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub SelectFontSize(ByVal value As Integer)
        If [Enum].IsDefined(GetType(FontSize), value) Then
            Me.SelectFontSize(CType(value, FontSize))
        End If
    End Sub

    ''' <summary> Select font size. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub SelectFontSize(ByVal value As FontSize)
        Me._ReceiveTextBox.Font = PortConsole.SelectedFont(value)
        Me._ReceiveRulerLabel.Font = Me._ReceiveTextBox.Font
        Me._TransmitTextBox.Font = Me._ReceiveTextBox.Font
        Me._TransmitRulerLabel.Font = Me._ReceiveTextBox.Font
        Me.ReceiveTextBoxCharacterWidth = PortConsole.EstimateCharacterWidth(Me._ReceiveTextBox)
        Me.TransmitTextBoxCharacterWidth = PortConsole.EstimateCharacterWidth(Me._TransmitTextBox)
        Serial.My.MySettings.Default.FontSize = value
        If Me._FontSizeComboBox.SelectedIndex <> value Then
            Me.InitializingComponents = True
            Me._FontSizeComboBox.SelectedIndex = value
            Me.InitializingComponents = False
        End If
    End Sub

    ''' <summary> Font size combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FontSizeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _FontSizeComboBox.SelectedIndexChanged

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = "selecting font size"
        Try
            Dim combo As ToolStripComboBox = TryCast(sender, ToolStripComboBox)
            If combo IsNot Nothing AndAlso combo.SelectedIndex > 0 Then
                Me.SelectFontSize(combo.SelectedIndex)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Shows the Messenger parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portParameters"> Options for controlling the port. </param>
    Private Sub ShowPortParameters(ByVal portParameters As PortParametersDictionary)
        Dim s As New System.Text.StringBuilder
        s.Append("Connected using ")
        s.Append(portParameters.Item(PortParameterKey.PortName))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.BaudRate))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.DataBits))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.StopBits))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.Parity))
        Me._StatusLabel.Text = s.ToString
    End Sub

#End Region

#Region " TRANSMIT REPEAT "

    ''' <summary> Toggle transmission repeat control enabled. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ToggleTransmissionRepeatControlEnabled()
        Me.ToggleTransmissionRepeatControlEnabled(Not Me._TransmitPlayButton.Visible)
    End Sub

    ''' <summary> Toggle transmission repeat control enabled. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="enabled"> True to enable, false to disable. </param>
    Public Sub ToggleTransmissionRepeatControlEnabled(ByVal enabled As Boolean)
        Me._TransmitPlayButton.Visible = enabled
        Me._TransmitDelayMillisecondsTextBox.Visible = enabled
        Me._TransmitRepeatCountTextBox.Visible = enabled
        Me._TransmitSeparator9.Visible = enabled
    End Sub

    ''' <summary> Transmit play button checked changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TransmitPlayButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _TransmitPlayButton.CheckedChanged
        If Me.InitializingComponents Then Return
        If Me._TransmitPlayButton.Enabled AndAlso Me._TransmitPlayButton.Visible Then
            Me._TransmitPlayButton.Text = If(Me._TransmitPlayButton.Checked, "STOP", "PLAY")
        End If
    End Sub

    #End Region

    #Region " MESSAGE NOTIFICATIONS "

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _ErrorProvider As ErrorProvider
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Enunciates the specified control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="control">            The control. </param>
    ''' <param name="errorIconAlignment"> The error icon alignment. </param>
    ''' <param name="padding">            The padding. </param>
    ''' <param name="e">                  The <see cref="TraceMessageEventArgs" /> instance containing
    '''                                   the event data. </param>
    Private Sub Enunciate(ByVal control As Control, ByVal errorIconAlignment As ErrorIconAlignment, ByVal padding As Integer, ByVal e As TraceMessage)
        If e.EventType <= TraceEventType.Warning Then
            Me._ErrorProvider.SetIconAlignment(control, errorIconAlignment)
            Me._ErrorProvider.SetIconPadding(control, padding)
            Me._ErrorProvider.SetError(control, e.Details)
        Else
            Me._ErrorProvider.Clear()
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds a listener to the device manager devices. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="listener"> The value. </param>
    Public Overrides Sub AddListener(ByVal listener As IMessageListener)
        Me.Port.AddListener(listener)
        MyBase.AddListener(listener)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " TOOL STRIP RENDERER "

    ''' <summary> A custom professional renderer. </summary>
    ''' <remarks>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2017-10-30 </para>
    ''' </remarks>
    Private Class CustomProfessionalRenderer
        Inherits ToolStripProfessionalRenderer

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
        ''' event.
        ''' </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
        '''                  contains the event data. </param>
        Protected Overrides Sub OnRenderLabelBackground(ByVal e As ToolStripItemRenderEventArgs)
            If e IsNot Nothing AndAlso (e.Item.BackColor <> System.Drawing.SystemColors.ControlDark) Then
                Using brush As New Drawing.SolidBrush(e.Item.BackColor)
                    e.Graphics.FillRectangle(brush, e.Item.ContentRectangle)
                End Using
            End If
        End Sub
    End Class

#End Region

End Class

