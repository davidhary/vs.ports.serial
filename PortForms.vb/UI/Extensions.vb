Imports System.Collections.Generic

Imports isr.Core.EscapeSequencesExtensions

Public Module Extensions

#Region " LABEL "

    ''' <summary> Displays a ruler on a <see cref="Windows.Forms.label">label</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="label">          The <see cref="Windows.Forms.RichTextBox">Rich Text Box</see>. </param>
    ''' <param name="width">          The width. </param>
    ''' <param name="characterWidth"> Width of the character. </param>
    ''' <param name="displayOption">  The display option. </param>
    ''' <returns> length of ruler. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function DisplayRuler(ByVal label As System.Windows.Forms.Label, ByVal width As Integer, ByVal characterWidth As Single, ByVal displayOption As DisplayOptions) As Integer

        If label Is Nothing Then Return 0
        Dim s As New System.Text.StringBuilder
        Dim anzMarks As Integer

        If DisplayOptions.Hex = (displayOption And DisplayOptions.Hex) Then
            anzMarks = CInt(Math.Floor((width / characterWidth) / 3))
            For i As Integer = 1 To anzMarks
                s.Append($".{i:00}")
            Next
        Else
            anzMarks = CInt(Math.Floor((width / characterWidth) / 5))
            For i As Integer = 1 To anzMarks
                If i < 2 Then
                    s.Append($"....{i * 5:0}")
                ElseIf i < 20 Then
                    s.Append($"...{i * 5:00}")
                Else
                    s.Append($"..{i * 5:000}")
                End If
            Next
        End If
        label.Text = s.ToString

        ' coloring ruler
        label.BackColor = System.Drawing.Color.LightGray
        Return s.Length

    End Function

#End Region

#Region " RICH TEXT BOX "

    ''' <summary>
    ''' Displays a ruler on a <see cref="Windows.Forms.RichTextBox">Rich Text Box</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="textBox">        The <see cref="Windows.Forms.RichTextBox">Rich Text Box</see>. </param>
    ''' <param name="characterWidth"> Width of the character. </param>
    ''' <param name="displayOption">  The display option. </param>
    ''' <returns> length of ruler. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function DisplayRuler(ByVal textBox As System.Windows.Forms.RichTextBox, ByVal characterWidth As Single, ByVal displayOption As DisplayOptions) As Integer

        If textBox Is Nothing Then Return 0
        Dim rbWidth As Integer = textBox.Width
        Dim s As New System.Text.StringBuilder
        Dim anzMarks As Integer

        If DisplayOptions.Hex = (displayOption And DisplayOptions.Hex) Then
            anzMarks = CInt((rbWidth / characterWidth) / 3)
            For i As Integer = 1 To anzMarks
                s.Append($" {i:00}")
            Next
        Else
            anzMarks = CInt((rbWidth / characterWidth) / 5)
            For i As Integer = 1 To anzMarks
                If i < 2 Then
                    s.Append($"    {i * 5:0}")
                ElseIf i < 20 Then
                    s.Append($"   {i * 5:00}")
                Else
                    s.Append($"  {i * 5:000}")
                End If
            Next
        End If

        ' coloring ruler
        Dim cl As System.Drawing.Color = textBox.BackColor
        textBox.Select(0, textBox.Lines(0).Length)
        textBox.SelectionBackColor = System.Drawing.Color.LightGray
        textBox.SelectedText = s.ToString
        If textBox.Lines.Length = 1 Then textBox.AppendText(vbCr)
        textBox.SelectionBackColor = cl
        textBox.SelectionLength = 0
        Return s.Length

    End Function

    ''' <summary> Builds hexadecimal string. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data"> data frame. </param>
    ''' <returns> A String. </returns>
    Private Function BuildHexString(ByVal data As IEnumerable(Of Byte)) As String
        Dim builder As New System.Text.StringBuilder
        For Each value As Byte In data
            builder.Append($" {value:X2}")
        Next
        Return builder.ToString
    End Function

    ''' <summary> Builds character string. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data">     data frame. </param>
    ''' <param name="encoding"> The encoding. </param>
    ''' <returns> A String. </returns>
    Private Function BuildCharacterString(ByVal data As IEnumerable(Of Byte), ByVal encoding As Text.Encoding) As String
        Dim builder As New System.Text.StringBuilder
        For Each value As Byte In data
            If value > 31 Then
                builder.Append($"  {encoding.GetString(value)}")
            ElseIf value.IsEscapeValue Then
                builder.Append($" {value.ToEscapeSequence}")
            Else
                builder.Append("  .")
            End If
        Next
        Return builder.ToString
    End Function

    ''' <summary> Builds ASCII string. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data">     data frame. </param>
    ''' <param name="encoding"> The encoding. </param>
    ''' <returns> A String. </returns>
    Private Function BuildAsciiString(ByVal data As IEnumerable(Of Byte), ByVal encoding As Text.Encoding) As String
        Dim builder As New System.Text.StringBuilder
        For Each value As Byte In data
            If value > 31 Then
                builder.Append($"{encoding.GetString(value)}")
            ElseIf value.IsEscapeValue Then
                builder.Append($"{value.ToEscapeSequence}")
            Else
                builder.Append(".")
            End If
        Next
        Return builder.ToString
    End Function

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value">  The value. </param>
    ''' <param name="length"> The length. </param>
    ''' <returns> A Queue(Of String) </returns>
    Private Function Enqueue(ByVal value As String, ByVal length As Integer) As Queue(Of String)
        Dim result As New Queue(Of String)
        Dim startIndex As Integer = 0

        Do While startIndex < value.Length
            Dim s As String = value.Substring(startIndex, Math.Min(length, value.Length - startIndex))

            result.Enqueue(s)
            startIndex += length
        Loop
        Return result
    End Function

    ''' <summary>
    ''' Append data bytes to the <see cref="Windows.Forms.TextBoxBase">Text Box</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="textBox">       The <see cref="Windows.Forms.TextBoxBase">Text Box</see>. </param>
    ''' <param name="data">          data frame. </param>
    ''' <param name="encoding">      The encoding. </param>
    ''' <param name="currentLength"> Max chars in box. </param>
    ''' <param name="displayOption"> The display option. </param>
    <Runtime.CompilerServices.Extension()>
    Public Sub AppendBytes(ByVal textBox As System.Windows.Forms.TextBoxBase, ByVal data As IEnumerable(Of Byte), ByVal encoding As Text.Encoding,
                           ByVal currentLength As Integer,
                           ByVal displayOption As DisplayOptions)

        If textBox Is Nothing Then Return
        If data Is Nothing OrElse Not data.Any Then Return
        If DisplayOptions.Ascii = displayOption Then
            Dim asciiString As String = Extensions.BuildAsciiString(data, encoding)
            Dim q As Queue(Of String) = Enqueue(asciiString, currentLength)
            Do While q.Any
                textBox.ScrollToCaret()
                textBox.AppendText($"{q.Dequeue}{Environment.NewLine}")
            Loop
        Else
            Dim hexString As String = Extensions.BuildHexString(data)
            Dim hexQ As Queue(Of String) = Enqueue(hexString, currentLength)
            If DisplayOptions.Ascii = (displayOption And DisplayOptions.Ascii) Then
                Dim charString As String = Extensions.BuildCharacterString(data, encoding)
                Dim charQ As Queue(Of String) = Enqueue(charString, currentLength)
                Do While (hexQ.Any OrElse charQ.Any)
                    If hexQ.Any Then
                        textBox.ScrollToCaret()
                        textBox.AppendText($"{hexQ.Dequeue}{Environment.NewLine}")
                    End If
                    If charQ.Any Then
                        textBox.ScrollToCaret()
                        textBox.AppendText($"{charQ.Dequeue}{Environment.NewLine}")
                    End If
                Loop
            Else
                Do While hexQ.Any
                    textBox.ScrollToCaret()
                    textBox.AppendText($"{hexQ.Dequeue}{Environment.NewLine}")
                Loop
            End If
        End If
    End Sub

#End Region

#Region " COMBO BOX EXTENSIUONS "

    ''' <summary> List port names. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control">         The control. </param>
    ''' <param name="currentPortName"> The current port name. </param>
    ''' <returns> A String() </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ListPortNames(ByVal control As Windows.Forms.ComboBox, ByVal currentPortName As String) As String()
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Dim portNames As String() = System.IO.Ports.SerialPort.GetPortNames
        control.Items.Clear()
        control.Items.AddRange(portNames)
        Dim portName As String

        If portNames.Any Then
            If portNames.Contains(Serial.My.MySettings.Default.PortName, StringComparer.CurrentCultureIgnoreCase) Then
            End If
            If portNames.Contains(currentPortName, StringComparer.CurrentCultureIgnoreCase) Then
                portName = currentPortName
            ElseIf portNames.Contains(Serial.My.MySettings.Default.PortName, StringComparer.CurrentCultureIgnoreCase) Then
                portName = Serial.My.MySettings.Default.PortName
            Else
                portName = portNames(0)
            End If
        Else
            portName = "NO PORTS"
        End If
        control.Text = portName
        Return portNames
    End Function

#End Region

End Module

''' <summary> A bit-field of flags for specifying display options. </summary>
''' <remarks> David, 2020-10-22. </remarks>
<Flags>
Public Enum DisplayOptions

    ''' <summary> An enum constant representing the Hexadecimal option. </summary>
    Hex = 1

    ''' <summary> An enum constant representing the ASCII option. </summary>
    Ascii = 2

    ''' <summary> An enum constant representing the both option. </summary>
    Both = 3
End Enum

