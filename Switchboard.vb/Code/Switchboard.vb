Imports System.Collections.Generic

Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary>
''' Includes code for Switchboard Form, which serves as a switchboard for this program.
''' </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2095-10-11, 1.0.2110.x. </para>
''' </remarks>
Public Class Switchboard
    Inherits isr.Core.Forma.UserFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        ' turn off saving to prevent exception reported due to access from two programs
        Me.SaveSettingsOnClosing = False
        Me._OpenForms = New isr.Core.Forma.ConsoleFormCollection
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' this disposes of any form that did not close.
                If Me._OpenForms IsNot Nothing Then Me._OpenForms.ClearDispose()
                Windows.Forms.Application.DoEvents()
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = Core.ApplicationInfo.BuildApplicationTitleCaption(NameOf(Switchboard))

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by form for shown events. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' populate the action list
            Me.PopulateActiveList()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default


        End Try
    End Sub


#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Populates the list of options in the action combo box. </summary>
    ''' <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
    Private Sub PopulateActiveList()

        ' set the action list
        Me._ApplicationsListBox.DataSource = Nothing
        Me._ApplicationsListBox.Items.Clear()
        Me._ApplicationsListBox.DataSource = GetType(ActionOption).ValueDescriptionPairs().ToList
        Me._ApplicationsListBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._ApplicationsListBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)

    End Sub

    ''' <summary> Gets the selected action. </summary>
    ''' <value> The selected action. </value>
    Private ReadOnly Property SelectedAction() As ActionOption
        Get
            Return CType(CType(Me._ApplicationsListBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, ActionOption)
        End Get
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        Me.Close()
    End Sub

    ''' <summary> Enumerates the action options. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Enum ActionOption

        ''' <summary> An enum constant representing the serial terminal option. </summary>
        <System.ComponentModel.Description("COM Terminal")>
        SerialTerminal

        ''' <summary> An enum constant representing the console form option. </summary>
        <System.ComponentModel.Description("Serial Console Dialog")>
        ConsoleForm
    End Enum

    ''' <summary> The terminal form. </summary>
    Private _TerminalForm As ComTerm.TerminalPanel

    ''' <summary> The open forms. </summary>
    Private ReadOnly _OpenForms As isr.Core.Forma.ConsoleFormCollection

    ''' <summary> Open selected items. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
                                  Justification:="Disposed when closed as part for the forms collection")>
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenButton.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Select Case Me.SelectedAction
            Case ActionOption.SerialTerminal
                If Me._TerminalForm Is Nothing Then Me._TerminalForm = New ComTerm.TerminalPanel
                Me._TerminalForm.Show()
            Case ActionOption.ConsoleForm
                Me._OpenForms.ShowNew($"Term #{Me._OpenForms.Count + 1}", New isr.Ports.Serial.Forms.PortConsole, My.Application.Logger)
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

#End Region

End Class
