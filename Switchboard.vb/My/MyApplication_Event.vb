Imports isr.Core
Imports isr.Ports.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Releases the splash screen. </summary>
        ''' <remarks> David, 2020-11-16. </remarks>
        Friend Sub ReleaseSplashScreen()
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary> Creates splash screen. </summary>
        ''' <remarks> David, 2020-11-16. </remarks>
        Friend Sub CreateSplashScreen()
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary> Writes a log entry and displays it on the splash screen if exists. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="id">        The identifier. </param>
        ''' <param name="details">   The details. </param>
        Private Sub WriteLogEntry(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String)
            CType(Me.SplashScreen, isr.Core.Forma.BlueSplash)?.DisplayMessage(details)
            Me.Logger.WriteLogEntry(eventType, id, details)
        End Sub

        ''' <summary>   Determines if user requested a close. </summary>
        ''' <remarks>   David, 2020-09-30. </remarks>
        ''' <returns>   True if close requested; false otherwise. </returns>
        Friend Function UserCloseRequested() As Boolean
            Return ((CType(Me.SplashScreen, isr.Core.Forma.BlueSplash))?.IsCloseRequested).GetValueOrDefault(False)
        End Function

        ''' <summary> Instantiates the application to its known state. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <returns> True if success or false if requesting to terminate. </returns>
        Private Function TryinitializeKnownState() As Boolean

            Dim activity As String = String.Empty
            Dim passed As Boolean

            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting
                activity = "Application is initializing"
                Me.WriteLogEntry(TraceEventType.Verbose, MyApplication.TraceEventId, activity)

                passed = True

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.WriteLogEntry(TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity};. {ex.ToFullBlownString()}")

                passed = False
                Throw

            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

                If Not passed Then
                    Try
                        Me.ReleaseSplashScreen()
                    Finally
                    End Try
                End If

            End Try
            Return passed

        End Function

        ''' <summary> Processes the shut down. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        Private Sub ProcessShutDown()
            If My.Application.SaveMySettingsOnExit Then
            End If
        End Sub

        ''' <summary>
        ''' Processes the startup. Sets the event arguments
        ''' <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see>
        ''' value if failed.
        ''' </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        '''                  instance containing the event data. </param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)

            e.Cancel = False
            If Not e.Cancel Then
                e.Cancel = Not CommandLineInfo.TryParseCommandLine(e.CommandLine)
            End If
            If Not e.Cancel Then
            End If
            If Not e.Cancel Then
                e.Cancel = Not Me.TryinitializeKnownState()
            End If

        End Sub

    End Class

End Namespace

