Imports isr.Ports.ExceptionExtensions
Namespace My

    ''' <summary> Provides assembly information for the application. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Ports.Serial.My.ProjectTraceEventId.Switchboard

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Serial Port Switchboard"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Serial Port Switchboard"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Ports.Serial.Switchboard"

    End Class

End Namespace

