Imports isr.Ports.Serial

''' <summary> Tests the <see cref="isr.Ports.Serial.SerialCommPort"/> port using a null model. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-26 </para>
''' </remarks>
<TestClass(), TestCategory("NullModemOnePort")>
Public Class NullModemOnePortTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(NullModemSettings.Get.Exists, $"{GetType(NullModemSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " NULL MODEM TESTS "

    ''' <summary> Assets the port open condition. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> The port. </param>
    Private Shared Sub AssertPortOpen(ByVal port As isr.Ports.Serial.SerialCommPort)
        Assert.AreEqual(True, port.IsOpen, $"Serial Port #{port.Port} should be open")
    End Sub

    ''' <summary> (Unit Test Method) tests null modem using two ports. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub SinglePortNullModemTest()
        If Not NullModemSettings.Get.CheckPort1Exists Then Assert.Inconclusive($"Serial Port #{NullModemSettings.Get.Port1Number} not found")
        Dim port1 As New SerialCommPort With {.Port = NullModemSettings.Get.Port1Number}
        Try
            port1.Open()
            Assert.AreEqual(NullModemSettings.Get.Port1Number, port1.Port, $"Serial port number should match")
            NullModemOnePortTests.AssertPortOpen(port1)
        Catch
            Throw
        Finally
        End Try
        Dim message As String = "ABC"
        port1.Write(message)
        isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(10))
        port1.Read(message.Length)
        Dim receivedMessage As String = port1.InputStreamString
        Assert.AreEqual(message, receivedMessage, $"Serial Port #{port1.Port} received message should match")

    End Sub

#End Region

End Class

