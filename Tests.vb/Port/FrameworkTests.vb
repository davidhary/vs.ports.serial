Imports isr.Core.MSTest
Imports isr.Ports.Serial
Imports isr.Ports.Serial.Methods

''' <summary> Tests the <see cref="isr.Ports.Serial.Port"/> port. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-26 </para>
''' </remarks>
<TestClass()>
Public Class FrameworkTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Ports.Serial.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
	Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

	''' <summary> Initializes before each test runs. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
		Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
		Dim expectedUpperLimit As Double = 12
		Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
		TestInfo.ClearMessageQueue()
		Assert.IsTrue(FrameworkSettings.Get.Exists, $"{GetType(FrameworkSettings)} settings should exist")
		TestInfo.ClearMessageQueue()
	End Sub

	''' <summary> Cleans up after each test has run. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	''' <summary>
	''' Gets the test context which provides information about and functionality for the current test
	''' run.
	''' </summary>
	''' <value> The test context. </value>
	Public Property TestContext() As TestContext
	''' <summary> The test site. </summary>
	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " EXTENSION: DELIMITER TESTS "

	''' <summary> A test for InsertDelimiter. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub InsertDelimiterTest()
		Dim value As String = "000003"
		Dim everyLength As Integer = 2
		Dim delimiter As String = " "
		Dim expected As String = "00 00 03"
		Dim actual As String
		actual = Serial.Methods.InsertDelimiter(value, everyLength, delimiter)
		Assert.AreEqual(expected, actual)
		Assert.AreEqual(expected, value.InsertDelimiter(everyLength, delimiter))
	End Sub

	''' <summary> A test for RemoveDelimiter. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub RemoveDelimiterTest()
		Dim value As String = "00 00 03"
		Dim delimiter As String = " "
		Dim expected As String = "000003"
		Dim actual As String
		actual = Serial.Methods.RemoveDelimiter(value, delimiter)
		Assert.AreEqual(expected, actual)
		Assert.AreEqual(expected, value.RemoveDelimiter(delimiter))
	End Sub

	''' <summary> A test for  Remove and then insert Delimiter. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub RemoveInsertDelimiterTest()
		Dim value As String = "00 00 03"
		Dim everyLength As Integer = 2
		Dim delimiter As String = " "
		Dim expected As String = "00 00 03"
		Dim actual As String
		actual = value.RemoveDelimiter(delimiter).InsertDelimiter(everyLength, delimiter)
		Assert.AreEqual(expected, actual)
	End Sub
#End Region

#Region " CHECK SUM TESTS "

	''' <summary> (Unit Test Method) tests checksum. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub ChecksumTest()
		Dim payload As String = "#1DOFF"
		Dim expected As String = "73"
		Dim actual As String = payload.Checksum
		Assert.AreEqual(expected, actual, $"Check sum of {payload}")
	End Sub

	''' <summary> (Unit Test Method) validates the checksum test. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub ValidateChecksumTest()
		Dim payload As String = "*1RD+00072.10A4"
		Dim expected As String = "A4"
		Dim message As String = payload.Remove(payload.Length - 2, 2)
		Dim expectedMessage As String = "*1RD+00072.10"
		Assert.AreEqual(expectedMessage, message, $"Stripped message")
		Dim actual As String = message.Checksum
		Assert.AreEqual(expected, actual, $"Validated check sum of {payload}")
	End Sub

#End Region

#Region " TRANSIT TIME "

	''' <summary> (Unit Test Method) tests default bit count. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub DefaultBitCountTest()
		Dim dix As PortParametersDictionary = PortParametersDictionary.DefaultPortParameters
		Assert.AreEqual(FrameworkSettings.Get.DefaultCharacterBitCount, dix.CharacterBitCount(1), "Bits per character")
	End Sub

	''' <summary> (Unit Test Method) tests default minimum transit time. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub DefaultMinimumTransitTimeTest()
		Dim dix As PortParametersDictionary = PortParametersDictionary.DefaultPortParameters
		Asserts.Instance.AreEqual(FrameworkSettings.Get.DefaultMinimumTransitTime, dix.MinimumTransitTimespan(1),
						  TimeSpan.FromTicks(CLng(TimeSpan.TicksPerMillisecond * 0.01)), "Minimum transit time")
	End Sub

#End Region

End Class

