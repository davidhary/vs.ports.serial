Imports isr.Ports.Serial

''' <summary> Tests the <see cref="isr.Ports.Serial.Port"/> port using a two port null model. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-26 </para>
''' </remarks>
<TestClass(), TestCategory("NullModemTwoPorts")>
Public Class NullModemTwoPortTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Ports.Serial.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(NullModemSettings.Get.Exists, $"{GetType(NullModemSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " NULL MODEM TESTS "

    ''' <summary> Check connected port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> The port. </param>
    Private Shared Sub CheckConnectedPort(ByVal port As isr.Ports.Serial.IPort)
        Assert.AreEqual(True, port.IsOpen, $"Port {port.PortParameters.PortName} is open")
    End Sub

    ''' <summary> (Unit Test Method) tests null modem using two ports. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub TwoPortNullModemTest()
        If Not NullModemSettings.Get.CheckPort1Exists Then Assert.Inconclusive($"{NullModemSettings.Get.Port1Name} not found")
        Dim e As New Core.ActionEventArgs
        Using port1 As isr.Ports.Serial.IPort = isr.Ports.Serial.Port.Create
            Try
                port1.AddListener(TestInfo.TraceMessagesQueueListener)
                port1.TryOpen(NullModemSettings.Get.Port1Name, e)
                Assert.AreEqual(False, e.Failed, $"Failed connecting {NullModemSettings.Get.Port1Name}: {e.Details}")
                Assert.AreEqual(NullModemSettings.Get.Port1Name, port1.SerialPort.PortName, $"Serial port name should match")
                NullModemTwoPortTests.CheckConnectedPort(port1)
            Catch
                Throw
            Finally
                e.Clear()
            End Try
            Using port2 As IPort = Port.Create
                Try
                    port2.AddListener(TestInfo.TraceMessagesQueueListener)
                    port2.TryOpen(NullModemSettings.Get.Port2Name, e)
                    Assert.AreEqual(False, e.Failed, $"Failed connecting {NullModemSettings.Get.Port2Name}: {e.Details}")
                    Assert.AreEqual(NullModemSettings.Get.Port2Name, port2.SerialPort.PortName, $"Serial port name should match")
                    NullModemTwoPortTests.CheckConnectedPort(port2)
                Catch
                    Throw
                Finally
                    e.Clear()
                End Try

                Dim message As String = "ABC"
                Port.SendAsciiData(port2, message)

                ' wait for the data to go out
                isr.Core.ApplianceBase.DoEventsWait(port2.PortParameters.MinimumTransitTimespan(message.Length))

                Dim sentMessage As String = Port.Decode(port2.TransmittedValues, port2.SerialPort.Encoding)

                ' wait for the characters to come in.
                port1.TryWaitReceiveCount(message.Length, 10)
                Dim receivedMessage As String = Port.Decode(port1.ReceivedValues, port1.SerialPort.Encoding)


                Assert.AreEqual(message, sentMessage, $"sent message should match")
                Assert.AreEqual(message, receivedMessage, $"received message should match")
            End Using


        End Using
    End Sub

#End Region

End Class

