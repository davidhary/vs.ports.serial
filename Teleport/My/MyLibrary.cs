﻿
namespace isr.Ports.Teleport.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Serial.My.ProjectTraceEventId.Teleport;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Serial Teleport Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Serial Teleport Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Ports.Teleport";
    }
}