using System.Collections.Generic;

namespace isr.Ports.Teleport
{

    /// <summary> Defines the interface for the payload. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public interface IPayload
    {

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        StatusCode StatusCode { get; set; }

        /// <summary> Gets or sets the payload. </summary>
        /// <value> The payload. </value>
        IEnumerable<byte> Payload { get; }

        /// <summary> Converts the payload to bytes. </summary>
        /// <returns> This object as an IEnumerable(Of Byte) </returns>
        IEnumerable<byte> Build();

        /// <summary> Populates the payload from the given data. </summary>
        /// <param name="payload"> The payload. </param>
        /// <returns> A StatusCode. </returns>
        StatusCode Parse( IEnumerable<byte> payload );
    }
}
