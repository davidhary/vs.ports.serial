using System.Collections.Generic;
using System.Linq;

namespace isr.Ports.Teleport
{

    /// <summary> Collection of bytes. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-06 </para>
    /// </remarks>
    public class ByteCollection : System.Collections.ObjectModel.Collection<byte>
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ByteCollection() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> An IEnumerable(OfByte) of items to append to this. </param>
        public ByteCollection( IEnumerable<byte> values ) : this()
        {
            this.AddRange( values );
        }

        /// <summary> Adds a range. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> An IEnumerable(OfByte) of items to append to this. </param>
        public void AddRange( IEnumerable<byte> values )
        {
            if ( values?.Any() == true )
            {
                foreach ( byte v in values )
                    this.Add( v );
            }
        }

        #endregion


    }
}
