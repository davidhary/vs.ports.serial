using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Ports.Teleport
{

    /// <summary> A communicator emulating a Teleport module, a listener or subscriber. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Collector : MessengerBase, IMessenger
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Collector" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        public Collector( IEnumerable<byte> moduleAddress, IProtocolMessage templateProtocolMessage ) : base( moduleAddress, MessengerRole.Collector, templateProtocolMessage )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="Collector" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        /// <param name="port">                    Specifies the
        /// <see cref="isr.Ports.Serial.IPort">port</see>. </param>
        public Collector( IEnumerable<byte> moduleAddress, IProtocolMessage templateProtocolMessage, Serial.IPort port ) : base( moduleAddress, MessengerRole.Collector, templateProtocolMessage, port )
        {
        }

        /// <summary> Creates a new collector. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        /// <returns> A collector. </returns>
        public static IMessenger Create( IEnumerable<byte> moduleAddress, IProtocolMessage templateProtocolMessage )
        {
            Collector result = null;
            try
            {
                result = new Collector( moduleAddress, templateProtocolMessage );
            }
            catch
            {
                if ( result is object )
                    result.Dispose();
                throw;
            }

            return result;
        }

        /// <summary> Creates a new collector. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        /// <param name="port">                    Specifies the
        /// <see cref="isr.Ports.Serial.IPort">port</see>. </param>
        /// <returns> A collector. </returns>
        public static Collector Create( IEnumerable<byte> moduleAddress, IProtocolMessage templateProtocolMessage, Serial.IPort port )
        {
            Collector result = null;
            try
            {
                result = new Collector( moduleAddress, templateProtocolMessage, port );
            }
            catch
            {
                if ( result is object )
                    result.Dispose();
                throw;
            }

            return result;
        }

        #endregion

        #region " RECEIVE MANAGMENT "

        /// <summary>
        /// Services an error. This is a strictly listener mode.  It sends back an error message to the
        /// Emitter.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> A StatusCode. </returns>
        public override StatusCode ServiceError( StatusCode status )
        {
            status = base.ServiceError( status );
            // set output error message
            this.OutputMessage.ModuleAddress = this.InputMessage.ModuleAddress;
            this.OutputMessage.MessageType = MessageType.Failure;
            this.OutputMessage.Command = this.InputMessage.Command;
            this.OutputMessage.Status = status;
            this.OutputMessage.UpdateChecksum();

            // report
            string activity = $"Sending error message;. Status='{status}'";
            _ = this.Publish( TraceEventType.Verbose, activity );

            // transmit error status
            status = this.SendProtocolMessage();

            // validate status of message
            if ( IsErrorCode( status ) )
            {
                _ = this.Publish( TraceEventType.Warning, $"Failed {activity};. status='{status}'" );
            }

            return status;
        }

        /// <summary> Services the request. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public override StatusCode ServiceRequest()
        {


            // prepare for a new output message
            this.OutputMessage.PayloadValues.Clear();
            StatusCode result;
            switch ( this.InputMessage.AddressMode )
            {
                case AddressModes.None:
                    {
                        this.OutputMessage.MessageType = MessageType.Response;
                        this.OutputMessage.ModuleAddress = this.InputMessage.ModuleAddress;
                        this.OutputMessage.Command = this.InputMessage.Command;
                        this.OutputMessage.Payload = this.InputMessage.Payload;
                        this.OutputMessage.UpdateChecksum();
                        result = StatusCode.ServiceMessageReadyToSend;
                        break;
                    }

                case AddressModes.Broadcast:
                    {

                        // Broadcast Message
                        // illegal for commands returning value (for now)
                        // set flag that commands validate against
                        // 
                        result = StatusCode.CommandNotAvailable;
                        break;
                    }

                case AddressModes.Echo:
                    {
                        // Echo Message
                        // valid all commands
                        // call echo message routine - will send message
                        result = this.SendEchoResponse();
                        break;
                    }

                case AddressModes.SpecialFunction:
                    {

                        // Special Function
                        // Reserved / Undefined - will ignore message
                        result = StatusCode.CommandNotAvailable;
                        break;
                    }

                default:
                    {
                        // ERROR - more than 1 bit set
                        // invalid all commands
                        result = StatusCode.InvalidCommandData;
                        break;
                    }
            }

            // return status
            return result;
        }

        #endregion

    }
}
