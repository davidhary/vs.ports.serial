using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;

using isr.Core;
using isr.Ports.Serial;
using isr.Ports.Teleport.ExceptionExtensions;

namespace isr.Ports.Teleport
{

    /// <summary> The Scribe class writes and reads information to and from the module. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Scribe : Core.Models.ViewModelTalkerBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected Scribe() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="Scribe" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="role">                    The role. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        public Scribe( IEnumerable<byte> moduleAddress, MessengerRole role, IProtocolMessage templateProtocolMessage ) : this( role == MessengerRole.Emitter ? Emitter.Create( moduleAddress, templateProtocolMessage ) : Collector.Create( moduleAddress, templateProtocolMessage ) )
        {
            this.IsMessengerOwner = true;
        }

        /// <summary> Initializes a new instance of the <see cref="Scribe" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messenger"> The Messenger. </param>
        public Scribe( IMessenger messenger ) : base()
        {
            this.AssignMessengerThis( messenger );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( disposing )
                    {
                        this._Messenger?.Dispose();
                    }

                    this.Talker?.Listeners.Clear();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~Scribe()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets the is port open. </summary>
        /// <value> The is port open. </value>
        public virtual bool IsPortOpen => this.Messenger is object && this.Messenger.IsPortOpen;

        #endregion

        #region " MESSENGER MANAGEMENT "

        /// <summary> The messenger. </summary>
        private IMessenger _Messenger;

        /// <summary> Gets or sets the Messenger. </summary>
        /// <value> The Messenger. </value>
        public IMessenger Messenger
        {
            get => this._Messenger;

            set => this.AssignMessengerThis( value );
        }

        /// <summary> Assigns the Messenger. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignMessengerThis( IMessenger value )
        {
            if ( this._Messenger is object )
            {
                this._Messenger.ConnectionChanged -= this.MessengerConnectionChanged;
                this._Messenger.DataReceived -= this.MessengerDataReceived;
                this._Messenger.DataSent -= this.MessengerDataSent;
                this._Messenger.MessageReceived -= this.MessangeMessageReceived;
                this._Messenger.MessageSent -= this.MessengerMessageSent;
                this._Messenger.SerialPortDisposed -= this.MessengerSerialPortDisposed;
                this._Messenger.SerialPortErrorReceived -= this.MessengerSerialPortErrorReceived;
                this._Messenger.Timeout -= this.MessengerTimeout;
                // this also closes the session. 

                if ( value is null && this.IsMessengerOwner )
                    this._Messenger.Dispose();
            }

            this._Messenger = value;
            if ( this._Messenger is object )
            {
                this._Messenger.ConnectionChanged += this.MessengerConnectionChanged;
                this._Messenger.DataReceived += this.MessengerDataReceived;
                this._Messenger.DataSent += this.MessengerDataSent;
                this._Messenger.MessageReceived += this.MessangeMessageReceived;
                this._Messenger.MessageSent += this.MessengerMessageSent;
                this._Messenger.SerialPortDisposed += this.MessengerSerialPortDisposed;
                this._Messenger.SerialPortErrorReceived += this.MessengerSerialPortErrorReceived;
                this._Messenger.Timeout += this.MessengerTimeout;
            }
        }

        /// <summary> Gets the is Messenger that owns this item. </summary>
        /// <value> The is Messenger owner. </value>
        public bool IsMessengerOwner { get; private set; }

        /// <summary> Assigns a Messenger. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public virtual void AssignMessenger( IMessenger value )
        {
            this.IsMessengerOwner = false;
            this.Messenger = value;
        }

        /// <summary> Releases the Messenger. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected virtual void ReleaseMessenger()
        {
            this._Messenger = null;
        }

        #endregion

        #region " TRANSPORT "

        /// <summary> Gets the <see cref="Transport">Transport</see>. </summary>
        /// <value> The transport. </value>
        public ITransport Transport => this.Messenger.Transport;

        /// <summary> Sends an hexadecimal message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hexMessage"> The message hex characters. </param>
        public void SendMessage( string hexMessage )
        {
            if ( string.IsNullOrEmpty( hexMessage ) )
            {
                this.TryNotifyDataSent( new PortEventArgs( false ) );
            }
            else
            {
                this.SendMessage( hexMessage.ToHexBytes() );
            }
        }

        /// <summary> Sends the data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message"> The message. </param>
        public void SendMessage( IEnumerable<byte> message )
        {
            if ( message is null )
            {
                this.TryNotifyDataSent( new PortEventArgs( false ) );
            }
            else
            {
                this.Messenger.SendData( message );
            }
        }

        #endregion

        #region " TRANSMIT/RECEIVE LOG "

        /// <summary> Gets or sets a value indicating whether [log events]. </summary>
        /// <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
        public bool IsLogEvents
        {
            get => this._Messenger.IsLogEvents;

            set => this._Messenger.IsLogEvents = value;
        }

        /// <summary> Gets or sets the name of the event log file. </summary>
        /// <value> The name of the event log file. </value>
        public string EventLogFileName
        {
            get => this._Messenger.EventLogFileName;

            set => this._Messenger.EventLogFileName = value;
        }

        #endregion

        #region " QUERY COMMANDS: GENERIC "

        /// <summary> Issues a command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The message. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <param name="e">           Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public ITransport TryQuery( IProtocolMessage message, TimeSpan readTimeout, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( message is null )
                throw new ArgumentNullException( nameof( message ) );
            string activity = "issuing command";
            ITransport t = null;
            try
            {
                activity = $"issuing {message.CommandAscii}";
                t = this.Query( message, readTimeout );
                if ( t.IsSuccess() )
                {
                    e.Clear();
                }
                else
                {
                    e.RegisterFailure( $"failed {activity}: {t.FailureMessage()}" );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }

            return t;
        }

        /// <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="hexMessage">     The hex message. </param>
        /// <param name="turnaroundTime"> The turnaround time. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> The <see cref="ITransport">transport</see> </returns>
        public ITransport Query( string hexMessage, TimeSpan turnaroundTime, TimeSpan readTimeout )
        {
            return hexMessage is null
                ? throw new ArgumentNullException( nameof( hexMessage ) )
                : this.Query( hexMessage.ToHexBytes(), turnaroundTime, readTimeout );
        }

        /// <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="data">           The data. </param>
        /// <param name="turnaroundTime"> The turnaround time. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> The <see cref="ITransport">transport</see> </returns>
        public ITransport Query( IEnumerable<byte> data, TimeSpan turnaroundTime, TimeSpan readTimeout )
        {
            return data is null ? throw new ArgumentNullException( nameof( data ) ) : this.Messenger.Query( data, turnaroundTime, readTimeout );
        }

        /// <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message">     The message. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> The <see cref="ITransport">transport</see> </returns>
        public ITransport Query( IProtocolMessage message, TimeSpan readTimeout )
        {
            return this.Messenger.Query( message, readTimeout );
        }

        #endregion

        #region " RETRY COMMANDS "

        /// <summary> Gets or sets the retry count. </summary>
        /// <value> The retry count. </value>
        public int RetryCount { get; set; }

        /// <summary> Echo command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The message. </param>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <param name="pauseTime">   The pause time. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <param name="e">           Cancel details event information. </param>
        /// <returns> An ITransport. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public ITransport EchoCommand( IProtocolMessage message, int repeatCount, TimeSpan pauseTime, TimeSpan readTimeout, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( message is null )
                throw new ArgumentNullException( nameof( message ) );
            string activity = "Echoing command";
            ITransport t = null;
            try
            {
                this.RetryCount = 0;
                if ( !this.IsPortOpen )
                {
                    activity = $"Open port {this.Messenger.Port.PortParameters.PortName}";
                    _ = this.Messenger.Port.TryOpen( e );
                }

                if ( this.IsPortOpen )
                {
                    int countDown = repeatCount;
                    do
                    {
                        countDown -= 1;
                        activity = "applying address mode";
                        activity = $"querying {message.ModuleAddress} w/ {message.CommandAscii}";
                        t = this.Messenger.Query( message, readTimeout );
                        if ( t.IsSuccess() )
                        {
                            e.Clear();
                        }
                        else
                        {
                            e.RegisterFailure( $"failed {this.RetryCount} {activity}: {t.FailureMessage()}" );
                        }

                        if ( e.Failed )
                        {
                            this.RetryCount += 1;
                            ApplianceBase.DoEventsWait( pauseTime );
                        }
                    }
                    while ( countDown != 0 && e.Failed );
                    if ( !e.Failed )
                    {
                        activity = $"comparing TX:{t.SentHexMessage} to RX:{t.ReceivedHexMessage} of {message.ModuleAddress} w/ {message.CommandAscii}";
                        if ( !t.SentHexMessage.Equals( t.ReceivedHexMessage ) )
                        {
                            e.RegisterFailure( $"failed {activity}" );
                        }
                    }
                }
                else
                {
                    e.RegisterFailure( $"failed {activity}" );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }

            return t;
        }

        /// <summary> Queries until success or count down. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The message. </param>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <param name="pauseTime">   The pause time. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <param name="e">           Action event information. </param>
        /// <returns> An ITransport. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public ITransport Query( IProtocolMessage message, int repeatCount, TimeSpan pauseTime, TimeSpan readTimeout, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( message is null )
                throw new ArgumentNullException( nameof( message ) );
            string activity = "Issuing command";
            ITransport t = null;
            try
            {
                this.RetryCount = 0;
                if ( !this.IsPortOpen )
                {
                    activity = $"Opening port {this.Messenger.Port.PortParameters.PortName}";
                    _ = this.Messenger.Port.TryOpen( e );
                }

                if ( this.IsPortOpen )
                {
                    int countDown = repeatCount;
                    do
                    {
                        countDown -= 1;
                        activity = "applying address mode";
                        activity = $"querying {message.ModuleAddress} w/ {message.CommandAscii}";
                        t = this.Messenger.Query( message, readTimeout );
                        if ( t.IsSuccess() )
                        {
                            e.Clear();
                        }
                        else
                        {
                            e.RegisterFailure( $"failed {this.RetryCount} {activity}: {t.FailureMessage()}" );
                        }

                        if ( e.Failed )
                        {
                            this.RetryCount += 1;
                            ApplianceBase.DoEventsWait( pauseTime );
                        }
                    }
                    while ( countDown != 0 && e.Failed );
                }
                else
                {
                    e.RegisterFailure( $"failed {activity}" );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }

            return t;
        }

        #endregion

        #region " EVENT MANAGEMENT "

        #region " CONNECTION CHANGED "

        /// <summary> Raises the connection event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void NotifyConnectionChanged( ConnectionEventArgs e )
        {
            _ = this.Publish( TraceEventType.Information, $"port {(this.IsPortOpen ? "open" : "closed")}" );
            this.SyncNotifyConnectionChanged( e );
        }

        /// <summary> Removes the Connection Changed event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveConnectionChangedEventHandlers()
        {
            this._ConnectionChangedEventHandlers?.RemoveAll();
        }

        /// <summary> The Connection Changed event handlers. </summary>
        private readonly EventHandlerContextCollection<ConnectionEventArgs> _ConnectionChangedEventHandlers = new EventHandlerContextCollection<ConnectionEventArgs>();

        /// <summary> Event queue for all listeners interested in Connection Changed events.
        /// Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
        /// </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ConnectionEventArgs> ConnectionChanged
        {
            add {
                this._ConnectionChangedEventHandlers.Add( new EventHandlerContext<ConnectionEventArgs>( value ) );
            }

            remove {
                this._ConnectionChangedEventHandlers.RemoveValue( value );
            }
        }

        private void OnConnectionChanged( object sender, ConnectionEventArgs e )
        {
            this._ConnectionChangedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="ConnectionChanged">Connection Changed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ConnectionEventArgs"/> instance containing the event
        /// Connection. </param>
        protected void SyncNotifyConnectionChanged( ConnectionEventArgs e )
        {
            this._ConnectionChangedEventHandlers.Send( this, e );
        }

        /// <summary> Raises the connection event within a try catch clause. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyConnectionChanged( ConnectionEventArgs e )
        {
            string activity = "notifying serial port connection changed";
            try
            {
                this.NotifyConnectionChanged( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Messenger connection changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Connection event information. </param>
        private void MessengerConnectionChanged( object sender, ConnectionEventArgs e )
        {
            this.TryNotifyConnectionChanged( e );
        }


        #endregion

        #region " DATA RECEIVED "

        /// <summary> Notifies a data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        protected virtual void NotifyDataReceived( PortEventArgs e )
        {
            this.SyncNotifyDataReceived( e );
        }

        /// <summary> Try notify data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyDataReceived( PortEventArgs e )
        {
            string activity = "notifying data received";
            try
            {
                this.NotifyDataReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Removes the <see cref="DataReceived">data received</see> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveDataReceivedEventHandlers()
        {
            this._DataReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="DataReceived">data received</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<PortEventArgs> _DataReceivedEventHandlers = new EventHandlerContextCollection<PortEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="DataReceived">data received</see>/> events
        /// Receipt status is reported along with the received data in the receive buffer
        /// using the <see cref="PortEventArgs">port event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<PortEventArgs> DataReceived
        {
            add {
                this._DataReceivedEventHandlers.Add( new EventHandlerContext<PortEventArgs>( value ) );
            }

            remove {
                this._DataReceivedEventHandlers.RemoveValue( value );
            }
        }

        private void OnDataReceived( object sender, PortEventArgs e )
        {
            this._DataReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="DataReceived">Data Received Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
        protected void SyncNotifyDataReceived( PortEventArgs e )
        {
            this._DataReceivedEventHandlers.Send( this, e );
        }

        /// <summary> Messenger data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Port event information. </param>
        private void MessengerDataReceived( object sender, PortEventArgs e )
        {
            this.TryNotifyDataReceived( e );
        }

        #endregion

        #region " DATA SENT "

        /// <summary> Notifies a data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        private void NotifyDataSent( PortEventArgs e )
        {
            this.SyncNotifyDataSent( e );
        }

        /// <summary> Try notify data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyDataSent( PortEventArgs e )
        {
            string activity = "notifying data sent";
            try
            {
                this.NotifyDataSent( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Removes the <see cref="DataSent">Data Sent event</see> handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveDataSentEventHandlers()
        {
            this._DataSentEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="DataSent">Data Sent event</see> handlers. </summary>
        private readonly EventHandlerContextCollection<PortEventArgs> _DataSentEventHandlers = new EventHandlerContextCollection<PortEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
        /// Receipt status is reported along with the Sent data in the receive buffer
        /// using the <see cref="PortEventArgs">port event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<PortEventArgs> DataSent
        {
            add {
                this._DataSentEventHandlers.Add( new EventHandlerContext<PortEventArgs>( value ) );
            }

            remove {
                this._DataSentEventHandlers.RemoveValue( value );
            }
        }

        private void OnDataSent( object sender, PortEventArgs e )
        {
            this._DataSentEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="DataSent">Data Sent Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
        protected void SyncNotifyDataSent( PortEventArgs e )
        {
            this._DataSentEventHandlers.Send( this, e );
        }

        /// <summary> Messenger data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Port event information. </param>
        private void MessengerDataSent( object sender, PortEventArgs e )
        {
            this.TryNotifyDataSent( e );
        }

        #endregion

        #region " MESSAGE RECEIVED "

        /// <summary> Notifies a Message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Protocol event information. </param>
        protected virtual void NotifyMessageReceived( ProtocolEventArgs e )
        {
            this.SyncNotifyMessageReceived( e );
        }

        /// <summary> Try notify Message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyMessageReceived( ProtocolEventArgs e )
        {
            string activity = "notifying message received";
            try
            {
                this.NotifyMessageReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Removes the <see cref="MessageReceived">Message Received</see> event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveMessageReceivedEventHandlers()
        {
            this._MessageReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="MessageReceived">Message Received</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<ProtocolEventArgs> _MessageReceivedEventHandlers = new EventHandlerContextCollection<ProtocolEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="MessageReceived">Message Received</see> events.
        /// The Received message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ProtocolEventArgs> MessageReceived
        {
            add {
                this._MessageReceivedEventHandlers.Add( new EventHandlerContext<ProtocolEventArgs>( value ) );
            }

            remove {
                this._MessageReceivedEventHandlers.RemoveValue( value );
            }
        }

        private void OnMessageReceived( object sender, ProtocolEventArgs e )
        {
            this._MessageReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="MessageReceived">MessageReceived Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyMessageReceived( ProtocolEventArgs e )
        {
            this._MessageReceivedEventHandlers.Send( this, e );
        }

        /// <summary> Messange message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Protocol event information. </param>
        private void MessangeMessageReceived( object sender, ProtocolEventArgs e )
        {
            this.TryNotifyMessageReceived( e );
        }

        #endregion

        #region " MESSAGE SENT "

        /// <summary> Notifies a Message Sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Protocol event information. </param>
        private void NotifyMessageSent( ProtocolEventArgs e )
        {
            this.SyncNotifyMessageSent( e );
        }

        /// <summary> Try notify Message Sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyMessageSent( ProtocolEventArgs e )
        {
            string activity = "notifying message sent";
            try
            {
                this.NotifyMessageSent( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Removes the <see cref="MessageSent">Message Sent</see> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveMessageSentEventHandlers()
        {
            this._MessageSentEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="MessageSent">Message Sent</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<ProtocolEventArgs> _MessageSentEventHandlers = new EventHandlerContextCollection<ProtocolEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="MessageSent">Message Sent</see>events.
        /// The Sent message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ProtocolEventArgs> MessageSent
        {
            add {
                this._MessageSentEventHandlers.Add( new EventHandlerContext<ProtocolEventArgs>( value ) );
            }

            remove {
                this._MessageSentEventHandlers.RemoveValue( value );
            }
        }

        private void OnMessageSent( object sender, ProtocolEventArgs e )
        {
            this._MessageSentEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="MessageSent">Message Sent Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyMessageSent( ProtocolEventArgs e )
        {
            this._MessageSentEventHandlers.Send( this, e );
        }

        /// <summary> Messenger message sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Protocol event information. </param>
        private void MessengerMessageSent( object sender, ProtocolEventArgs e )
        {
            this.TryNotifyMessageSent( e );
        }

        #endregion

        #region " SERIAL PORT DISPOSED "

        /// <summary>
        /// Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifySerialPortDisposed( EventArgs e )
        {
            this.SyncNotifySerialPortDisposed( e );
        }

        /// <summary> Try notify serial port disposed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifySerialPortDisposed( EventArgs e )
        {
            string activity = "notifying serial port disposed";
            try
            {
                _ = this.Publish( TraceEventType.Information, activity );
                this.NotifySerialPortDisposed( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Removes the <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveSerialPortDisposedEventHandlers()
        {
            this._SerialPortDisposedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _SerialPortDisposedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port Disposed</see> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> SerialPortDisposed
        {
            add {
                this._SerialPortDisposedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._SerialPortDisposedEventHandlers.RemoveValue( value );
            }
        }

        private void OnSerialPortDisposed( object sender, EventArgs e )
        {
            this._SerialPortDisposedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="SerialPortDisposed">Serial Port Disposed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifySerialPortDisposed( EventArgs e )
        {
            this._SerialPortDisposedEventHandlers.Send( this, e );
        }

        /// <summary>
        /// Handles the Disposed event of the _serialPort control. Propagates the events to the calling
        /// controls.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void MessengerSerialPortDisposed( object sender, EventArgs e )
        {
            this.TryNotifySerialPortDisposed( e );
        }

        #endregion

        #region " SERIAL PORT ERROR RECEIVED "

        /// <summary> Raises the serial port error received event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void NotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            _ = this.Publish( TraceEventType.Error, "Serial port error type {0} occurred", e.EventType );
            this.SyncNotifySerialPortErrorReceived( e );
        }

        /// <summary> Try notify serial port error received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Serial error received event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            string activity = "notifying serial port error received";
            try
            {
                this.NotifySerialPortErrorReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the ErrorReceived event of the talker. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.IO.Ports.SerialErrorReceivedEventArgs" /> instance
        /// containing the event data. </param>
        private void MessengerSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this.TryNotifySerialPortErrorReceived( e );
        }

        /// <summary>
        /// Removes the <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event of
        /// the <see cref="IPort"/> Port.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveSerialPortErrorReceivedEventHandlers()
        {
            this._SerialPortErrorReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The SerialPortErrorReceived event handlers. </summary>
        private readonly EventHandlerContextCollection<SerialErrorReceivedEventArgs> _SerialPortErrorReceivedEventHandlers = new EventHandlerContextCollection<SerialErrorReceivedEventArgs>();

        /// <summary> Event queue for all listeners interested in the
        /// <see cref="SerialPortErrorReceived">Serial port error receivd </see>> event of the <see cref="IPort"/> Port.
        /// Serial Port Error Received status is reported with the
        /// <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
        /// </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<SerialErrorReceivedEventArgs> SerialPortErrorReceived
        {
            add {
                this._SerialPortErrorReceivedEventHandlers.Add( new EventHandlerContext<SerialErrorReceivedEventArgs>( value ) );
            }

            remove {
                this._SerialPortErrorReceivedEventHandlers.RemoveValue( value );
            }
        }

        private void OnSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this._SerialPortErrorReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="SerialPortErrorReceived">SerialPortErrorReceived Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="SerialErrorReceivedEventArgs" /> instance containing the event
        /// data. </param>
        protected void SyncNotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            this._SerialPortErrorReceivedEventHandlers.Send( this, e );
        }

        #endregion

        #region " TIMEOUT "

        /// <summary> Removes the <see cref="Timeout"/> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveTimeoutEventHandlers()
        {
            this._TimeoutEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="Timeout"/> event handlers. </summary>
        private readonly EventHandlerContextCollection<ProtocolEventArgs> _TimeoutEventHandlers = new EventHandlerContextCollection<ProtocolEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ProtocolEventArgs> Timeout
        {
            add {
                this._TimeoutEventHandlers.Add( new EventHandlerContext<ProtocolEventArgs>( value ) );
            }

            remove {
                this._TimeoutEventHandlers.RemoveValue( value );
            }
        }

        private void OnTimeout( object sender, ProtocolEventArgs e )
        {
            this._TimeoutEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="Timeout">Timeout Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyTimeout( ProtocolEventArgs e )
        {
            this._TimeoutEventHandlers.Send( this, e );
        }

        /// <summary> Notifies a timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        protected virtual void NotifyTimeout( ProtocolEventArgs e )
        {
            if ( e is object && this.Transport is object )
                this.Transport.TransportStatus = StatusCode.ReceiveTimeout;
            this.SyncNotifyTimeout( e );
        }

        /// <summary> Try notify data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyTimeout( ProtocolEventArgs e )
        {
            string activity = "notifying serial port timeout";
            try
            {
                this.NotifyTimeout( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Talker timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Protocol event information. </param>
        private void MessengerTimeout( object sender, ProtocolEventArgs e )
        {
            this.TryNotifyTimeout( e );
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
