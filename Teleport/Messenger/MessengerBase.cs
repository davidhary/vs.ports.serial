using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;

using isr.Core;
using isr.Ports.Serial;
using isr.Ports.Teleport.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.Ports.Teleport
{

    /// <summary>
    /// A communicator capable of controlling or emulating a Rooster module. The communicator could
    /// be a system controller also called <see cref="Emitter"/> following the SCPI/GPIB standard.
    /// The communicator could also be the Rooster module, or a <see cref="Collector"/> sending
    /// messages back to the controller.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-08 </para>
    /// </remarks>
    public abstract partial class MessengerBase : Core.Models.ViewModelTalkerBase, IMessenger
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MessengerBase" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="messengerRole">           The messenger role. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        protected MessengerBase( IEnumerable<byte> moduleAddress, MessengerRole messengerRole, IProtocolMessage templateProtocolMessage ) : this( moduleAddress, messengerRole, templateProtocolMessage, Serial.Port.Create() )
        {
            this.IsPortOwner = true;
        }

        /// <summary> Initializes a new instance of the <see cref="MessengerBase" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="messengerRole">           The messenger role. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        /// <param name="port">                    Specifies the
        /// <see cref="isr.Ports.Serial.IPort">port</see>. </param>
        protected MessengerBase( IEnumerable<byte> moduleAddress, MessengerRole messengerRole, IProtocolMessage templateProtocolMessage, IPort port ) : base()
        {

            // tag this as implementing the message parser. 
            // This instance will be assigned to the port as the message parser
            this.ParseEnabled = true;
            this.MessengerRole = messengerRole;
            this.ModuleAddress = moduleAddress;
            this.Transport = new Transport( templateProtocolMessage );
            this.AssignPort_( port );
            this.StreamTerminationValue = 13;
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this._Port?.Dispose();
                    this._StreamWriter?.Dispose();
                    this.OutputMessage?.Dispose();
                    this.InputMessage?.Dispose();
                    this.Transport?.Dispose();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~MessengerBase()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " STATUS CODE MANAGEMENT "

        /// <summary> Parses the status. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Indicator. </returns>
        public static Indicator ParseStatus( StatusCode value )
        {
            if ( Enum.IsDefined( typeof( StatusCode ), value ) )
            {
                switch ( value )
                {
                    case StatusCode.ValueNotSet:
                        {
                            return Indicator.None;
                        }

                    case StatusCode.Okay:
                        {
                            return Indicator.Okay;
                        }

                    case var @case when @case < StatusCode.InvalidBaud:
                        {
                            return Indicator.Warning;
                        }

                    case var case1 when case1 < StatusCode.ValueNotSet:
                        {
                            return Indicator.Error;
                        }

                    default:
                        {
                            return Indicator.Warning;
                        }
                }
            }
            else
            {
                return Indicator.None;
            }
        }

        /// <summary> Query if 'value' represents an error code. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if error; otherwise <c>false</c> </returns>
        public static bool IsErrorCode( StatusCode value )
        {
            return ParseStatus( value ) == Indicator.Error;
        }

        #endregion

        #region " TRANSMIT/RECEIVE LOG "

        /// <summary> True to log events. </summary>
        private bool _LogEvents;

        /// <summary> Gets or sets a value indicating whether [log events]. </summary>
        /// <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
        public bool IsLogEvents
        {
            get => this._LogEvents;

            set {
                this._LogEvents = value;
                if ( this._StreamWriter is null && value && !string.IsNullOrEmpty( this.EventLogFileName ) )
                {
                    this.OpenEventLogFile();
                }
            }
        }

        /// <summary> Gets the name of the event log file. </summary>
        /// <value> The name of the event log file. </value>
        public string EventLogFileName { get; set; }

        /// <summary> The stream writer. </summary>
        private System.IO.StreamWriter _StreamWriter;

        /// <summary> Opens the event log file. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void OpenEventLogFile()
        {
            this._StreamWriter = new System.IO.StreamWriter( this.EventLogFileName );
        }

        /// <summary> Logs the message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message"> The message. </param>
        /// <param name="isSent">  if set to <c>true</c> [is sent]. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LogMessage( string message, bool isSent )
        {
            string activity = "logging message";
            try
            {
                if ( this._StreamWriter is object )
                {
                    this._StreamWriter.WriteLine( "{0:HH:mm:ss:ffff},{1},{2}", DateTimeOffset.Now, Interaction.IIf( isSent, "Tx", "Rx" ), message );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MESSENGER MODULE AND SERVICING IMPLEMENTATON "

        /// <summary> Gets the module address for this messenger. </summary>
        /// <value> The module address. </value>
        public IEnumerable<byte> ModuleAddress { get; set; }

        /// <summary>
        /// Services an error. This is a strictly listener mode.  It sends back an error message to the
        /// talker.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> A StatusCode. </returns>
        public virtual StatusCode ServiceError( StatusCode status )
        {
            return StatusCode.Okay;
        }

        /// <summary> Services the request. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public virtual StatusCode ServiceRequest()
        {
            return StatusCode.Okay;
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets a value indicating whether this instance is open. </summary>
        /// <value> The is port open. </value>
        public bool IsPortOpen => this.Port is object && this.Port.IsOpen;

        #endregion

        #region " PORT MANAGEMENT "

        /// <summary> Gets or sets the data buffering option. </summary>
        /// <value> The data buffering option. </value>
        public DataBufferingOption InputBufferingOption
        {
            get => this._Port.InputBufferingOption;

            set => this._Port.InputBufferingOption = value;
        }

        /// <summary> The port. </summary>
        private IPort _Port;

        /// <summary> Gets or sets the port. </summary>
        /// <value> The port. </value>
        public IPort Port
        {
            get => this._Port;

            set => this.AssignPort_( value );
        }

        /// <summary> Gets the elapsed time stop watch. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The elapsed time stop watch. </value>
        protected Stopwatch ElapsedTimeStopwatch { get; private set; }

        /// <summary> Assigns the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignPort_( IPort value )
        {
            if ( this._Port is object )
            {
                this._Port.ConnectionChanged -= this.PortConnectionChanged;
                this._Port.DataReceived -= this.PortDataReceived;
                this._Port.DataSent -= this.PortDataSent;
                this._Port.SerialPortDisposed -= this.PortSerialPortDisposed;
                this._Port.SerialPortErrorReceived -= this.PortSerialPortErrorReceived;
                this._Port.Timeout -= this.PortTimeout;
                // this also closes the session. 
                if ( value is null && this.IsPortOwner )
                    this._Port.Dispose();
            }

            this.ElapsedTimeStopwatch = new Stopwatch();
            this._Port = value;
            if ( this._Port is object )
            {
                this._Port.ConnectionChanged += this.PortConnectionChanged;
                this._Port.DataReceived += this.PortDataReceived;
                this._Port.DataSent += this.PortDataSent;
                this._Port.SerialPortDisposed += this.PortSerialPortDisposed;
                this._Port.SerialPortErrorReceived += this.PortSerialPortErrorReceived;
                this._Port.Timeout += this.PortTimeout;
            }
        }

        /// <summary> Gets the is port that owns this item. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is port owner. </value>
        public bool IsPortOwner { get; private set; }

        /// <summary> Assigns a Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public virtual void AssignPort( IPort value )
        {
            this.IsPortOwner = false;
            this.Port = value;
            this.TryNotifyConnectionChanged();
        }

        /// <summary> Releases the Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected virtual void ReleasePort()
        {
            this._Port = null;
        }

        #endregion

        #region " RECEIVE MANAGMENT "

        /// <summary> The received message. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> A message describing the input. </value>
        public IProtocolMessage InputMessage { get; set; }

        /// <summary> Gets the last process status. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The last process status. </value>
        public StatusCode LastProcessStatus { get; protected set; }

        /// <summary>
        /// Gets the sentinel indicating if the messenger is implementing the message parser interface
        /// and has applied it to the <see cref="isr.Ports.Serial.Port">port.</see>
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The parse enabled. </value>
        public bool ParseEnabled { get; set; }

        /// <summary> Parses the received byte values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The byte values received from the port. </param>
        /// <returns> A StatusCode. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private StatusCode ParseReceivedMesssage( IEnumerable<byte> values )
        {
            string activity = "parsing";
            if ( values is null )
                values = Array.Empty<byte>();
            if ( this.IsLogEvents )
                this.LogMessage( values.ToHex(), false );
            this.InputMessage.Clear();
            StatusCode transportStatus;
            try
            {
                activity = "populating receive buffer";
                this._ReadPointer = 0;
                this._ReceivedBuffer = new List<byte>( values );
                activity = "parsing receive buffer into the input message";
                transportStatus = this.InputMessage.ParseStream( this._ReceivedBuffer );
                activity = "validating input against the output message";
                // if the message is okay, validate it against the output message. 
                if ( transportStatus == StatusCode.Okay )
                    transportStatus = this.OutputMessage.Validate( this.InputMessage );
                if ( transportStatus != StatusCode.Okay )
                {
                    _ = this.Publish( TraceEventType.Error, $"failed @{activity};. status ={transportStatus}" );
                }

                activity = "updating the transport information";
                this.UpdateTransportInfo( this.InputMessage, transportStatus, false );
            }
            catch ( Exception ex )
            {
                transportStatus = StatusCode.ExceptionParsing;
                this.UpdateTransportInfo( transportStatus );
                _ = this.PublishException( activity, ex );
            }

            this.LastProcessStatus = transportStatus;
            return transportStatus;
        }

        /// <summary>
        /// Parses byte values received from the port. This can be called automatically at the port when
        /// the messenger is set to be <see cref="ParseEnabled"/>
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The byte values received from the port. </param>
        /// <returns> An isr.Ports.Serial.MessageParserOutcome. </returns>
        public MessageParserOutcome Parse( IEnumerable<byte> values )
        {
            var outcome = MessageParserOutcome.None;
            if ( this.ParseEnabled )
            {
                var transportStatus = this.ParseReceivedMesssage( values );
                if ( transportStatus == StatusCode.Okay )
                {
                    outcome = MessageParserOutcome.Complete;
                }
                else if ( this.OutputMessage.IsIncomplete() )
                {
                    outcome = MessageParserOutcome.Incomplete;
                }
                else if ( this.OutputMessage.IsInvalid() )
                {
                    outcome = MessageParserOutcome.Invalid;
                }
                else
                {
                    // if the message is not defined as incomplete or invalid, it is complete even if broken
                    // for any reason. 
                    outcome = MessageParserOutcome.Complete;
                }
                // notify the top level of the received message to allow handling responses as necessary
                this.TryNotifyMessageReceived( new ProtocolEventArgs( transportStatus, this.InputMessage ) );
            }

            return outcome;
        }

        /// <summary> Process the received message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="receivedStatus"> The received status. </param>
        /// <param name="receivedData">   Information describing the received. </param>
        /// <returns> A StatusCode. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public StatusCode ProcessReceivedMessage( Indicator receivedStatus, IEnumerable<byte> receivedData )
        {
            string activity = "processing message received";
            this.InputMessage.Clear();
            var transportStatus = StatusCode.Okay;
            try
            {
                if ( this.IsPortOpen )
                {
                    if ( receivedStatus == Indicator.Okay )
                    {
                        if ( this.InputBufferingOption == DataBufferingOption.CircularBuffer )
                        {
                        }
                        else
                        {
                            activity = "building receive buffer";
                            this._ReadPointer = 0;
                            this._ReceivedBuffer = new List<byte>( receivedData );
                        }
                    }
                    else
                    {
                        _ = this.Publish( TraceEventType.Error, $"failed @{activity};. status ={receivedStatus}" );
                    }
                }
                else
                {
                    transportStatus = StatusCode.Closed;
                }
            }
            catch ( Exception ex )
            {
                transportStatus = StatusCode.ExceptionParsing;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.TryNotifyDataReceived( new PortEventArgs( receivedStatus == Indicator.Okay, receivedData ) );
            }

            try
            {
                if ( transportStatus == StatusCode.Okay )
                {
                    activity = "parsing receive buffer";
                    transportStatus = this.ParseReceivedMesssage( this._ReceivedBuffer );
                }
                else
                {
                    this.InputMessage.Clear();
                    this.InputMessage.Status = transportStatus;
                    _ = this.Publish( TraceEventType.Error, $"Receive failed;. status='{transportStatus}'" );
                }
            }
            catch ( Exception ex )
            {
                transportStatus = StatusCode.ExceptionParsing;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.UpdateTransportInfo( transportStatus );
                // notify the top level of the received message to allow handling responses as necessary
                this.TryNotifyMessageReceived( new ProtocolEventArgs( transportStatus, this.InputMessage ) );
            }

            this.LastProcessStatus = transportStatus;
            return transportStatus;
        }

        /// <summary>
        /// Handles the DataReceived event of the _serialPort control. Updates the data boxes and the
        /// received status.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the
        /// event data. </param>
        private void PortDataReceived( object sender, PortEventArgs e )
        {
            if ( this.ParseEnabled )
            {
                // if parse enabled, the 
                this.TryNotifyDataReceived( new PortEventArgs( e ) );
            }
            else
            {
                _ = this.ProcessReceivedMessage( e.StatusOkay ? Indicator.Okay : Indicator.Error, e.DataBuffer );
            }

            if ( this.MessengerRole == MessengerRole.Collector && IsErrorCode( this.InputMessage.Status ) )
            {
                _ = this.ServiceError( this.InputMessage.Status );
            }
        }

        #endregion

        #region " TRANSMIT MANAGEMENT "

        /// <summary> The sent message. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> A message describing the output. </value>
        public IProtocolMessage OutputMessage { get; set; }

        /// <summary>
        /// Holds a copy of the transmitted data.
        /// </summary>
        private List<byte> _TransmittedBuffer;

        /// <summary> Sends byte data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> The Byte data. </param>
        public void SendData( IEnumerable<byte> data )
        {
            // clear the input message.
            this.InputMessage.Clear();
            // clear the transport send and receive status.
            this.Transport.ClearSendReceiveStatus();
            if ( data?.Any() == true )
            {
                this.Port.SendData( data );
            }
        }

        /// <summary> Sends the specified message and returns a reply. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hexMessage">     The hex message. </param>
        /// <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
        /// module starts to transmit a response. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> An ITransport. </returns>
        public ITransport Query( string hexMessage, TimeSpan turnaroundTime, TimeSpan readTimeout )
        {
            return this.Query( hexMessage.ToHexBytes(), turnaroundTime, readTimeout );
        }

        /// <summary> Sends the specified data and returns a reply. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="data">           The data. </param>
        /// <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
        /// module starts to transmit a response. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> An ITransport. </returns>
        public ITransport Query( IEnumerable<byte> data, TimeSpan turnaroundTime, TimeSpan readTimeout )
        {
            if ( data?.Any() == true )
            {
                this.Port.Resync();
                this.SendData( data );

                // TO_DO: Change to task; See VI Session Base wait for service request.
                ApplianceBase.DoEventsWait( turnaroundTime );
                var sw = Stopwatch.StartNew();
                while ( this.Transport.ReceiveStatus == StatusCode.ValueNotSet && sw.Elapsed <= readTimeout )
                {
                    ApplianceBase.DoEvents();
                    Thread.SpinWait( 10 );
                    ApplianceBase.DoEvents();
                    Thread.SpinWait( 10 );
                }
            }
            else
            {
                throw new ArgumentNullException( nameof( data ) );
            }

            return new Transport( this.Transport );
        }

        /// <summary> Sends the specified message and returns a reply. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The protocol message. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> An ITransport. </returns>
        public ITransport Query( IProtocolMessage message, TimeSpan readTimeout )
        {
            return message is null
                ? throw new ArgumentNullException( nameof( message ) )
                : this.Query( message.BuildStream( this.MessengerRole ), this.Transport.SelectCommand( message.CommandAscii ).TurnaroundTime, readTimeout );
        }

        /// <summary> Sends the protocol message. </summary>
        /// <remarks>
        /// Assumes data buffer/structure for appropriate channel has been loaded with data for transmit.
        /// </remarks>
        /// <returns> <see cref="StatusCode">global system status value</see> </returns>
        protected StatusCode SendProtocolMessage()
        {
            this._Port.SendData( this.OutputMessage.BuildStream( this.MessengerRole ) );
            return StatusCode.Okay;
        }

        /// <summary> Echo back the received message as is. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        protected virtual StatusCode SendEchoResponse()
        {
            this.OutputMessage.CopyFrom( this.InputMessage );
            return this.SendProtocolMessage();
        }

        /// <summary>
        /// Gets the messenger role. The messenger could be a talker (emitter or controller), controlling
        /// a module or a listener (collector or receiver) emulating a module.
        /// </summary>
        /// <value> The messenger role. </value>
        public MessengerRole MessengerRole { get; }

        #endregion

        #region " PORT DATA SENT MANAGEMENT "

        /// <summary> Parses the sent byte values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The byte values sent from the port. </param>
        /// <returns> A StatusCode. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private StatusCode ParseSentMesssage( IEnumerable<byte> values )
        {
            string activity = "parsing";
            if ( values is null )
                values = Array.Empty<byte>();
            if ( this.IsLogEvents )
                this.LogMessage( values.ToHex(), false );
            this.InputMessage.Clear();
            StatusCode transportStatus;
            try
            {
                activity = "populating transmitter buffer";
                this._TransmittedBuffer = new List<byte>( values );
                activity = "parsing output message";
                transportStatus = this.OutputMessage.ParseStream( this._TransmittedBuffer );
                if ( transportStatus != StatusCode.Okay )
                {
                    _ = this.Publish( TraceEventType.Error, $"failed @{activity};. status={transportStatus}" );
                }

                activity = "updating the transport information";
                this.UpdateTransportInfo( this.OutputMessage, transportStatus, true );
            }
            catch ( Exception ex )
            {
                transportStatus = StatusCode.ExceptionParsing;
                this.UpdateTransportInfo( transportStatus );
                _ = this.PublishException( activity, ex );
            }

            this.LastProcessStatus = transportStatus;
            return transportStatus;
        }

        /// <summary> Process the sent message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sentStatus"> The sent status. </param>
        /// <param name="sentData">   Information describing the sent. </param>
        /// <returns> A StatusCode. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public StatusCode ProcessSentMessage( Indicator sentStatus, IEnumerable<byte> sentData )
        {
            string activity = "processing message sent";
            this.OutputMessage.Clear();
            var transportStatus = StatusCode.Okay;
            try
            {
                if ( this.IsPortOpen )
                {
                    if ( this.IsLogEvents )
                    {
                        activity = "logging message";
                        this.LogMessage( sentData.ToHex(), true );
                    }
                }
                else
                {
                    transportStatus = StatusCode.Closed;
                }

                if ( sentStatus != Indicator.Okay )
                {
                    _ = this.Publish( TraceEventType.Error, $"failed @{activity};. status={sentStatus}" );
                }
            }
            catch ( Exception ex )
            {
                transportStatus = StatusCode.ExceptionParsing;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.TryNotifyDataSent( new PortEventArgs( sentStatus == Indicator.Okay, sentData ) );
            }

            try
            {
                if ( transportStatus == StatusCode.Okay )
                {
                    transportStatus = this.ParseSentMesssage( sentData );
                }
                else
                {
                    this.OutputMessage.Clear();
                    this.OutputMessage.Status = transportStatus;
                    _ = this.Publish( TraceEventType.Error, $"Transmit failed;. status='{transportStatus}'" );
                }
            }
            catch ( Exception ex )
            {
                transportStatus = StatusCode.ExceptionParsing;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.UpdateTransportInfo( transportStatus );
                // notify the top level of the sent message to allow handling responses as necessary
                this.TryNotifyMessageSent( new ProtocolEventArgs( transportStatus, this.OutputMessage ) );
            }

            this.LastProcessStatus = transportStatus;
            return transportStatus;
        }

        /// <summary>
        /// Handles the DataSent event of the _serialPort control. Updates controls and data.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the
        /// event data. </param>
        private void PortDataSent( object sender, PortEventArgs e )
        {
            _ = this.ProcessSentMessage( e.StatusOkay ? Indicator.Okay : Indicator.Error, e.DataBuffer );
        }

        /// <summary> Handles the Connection Changed event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="isr.Ports.Serial.ConnectionEventArgs" /> instance
        /// containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortConnectionChanged( object sender, ConnectionEventArgs e )
        {
            string activity = "notifying connection changed";
            try
            {
                this.TryNotifyConnectionChanged();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRANSPORT "

        /// <summary> The transport. </summary>

        /// <summary> Gets the <see cref="Transport">Transport</see>. </summary>
        /// <value> The transport. </value>
        public ITransport Transport { get; }

        /// <summary> Updates the <see cref="Transport">Transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="status"> The <see cref="StatusCode">status code</see> </param>
        private void UpdateTransportInfo( StatusCode status )
        {
            if ( this.Transport is object )
                this.Transport.TransportStatus = status;
        }

        /// <summary> Updates the <see cref="Transport">Transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> Message describing the protocol. </param>
        /// <param name="transportStatus"> The transport status. </param>
        /// <param name="isSent">          if set to <c>true</c> [is sent]. </param>
        private void UpdateTransportInfo( IProtocolMessage protocolMessage, StatusCode transportStatus, bool isSent )
        {
            if ( protocolMessage is object && this.Transport is object )
            {
                if ( isSent )
                {
                    _ = protocolMessage.ParseStream();
                    this.Transport.SentProtocolMessage = protocolMessage;
                    if ( protocolMessage.HasStatusValue )
                        this.Transport.SendStatus = protocolMessage.Status;
                    if ( transportStatus != StatusCode.Okay )
                        this.Transport.TransportStatus = transportStatus;
                }
                else
                {
                    _ = protocolMessage.ParseStream();
                    this.Transport.ReceivedProtocolMessage = protocolMessage;
                    if ( protocolMessage.HasStatusValue )
                        this.Transport.ReceiveStatus = protocolMessage.Status;
                    this.Transport.TransportStatus = transportStatus;
                }
            }
        }

        #endregion

        #region " EVENT MANAGEMENT "

        #region " CONNECTION CHANGED "

        /// <summary> Raises the connection event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifyConnectionChanged( ConnectionEventArgs e )
        {
            this.Port.MessageParser = this.Port.IsOpen ? this : null;
            string activity = $"notifying {this.Port.SerialPort.PortName} port {(this.IsPortOpen ? "open" : "closed")}";
            _ = this.Publish( TraceEventType.Information, activity );
            this.SyncNotifyConnectionChanged( e );
        }

        /// <summary> Raises the connection event within a try catch clause. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyConnectionChanged( ConnectionEventArgs e )
        {
            string activity = "notifying connection changed";
            try
            {
                this.NotifyConnectionChanged( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Safely Invokes the <see cref="ConnectionChanged">connection changed event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void TryNotifyConnectionChanged()
        {
            this.TryNotifyConnectionChanged( new ConnectionEventArgs( this.IsPortOpen ) );
        }

        /// <summary> Raises the connection event within a try catch clause. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="isOpen"> True if is open, false if not. </param>
        protected void TryNotifyConnectionChanged( bool isOpen )
        {
            // clear the i/o messages
            this.InputMessage.Clear();
            this.OutputMessage.Clear();
            this.TryNotifyConnectionChanged( new ConnectionEventArgs( isOpen ) );
        }

        /// <summary>
        /// Invokes the <see cref="ConnectionChanged">connection changed event</see>. Must be called with
        /// the <see cref="SynchronizationContext">sync context</see>
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="obj"> The obj. </param>
        private void UnsafeInvokeConnectionChanged( object obj )
        {
            this.UnsafeInvokeConnectionChanged( ( ConnectionEventArgs ) obj );
        }

        /// <summary> Removes the Connection Changed event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveConnectionChangedEventHandlers()
        {
            this._ConnectionChangedEventHandlers?.RemoveAll();
        }

        /// <summary> The Connection Changed event handlers. </summary>
        private readonly EventHandlerContextCollection<ConnectionEventArgs> _ConnectionChangedEventHandlers = new EventHandlerContextCollection<ConnectionEventArgs>();

        /// <summary> Event queue for all listeners interested in Connection Changed events.
        /// Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
        /// </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ConnectionEventArgs> ConnectionChanged
        {
            add {
                this._ConnectionChangedEventHandlers.Add( new EventHandlerContext<ConnectionEventArgs>( value ) );
            }

            remove {
                this._ConnectionChangedEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the connection event. </summary>
        /// <remarks>   David, 2020-11-17. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnConnectionChanged( object sender, ConnectionEventArgs e )
        {
            this._ConnectionChangedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="ConnectionChanged">Connection Changed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ConnectionEventArgs"/> instance containing the event
        /// Connection. </param>
        protected void SyncNotifyConnectionChanged( ConnectionEventArgs e )
        {
            this._ConnectionChangedEventHandlers.Send( this, e );
        }

        #endregion

        #region " DATA RECEIVED "

        /// <summary> Notifies a data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        private void NotifyDataReceived( PortEventArgs e )
        {
            this.SyncNotifyDataReceived( e );
        }

        /// <summary> Try notify data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyDataReceived( PortEventArgs e )
        {
            string activity = "notifying data received";
            try
            {
                this.NotifyDataReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Removes the <see cref="DataReceived">data received</see> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveDataReceivedEventHandlers()
        {
            this._DataReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="DataReceived">data received</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<PortEventArgs> _DataReceivedEventHandlers = new EventHandlerContextCollection<PortEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="DataReceived">data received</see>/> events
        /// Receipt status is reported along with the received data in the receive buffer
        /// using the <see cref="PortEventArgs">port event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<PortEventArgs> DataReceived
        {
            add {
                this._DataReceivedEventHandlers.Add( new EventHandlerContext<PortEventArgs>( value ) );
            }

            remove {
                this._DataReceivedEventHandlers.RemoveValue( value );
            }
        }

        private void OnDataReceived( object sender, PortEventArgs e )
        {
            this._DataReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="DataReceived">Data Received Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
        protected void SyncNotifyDataReceived( PortEventArgs e )
        {
            this._DataReceivedEventHandlers.Send( this, e );
        }

        #endregion

        #region " DATA SENT "

        /// <summary> Notifies a data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        private void NotifyDataSent( PortEventArgs e )
        {
            this.SyncNotifyDataSent( e );
        }

        /// <summary> Try notify data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyDataSent( PortEventArgs e )
        {
            string activity = "notifying data sent";
            try
            {
                this.NotifyDataSent( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Removes the <see cref="DataSent">Data Sent event</see> handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveDataSentEventHandlers()
        {
            this._DataSentEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="DataSent">Data Sent event</see> handlers. </summary>
        private readonly EventHandlerContextCollection<PortEventArgs> _DataSentEventHandlers = new EventHandlerContextCollection<PortEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
        /// Receipt status is reported along with the Sent data in the receive buffer
        /// using the <see cref="PortEventArgs">port event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<PortEventArgs> DataSent
        {
            add {
                this._DataSentEventHandlers.Add( new EventHandlerContext<PortEventArgs>( value ) );
            }

            remove {
                this._DataSentEventHandlers.RemoveValue( value );
            }
        }

        private void OnDataSent( object sender, PortEventArgs e )
        {
            this._DataSentEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="DataSent">Data Sent Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
        protected void SyncNotifyDataSent( PortEventArgs e )
        {
            this._DataSentEventHandlers.Send( this, e );
        }

        #endregion

        #region " MESSAGE RECEIVED "

        /// <summary> Notifies a Message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Protocol event information. </param>
        private void NotifyMessageReceived( ProtocolEventArgs e )
        {
            this.SyncNotifyMessageReceived( e );
        }

        /// <summary> Try notify Message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyMessageReceived( ProtocolEventArgs e )
        {
            string activity = "notifying message received";
            try
            {
                this.NotifyMessageReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Removes the <see cref="MessageReceived">Message Received</see> event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveMessageReceivedEventHandlers()
        {
            this._MessageReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="MessageReceived">Message Received</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<ProtocolEventArgs> _MessageReceivedEventHandlers = new EventHandlerContextCollection<ProtocolEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="MessageReceived">Message Received</see> events.
        /// The Received message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ProtocolEventArgs> MessageReceived
        {
            add {
                this._MessageReceivedEventHandlers.Add( new EventHandlerContext<ProtocolEventArgs>( value ) );
            }

            remove {
                this._MessageReceivedEventHandlers.RemoveValue( value );
            }
        }

        private void OnMessageReceived( object sender, ProtocolEventArgs e )
        {
            this._MessageReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="MessageReceived">MessageReceived Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyMessageReceived( ProtocolEventArgs e )
        {
            this._MessageReceivedEventHandlers.Send( this, e );
        }

        #endregion

        #region " MESSAGE SENT "

        /// <summary> Notifies a Message Sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Protocol event information. </param>
        private void NotifyMessageSent( ProtocolEventArgs e )
        {
            this.SyncNotifyMessageSent( e );
        }

        /// <summary> Try notify Message Sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyMessageSent( ProtocolEventArgs e )
        {
            string activity = "notifying message sent";
            try
            {
                this.NotifyMessageSent( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Removes the <see cref="MessageSent">Message Sent</see> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveMessageSentEventHandlers()
        {
            this._MessageSentEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="MessageSent">Message Sent</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<ProtocolEventArgs> _MessageSentEventHandlers = new EventHandlerContextCollection<ProtocolEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="MessageSent">Message Sent</see>events.
        /// The Sent message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ProtocolEventArgs> MessageSent
        {
            add {
                this._MessageSentEventHandlers.Add( new EventHandlerContext<ProtocolEventArgs>( value ) );
            }

            remove {
                this._MessageSentEventHandlers.RemoveValue( value );
            }
        }

        private void OnMessageSent( object sender, ProtocolEventArgs e )
        {
            this._MessageSentEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="MessageSent">Message Sent Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyMessageSent( ProtocolEventArgs e )
        {
            this._MessageSentEventHandlers.Send( this, e );
        }

        #endregion

        #region " SERIAL PORT DISPOSED "

        /// <summary>
        /// Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifySerialPortDisposed( EventArgs e )
        {
            this.SyncNotifySerialPortDisposed( e );
        }

        /// <summary> Try notify serial port disposed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifySerialPortDisposed( EventArgs e )
        {
            string activity = "notifying serial port disposed";
            try
            {
                _ = this.Publish( TraceEventType.Information, activity );
                this.NotifySerialPortDisposed( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Port disposed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PortSerialPortDisposed( object sender, EventArgs e )
        {
            this.TryNotifySerialPortDisposed( e );
        }

        /// <summary>
        /// Removes the <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveSerialPortDisposedEventHandlers()
        {
            this._SerialPortDisposedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _SerialPortDisposedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port Disposed</see> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> SerialPortDisposed
        {
            add {
                this._SerialPortDisposedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._SerialPortDisposedEventHandlers.RemoveValue( value );
            }
        }

        private void OnSerialPortDisposed( object sender, EventArgs e )
        {
            this._SerialPortDisposedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="SerialPortDisposed">Serial Port Disposed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifySerialPortDisposed( EventArgs e )
        {
            this._SerialPortDisposedEventHandlers.Send( this, e );
        }

        #endregion

        #region " SERIAL PORT ERROR RECEIVED "

        /// <summary> Raises the serial port error received event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            _ = this.Publish( TraceEventType.Warning, $"Error {e.EventType} occurred in serial port {this.Port.SerialPort.PortName}" );
            switch ( e.EventType )
            {
                case SerialError.Frame:
                    {
                        this.UpdateTransportInfo( StatusCode.FrameError );
                        break;
                    }

                case SerialError.Overrun:
                    {
                        this.UpdateTransportInfo( StatusCode.ReceiveOverrun );
                        break;
                    }

                case SerialError.RXOver:
                    {
                        this.UpdateTransportInfo( StatusCode.ReceiveOverrun );
                        break;
                    }

                case SerialError.RXParity:
                    {
                        this.UpdateTransportInfo( StatusCode.ParityError );
                        break;
                    }

                case SerialError.TXFull:
                    {
                        this.UpdateTransportInfo( StatusCode.TransmitOverrun );
                        break;
                    }

                default:
                    {
                        this.UpdateTransportInfo( StatusCode.TransmitFailed );
                        break;
                    }
            }

            this.SyncNotifySerialPortErrorReceived( e );
        }

        /// <summary> Try notify serial port error received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Serial error received event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            string activity = "notifying serial port error received";
            try
            {
                this.NotifySerialPortErrorReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event of
        /// the <see cref="IPort"/> Port.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.IO.Ports.SerialErrorReceivedEventArgs" /> instance
        /// containing the event data. </param>
        private void PortSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this.TryNotifySerialPortErrorReceived( e );
        }

        /// <summary>
        /// Removes the <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event of
        /// the <see cref="IPort"/> Port.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveSerialPortErrorReceivedEventHandlers()
        {
            this._SerialPortErrorReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The SerialPortErrorReceived event handlers. </summary>
        private readonly EventHandlerContextCollection<SerialErrorReceivedEventArgs> _SerialPortErrorReceivedEventHandlers = new EventHandlerContextCollection<SerialErrorReceivedEventArgs>();

        /// <summary> Event queue for all listeners interested in the
        /// <see cref="SerialPortErrorReceived">Serial port error receivd </see>> event of the <see cref="IPort"/> Port.
        /// Serial Port Error Received status is reported with the
        /// <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
        /// </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<SerialErrorReceivedEventArgs> SerialPortErrorReceived
        {
            add {
                this._SerialPortErrorReceivedEventHandlers.Add( new EventHandlerContext<SerialErrorReceivedEventArgs>( value ) );
            }

            remove {
                this._SerialPortErrorReceivedEventHandlers.RemoveValue( value );
            }
        }

        private void OnSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this._SerialPortErrorReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="SerialPortErrorReceived">SerialPortErrorReceived Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="SerialErrorReceivedEventArgs" /> instance containing the event
        /// data. </param>
        protected void SyncNotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            this._SerialPortErrorReceivedEventHandlers.Send( this, e );
        }

        #endregion

        #region " TIMEOUT MANAGER "

        /// <summary> Notifies a timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifyTimeout( ProtocolEventArgs e )
        {
            if ( e is object && this.Transport is object )
                this.Transport.TransportStatus = StatusCode.ReceiveTimeout;
            this.SyncNotifyTimeout( e );
        }

        /// <summary> Try notify data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyTimeout( ProtocolEventArgs e )
        {
            string activity = "notifying serial port timeout";
            try
            {
                this.NotifyTimeout( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Safely posts the <see cref="Timeout">Timeout event</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void TryNotifyTimeout()
        {
            this.TryNotifyTimeout( new ProtocolEventArgs( StatusCode.ReceiveTimeout, this.InputMessage ) );
        }

        /// <summary> Handles the Timeout event of the _port control. Propagates the timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void PortTimeout( object sender, EventArgs e )
        {
            this.TryNotifyTimeout();
        }

        /// <summary> Removes the <see cref="Timeout"/> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveTimeoutEventHandlers()
        {
            this._TimeoutEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="Timeout"/> event handlers. </summary>
        private readonly EventHandlerContextCollection<ProtocolEventArgs> _TimeoutEventHandlers = new EventHandlerContextCollection<ProtocolEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ProtocolEventArgs> Timeout
        {
            add {
                this._TimeoutEventHandlers.Add( new EventHandlerContext<ProtocolEventArgs>( value ) );
            }

            remove {
                this._TimeoutEventHandlers.RemoveValue( value );
            }
        }

        private void OnTimeout( object sender, ProtocolEventArgs e )
        {
            this._TimeoutEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="Timeout">Timeout Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyTimeout( ProtocolEventArgs e )
        {
            this._TimeoutEventHandlers.Send( this, e );
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
