using System;
using System.ComponentModel;

namespace isr.Ports.Teleport
{

    /// <summary> Enumerates the addressing mode. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    [Flags()]
    public enum AddressModes
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None,

        /// <summary> An enum constant representing the broadcast option. </summary>
        /// <remarks>
        /// illegal for commands returning value (for now) set flag that commands validate against
        /// </remarks>
        [Description( "Broadcast Message" )]
        Broadcast = 1,

        /// <summary> An enum constant representing the echo option. </summary>
        /// <remarks>
        /// valid all commands. call echo message routine - will send message
        /// </remarks>
        [Description( "Echo Message" )]
        Echo = 2,

        /// <summary> An enum constant representing the special function option. </summary>
        /// <remarks>
        /// Reserved / Undefined - will ignore message
        /// </remarks>
        [Description( "Special Function" )]
        SpecialFunction = 4
    }

    /// <summary> Values that represent message prompt. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public enum MessageType
    {

        /// <summary> An enum constant representing the none option. Required if the message type cannot be parsed from the prefix. </summary>
        [Description( "Unknown" )]
        None,

        /// <summary> An enum constant representing the command option. </summary>
        [Description( "Command to module" )]
        Command,

        /// <summary> An enum constant representing the response option. </summary>
        [Description( "Response from module" )]
        Response,

        /// <summary> An enum constant representing the failure option. </summary>
        [Description( "Error response" )]
        Failure
    }

    /// <summary> Enumerates the status codes. Global usage for function state. </summary>
    /// <remarks>
    /// This is a general status list that will give more detail of problems used for function return
    /// status.
    /// </remarks>
    public enum StatusCode : byte
    {

        /// <summary> An enum constant representing the okay option. </summary>
        /// general status
        [Description( "Okay" )]
        Okay = 0,

        /// <summary> An enum constant representing the transmit buffer empty option. </summary>
        [Description( "Transmit Buffer Empty" )]
        TransmitBufferEmpty,

        /// <summary> An enum constant representing the receive data available option. </summary>
        [Description( "Receive Data Available" )]
        ReceiveDataAvailable,

        /// <summary> An enum constant representing the service message available option. </summary>
        [Description( "Service Message Available" )]
        ServiceMessageAvailable,

        /// <summary> An enum constant representing the service message for decode option. </summary>
        [Description( "Service Message For Decode" )]
        ServiceMessageForDecode,

        /// <summary> An enum constant representing the service message ready to send option. </summary>
        [Description( "Service Message Ready To Send" )]
        ServiceMessageReadyToSend,

        /// <summary> An enum constant representing the data acquired option. </summary>
        [Description( "Data Acquired" )]
        DataAcquired,

        /// <summary> An enum constant representing the alarm state changed option. </summary>
        [Description( "Alarm State Changed" )]
        AlarmStateChanged,

        /// <summary> An enum constant representing the system state changed option. </summary>
        [Description( "System State Changed" )]
        SystemStateChanged,

        /// <summary> An enum constant representing the enable module option. </summary>
        [Description( "Enable Module" )]
        EnableModule,

        /// <summary> An enum constant representing the disable module option. </summary>
        [Description( "Disable Module" )]
        DisableModule,

        /// <summary> An enum constant representing the invalid baud option. </summary>
        /// communication errors
        [Description( "Invalid Baud Rate" )]
        InvalidBaud,

        /// <summary> An enum constant representing the message incomplete option. </summary>
        [Description( "Message Incomplete" )]
        MessageIncomplete,

        /// <summary> An enum constant representing the checksum invalid option. </summary>
        [Description( "Checksum Invalid" )]
        ChecksumInvalid,

        /// <summary> An enum constant representing the transmit overrun option. </summary>
        [Description( "Transmit Overrun" )]
        TransmitOverrun,

        /// <summary> An enum constant representing the receive overrun option. </summary>
        [Description( "Receive Overrun" )]
        ReceiveOverrun,

        /// <summary> An enum constant representing the receive timeout option. </summary>
        [Description( "Receive Timeout" )]
        ReceiveTimeout,

        /// <summary> An enum constant representing the port closed option. </summary>
        [Description( "Port Closed" )]
        PortClosed,

        /// <summary> An enum constant representing the invalid stream start option. </summary>
        [Description( "Invalid Initiation" )]
        InvalidStreamStart,

        /// <summary> An enum constant representing the invalid stream end option. </summary>
        [Description( "Invalid termination" )]
        InvalidStreamEnd,

        /// <summary> An enum constant representing the warning detected option. </summary>
        /// data errors
        [Description( "Warning Detected" )]
        WarningDetected,

        /// <summary> An enum constant representing the alarm detected option. </summary>
        [Description( "Alarm Detected" )]
        AlarmDetected,

        /// <summary> An enum constant representing the fault detected option. </summary>
        [Description( "Fault Detected" )]
        FaultDetected,

        /// <summary> Module shut-down. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [Description( "Module Shutdown" )]
        ModuleShutdown,

        /// <summary> An enum constant representing the voltage supply threshold option. </summary>
        [Description( "Voltage Supply Threshold" )]
        VoltageSupplyThreshold,

        /// <summary> An enum constant representing the negative overflow option. </summary>
        [Description( "Negative Overflow" )]
        NegativeOverflow,

        /// <summary> An enum constant representing the positive overflow option. </summary>
        [Description( "Positive Overflow" )]
        PositiveOverflow,

        /// <summary> An enum constant representing the command error option. </summary>
        /// message errors (Service Level)
        [Description( "Command Error" )]
        CommandError,

        /// <summary> An enum constant representing the invalid response prompt option. </summary>
        [Description( "Invalid response prompt" )]
        InvalidResponsePrompt,

        /// <summary> An enum constant representing the invalid command code option. </summary>
        [Description( "Invalid Command Code" )]
        InvalidCommandCode,

        /// <summary> An enum constant representing the invalid command data option. </summary>
        [Description( "Invalid Command Data" )]
        InvalidCommandData,

        /// <summary> An enum constant representing the invalid message data option. </summary>
        [Description( "Invalid Message Data" )]
        InvalidMessageData,

        /// <summary> An enum constant representing the access denied option. </summary>
        [Description( "Access Denied" )]
        AccessDenied,

        /// <summary> An enum constant representing the command not available option. </summary>
        [Description( "Command Not Available" )]
        CommandNotAvailable,

        /// <summary> An enum constant representing the data not available option. </summary>
        [Description( "Data Not Available" )]
        DataNotAvailable,

        /// <summary> An enum constant representing the invalid parameter option. </summary>
        /// system errors
        [Description( "Invalid Parameter" )]
        InvalidParameter,

        /// <summary> An enum constant representing the message too short option. Message Parsing Error. </summary>
        [Description( "Message too short" )]
        MessageTooShort,

        /// <summary> An enum constant representing the invalid module address option. Message Parsing Error. </summary>
        [Description( "Invalid module address" )]
        InvalidModuleAddress,

        /// <summary> An enum constant representing the invalid message length option. Message Parsing Error. </summary>
        [Description( "Mismatch between message length and actual data length" )]
        InvalidMessageLength,

        /// <summary> An enum constant representing the unknown status code option. Message Parsing Error. </summary>
        [Description( "Unknown status code" )]
        UnknownStatusCode,

        /// <summary> An enum constant representing the payload parse error option. Message Parsing Error. </summary>
        [Description( "Payload parse error" )]
        PayloadParseError,

        /// <summary> An enum constant representing the closed option. Transport Failure. </summary>
        [Description( "Not connected" )]
        Closed,

        /// <summary> An enum constant representing the exception parsing option. Transport Failure. </summary>
        [Description( "Exception occurred parsing message" )]
        ExceptionParsing,

        /// <summary> An enum constant representing the transmit failed option. Transport Failure. </summary>
        [Description( "Transmit failed" )]
        TransmitFailed,

        /// <summary> An enum constant representing the receive failed option. Transport Failure. </summary>
        [Description( "Receive failed" )]
        ReceiveFailed,
        /// <summary>
        /// An enum constant representing the incorrect controller address received option. Transport Failure.
        /// </summary>
        [Description( "Received incorrect controller address" )]
        IncorrectControllerAddressReceived,
        /// <summary>
        /// An enum constant representing the incorrect module address received option. Transport Failure.
        /// </summary>
        [Description( "Received incorrect module address" )]
        IncorrectModuleAddressReceived,

        /// <summary> An enum constant representing the incorrect command received option. Transport Failure. </summary>
        [Description( "Received incorrect command" )]
        IncorrectCommandReceived,

        /// <summary> An enum constant representing the frame error option. Transport Failure. </summary>
        [Description( "The hardware detected a frame error" )]
        FrameError,

        /// <summary> An enum constant representing the parity error option. Transport Failure. </summary>
        [Description( "The hardware detected a parity error" )]
        ParityError,

        /// <summary> An enum constant representing the value not set option. Transport Failure. Required for determining if a status was set. </summary>
        [Description( "Value not set" )]
        ValueNotSet = 255
    }

    /// <summary> Values that represent the status code indicators. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public enum Indicator
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "None" )]
        None,

        /// <summary> An enum constant representing the okay option. </summary>
        [Description( "Okay" )]
        Okay,

        /// <summary> An enum constant representing the warning option. </summary>
        [Description( "Warning" )]
        Warning,

        /// <summary> An enum constant representing the error] option. </summary>
        [Description( "Error" )]
        Error
    }
}
