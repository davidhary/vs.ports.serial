﻿using System;
using System.Collections.Generic;

namespace isr.Ports.Teleport
{

    /// <summary> Defines an interface to the protocol message. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IProtocolMessage : ICloneable, IDisposable, System.ComponentModel.INotifyPropertyChanged, Serial.IMessageParser
    {

        #region " CONTENTS "

        /// <summary> Gets or sets the address mode. </summary>
        /// <value> The address mode. </value>
        AddressModes AddressMode { get; set; }

        /// <summary> Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        IEnumerable<byte> ModuleAddress { get; set; }

        /// <summary> Gets or sets the module address in Ascii. </summary>
        /// <value> The controller address hexadecimal. </value>
        string ModuleAddressAscii { get; set; }

        /// <summary> Gets or sets the message type. </summary>
        /// <value> The message type. </value>
        MessageType MessageType { get; set; }

        /// <summary>
        /// Gets or sets the length of the message including the address byte, checksum, command and
        /// payload.
        /// </summary>
        /// <value> The length of the message. </value>
        int TotalMessageLength { get; set; }

        /// <summary> Gets or sets the command in ASCII. </summary>
        /// <value> The command in Ascii. </value>
        string CommandAscii { get; set; }

        /// <summary> Gets or sets the command. </summary>
        /// <value> The command. </value>
        IEnumerable<byte> Command { get; set; }

        /// <summary> Gets or sets the command timeout. </summary>
        /// <value> The command timeout. </value>
        TimeSpan CommandTimeout { get; set; }

        /// <summary>
        /// Gets or sets the payload.  The represents the message excluding the command and checksum. Not
        /// every message has a payload.
        /// </summary>
        /// <value> The payload. </value>
        IEnumerable<byte> Payload { get; set; }

        /// <summary> Gets or sets the payload in hex. </summary>
        /// <value> The payload hexadecimal. </value>
        string PayloadHex { get; }

        /// <summary> Gets or sets the checksum. </summary>
        /// <value> The checksum. </value>
        IEnumerable<byte> Checksum { get; set; }

        /// <summary> Gets or sets the checksum hexadecimal. </summary>
        /// <value> The checksum hexadecimal. </value>
        string ChecksumHex { get; set; }

        /// <summary> Gets or sets a value indicating whether this instance has status value. </summary>
        /// <value> The has status value. </value>
        bool HasStatusValue { get; }

        /// <summary> Gets or sets the status. </summary>
        /// <value> The status. </value>
        StatusCode Status { get; set; }

        #endregion

        #region " STREAM DELINEATION "

        /// <summary> Gets or sets the stream initiation byte(s). </summary>
        /// <value> The stream initiation byte(s). </value>
        IEnumerable<byte> StreamInitiation { get; set; }

        /// <summary> Gets or sets the stream termination bytes(s). </summary>
        /// <value> The stream termination bytes(s). </value>
        IEnumerable<byte> StreamTermination { get; set; }

        #endregion

        #region " PREFIX "

        /// <summary> Gets or sets the command, response or error prefix. </summary>
        /// <value> The command, response or error prompt. </value>
        byte Prefix { get; set; }

        /// <summary> Gets or sets the command, response or error prefix in ASCII. </summary>
        /// <value> The command, response or error prefix in ASCII. </value>
        string PrefixAscii { get; set; }

        #endregion

        #region " CONTENT MANAGEMENT "

        /// <summary> Clears the contents of an existing <see cref="IProtocolMessage" />. </summary>
        void Clear();

        /// <summary> Copies the contents of an existing <see cref="IProtocolMessage" />. </summary>
        /// <param name="protocolMessage"> The validated protocol message. </param>
        void CopyFrom( IProtocolMessage protocolMessage );

        #endregion

        #region " PAYLOAD MANAGEMENT "

        /// <summary> Gets or sets the payload values. </summary>
        /// <value> The payload values. </value>
        ByteCollection PayloadValues { get; }

        /// <summary> Notifies the payload changed. </summary>
        void NotifyPayloadChanged();

        #endregion

        #region " STREAM "

        /// <summary> Gets or sets a list of streams. </summary>
        /// <value> A List of streams. </value>
        ByteCollection StreamValues { get; }

        /// <summary> Gets or sets the stream. </summary>
        /// <value> The stream. </value>
        IEnumerable<byte> Stream { get; set; }

        /// <summary> Gets or sets the encoding. </summary>
        /// <value> The encoding. </value>
        System.Text.Encoding Encoding { get; set; }

        /// <summary> Gets or sets a list of internal streams. </summary>
        /// <value> A List of internal streams. </value>
        ByteCollection InternalStreamValues { get; }

        /// <summary>
        /// Gets or sets the internal stream excluding the initiation and termination characters.
        /// </summary>
        /// <value> The internal stream. </value>
        IEnumerable<byte> InternalStream { get; set; }

        /// <summary> Gets or sets a message describing the internal ASCII. </summary>
        /// <value> A message describing the internal ASCII. </value>
        string InternalAsciiMessage { get; }

        /// <summary> Gets or sets a message as hexadecimal. </summary>
        /// <value> A message describing the hexadecimal. </value>
        string HexMessage { get; }

        #endregion

        #region " PARSERS "

        /// <summary> Determines whether the message status signifies an incomplete message. </summary>
        /// <returns>
        /// <c>true</c> if the specified status is incomplete; otherwise, <c>false</c>.
        /// </returns>
        bool IsIncomplete();

        /// <summary> Determines whether the message status signifies an invalid message. </summary>
        /// <returns> <c>true</c> if the message is invalid; otherwise, <c>false</c>. </returns>
        bool IsInvalid();

        /// <summary> Parse hexadecimal message. </summary>
        /// <param name="hexMessage"> A message describing the hexadecimal. </param>
        /// <returns> A StatusCode. </returns>
        StatusCode ParseHexMessage( string hexMessage );

        /// <summary>
        /// Parses the <paramref name="data">byte data</paramref> into a protocol message.
        /// </summary>
        /// <param name="messageType"> The message type. </param>
        /// <param name="data">        The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        StatusCode ParseStream( MessageType messageType, IEnumerable<byte> data );

        /// <summary>
        /// Parses the <paramref name="data">byte data</paramref> into a protocol message.
        /// </summary>
        /// <param name="data"> The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        StatusCode ParseStream( IEnumerable<byte> data );

        /// <summary>
        /// Parses the <paramref name="data">byte data</paramref> into a protocol message.
        /// </summary>
        /// <param name="messengerRole"> The messenger Role. </param>
        /// <param name="data">          The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        StatusCode ParseStream( MessengerRole messengerRole, IEnumerable<byte> data );

        /// <summary> Parses the stream into a protocol message. </summary>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        StatusCode ParseStream();

        /// <summary> Validates the protocol message relative to this protocol message. </summary>
        /// <param name="protocolMessage"> The validated protocol message. </param>
        /// <returns> A StatusCode. </returns>
        StatusCode Validate( IProtocolMessage protocolMessage );

        #endregion

        #region " BUILDERS "

        /// <summary> Calculates the checksum hexadecimal. </summary>
        /// <returns> The calculated checksum hexadecimal. </returns>
        string CalculateChecksumHex();

        /// <summary> Calculates the checksum. </summary>
        /// <returns> The calculated checksum. </returns>
        IEnumerable<byte> CalculateChecksum();

        /// <summary> Calculates and sets the checksum value. </summary>
        void UpdateChecksum();

        /// <summary> Builds the message stream including termination and initiation bytes. </summary>
        /// <param name="messageType"> The message type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        IEnumerable<byte> BuildStream( MessageType messageType );

        /// <summary> Builds the message stream including termination and initiation bytes. </summary>
        /// <param name="messengerRole"> The messenger role. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        IEnumerable<byte> BuildStream( MessengerRole messengerRole );

        /// <summary> Converts a value to a message type. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a MessageType. </returns>
        MessageType ToMessageType( MessengerRole value );

        #endregion

    }
}