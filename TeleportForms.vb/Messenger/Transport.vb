Imports System.Drawing
Imports System.Windows.Forms
Imports System.Windows.Threading
Imports System.IO
Imports isr.Ports.Serial
Imports isr.Core.DispatcherExtensions
Imports isr.Core.EnumExtensions

''' <summary>
''' Includes <see cref="IProtocolMessage">protocol messages</see> that are sent and received from
''' the module and their status values.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Transport
    Inherits isr.Ports.Teleport.Transport

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    Public Sub New(ByVal templateProtocolMessage As IProtocolMessage)
        MyBase.New(templateProtocolMessage)
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transportProtocolMessage"> Message describing the transport protocol. </param>
    ''' <param name="sentProtocolMessage">      Message describing the sent protocol. </param>
    ''' <param name="receivedProtocolMessage">  Message describing the received protocol. </param>
    Public Sub New(ByVal transportProtocolMessage As IProtocolMessage, sentProtocolMessage As IProtocolMessage, receivedProtocolMessage As IProtocolMessage)
        MyBase.New(transportProtocolMessage, sentProtocolMessage, receivedProtocolMessage)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transport"> The message. </param>
    Public Sub New(ByVal transport As ITransport)
        MyBase.New(transport)
    End Sub

#End Region

#Region " PARSE GRID ROW "

    ''' <summary> Parses the specified row. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="row"> The row. </param>
    Public Sub Parse(ByVal row As DataGridViewRow)

        Me.Clear()
        If row Is Nothing Then
            Return
        End If
        For Each cell As DataGridViewCell In row.Cells

            Select Case row.DataGridView.Columns(cell.ColumnIndex).Name
                Case NameOf(Transport.ItemNumber)
                    Me.ItemNumber = CType(cell.Value, Integer)
                Case NameOf(Transport.TransportStatus)
                    If cell.Value IsNot Nothing Then
                        Me.TransportStatus = CType(cell.Value, StatusCode)
                    End If
                Case NameOf(Transport.TransportHexMessage)
                    Dim stringValue As String = String.Empty
                    stringValue = TryCast(cell.Value, String)
                    If Not String.IsNullOrEmpty(stringValue) Then
                        Me.TransportProtocolMessage.ParseHexMessage(stringValue)
                    End If
                Case NameOf(Transport.SentHexMessage)
                    Dim stringValue As String = String.Empty
                    stringValue = TryCast(cell.Value, String)
                    If Not String.IsNullOrEmpty(stringValue) Then
                        Me.SentProtocolMessage.ParseHexMessage(stringValue)
                    End If
                Case NameOf(Transport.SendStatus)
                    If cell.Value IsNot Nothing Then
                        Me.ReceiveStatus = CType(cell.Value, StatusCode)
                    End If
                Case NameOf(Transport.ReceivedHexMessage)
                    Dim stringValue As String = TryCast(cell.Value, String)
                    If Not String.IsNullOrEmpty(stringValue) Then
                        Me.ReceivedProtocolMessage.ParseHexMessage(stringValue)
                    End If
                Case NameOf(Transport.ReceiveStatus)
                    If cell.Value IsNot Nothing Then
                        Me.ReceiveStatus = CType(cell.Value, StatusCode)
                    End If
                Case NameOf(Transport.CommandDescription)
                    ' ignored.
                Case Else
                    If row.DataGridView.Columns(cell.ColumnIndex).Visible Then
                        ' this no longer works because not all columns are displayed and the visible
                        ' property does not seem to mark an invisible column
                        ' Debug.Assert(False, "Unhandled Column Name")
                    End If
            End Select
        Next
    End Sub

#End Region
End Class

''' <summary> Implements a collection of <see cref="Transport">Transport</see> entities. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Class TransportCollection
    Inherits isr.Ports.Teleport.TransportCollection

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TransportCollection" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transport"> The transport. </param>
    Public Sub New(ByVal transport As ITransport)
        MyBase.New(transport)
    End Sub

#End Region

#Region " DISPLAY MANAGEMENT "

    ''' <summary> Configure display values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="grid"> The grid. </param>
    ''' <returns> An Integer. </returns>
    Public Function ConfigureDisplayValues(ByVal grid As DataGridView) As Integer

        If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

        Dim wasEnabled As Boolean = grid.Enabled
        grid.Enabled = False
        grid.Enabled = wasEnabled

        grid.Enabled = False
        grid.DataSource = Nothing
        grid.AlternatingRowsDefaultCellStyle.BackColor = Drawing.Color.LightSteelBlue
        grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
        grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True
        grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
        grid.AutoGenerateColumns = False
        grid.RowHeadersVisible = False
        grid.ReadOnly = True
        grid.DataSource = Me

        grid.Columns.Clear()
        grid.Refresh()
        Dim displayIndex As Integer = 0
        Dim width As Integer = 0
        Dim column As DataGridViewTextBoxColumn = Nothing
        Try
            displayIndex = 0
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.ItemNumber),
                .Name = "Item",
                .Width = 40,
                .DisplayIndex = displayIndex
            }
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.CommandDescription),
                .Name = "Description",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = 70
            }
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.TransportStatus),
                .Name = "Status",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = 50
            }
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.TransportHexMessage),
                .Name = "Message",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = 50
            }
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.SendStatus),
                .Name = "Send",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = 50
            }
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.SentHexMessage),
                .Name = "Sent",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = 50
            }

            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.ReceiveStatus),
                .Name = "Receive",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = 50
            }
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(Transport.ReceivedHexMessage),
                .Name = "Received",
                .Visible = True,
                .DisplayIndex = displayIndex,
                .Width = grid.Width - width - grid.Columns.Count
            }
            grid.Columns.Add(column)
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        grid.Enabled = True
        Return If(grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0, grid.Columns.Count, 0)

    End Function

    ''' <summary> Displays the values described by grid. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="grid"> The grid. </param>
    ''' <returns> An Integer. </returns>
    Public Function DisplayValues(ByVal grid As DataGridView) As Integer

        If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

        Dim wasEnabled As Boolean = grid.Enabled
        grid.Enabled = False
        grid.Enabled = wasEnabled

        If grid.DataSource Is Nothing Then
            Me.ConfigureDisplayValues(grid)
            isr.Core.ApplianceBase.DoEvents()
        End If

        grid.DataSource = Me.ToList
        isr.Core.ApplianceBase.DoEvents()
        Return grid.Columns.Count

    End Function


#End Region

End Class

''' <summary> A class handling the implementation of display ordering. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Class ColumnDisplayOrderCollection

    Inherits List(Of KeyValuePair(Of String, Integer))

    ''' <summary> Adds key. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="key">   The key. </param>
    ''' <param name="value"> The value. </param>
    Public Overloads Sub Add(ByVal key As String, ByVal value As Integer)
        Me.Add(New KeyValuePair(Of String, Integer)(key, value))
    End Sub

End Class

