''' <summary> Defines an event arguments class for <see cref="Port">port messages</see>. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class MessageEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="details"> The details. </param>
    Public Sub New(ByVal details As String)
        Me.New(Diagnostics.TraceEventType.Information, details)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="severity"> The severity. </param>
    ''' <param name="details">  The details. </param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal details As String)
        MyBase.New()
        Me.Severity = severity
        If String.IsNullOrEmpty(details) Then
            details = String.Empty
        End If
        Me.Details = details
    End Sub

#End Region

    ''' <summary> Gets or sets the message severity. </summary>
    ''' <value> The severity. </value>
    Public ReadOnly Property Severity As Diagnostics.TraceEventType

    ''' <summary> Gets or sets the message details. </summary>
    ''' <value> The details. </value>
    Public ReadOnly Property Details As String

End Class
