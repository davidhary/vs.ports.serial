''' <summary> Defines an event arguments class for <see cref="Port">port messages</see>. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class PortEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(True, Array.Empty(Of Byte)())
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="okay"> if set to <c>True</c> [okay]. </param>
    Public Sub New(ByVal okay As Boolean)
        Me.New(okay, Array.Empty(Of Byte)())
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="okay"> if set to <c>True</c> [okay]. </param>
    ''' <param name="data"> The data. </param>
    Public Sub New(ByVal okay As Boolean, ByVal data As Byte())
        MyBase.New()
        Me.StatusOkay = okay
        Me._DataBuffer = data
    End Sub

#End Region

    ''' <summary>
    ''' Gets or sets a value indicating whether status is okay, true; Otherwise, False.
    ''' </summary>
    ''' <value> The status okay. </value>
    Public ReadOnly Property StatusOkay As Boolean

    ''' <summary> Buffer for data data. </summary>
    Private ReadOnly _DataBuffer As Byte()

    ''' <summary> Gets the data buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A Byte() </returns>
    Public Function DataBuffer() As Byte()
        Return Me._DataBuffer
    End Function

End Class
