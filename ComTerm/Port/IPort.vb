''' <summary> Defines the interface for the <see cref="Port">serial port</see> </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-08-31, 0.1.4626 Created </para>
''' </remarks>
Public Interface IPort

    Inherits IDisposable

#Region " CONNECTION INFO "

    ''' <summary> Gets or sets the time in ms the Data Received handler waits. </summary>
    ''' <value> The receive delay. </value>
    Property ReceiveDelay() As Integer

    ''' <summary>
    ''' Gets or sets the value defining the number of counts to wait before the data received event
    ''' fires.
    ''' </summary>
    ''' <value> The threshold. </value>
    Property Threshold() As Integer

#End Region

#Region " CONNECTION STATUS "

    ''' <summary> Gets or sets a value indicating whether this instance is connected. </summary>
    ''' <value> The is connected. </value>
    ReadOnly Property IsConnected As Boolean

    ''' <summary> Gets or sets a value indicating whether this instance is last send okay. </summary>
    ''' <value> The is last send okay. </value>
    ReadOnly Property IsLastSendOkay As Boolean

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Connects the port. </summary>
    ''' <param name="portParameters"> The port parameters ordered by the
    '''                               <see cref="PortParameterIndex">index</see>
    '''                               {"COM1", "9600", "8", "None", "One", "1", "1"} </param>
    Sub Connect(ByVal portParameters() As String)

    ''' <summary> Disconnects the port. </summary>
    Sub Disconnect()

#End Region

#Region " DATA MANAGEMENT "

    ''' <summary> Sends data. </summary>
    ''' <param name="data"> byte array data. </param>
    Sub SendData(ByVal data() As Byte)

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Occurs when connection changed.
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
    ''' </summary>
    Event ConnectionChanged As EventHandler(Of ConnectionEventArgs)

    ''' <summary>
    ''' Occurs when data is received.
    ''' Reception status is report along with the received data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataReceived As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when data is Sent.
    ''' Reception status is report along with the Sent data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataSent As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when Message is Available.
    ''' </summary>
    Event MessageAvailable As EventHandler(Of MessageEventArgs)

#End Region

End Interface
