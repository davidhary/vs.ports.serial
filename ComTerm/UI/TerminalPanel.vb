Imports System.Text
Imports System.Globalization
Imports System.IO
Imports System.IO.Ports
Imports System.Drawing
Imports System.Collections.Generic

''' <summary> Demo Serial Port terminal Ellen RAMCKE 2012. </summary>
''' <remarks> update 2012-06-27. </remarks>
Public Class TerminalPanel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TerminalPanel" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(New Port)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="TerminalPanel" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> Specifies the <see cref="Port">port</see>. </param>
    Public Sub New(ByVal port As Port)

        MyBase.New()

        Me.InitializeComponent()

        Me._Port = port
        Me._CharacterWidth = 8.3 ' pixels / Char 
        Me._CharsInline = 80  ' def n start
        Me._PortParameters = Port.DefaultPortParameters
        Me._ConfigFileName = My.Application.Info.AssemblyName & ".ini"
    End Sub

    ''' <summary> Disposes the managed resources. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub DisposeManagedResources()
        If Me._Port IsNot Nothing Then
            Me._Port.Dispose()
            Me._Port = Nothing
        End If
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the Load event of the form control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.OnFormShown()
    End Sub

    ''' <summary> form resize. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_ResizeEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ResizeEnd
        Me._CharsInline = Me.SetRuler(Me._RecvTextBox, Me._ShowReceivedHexCheckBox.Checked)
        Me._CharsInline = Me.SetRuler(Me._XmitTextBox, Me._ShowXmitHexCheckBox.Checked)
    End Sub

    ''' <summary> Handles the display of the controls. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub OnFormShown()

        ' set the form caption
        Me.Text = isr.Core.ApplicationInfo.BuildApplicationDescriptionCaption("")

        Me.Text &= $" Version {My.Application.Info.Version.Major}.{My.Application.Info.Version.Minor:00}"

        Me._StatusLabel.Text = "Terminal connect: none"
        Me.SetRuler(Me._RecvTextBox, False)
        Me.SetRuler(Me._XmitTextBox, False)

        ' read available ports on system
        Dim portNames As String() = SerialPort.GetPortNames

        If portNames.Length > 0 Then
            Me._PortNumberCombo.Text = portNames(0)
        Else
            Me._StatusLabel.Text = "no ports detected"
            Exit Sub
        End If

        Me._PortNumberCombo.Items.Clear()
        Me._PortNumberCombo.Items.AddRange(portNames)
        Me.OnConnectionChanged(Me._Port.IsConnected)

    End Sub

    ''' <summary> exit. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitMenuItem.Click
        Me.Close()
    End Sub

#End Region

#Region " TRANSMIT MANAGEMENT "

    ''' <summary>
    ''' Handles the Click event of the Transmit CopyMenuItem control. Copies a transmit box selection.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub XmitCopyMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitCopyMenuItem.Click
        Me._XmitTextBox.Copy()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Transmit Paste Menu Item control. Pases a transmit box
    ''' selection.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub XmitPasteMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitPasteMenuItem.Click
        Me._XmitTextBox.Paste()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Transmit Cut Menu Item control. Cuts the XMIT text box item.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub XmitCutMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitCutMenuItem.Click
        Me._XmitTextBox.Cut()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Transmit SendMenuItem control. Sends position of caret line
    ''' from transmit text box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub XmitSendMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitSendMenuItem.Click
        Dim loc As Integer = Me._XmitTextBox.GetFirstCharIndexOfCurrentLine
        Dim ln As Integer = Me._XmitTextBox.GetLineFromCharIndex(loc)
        If ln > 0 Then
            Me.SendData(Me._XmitTextBox.Lines(ln))
            Me._XmitMessageCombo.Text = String.Empty
        End If
    End Sub

    ''' <summary> Sends only selection in tx box to com. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub XmitSendSelectionMenuItemt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitSendSelectionMenuItem.Click
        If Me._XmitTextBox.SelectionLength > 0 Then
            Me.SendData(Me._XmitTextBox.SelectedText)
            Me._XmitMessageCombo.Text = String.Empty
        End If
    End Sub

    ''' <summary> fetches a line from the transmit box to the transmit message box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub XmitTextBox_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _XmitTextBox.MouseClick
        Dim rb As RichTextBox = CType(sender, RichTextBox)
        Dim loc As Integer = rb.GetFirstCharIndexOfCurrentLine
        Dim ln As Integer = rb.GetLineFromCharIndex(loc)
        Me._XmitMessageCombo.Text = rb.Lines(ln)
    End Sub

    ''' <summary> Clears the transmit box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ClearTransmitBox()
        Me._XmitTextBox.Clear()
        Me._XmitTextBox.Text = "1"
        Me._CharsInline = Me.SetRuler(Me._XmitTextBox, Me._ShowXmitHexCheckBox.Checked)
        Me.TransmitCount = 0
        Me._TransmitStatusLabel.Image = If(Me.IsConnected, My.Resources.LedOrange, My.Resources.LedGray)
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _ClearTransmitBoxButton control. Clears the transmit box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ClearTransmitBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearTransmitBoxButton.Click
        Me.ClearTransmitBox()
    End Sub

    ''' <summary> Build hex string in text box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub XmitMessageCombo_TextUpdate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitMessageCombo.TextUpdate
        If Me._EnterXmitHexCheckBox.Checked Then
            Dim cb As ToolStripComboBox = CType(sender, ToolStripComboBox)
            cb.Text = cb.Text.ToHexFormatted
            cb.SelectionStart = cb.Text.Length
        End If


    End Sub

    ''' <summary>
    ''' Event handler to suppress ding sound on enter. Called by _LotNumberTextBox for key down
    ''' events.
    ''' </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub SuppressDingHandler(sender As Object, e As KeyEventArgs) Handles _XmitMessageCombo.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Return Then
            e.Handled = True
            e.SuppressKeyPress = True
        End If
    End Sub

    ''' <summary> Event handler. Called by _XmitMessageCombo for key up events. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub EnterKeyHandler(sender As Object, e As KeyEventArgs) Handles _XmitMessageCombo.KeyUp
        If e Is Nothing Then Return
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Return Then
            Me.SendData(Me._XmitMessageCombo.Text)
            Me._XmitMessageCombo.Text = String.Empty
        ElseIf Me._EnterXmitHexCheckBox.Checked AndAlso Not e.KeyValue.IsHexadecimal Then
            e.Handled = True
        End If
    End Sub

    ''' <summary>
    ''' Handles the KeyPress event of the cboEnterMessage control. Enter only allowed keys in hex
    ''' mode.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.KeyPressEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub XmitMessageCombo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _XmitMessageCombo.KeyPress
        If e.KeyChar <> vbCr Then
            If Me._EnterXmitHexCheckBox.Checked Then
                If Not e.KeyChar.IsHexadecimal Then
                    e.Handled = True
                End If
            End If
        Else
            Me.SendData(Me._XmitMessageCombo.Text)
            Me._XmitMessageCombo.Text = String.Empty
        End If
    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _ShowXmitHexCheckBox control. Toggles showing the
    ''' transmit data in hex.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ShowXmitHexCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ShowXmitHexCheckBox.CheckedChanged
        Me._CharsInline = Me.SetRuler(Me._XmitTextBox, Me._ShowXmitHexCheckBox.Checked)
    End Sub

    ''' <summary> Gets or sets a value indicating whether show transmit data in hex. </summary>
    ''' <value> <c>True</c> if [show transmit hex data]; otherwise, <c>False</c>. </value>
    Public Property ShowTransmitHexData As Boolean
        Get
            Return Me._ShowXmitHexCheckBox.Checked
        End Get
        Set(value As Boolean)
            Me._ShowXmitHexCheckBox.Checked = value
        End Set
    End Property

    ''' <summary> Number of transmits. </summary>
    Private _TransmitCount As Integer

    ''' <summary> Gets or sets the transmit count. </summary>
    ''' <value> The transmit count. </value>
    Public Property TransmitCount As Integer
        Get
            Return Me._TransmitCount
        End Get
        Set(value As Integer)
            Me._TransmitCount = value
            Me._TransmitCountLabel.Text = $"{Me.TransmitCount:D6}"
        End Set
    End Property

    ''' <summary> Sends the data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="textData"> The text data. </param>
    Public Sub SendData(ByVal textData As String)

        Dim data() As Byte = Nothing
        If String.IsNullOrEmpty(textData) Then
            Return
        End If

        If Not Me._EnterXmitHexCheckBox.Checked Then
            If Me._AddCarriageReturnCheckBox.Checked Then textData &= ControlChars.Cr
            If Me._AddLineFeedCheckBox.Checked Then textData &= ControlChars.Lf
            data = Encoding.ASCII.GetBytes(textData)
        Else
            data = ConvertHexToBytes(textData)
        End If

        ' send data:
        Me._Port.SendData(data)

        Me.TransmitCount += data.Length
        Me._TransmitStatusLabel.Image = My.Resources.LedGray

        ' display in box:
        If Me._ShowXmitHexCheckBox.Checked And Not Me._ShowXmitAsciiCheckBox.Checked Then
            AppendBytes(Me._XmitTextBox, data, Me._CharsInline, False)
        ElseIf Me._ShowXmitHexCheckBox.Checked And Me._ShowXmitAsciiCheckBox.Checked Then
            AppendBytes(Me._XmitTextBox, data, Me._CharsInline, True)
        ElseIf Not Me._ShowXmitHexCheckBox.Checked And Me._ShowXmitAsciiCheckBox.Checked Then
            Me._XmitTextBox.ScrollToCaret()
            Me._XmitTextBox.AppendText(textData & vbCr)
        End If

        ' Store the data in the transmit combo box.
        Me._XmitMessageCombo.Items.Add(textData)

    End Sub

#End Region

#Region " RECEIVE HANDLERS "

    ''' <summary> Number of receives. </summary>
    Private _ReceiveCount As Integer

    ''' <summary> Gets or sets the Receive count. </summary>
    ''' <value> The Receive count. </value>
    Public Property ReceiveCount As Integer
        Get
            Return Me._ReceiveCount
        End Get
        Set(value As Integer)
            Me._ReceiveCount = value
            Me._ReceiveCountLabel.Text = $"{Me.ReceiveCount:D6}"
        End Set
    End Property

    ''' <summary>
    ''' Handles the Click event of the _ClearReceiveBoxButton control. clear receive box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ClearReceiveBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearReceiveBoxButton.Click
        Me._RecvTextBox.Clear()
        Me._RecvTextBox.Text = "1"
        Me._CharsInline = Me.SetRuler(Me._RecvTextBox, Me._ShowReceivedHexCheckBox.Checked)
        Me.ReceiveCount = 0
        Me._ReceiveStatusLabel.Image = If(Me.IsConnected, My.Resources.LedOrange, My.Resources.LedGray)

    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _ShowReceivedHexCheckBox control. View hex in receive
    ''' box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ShowReceivedHexCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ShowReceivedHexCheckBox.CheckedChanged
        Me._CharsInline = Me.SetRuler(Me._RecvTextBox, Me._ShowReceivedHexCheckBox.Checked)
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _SaveReceiveTextBoxButton control. Save receive box box to
    ''' file.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    Private Sub SaveReceiveTextBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveReceiveTextBoxButton.Click

        Me._SaveFileDialog.DefaultExt = "*.TXT"
        Me._SaveFileDialog.Filter = "Text files (*.txt)|*.txt"

        If Me._SaveFileDialog.ShowDialog() = DialogResult.OK Then

            Dim fullpath As String = Me._SaveFileDialog.FileName()
            Me._RecvTextBox.SaveFile(fullpath, RichTextBoxStreamType.PlainText)
            System.Windows.Forms.MessageBox.Show(IO.Path.GetFileName(fullpath) & " written")

        Else
            System.Windows.Forms.MessageBox.Show("no data chosen")
        End If
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _LoadTransmitFileButton control. load file into tx box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    Private Sub LoadTransmitFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadTransmitFileButton.Click

        Me._OpenFileDialog.DefaultExt = "*.TXT"

        Me._OpenFileDialog.Filter = "Text files (*.txt)|*.txt"
        If Me._OpenFileDialog.ShowDialog() = DialogResult.OK Then

            Dim fullpath As String = Me._OpenFileDialog.FileName()
            Me._XmitTextBox.Clear()
            Me._XmitTextBox.LoadFile(fullpath, RichTextBoxStreamType.PlainText)

        Else
            System.Windows.Forms.MessageBox.Show("no data chosen")
        End If
    End Sub

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> The disconnect title. </summary>
    Private Const _DisconnectTitle As String = "DISCONNECT"

    ''' <summary> The connect title. </summary>
    Private Const _ConnectTitle As String = "      CONNECT"

    ''' <summary> Handles the Click event of the _ConnectButton control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ConnectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConnectButton.Click
        Me.OnConnecting(Me.IsConnected)

    End Sub

    ''' <summary> Options for controlling the port. </summary>
    Private ReadOnly _PortParameters As String()

    ''' <summary> Returns the port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A String() </returns>
    Public Function PortParameters() As String()
        Return Me._PortParameters
    End Function

    ''' <summary> Process connecting. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
    Private Sub OnConnecting(ByVal isConnected As Boolean)
        If isConnected Then
            Me._Port.Disconnect()
        Else
            Me.PortParameters(PortParameterIndex.PortNumber) = Me._PortNumberCombo.Text
            Me.PortParameters(PortParameterIndex.BaudRate) = Me._BaudRateCombo.Text
            Me.PortParameters(PortParameterIndex.DataBits) = Me._DataBitsCombo.Text
            Me.PortParameters(PortParameterIndex.Parity) = Me._ParityCombo.Text
            Me.PortParameters(PortParameterIndex.StopBits) = Me._StopBitsCombo.Text
            Me.PortParameters(PortParameterIndex.DelayTime) = Me._ReceiveDelayCombo.Text
            Me.PortParameters(PortParameterIndex.Threshold) = Me._ReceiveThresholdCombo.Text
            Me._Port.Connect(Me.PortParameters)
        End If
    End Sub

    ''' <summary> Handles the change of connection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
    Private Sub OnConnectionChanged(ByVal isConnected As Boolean)

        If isConnected Then
            Me._ParityCombo.Enabled = False
            Me._StopBitsCombo.Enabled = False
            Me._PortNumberCombo.Enabled = False
            Me._BaudRateCombo.Enabled = False
            Me._DataBitsCombo.Enabled = False
            Me._ReceiveDelayCombo.Enabled = False
            Me._ReceiveThresholdCombo.Enabled = False
            Me._ConnectButton.Text = _DisconnectTitle
            Me._ConnectStatusLabel.Image = My.Resources.LedGreen
            Me._ReceiveStatusLabel.Image = My.Resources.LedOrange
            Me._TransmitStatusLabel.Image = My.Resources.LedOrange
            Me.ShowPortParameters(Me.PortParameters)
        Else
            Me._ParityCombo.Enabled = True
            Me._StopBitsCombo.Enabled = True
            Me._PortNumberCombo.Enabled = True
            Me._BaudRateCombo.Enabled = True
            Me._DataBitsCombo.Enabled = True
            Me._ReceiveDelayCombo.Enabled = True
            Me._ReceiveThresholdCombo.Enabled = True
            Me._ConnectButton.Text = _ConnectTitle
            Me._ConnectStatusLabel.Image = My.Resources.LedRed
            Me._ReceiveStatusLabel.Image = My.Resources.LedGray
            Me._TransmitStatusLabel.Image = My.Resources.LedGray
            Me._StatusLabel.Text = "Disconnected"
        End If
    End Sub

#End Region

#Region " CONFIGURATION FILE MANAGEMENT "

    ''' <summary> Filename of the configuration file. </summary>
    Private ReadOnly _ConfigFileName As String

    ''' <summary> load config. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LoadConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadConfigMenuItem.Click
        Try
            Using sr As New StreamReader(Me._ConfigFileName)
                Me._PortNumberCombo.Text = sr.ReadLine()
                Me._BaudRateCombo.Text = sr.ReadLine()
                Me._DataBitsCombo.Text = sr.ReadLine()
                Me._ParityCombo.Text = sr.ReadLine()
                Me._StopBitsCombo.Text = sr.ReadLine()
                Me._ReceiveThresholdCombo.Text = sr.ReadLine
                Me._ReceiveDelayCombo.Text = sr.ReadLine
            End Using
            If sender IsNot Nothing Then System.Windows.Forms.MessageBox.Show(Me._ConfigFileName & " read")
        Catch ex As IOException
            System.Windows.Forms.MessageBox.Show(Me._ConfigFileName & " error: " & ex.Message)
        End Try

    End Sub

    ''' <summary> save port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SaveConfigMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveConfigMenuItem.Click

        Using sw As New StreamWriter(Me._ConfigFileName)
            sw.WriteLine(Me._PortNumberCombo.Text)
            sw.WriteLine(Me._BaudRateCombo.Text)
            sw.WriteLine(Me._DataBitsCombo.Text)
            sw.WriteLine(Me._ParityCombo.Text)
            sw.WriteLine(Me._StopBitsCombo.Text)
            sw.WriteLine(Me._ReceiveThresholdCombo.Text)
            sw.WriteLine(Me._ReceiveDelayCombo.Text)
        End Using
        System.Windows.Forms.MessageBox.Show(Me._ConfigFileName & " written")
    End Sub

#End Region

#Region " COM PORT MANAGEMENT "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Port As Port
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the port. </summary>
    ''' <value> The port. </value>
    Public Property Port As Port
        Get
            Return Me._Port
        End Get
        Set(value As Port)
            Me._Port = value
            Me.OnConnectionChanged(Me.IsConnected)
        End Set
    End Property

    ''' <summary> Gets a value indicating whether this instance is connected. </summary>
    ''' <value> The is connected. </value>
    Public ReadOnly Property IsConnected As Boolean
        Get
            Return Me._Port IsNot Nothing AndAlso Me._Port.IsConnected
        End Get
    End Property

    ''' <summary>
    ''' Handles the DataReceived event of the _serialPort control. Updates the data boxes and the
    ''' received status.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="comTerm.PortEventArgs" /> instance containing the event
    '''                       data. </param>
    Private Sub SerialPort_DataReceived(sender As Object, e As PortEventArgs) Handles _Port.DataReceived

        If e.StatusOkay Then
            Me._ReceiveStatusLabel.Image = My.Resources.LedGreen

            If e.DataBuffer IsNot Nothing Then
                Me.ReceiveCount += e.DataBuffer.Length

                If Me._ShowReceivedHexCheckBox.Checked And Not Me._ShowReceiveAsciiCheckBox.Checked Then
                    AppendBytes(Me._RecvTextBox, e.DataBuffer, Me._CharsInline, False)

                ElseIf Me._ShowReceivedHexCheckBox.Checked And Me._ShowReceiveAsciiCheckBox.Checked Then
                    AppendBytes(Me._RecvTextBox, e.DataBuffer, Me._CharsInline, True)

                ElseIf Not Me._ShowReceivedHexCheckBox.Checked And Me._ShowReceiveAsciiCheckBox.Checked Then
                    Dim s As String = Encoding.ASCII.GetString(e.DataBuffer, 0, e.DataBuffer.Length)
                    Me._RecvTextBox.ScrollToCaret()
                    'TO_DO
                    Me._RecvTextBox.AppendText(s) ' & vbCr)
                End If
            End If


        Else
            Me._ReceiveStatusLabel.Image = My.Resources.LedRed
        End If

    End Sub

    ''' <summary>
    ''' Handles the DataSent event of the _serialPort control. Updates controls and data.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="comTerm.PortEventArgs" /> instance containing the event
    '''                       data. </param>
    Private Sub SerialPort_DataSent(sender As Object, e As PortEventArgs) Handles _Port.DataSent
        Me._TransmitStatusLabel.Image = If(e.StatusOkay, My.Resources.LedGreen, My.Resources.LedRed)
    End Sub

    ''' <summary> Handles the ConnectionChanged event of the _serialPort control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="comTerm.ConnectionEventArgs" /> instance containing the
    '''                       event data. </param>
    Private Sub SerialPort_ConnectionChanged(sender As Object, e As ConnectionEventArgs) Handles _Port.ConnectionChanged
        Me.OnConnectionChanged(e.IsConnected)
    End Sub

    ''' <summary>
    ''' Handles the MessageAvailable event of the _serialPort control. Displays an error message box.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="comTerm.MessageEventArgs" /> instance containing the event
    '''                       data. </param>
    Private Sub SerialPort_MessageAvailable(sender As Object, e As MessageEventArgs) Handles _Port.MessageAvailable

        Dim icon As MessageBoxIcon

        Select Case e.Severity
            Case TraceEventType.Critical, TraceEventType.Error
                icon = MessageBoxIcon.Error
            Case TraceEventType.Information
                icon = MessageBoxIcon.Information
            Case TraceEventType.Verbose
                icon = MessageBoxIcon.Information
            Case TraceEventType.Warning
                icon = MessageBoxIcon.Exclamation
            Case Else
                icon = MessageBoxIcon.Information
        End Select
        System.Windows.Forms.MessageBox.Show(e.Details, "Port Message", MessageBoxButtons.OK, icon,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    End Sub



#End Region

#Region " DISPLAY MAANAGEMENT "

    ''' <summary>
    ''' Average character width.
    ''' </summary>
    Private _CharacterWidth As Single

    ''' <summary>
    ''' Number of characters per line.
    ''' </summary>
    Private _CharsInline As Integer

    ''' <summary> set ruler in box. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="rb">    [in,out] tx or rx box here. </param>
    ''' <param name="isHex"> True if is hexadecimal, false if not. </param>
    ''' <returns> length of ruler. </returns>
    Private Function SetRuler(ByRef rb As RichTextBox, ByVal isHex As Boolean) As Integer

        Dim rbWidth As Integer = rb.Width
        Dim s As New System.Text.StringBuilder
        Dim anzMarks As Integer

        If Not isHex Then
            anzMarks = CInt((rbWidth / Me._CharacterWidth) / 5)
            For i As Integer = 1 To anzMarks
                If i < 2 Then
                    s.Append($"    {(i * 5):0}")
                ElseIf i < 20 Then
                    s.Append($"   {(i * 5):00}")
                Else
                    s.Append($"  {(i * 5):000}")
                End If
            Next
        Else
            anzMarks = CInt((rbWidth / Me._CharacterWidth) / 3)
            For i As Integer = 1 To anzMarks
                s.Append($" {i:00}")
            Next
        End If

        ' coloring ruler
        Dim cl As Color = rb.BackColor
        rb.Select(0, rb.Lines(0).Length)
        rb.SelectionBackColor = Color.LightGray
        rb.SelectedText = s.ToString
        If rb.Lines.Length = 1 Then rb.AppendText(vbCr)
        rb.SelectionBackColor = cl
        rb.SelectionLength = 0
        Return s.Length

    End Function

    ''' <summary> select a font. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LargeFontMenuItem.Click, _MediumFontMenuItem.Click, _SmallFontMenuItem.Click

        Dim s As String = CType(sender, ToolStripMenuItem).Text

        If s = "Large" Then
            Me._RecvTextBox.Font = New Font("Lucida Console", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ElseIf s = "Medium" Then
            Me._RecvTextBox.Font = New Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ElseIf s = "Small" Then
            Me._RecvTextBox.Font = New Font("Lucida Console", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        End If

        Dim g As Graphics = Me._XmitTextBox.CreateGraphics
        ' measure with test string
        Dim szF As SizeF = g.MeasureString("0123456789", Me._RecvTextBox.Font)
        Me._CharacterWidth = 0.1F * szF.Width
        g.Dispose()

        Me._XmitTextBox.Font = Me._RecvTextBox.Font

    End Sub

    ''' <summary> Append frame in one RichTextBox. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="rb">              tx or rx box here. </param>
    ''' <param name="data">            data frame. </param>
    ''' <param name="currentLength">   Max chars in box. </param>
    ''' <param name="showHexAndAscii"> determines whether also displaying Hex True. </param>
    Private Shared Sub AppendBytes(ByVal rb As RichTextBox, ByVal data() As Byte, ByVal currentLength As Integer, ByVal showHexAndAscii As Boolean)

        Dim hexString As New System.Text.StringBuilder
        Dim charString As New System.Text.StringBuilder
        Dim count As Integer = 0

        For i As Integer = 0 To data.Length - 1

            hexString.Append($" {data(i):X2}")
            If data(i) > 31 Then
                charString.Append($"  {Chr(data(i))}")
            Else
                charString.Append("  .")
            End If
            count += 3

            ' start a new line
            If count >= currentLength Then
                rb.ScrollToCaret()
                hexString.AppendLine()
                rb.AppendText(hexString.ToString)
                If showHexAndAscii Then
                    rb.ScrollToCaret()
                    charString.AppendLine()
                    rb.AppendText(charString.ToString)
                End If
                hexString = New System.Text.StringBuilder
                charString = New System.Text.StringBuilder
                count = 0
            End If

        Next

        rb.ScrollToCaret()
        hexString.AppendLine()
        rb.AppendText(hexString.ToString)
        If showHexAndAscii Then
            rb.ScrollToCaret()
            charString.AppendLine()
            rb.AppendText(charString.ToString)
        End If
    End Sub

    ''' <summary> Convert HEX string to its representing binary values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hex"> The hexadecimal. </param>
    ''' <returns> The hexadecimal converted to bytes. </returns>
    Private Shared Function ConvertHexToBytes(ByVal hex As String) As Byte()
        Dim s() As String = hex.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)
        Dim data As Byte() = New Byte(s.Length - 1) {}
        For i As Integer = 0 To data.Length - 1
            If Not Byte.TryParse(s(i), NumberStyles.HexNumber, CultureInfo.CurrentCulture, data(i)) Then
                data(i) = 255
                System.Windows.Forms.MessageBox.Show("Conversion failed", "Conversion failed!", MessageBoxButtons.OK, MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End If
        Next
        Return data
    End Function

    ''' <summary> Shows the port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portParameters"> Options for controlling the port. </param>
    Private Sub ShowPortParameters(ByVal portParameters() As String)

        Dim s As New System.Text.StringBuilder
        s.Append(" Terminal connect: ")
        For Each param As String In portParameters
            s.Append(" -")
            s.Append(param)
        Next
        Me._StatusLabel.Text = s.ToString
    End Sub

#End Region

End Class

Friend Module Extensions

    ''' <summary> The hexadecimal characters. </summary>
    Private Const _HexChars As String = "0123456789ABCDEFabcdef"

    ''' <summary> The hexadecimal character values. </summary>
    Private _HexCharValues As List(Of Integer)

    ''' <summary> Gets the hexadecimal character values. </summary>
    ''' <value> The hexadecimal character values. </value>
    Private ReadOnly Property HexCharValues As IEnumerable(Of Integer)
        Get
            If Extensions._HexCharValues Is Nothing Then
                Extensions._HexCharValues = New List(Of Integer)
                For Each c As Char In Extensions._HexChars.ToCharArray()
                    Extensions._HexCharValues.Add(Convert.ToByte(c))
                Next
            End If
            Return Extensions._HexCharValues
        End Get
    End Property

    ''' <summary>
    ''' Determines whether the specified ASCII character value represents an Hexadecimal character.
    ''' </summary>
    ''' <remarks> David, 2020-10-21. </remarks>
    ''' <param name="keyValue"> The key value. </param>
    ''' <returns> <c>True</c> if the specified value is allowed; otherwise, <c>False</c>. </returns>
    <Runtime.CompilerServices.Extension()>
    Friend Function IsHexadecimal(ByVal keyValue As Integer) As Boolean
        Return Extensions.HexCharValues.Contains(keyValue)
    End Function

    ''' <summary> Determines whether the specified character is an Hexadecimal character. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if the specified value is allowed; otherwise, <c>False</c>. </returns>
    <Runtime.CompilerServices.Extension()>
    Friend Function IsHexadecimal(ByVal value As Char) As Boolean
        Return Not String.IsNullOrEmpty(value) AndAlso Extensions._HexChars.Contains(value)
    End Function

    ''' <summary> Builds the hex string by adding spaces between pairs. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a String. </returns>
    <Runtime.CompilerServices.Extension()>
    Friend Function ToHexFormatted(ByVal value As String) As String

        If String.IsNullOrEmpty(value) Then Return String.Empty
        Const space As Char = " "c
        Dim builder As New System.Text.StringBuilder
        'remove existing spaces.
        value = value.ToUpperInvariant
        For Each s As Char In value
            If Not s.Equals(space) Then
                builder.Append(s)
            End If
        Next
        value = builder.ToString
        builder = New System.Text.StringBuilder

        'insert space every 2 characters.
        Dim pairCount As Integer = 0
        For Each s As Char In value
            pairCount += 1
            If pairCount > 2 Then
                builder.Append(space)
                pairCount = 1
            End If
            builder.Append(s)
        Next
        value = builder.ToString
        Return value

    End Function

End Module
