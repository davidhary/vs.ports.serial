﻿using System.ComponentModel;

namespace isr.Ports.Serial.My
{

    /// <summary> Values that represent project trace event identifiers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
    public enum ProjectTraceEventId
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None,

        /// <summary> An enum constant representing the serial option. </summary>
        [Description( "Serial" )]
        Serial = Core.ProjectTraceEventId.Serial,

        /// <summary> An enum constant representing the rooster option. </summary>
        [Description( "Rooster" )]
        Rooster = Serial + 0x2,

        /// <summary> An enum constant representing the teleport option. </summary>
        [Description( "Teleport" )]
        Teleport = Serial + 0x2,

        /// <summary> An enum constant representing the 1000 option. </summary>
        [Description( "D1000" )]
        D1000 = Serial + 0x3,

        /// <summary> An enum constant representing the love option. </summary>
        [Description( "Love" )]
        Love = Serial + 0x4,

        /// <summary> An enum constant representing the switchboard option. </summary>
        [Description( "Switchboard" )]
        Switchboard = Serial + 0xA,

        /// <summary> An enum constant representing the serial terminal option. </summary>
        [Description( "Serial Terminal" )]
        SerialTerminal = Serial + 0xB,

        /// <summary> An enum constant representing the port forms option. </summary>
        [Description( "Port Forms" )]
        PortForms = Serial + 0xC,

        /// <summary> An enum constant representing the teleport forms option. </summary>
        [Description( "Teleport Forms" )]
        TeleportForms = Serial + 0xD
    }
}