﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports.Serial
{

    /// <summary>
        /// Packs or unpacks two <see cref="T:Byte">bytes</see> into an <see cref="T:UInt16">unsigned
        /// short</see>.
        /// </summary>
        /// <remarks>
        /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    [CLSCompliant( false )]
    public sealed class PackedUInt16
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PackedUInt16() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedUInt16( ushort value ) : this()
        {
            this.Value = value;
            this.FromValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedUInt16( short value ) : this()
        {
            this.Value = Conversions.ToUShort( value );
            this.FromValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highByte"> The high byte. </param>
        /// <param name="lowByte">  The low byte. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "byte" )]
        public PackedUInt16( byte highByte, byte lowByte ) : this()
        {
            this.LowByte = lowByte;
            this.HighByte = highByte;
            this.ToValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        public PackedUInt16( IEnumerable<byte> values ) : this()
        {
            if ( values is object && values.Count() >= 2 )
            {
                this.LowByte = values.ElementAtOrDefault( 1 );
                this.HighByte = values.ElementAtOrDefault( 0 );
                this.ToValueThis();
            }
        }

        #endregion

        #region " MEMBERS "

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public ushort Value { get; set; }

        /// <summary> Gets or sets the high byte. </summary>
        /// <value> The high byte. </value>
        public byte HighByte { get; set; }

        /// <summary> Gets or sets the low byte. </summary>
        /// <value> The low byte. </value>
        public byte LowByte { get; set; }

        #endregion

        #region " PARSERS "

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void FromValueThis()
        {
            this.HighByte = ToHighByte( this.Value );
            this.LowByte = ToLowByte( this.Value );
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void FromValue()
        {
            this.FromValueThis();
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public void FromValue( ushort value )
        {
            this.Value = value;
            this.FromValue();
        }

        /// <summary> Parses the high byte. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToHighByte( ushort value )
        {
            return ( byte ) ((value & 0xFF00) >> 8);
        }

        /// <summary> Parses the low byte. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToLowByte( ushort value )
        {
            return ( byte ) (value & 0xFF);
        }

        /// <summary> Combines the bytes into the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ToValueThis()
        {
            this.Value = ToValue( this.HighByte, this.LowByte );
        }

        /// <summary> Combines the bytes into the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToValue()
        {
            this.ToValueThis();
        }

        /// <summary> Combines the bytes into a value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highByte"> The high byte. </param>
        /// <param name="lowByte">  The low byte. </param>
        /// <returns> The given data converted to an UInt16. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "byte" )]
        public static ushort ToValue( byte highByte, byte lowByte )
        {
            ushort w = Conversions.ToUShort( highByte & 0xFF );
            w = Conversions.ToUShort( lowByte | 0xFF00 & w << 8 );
            return w;
        }

        /// <summary> Converts the 1-dimension byte array to 2-dimensions array. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">       The values. </param>
        /// <param name="startIndex">   The start index. </param>
        /// <param name="rowsCount">    The rows count. </param>
        /// <param name="columnsCount"> The columns count. </param>
        /// <returns> A Short(,) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body" )]
        public static short[,] FromBytes( IEnumerable<byte> values, int startIndex, int rowsCount, int columnsCount )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            var t = new short[rowsCount, columnsCount];
            int n = startIndex;
            PackedUInt16 packedValue;
            for ( int row = 0, loopTo = rowsCount - 1; row <= loopTo; row++ )
            {
                // store in LUT row
                packedValue = new PackedUInt16( values.ElementAtOrDefault( n ), values.ElementAtOrDefault( n + 1 ) );
                n += 2;
                t[row, 0] = ( short ) packedValue.Value;
                packedValue = new PackedUInt16( values.ElementAtOrDefault( n ), values.ElementAtOrDefault( n + 1 ) );
                n += 2;
                t[row, 1] = ( short ) packedValue.Value;
            }

            return t;
        }

        /// <summary> Returns the data as a 1-Dimension byte array. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The array of values. </param>
        /// <returns> Values as an IEnumerable(Of Byte) </returns>
        public static IEnumerable<byte> ToBytes( short[,] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            var bytes = new List<byte>();
            PackedUInt16 packedValue;
            for ( int row = 0, loopTo = values.GetLength( 0 ) - 1; row <= loopTo; row++ )
            {
                // store in LUT row
                for ( int col = 0, loopTo1 = values.GetLength( 1 ) - 1; col <= loopTo1; col++ )
                {
                    packedValue = new PackedUInt16( ( ushort ) values[row, col] );
                    bytes.Add( packedValue.HighByte );
                    bytes.Add( packedValue.LowByte );
                }
            }

            return bytes.ToArray();
        }

        #endregion

    }
}