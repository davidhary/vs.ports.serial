using System.Collections.Generic;
using System.Linq;

namespace isr.Ports.Serial
{

    /// <summary> Packs or unpacks a <see cref="T:Short">signed value</see>. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public sealed class PackedInt16
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PackedInt16() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedInt16( short value ) : this()
        {
            this.Value = value;
            this.FromValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highByte"> The high byte. </param>
        /// <param name="lowByte">  The low byte. </param>
        public PackedInt16( byte highByte, byte lowByte ) : this()
        {
            this.LowByte = lowByte;
            this.HighByte = highByte;
            this.ToValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt16" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        public PackedInt16( IEnumerable<byte> values ) : this()
        {
            if ( values is object && values.Count() >= 2 )
            {
                this.LowByte = values.ElementAtOrDefault( 1 );
                this.HighByte = values.ElementAtOrDefault( 0 );
                this.ToValueThis();
            }
        }

        #endregion

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public short Value { get; set; }

        /// <summary> Gets or sets the high byte. </summary>
        /// <value> The high byte. </value>
        public byte HighByte { get; set; }

        /// <summary> Gets or sets the low byte. </summary>
        /// <value> The low byte. </value>
        public byte LowByte { get; set; }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void FromValueThis()
        {
            this.HighByte = ToHighByte( this.Value );
            this.LowByte = ToLowByte( this.Value );
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void FromValue()
        {
            this.FromValueThis();
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public void FromValue( short value )
        {
            this.Value = value;
            this.FromValue();
        }

        /// <summary> Parses the high byte. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToHighByte( short value )
        {
            return ( byte ) ((value & 0xFF00) >> 8);
        }

        /// <summary> Parses the low byte. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToLowByte( short value )
        {
            return ( byte ) (value & 0xFF);
        }

        /// <summary> Combines the bytes into a value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ToValueThis()
        {
            this.Value = ToValue( this.HighByte, this.LowByte );
        }

        /// <summary> Combines the bytes into a value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToValue()
        {
            this.ToValueThis();
        }

        /// <summary> Combines the bytes into a value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highPart"> The high byte. </param>
        /// <param name="lowPart">  The low byte. </param>
        /// <returns> The given data converted to an Int16. </returns>
        public static short ToValue( byte highPart, byte lowPart )
        {
            short result = ( short ) (highPart & 0xFF);
            result = ( short ) (lowPart | 0xFF00 & result << 8);
            return result;
        }
    }
}
