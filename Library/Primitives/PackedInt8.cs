﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Ports.Serial
{

    /// <summary>
        /// Packs or unpacks two <see cref="T:Byte">byte nibble value (0-15), into a single byte</see>.
        /// </summary>
        /// <remarks>
        /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    public sealed class PackedInt8
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PackedInt8() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt8" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedInt8( byte value ) : this()
        {
            this.FromValueThis( value );
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt8" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highNibble"> The high nibble. </param>
        /// <param name="lowNibble">  The low nibble. </param>
        public PackedInt8( byte highNibble, byte lowNibble ) : this()
        {
            this.FromValueThis( ToValue( highNibble, lowNibble ) );
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt8" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        public PackedInt8( IEnumerable<byte> values ) : this()
        {
            if ( values is object && values.Count() >= 2 )
            {
                this.FromValueThis( ToValue( values.ElementAtOrDefault( 0 ), values.ElementAtOrDefault( 1 ) ) );
            }
        }

        #endregion

        #region " MEMBERS "

        /// <summary> Gets or sets the packed value. </summary>
        /// <value> The value. </value>
        public byte Value { get; set; }

        /// <summary> Gets or sets the high byte value. </summary>
        /// <value> The high nibble. </value>
        public byte HighNibble { get; set; }

        /// <summary> Gets or sets the low byte value. </summary>
        /// <value> The low nibble. </value>
        public byte LowNibble { get; set; }

        #endregion

        #region " PARSERS "

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void FromValueThis()
        {
            this.HighNibble = ToHighNibble( this.Value );
            this.LowNibble = ToLowNibble( this.Value );
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void FromValue()
        {
            this.FromValueThis();
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void FromValueThis( byte value )
        {
            this.Value = value;
            this.FromValueThis();
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public void FromValue( byte value )
        {
            this.Value = value;
            this.FromValueThis();
        }

        /// <summary> Parses the high nibble. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToHighNibble( byte value )
        {
            return ( byte ) ((value & 0xF0) >> 4);
        }

        /// <summary> From High nibble. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Byte. </returns>
        public static byte FromHighNibble( byte value )
        {
            return ( byte ) ((value & 0xF) << 4);
        }

        /// <summary> Parses the low nibble. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToLowNibble( byte value )
        {
            return ( byte ) (value & 0xF);
        }

        /// <summary> Initializes this object from the given from low nibble. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Byte. </returns>
        public static byte FromLowNibble( byte value )
        {
            return ( byte ) (value & 0xF);
        }

        /// <summary> Converts a value to a nibble. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Byte. </returns>
        public static byte ToNibble( byte value )
        {
            return ( byte ) (value & 0xF);
        }

        /// <summary> Combines the bytes into a word. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ToValueThis()
        {
            this.Value = ToValue( this.HighNibble, this.LowNibble );
        }

        /// <summary> Combines the bytes into a byte value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToValue()
        {
            this.ToValueThis();
        }

        /// <summary> Combines the bytes into a word. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="highNibble"> The high nibble. </param>
        /// <param name="lowNibble">  The low nibble. </param>
        /// <returns> The given data converted to a Byte. </returns>
        public static byte ToValue( byte highNibble, byte lowNibble )
        {
            if ( highNibble >= 0x10 )
                throw new ArgumentException( $"value {highNibble} must be lower than {0x10}", nameof( highNibble ) );
            return lowNibble >= 0x10
                ? throw new ArgumentException( $"value {lowNibble} must be lower than {0x10}", nameof( lowNibble ) )
                : ( byte ) (FromHighNibble( highNibble ) + FromLowNibble( lowNibble ));
        }

        #endregion

    }
}