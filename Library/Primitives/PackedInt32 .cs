using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports.Serial
{

    /// <summary> Packs or unpacks a <see cref="T:Int32">unsigned value</see>. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [CLSCompliant( false )]
    public sealed class PackedInt32
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PackedInt32() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt32" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedInt32( int value ) : this()
        {
            this.Value = value;
            this.FromValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedInt32" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highPart"> The high part. </param>
        /// <param name="lowPart">  The low part. </param>
        public PackedInt32( PackedUInt16 highPart, PackedUInt16 lowPart ) : this()
        {
            this.HighPart = highPart;
            this.LowPart = lowPart;
            this.ToValueThis();
        }

        #endregion

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public int Value { get; set; }

        /// <summary> Gets or sets the high part. </summary>
        /// <value> The high part. </value>
        public PackedUInt16 HighPart { get; set; }

        /// <summary> Gets or sets the low part. </summary>
        /// <value> The low part. </value>
        public PackedUInt16 LowPart { get; set; }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void FromValueThis()
        {
            this.LowPart = ToLowUnsignedPart( this.Value );
            this.HighPart = ToHighUnsignedPart( this.Value );
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void FromValue()
        {
            this.FromValueThis();
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public void FromValue( int value )
        {
            this.Value = value;
            this.FromValue();
        }

        /// <summary> Builds the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ToValueThis()
        {
            this.Value = this.HighPart.Value & 0xFFFF;
            this.Value = ( int ) (this.LowPart.Value | 0xFFFF0000 & this.Value << 16);
        }

        /// <summary> Builds the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToValue()
        {
            this.ToValueThis();
        }

        /// <summary> Parses the high unsigned part. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a PackedUInt16. </returns>
        public static PackedUInt16 ToHighUnsignedPart( int value )
        {
            return new PackedUInt16( Conversions.ToUShort( (value & 0xFFFF0000) >> 16 ) );
        }

        /// <summary> Parses the low unsigned part. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a PackedUInt16. </returns>
        public static PackedUInt16 ToLowUnsignedPart( int value )
        {
            return new PackedUInt16( Conversions.ToUShort( value & 0xFFFF ) );
        }

        /// <summary> Combines the unsigned parts into a signed value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="highPart"> The high part. </param>
        /// <param name="lowPart">  The low part. </param>
        /// <returns> The given data converted to an Int32. </returns>
        public static int ToValue( PackedUInt16 highPart, PackedUInt16 lowPart )
        {
            if ( highPart is null )
                throw new ArgumentNullException( nameof( highPart ) );
            if ( lowPart is null )
                throw new ArgumentNullException( nameof( lowPart ) );
            int result = highPart.Value & 0xFFFF;
            result = ( int ) (lowPart.Value | 0xFFFF0000 & result << 16);
            return result;
        }
    }
}
