﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Ports.Serial
{

    /// <summary> Defines an event arguments class for <see cref="Port">port messages</see>. </summary>
        /// <remarks>
        /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    public class PortEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PortEventArgs() : this( true, Array.Empty<byte>() )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="okay"> if set to <c>True</c> [okay]. </param>
        public PortEventArgs( bool okay ) : this( okay, Array.Empty<byte>() )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        public PortEventArgs( PortEventArgs e ) : this( Validated( e ).StatusOkay, Validated( e ).DataBuffer )
        {
        }

        /// <summary> Validated the given e. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Port event information. </param>
        /// <returns> The PortEventArgs. </returns>
        public static PortEventArgs Validated( PortEventArgs e )
        {
            return e is null ? throw new ArgumentNullException( nameof( e ) ) : e;
        }

        /// <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="okay"> if set to <c>True</c> [okay]. </param>
        /// <param name="data"> The data. </param>
        public PortEventArgs( bool okay, IEnumerable<byte> data ) : base()
        {
            this.StatusOkay = okay;
            this.DataBuffer = data;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets a value indicating whether status is okay, true; Otherwise, False.
        /// </summary>
        /// <value> The status okay. </value>
        public bool StatusOkay { get; private set; }

        /// <summary> Gets or sets the data buffer. </summary>
        /// <value> A buffer for data data. </value>
        public IEnumerable<byte> DataBuffer { get; private set; }

        /// <summary> Gets a string. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="encoding"> The encoding. </param>
        /// <returns> The string. </returns>
        public string GetString( System.Text.Encoding encoding )
        {
            return encoding is null
                ? throw new ArgumentNullException( nameof( encoding ) )
                : encoding.GetString( this.DataBuffer.ToArray(), 0, this.DataBuffer.Count() );
        }
        #endregion

    }
}