using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Core;
using isr.Ports.Serial.ExceptionExtensions;

namespace isr.Ports.Serial
{

    /// <summary>
        /// Defines a wrapper around the Visual Studio <see cref="SerialPort">serial port</see>
        /// </summary>
        /// <remarks>
        /// Based on Extended Serial Port Windows Forms Sample
        /// http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37 <para>
        /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    public class Port : Core.Models.ViewModelTalkerBase, IPort
    {

        /// <summary> Initializes a new instance of the <see cref="Port" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort"> The <see cref="SerialPort">serial port.</see> </param>
        public Port( SerialPort serialPort ) : base()
        {
            this.SerialPortThis = new SerialPort();
            this._PortParametersFileName = Path.Combine( My.MyProject.Application.Info.DirectoryPath, $"{My.MyProject.Application.Info.AssemblyName}.ini" );
            this._TimeoutTimer = new System.Timers.Timer();

            // create a new instance of the serial port.
            this.SerialPortThis = serialPort;

            // initialize the default port parameters.
            this._PortParameters = new PortParametersDictionary( this.SerialPort, 1 ) { PortName = DefaultPortName() };
            this._InputBufferingOption = DataBufferingOption.LinearBuffer;
            this.ReceivedBytes = new List<byte>();
            this.TransmittedBytes = new List<byte>();
            My.Settings.Default.PropertyChanged += this.MySettings_PropertyChanged;
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> initialize a new instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public Port() : this( new SerialPort() )
        {
        }

        /// <summary> Gets the is port that owns this item. </summary>
        /// <value> The is port owner. </value>
        public bool IsPortOwner { get; set; }

        /// <summary> Creates a new Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A Port. </returns>
        public static Port Create()
        {
            Port port = null;
            try
            {
                port = new Port();
            }
            catch
            {
                port?.Dispose();
                throw;
            }

            return port;
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    My.Settings.Default.PropertyChanged -= this.MySettings_PropertyChanged;
                    this.RemoveConnectionChangedEventHandlers();
                    this.RemoveDataReceivedEventHandlers();
                    this.RemoveDataSentEventHandlers();
                    this.RemoveSerialPortDisposedEventHandlers();
                    this.RemoveTimeoutEventHandlers();
                    if ( this._TimeoutTimer is object )
                    {
                        this._TimeoutTimer.Enabled = false;
                        this._TimeoutTimer.Dispose();
                        this._TimeoutTimer = null;
                    }

                    if ( this.IsPortOwner && this.SerialPortThis is object )
                        this.SerialPortThis.Dispose();
                    this.SerialPortThis = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~Port()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " SHARED "

        /// <summary> Default port name. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String. </returns>
        public static string DefaultPortName()
        {
            var portNames = SerialPort.GetPortNames();
            return portNames.Any() && !portNames.Contains( My.Settings.Default.PortName, StringComparer.OrdinalIgnoreCase ) ? portNames[0] : My.Settings.Default.PortName;
        }

        /// <summary> Queries if a given port exists. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portName"> Name of the port. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool PortExists( string portName )
        {
            return PortExists( SerialPort.GetPortNames(), portName );
        }

        /// <summary> Queries if a given port exists. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portNames"> List of names of the ports. </param>
        /// <param name="portName">  Name of the port. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool PortExists( string[] portNames, string portName )
        {
            return portNames is object && portNames.Contains( portName, StringComparer.CurrentCultureIgnoreCase );
        }

        #endregion

        #region " PORT PARAMETERS "

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> The with events. </summary>
        private PortParametersDictionary __PortParameters;

        private PortParametersDictionary _PortParameters
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__PortParameters;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__PortParameters != null )
                {
                    this.__PortParameters.PropertyChanged -= this.PortProperties_PropertyChanged;
                }

                this.__PortParameters = value;
                if ( this.__PortParameters != null )
                {
                    this.__PortParameters.PropertyChanged += this.PortProperties_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets the port parameters. </summary>
        /// <value> The port properties. </value>
        public PortParametersDictionary PortParameters => this._PortParameters;

        /// <summary> Port properties property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortProperties_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is object && e is object )
                {
                    activity = $"handling {nameof( PortParametersDictionary )}.{e.PropertyName} change";
                    this.NotifyPropertyChanged( $"{nameof( this.PortParameters )}.{e.PropertyName}" );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Returns the port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portName"> Name of the port. </param>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// </returns>
        public PortParametersDictionary ToPortParameters( string portName )
        {
            var p = PortParametersDictionary.ToPortParameters( this.SerialPort, this.PortParameters.ReceiveDelay );
            p.PortName = portName;
            return p;
        }

        /// <summary> Returns the port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// </returns>
        public PortParametersDictionary ToPortParameters()
        {
            return PortParametersDictionary.ToPortParameters( this.SerialPort, this.PortParameters.ReceiveDelay );
        }

        /// <summary> Assign port parameters to the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
        /// parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see> </param>
        public void FromPortParameters( PortParametersDictionary portParameters )
        {
            if ( this.SerialPortThis is object && portParameters is object )
            {
                PortParametersDictionary.FromPortParameters( this.SerialPort, portParameters );
            }
        }

        /// <summary> Filename of the port parameters file. </summary>
        private string _PortParametersFileName;

        /// <summary> Gets or sets the filename of the port parameters file. </summary>
        /// <value> The filename of the port parameters file. </value>
        public string PortParametersFileName
        {
            get => this._PortParametersFileName;

            set {
                if ( !string.Equals( value, this.PortParametersFileName ) )
                {
                    this._PortParametersFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Attempts to store port parameters from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryStorePortParameters( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            string activity = string.Empty;
            try
            {
                activity = $"storing port parameters to {this.PortParametersFileName}";
                _ = this.Publish( TraceEventType.Verbose, activity );
                this.PortParameters.Store( this.PortParametersFileName );
            }
            catch ( System.IO.IOException ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }

            return !e.Failed;
        }

        /// <summary> Attempts to restore port parameters from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryRestorePortParameters( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            string activity = string.Empty;
            try
            {
                activity = $"restoring port parameters from {this.PortParametersFileName}";
                _ = this.Publish( TraceEventType.Verbose, activity );
                this.PortParameters.Restore( this.PortParametersFileName );
                activity = $"applying port parameters to port {this.SerialPort.PortName}";
                PortParametersDictionary.FromPortParameters( this.SerialPort, this.PortParameters );
                _ = this.Publish( TraceEventType.Verbose, activity );
            }
            catch ( System.IO.IOException ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }

            return !e.Failed;
        }

        /// <summary> The supported baud rates. </summary>
        private List<int> _SupportedBaudRates;

        /// <summary> Gets the supported baud rates. </summary>
        /// <value> The supported baud rates. </value>
        public IEnumerable<int> SupportedBaudRates
        {
            get {
                if ( this._SupportedBaudRates is null )
                {
                    this._SupportedBaudRates = new List<int>() { 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600 };
                }

                return this._SupportedBaudRates;
            }
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets the is open. </summary>
        /// <value> The is open. </value>
        public bool IsOpen => this.SerialPort is object && this.SerialPort.IsOpen;

        private SerialPort _SerialPortThis;

        /// <summary>   Gets or sets the serial port. </summary>
        /// <value> The serial port. </value>
        private SerialPort SerialPortThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._SerialPortThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._SerialPortThis != null )
                {

                    this._SerialPortThis.DataReceived -= this.SerialPort_DataReceived;
                    this._SerialPortThis.ErrorReceived -= this.SerialPort_ErrorReceived;

                    this._SerialPortThis.Disposed -= this.SerialPort_Disposed;
                }

                this._SerialPortThis = value;
                if ( this._SerialPortThis != null )
                {
                    this._SerialPortThis.DataReceived += this.SerialPort_DataReceived;
                    this._SerialPortThis.ErrorReceived += this.SerialPort_ErrorReceived;
                    this._SerialPortThis.Disposed += this.SerialPort_Disposed;
                }
            }
        }

        /// <summary> Gets or sets the <see cref="SerialPort">Serial Port</see>. </summary>
        /// <value> The serial port. </value>
        public SerialPort SerialPort
        {
            get => this.SerialPortThis;

            set {
                this.SerialPortThis = value;
                if ( value is null )
                {
                }
                else
                {
                    this.TryNotifyConnectionChanged();
                }
            }
        }

        /// <summary> Opens the port using current port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Action event information. </param>
        /// <returns> True if success; otherwise, false. </returns>
        public bool TryOpen( ActionEventArgs e )
        {
            return this.TryOpen( this.PortParameters, e );
        }

        /// <summary> Opens the port using current port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="portName"> Name of the port. </param>
        /// <param name="e">        Action event information. </param>
        /// <returns> True if success; otherwise, false. </returns>
        public bool TryOpen( string portName, ActionEventArgs e )
        {
            return string.IsNullOrWhiteSpace( portName )
                ? throw new ArgumentNullException( nameof( portName ) )
                : this.TryOpen( this.ToPortParameters( portName ), e );
        }

        /// <summary> Opens the port using specified port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
        /// parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see> </param>
        /// <param name="e">              Action event information. </param>
        /// <returns> True if success; otherwise, false. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryOpen( PortParametersDictionary portParameters, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( portParameters is null )
                throw new ArgumentNullException( nameof( portParameters ) );
            string activity = string.Empty;
            bool wasOpen = this.IsOpen;
            try
            {
                activity = $"setting {portParameters.PortName} port parameters";
                _ = this.Publish( TraceEventType.Verbose, activity );
                this.FromPortParameters( portParameters );
                // open and check device if is available?
                activity = $"opening {this.SerialPort.PortName}...";
                _ = this.Publish( TraceEventType.Verbose, activity );
                this.SerialPort.Open();
                if ( this.IsOpen )
                {
                    _ = this.Publish( TraceEventType.Verbose, $"done {activity}" );
                    this.PortParameters.Populate( this.SerialPort, portParameters.ReceiveDelay );
                }
                else
                {
                    _ = this.Publish( TraceEventType.Warning, $"failed {activity}; not open" );
                    e.RegisterOutcomeEvent( TraceEventType.Warning, $"failed {activity}; not open" );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }
            finally
            {
                // if port was closed and is now open
                if ( wasOpen != this.IsOpen )
                    this.TryNotifyConnectionChanged();
            }

            return !e.Failed;
        }

        /// <summary> C;pses the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> True if success; otherwise, false. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryClose( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            string activity = string.Empty;
            bool isOpen = this.IsOpen;
            try
            {
                activity = $"closing {this.SerialPort.PortName}...";
                _ = this.Publish( TraceEventType.Verbose, activity );
                activity = $"discarding {this.SerialPort.PortName} input buffer";
                this.SerialPort.DiscardInBuffer();
                activity = $"closing {this.SerialPort.PortName}";
                this.SerialPort.Close();
                if ( this.IsOpen )
                {
                    _ = this.Publish( TraceEventType.Warning, $"failed {activity}; still open" );
                    e.RegisterOutcomeEvent( TraceEventType.Warning, $"failed {activity}; still open" );
                }
                else
                {
                    _ = this.Publish( TraceEventType.Verbose, $"done {activity}" );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterFailure( $"Exception {activity};. {ex.ToFullBlownString()}" );
            }
            finally
            {
                if ( isOpen != this.IsOpen )
                    this.TryNotifyConnectionChanged();
            }

            return !e.Failed;
        }

        #endregion

        #region " BUFFERS "

        /// <summary> Gets the received bytes. </summary>
        /// <value> The received bytes. </value>
        private List<byte> ReceivedBytes { get; set; }

        /// <summary> Holds the received values. </summary>
        /// <value> The received values. </value>
        public IEnumerable<byte> ReceivedValues => this.ReceivedBytes is null ? Array.Empty<byte>() : this.ReceivedBytes.ToArray();

        /// <summary> Gets the number of bytes received. </summary>
        /// <value> The number of bytes received. </value>
        public int ReceivedBytesCount => this.ReceivedBytes.Count;

        /// <summary> Discard input buffers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void DiscardInputBuffers()
        {
            this.ReceivedBytes.Clear();
            this.SerialPort?.DiscardInBuffer();
        }

        /// <summary> Attempts to wait for the receive count from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="count">      Number of. </param>
        /// <param name="trialCount"> Number of trials. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryWaitReceiveCount( int count, int trialCount )
        {
            // wait for the data to come in: 
            var minimumTransitTime = this.PortParameters.MinimumTransitTimespan( count );
            // timeout after number of trial of transit time
            var timeout = this.PortParameters.MinimumTransitTimespan( trialCount * count );
            return this.TryWaitReceiveCount( count, minimumTransitTime, timeout );
        }

        /// <summary> Attempts to wait for the receive count from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="count">        Number of. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryWaitReceiveCount( int count, TimeSpan pollInterval, TimeSpan timeout )
        {
            var sw = Stopwatch.StartNew();
            _ = ApplianceBase.DoEventsWaitUntil( pollInterval, timeout, () => this.ReceivedBytesCount >= count );
            // failed if timeout before reaching the desired count
            return this.ReceivedBytesCount < count;
        }

        /// <summary> Returns the encoded string from the byte values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A String. </returns>
        public string Decode( IEnumerable<byte> values )
        {
            return Decode( values, this.SerialPort.Encoding );
        }

        /// <summary> Returns the encoded string from the byte values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">   The values. </param>
        /// <param name="encoding"> The encoding. </param>
        /// <returns> A String. </returns>
        public static string Decode( IEnumerable<byte> values, System.Text.Encoding encoding )
        {
            return encoding is null
                ? throw new ArgumentNullException( nameof( encoding ) )
                : values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Any() ? encoding.GetString( values.ToArray(), 0, values.Count() ) : string.Empty;
        }

        /// <summary> Gets the transmitted bytes. </summary>
        /// <value> The transmitted bytes. </value>
        private List<byte> TransmittedBytes { get; set; }

        /// <summary> Holds the transmitted values. </summary>
        /// <value> The transmitted values. </value>
        public IEnumerable<byte> TransmittedValues => this.TransmittedBytes is null ? Array.Empty<byte>() : this.TransmittedBytes.ToArray();

        /// <summary> Discard output buffers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void DiscardOutputBuffers()
        {
            this.TransmittedBytes.Clear();
            this.SerialPort?.DiscardOutBuffer();
        }

        /// <summary> Resynchronizes the circular buffer or clears the receive buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Resync()
        {
            if ( this.InputBufferingOption == DataBufferingOption.CircularBuffer )
            {
                this.ReceiveBuffer.Resync();
            }
            else
            {
                _ = this.SerialPort.ReadExisting();
            }
        }

        #endregion

        #region " CIRCULAR BUFFER "

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> The with events. </summary>
        private CircularCollection<byte> __ReceiveBuffer;

        private CircularCollection<byte> _ReceiveBuffer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__ReceiveBuffer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__ReceiveBuffer != null )
                {

                    this.__ReceiveBuffer.WatermarkNotify -= this.Buffer_WaterMarkNotify;
                }

                this.__ReceiveBuffer = value;
                if ( this.__ReceiveBuffer != null )
                {
                    this.__ReceiveBuffer.WatermarkNotify += this.Buffer_WaterMarkNotify;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets the buffer for receive data. </summary>
        /// <value> A Buffer for receive data. </value>
        public CircularCollection<byte> ReceiveBuffer => this._ReceiveBuffer;

        /// <summary> Returns the data count in the circular buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An Integer. </returns>
        public int DataCount()
        {
            return this.ReceiveBuffer.Count;
        }

        /// <summary> Reads the next byte from the circular buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The next. </returns>
        public byte ReadNext()
        {
            return this.ReceiveBuffer.Dequeue();
        }

        /// <summary> Notify of buffer overflow. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Buffer_WaterMarkNotify( object sender, EventArgs e )
        {
        }

        /// <summary> The input buffering option. </summary>
        private DataBufferingOption _InputBufferingOption;

        /// <summary> Gets or sets the data buffering option. </summary>
        /// <value> The data buffering option. </value>
        public DataBufferingOption InputBufferingOption
        {
            get => this._InputBufferingOption;

            set {
                this._InputBufferingOption = value;
                if ( value == DataBufferingOption.CircularBuffer && (this._ReceiveBuffer is null || 0 > this._ReceiveBuffer.Capacity) )
                {
                    this._ReceiveBuffer = new CircularCollection<byte>( 1024 );
                    this.ReceivedBytes.Clear();
                }
            }
        }

        #endregion

        #region " SERIAL PORT EVENTS "

        /// <summary> The send lock. </summary>
        private object _SendLock;

        /// <summary> Sends data. </summary>
        /// <remarks> Clears the input buffer. </remarks>
        /// <param name="data"> byte array data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void SendData( IEnumerable<byte> data )
        {
            if ( this._SendLock is null )
                this._SendLock = new object();
            string activity = string.Empty;
            PortEventArgs portEventArgs = null;
            try
            {
                activity = "setting the send lock";
                lock ( this._SendLock )
                {
                    activity = "clearing input buffers";
                    // clear the input buffers in preparation from the returned message.
                    this.DiscardInputBuffers();
                    // clear the output buffers.
                    activity = "clearing output buffers";
                    this.DiscardOutputBuffers();
                    if ( data?.Any() == true )
                    {
                        activity = "setting output buffer to new data";
                        this.TransmittedBytes.AddRange( data );
                        activity = $"{this.SerialPort.PortName} sending {this.TransmittedValues.Count()} byes; {this.TransmittedValues.ToHex( 0, 10 )}... ";
                        _ = this.Publish( TraceEventType.Verbose, activity );
                        this.SerialPort.Write( this.TransmittedValues.ToArray(), 0, this.TransmittedValues.Count() );
                        activity = $"{this.SerialPort.PortName} sent {this.TransmittedValues.Count()} byes";
                        _ = this.Publish( TraceEventType.Information, activity );
                        activity = "setting new port event arguments";
                        portEventArgs = new PortEventArgs( true, this.TransmittedValues );
                    }
                    else
                    {
                        activity = "attempt at sending data ignored -- nothing to send";
                        _ = this.Publish( TraceEventType.Information, activity );
                        activity = "setting new port event arguments";
                        portEventArgs = new PortEventArgs( false, this.TransmittedValues );
                    }
                }
            }
            catch ( Exception ex )
            {
                portEventArgs = new PortEventArgs( false, this.TransmittedValues );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.TryNotifyDataSent( portEventArgs );
            }
        }

        /// <summary> Sends a hexadecimal data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="port"> The port. </param>
        /// <param name="data"> byte array data. </param>
        public static void SendHexData( IPort port, string data )
        {
            if ( port is null || string.IsNullOrWhiteSpace( data ) )
                return;
            if ( !string.IsNullOrWhiteSpace( data ) )
            {
                var bytes = data.ToHexBytes();
                if ( bytes.ElementAtOrDefault( 0 ) == 255 )
                    throw new OperationFailedException( $"failed converting '{data}'; '{( char ) bytes.ElementAtOrDefault( 1 )}' is not a valid HEX character" );
                port.SendData( bytes );
            }
        }

        /// <summary> Sends a hexadecimal data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> byte array data. </param>
        public void SendHexData( string data )
        {
            SendHexData( this, data );
        }

        /// <summary> Sends an ASCII data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        /// <param name="data"> byte array data. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process send ASCII data in this collection.
        /// </returns>
        public static IEnumerable<byte> SendAsciiData( IPort port, string data )
        {
            IEnumerable<byte> bytes = Array.Empty<byte>();
            if ( port is null || string.IsNullOrWhiteSpace( data ) )
                return bytes;
            if ( !string.IsNullOrWhiteSpace( data ) )
            {
                bytes = System.Text.Encoding.ASCII.GetBytes( data );
                port.SendData( bytes );
            }

            return bytes;
        }

        /// <summary> Sends an ASCII data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> byte array data. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process send ASCII data in this collection.
        /// </returns>
        public IEnumerable<byte> SendAsciiData( string data )
        {
            return SendAsciiData( this, data );
        }

        /// <summary> Try receive data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort"> The <see cref="SerialPort">serial port.</see> </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryReceiveData( SerialPort serialPort )
        {
            string activity = string.Empty;
            PortEventArgs portEventArgs = null;
            try
            {
                if ( serialPort.IsOpen )
                {
                    activity = $"awaiting {serialPort.PortName} receive delay {this.PortParameters.ReceiveDelay} ms";
                    ApplianceBase.Delay( this.PortParameters.ReceiveDelay );
                    int len = serialPort.BytesToRead;
                    if ( len > 0 )
                    {
                        activity = $"reading {len} bytes from {serialPort.PortName}...";
                        _ = this.Publish( TraceEventType.Verbose, activity );
                        var values = Array.Empty<byte>();
                        Array.Resize( ref values, len );
                        _ = serialPort.Read( values, 0, len );
                        this.ReceivedBytes.AddRange( values );
                        activity = $"received {values.Length} bytes {values.ToHex( 0, 10 )}... from {serialPort.PortName}";
                        _ = this.Publish( TraceEventType.Information, activity );
                        if ( this.MessageParser is object && this.MessageParser.ParseEnabled )
                        {
                            // if the parser is defined, use it to parse the message. 
                            var outcome = this.MessageParser.Parse( this.ReceivedValues );
                            if ( outcome == MessageParserOutcome.Complete )
                            {
                                portEventArgs = new PortEventArgs( true, this.ReceivedValues );
                                activity = $"{serialPort.PortName} message completed";
                                _ = this.Publish( TraceEventType.Information, activity );
                            }
                            else if ( outcome == MessageParserOutcome.Invalid )
                            {
                                portEventArgs = new PortEventArgs( false, this.ReceivedValues );
                                activity = $"{serialPort.PortName} failed parsing the input message -- message is invalid";
                                _ = this.Publish( TraceEventType.Warning, activity );
                            }
                            else
                            {
                                // if not complete, wait for more characters.
                                activity = $"{serialPort.PortName} message incomplete; awaiting more characters";
                                _ = this.Publish( TraceEventType.Information, activity );
                            }
                        }
                        else
                        {
                            activity = $"{serialPort.PortName} message received";
                            _ = this.Publish( TraceEventType.Verbose, activity );
                            portEventArgs = new PortEventArgs( true, this.ReceivedValues );
                        }
                    }
                }
                else
                {
                    activity = $"{serialPort.PortName} data received event aborted; port no longer open";
                    _ = this.Publish( TraceEventType.Warning, activity );
                    portEventArgs = new PortEventArgs( false, this.ReceivedValues );
                }
            }
            catch ( Exception ex )
            {
                portEventArgs = new PortEventArgs( false, this.ReceivedValues );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                if ( portEventArgs is object )
                    this.TryNotifyDataReceived( portEventArgs );
            }
        }

        /// <summary> Handles the DataReceived event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.IO.Ports.SerialDataReceivedEventArgs" /> instance
        /// containing the event data. </param>
        private void SerialPort_DataReceived( object sender, SerialDataReceivedEventArgs e )
        {
            if ( sender is SerialPort port && e is object )
            {
                _ = this.Publish( TraceEventType.Verbose, $"{e.EventType} received" );
                this.TryReceiveData( port );
            }
        }

        #endregion


        #region " MESSAGE PARSER "

        /// <summary> Gets or sets the message parser. </summary>
        /// <value> The message parser. </value>
        public IMessageParser MessageParser { get; set; }

        #endregion

        #region " EVENT MANAGEMENT "

        #region " SERIAL PORT ERROR RECEIVED "

        /// <summary> Raises the serial port error received event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            this.SyncNotifySerialPortErrorReceived( e );
        }

        /// <summary>
        /// Removes the  <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveSerialPortErrorReceivedEventHandlers()
        {
            this._SerialPortErrorReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The  <see cref="SerialPortErrorReceived">Serial port error receivd </see>> event handlers. </summary>
        private readonly EventHandlerContextCollection<SerialErrorReceivedEventArgs> _SerialPortErrorReceivedEventHandlers = new EventHandlerContextCollection<SerialErrorReceivedEventArgs>();

        /// <summary> Event queue for all listeners interested in
        /// <see cref="SerialPortErrorReceived">Serial port error receivd </see>> events.
        /// Serial Port Error Received status is reported with the
        /// <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
        /// </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<SerialErrorReceivedEventArgs> SerialPortErrorReceived
        {
            add {
                this._SerialPortErrorReceivedEventHandlers.Add( new EventHandlerContext<SerialErrorReceivedEventArgs>( value ) );
            }

            remove {
                this._SerialPortErrorReceivedEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the serial error received event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this._SerialPortErrorReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="SerialPortErrorReceived">Serial Port
        /// Error Received Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    The <see cref="SerialErrorReceivedEventArgs"/> instance containing the
        ///                     event data. </param>
        protected void SyncNotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            this._SerialPortErrorReceivedEventHandlers.Send( this, e );
        }

        /// <summary> Handles the Error Received event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="SerialErrorReceivedEventArgs" /> instance containing the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SerialPort_ErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is SerialPort port )
                {
                    activity = $"notifying {port.PortName} {nameof( this.SerialPort.ErrorReceived )} event";
                    this.NotifySerialPortErrorReceived( e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SERIAL PORT DISPOSED "

        /// <summary>
        /// Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifySerialPortDisposed( EventArgs e )
        {
            this.SyncNotifySerialPortDisposed( e );
        }

        /// <summary>
        /// Removes the <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveSerialPortDisposedEventHandlers()
        {
            this._SerialPortDisposedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _SerialPortDisposedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port Disposed</see> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> SerialPortDisposed
        {
            add {
                this._SerialPortDisposedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._SerialPortDisposedEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the serial port disposed event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnSerialPortDisposed( object sender, EventArgs e )
        {
            this._SerialPortDisposedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="SerialPortDisposed">Serial Port
        /// Disposed Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifySerialPortDisposed( EventArgs e )
        {
            this._SerialPortDisposedEventHandlers.Send( this, e );
        }

        /// <summary> Try notify serial port disposed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifySerialPortDisposed( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "notifying serial port disposed";
                _ = this.Publish( TraceEventType.Information, activity );
                this.NotifySerialPortDisposed( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Serial port disposed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SerialPort_Disposed( object sender, EventArgs e )
        {
            this.TryNotifySerialPortDisposed( e );
        }

        #endregion

        #region " CONNECTION CHANGED "

        /// <summary> Raises the connection event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifyConnectionChanged( ConnectionEventArgs e )
        {
            string activity = $"notifying port {this.SerialPort?.PortName} {(this.IsOpen ? "open" : "closed")}";
            _ = this.Publish( TraceEventType.Information, activity );
            this.SyncNotifyConnectionChanged( e );
        }

        /// <summary> Removes the Connection Changed event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveConnectionChangedEventHandlers()
        {
            this._ConnectionChangedEventHandlers?.RemoveAll();
        }

        /// <summary> The Connection Changed event handlers. </summary>
        private readonly EventHandlerContextCollection<ConnectionEventArgs> _ConnectionChangedEventHandlers = new EventHandlerContextCollection<ConnectionEventArgs>();

        /// <summary> Event queue for all listeners interested in Connection Changed events.
        /// Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<ConnectionEventArgs> ConnectionChanged
        {
            add {
                this._ConnectionChangedEventHandlers.Add( new EventHandlerContext<ConnectionEventArgs>( value ) );
            }

            remove {
                this._ConnectionChangedEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the connection event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnConnectionChanged( object sender, ConnectionEventArgs e )
        {
            this._ConnectionChangedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="ConnectionChanged">Connection
        /// Changed Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    The <see cref="ConnectionEventArgs"/> instance containing the event
        ///                     Connection. </param>
        protected void SyncNotifyConnectionChanged( ConnectionEventArgs e )
        {
            this._ConnectionChangedEventHandlers.Send( this, e );
        }

        /// <summary> Raises the connection event within a try catch clause. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyConnectionChanged( ConnectionEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"notifying port {this.SerialPort?.PortName} {(e.IsPortOpen ? "open" : "closed")}";
                this.NotifyConnectionChanged( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Safely Invokes the <see cref="ConnectionChanged">connection changed event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void TryNotifyConnectionChanged()
        {
            this.TryNotifyConnectionChanged( new ConnectionEventArgs( this.IsOpen ) );
        }

        #endregion

        #region " DATA RECEIVED "

        /// <summary> Notifies a data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        private void NotifyDataReceived( PortEventArgs e )
        {
            string activity = $"notifying {this.SerialPort?.PortName} data received";
            _ = this.Publish( TraceEventType.Verbose, activity );
            if ( this._TimeoutTimer is object )
                this._TimeoutTimer.Enabled = false;
            this.SyncNotifyDataReceived( e );
        }

        /// <summary>
        /// Removes the <see cref="DataReceived">data received</see>/&gt; event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveDataReceivedEventHandlers()
        {
            this._DataReceivedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="DataReceived">data received</see>/> event handlers. </summary>
        private readonly EventHandlerContextCollection<PortEventArgs> _DataReceivedEventHandlers = new EventHandlerContextCollection<PortEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="DataReceived">data received</see>/> events
        /// Receipt status is reported along with the received data in the receive buffer
        /// using the <see cref="PortEventArgs">port event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<PortEventArgs> DataReceived
        {
            add {
                this._DataReceivedEventHandlers.Add( new EventHandlerContext<PortEventArgs>( value ) );
            }

            remove {
                this._DataReceivedEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the port event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataReceived( object sender, PortEventArgs e )
        {
            this._DataReceivedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="DataReceived">Data Received
        /// Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    The <see cref="PortEventArgs"/> instance containing the event data. </param>
        protected void SyncNotifyDataReceived( PortEventArgs e )
        {
            this._DataReceivedEventHandlers.Send( this, e );
        }

        /// <summary> Try notify data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyDataReceived( PortEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"notifying {this.SerialPort?.PortName} data received";
                this.NotifyDataReceived( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DATA SENT "

        /// <summary> Notifies a data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        private void NotifyDataSent( PortEventArgs e )
        {
            if ( e.StatusOkay && this.SerialPort.ReadTimeout > 0 )
            {
                this._TimeoutTimer.AutoReset = false;
                this._TimeoutTimer.Interval = this.SerialPort.ReadTimeout;
                this._TimeoutTimer.Enabled = true;
            }

            string activity = $"notifying {this.SerialPort?.PortName} data sent";
            _ = this.Publish( TraceEventType.Verbose, activity );
            this.SyncNotifyDataSent( e );
        }

        /// <summary> Removes the <see cref="DataSent">Data Sent event</see> handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveDataSentEventHandlers()
        {
            this._DataSentEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="DataSent">Data Sent event</see> handlers. </summary>
        private readonly EventHandlerContextCollection<PortEventArgs> _DataSentEventHandlers = new EventHandlerContextCollection<PortEventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
        /// Receipt status is reported along with the Sent data in the receive buffer
        /// using the <see cref="PortEventArgs">port event arguments.</see> </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<PortEventArgs> DataSent
        {
            add {
                this._DataSentEventHandlers.Add( new EventHandlerContext<PortEventArgs>( value ) );
            }

            remove {
                this._DataSentEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the port event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataSent( object sender, PortEventArgs e )
        {
            this._DataSentEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="DataSent">Data Sent Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    The <see cref="PortEventArgs"/> instance containing the event data. </param>
        protected void SyncNotifyDataSent( PortEventArgs e )
        {
            this._DataSentEventHandlers.Send( this, e );
        }

        /// <summary> Try notify data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyDataSent( PortEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"notifying {this.SerialPort?.PortName} data sent";
                this.NotifyDataSent( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #endregion

        #region " TIMEOUT MANAGER "

        /// <summary> Notifies a timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifyTimeout( EventArgs e )
        {
            this.SyncNotifyTimeout( e );
        }

        /// <summary> Removes the <see cref="Timeout"/> event handlers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected void RemoveTimeoutEventHandlers()
        {
            this._TimeoutEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="Timeout"/> event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _TimeoutEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> Timeout
        {
            add {
                this._TimeoutEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._TimeoutEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the timeout event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnTimeout( object sender, EventArgs e )
        {
            this._TimeoutEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously  sends or invokes the <see cref="Timeout">Timeout Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    The <see cref="EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyTimeout( EventArgs e )
        {
            this._TimeoutEventHandlers.Send( this, e );
        }

        /// <summary> Try notify data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyTimeout( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"notifying {this.SerialPort.PortName} timeout";
                _ = this.Publish( TraceEventType.Verbose, activity );
                this.NotifyTimeout( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary>
        /// Handles the timeout management
        /// </summary>
        private System.Timers.Timer __TimeoutTimer;

        private System.Timers.Timer _TimeoutTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__TimeoutTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__TimeoutTimer != null )
                {
#pragma warning restore IDE1006 // Naming Styles

                    this.__TimeoutTimer.Elapsed -= this.TimeoutTimer_Elapsed;
                }

                this.__TimeoutTimer = value;
                if ( this.__TimeoutTimer != null )
                {
                    this.__TimeoutTimer.Elapsed += this.TimeoutTimer_Elapsed;
                }
            }
        }

        /// <summary> Handles the Elapsed event of the _timeoutTimer control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Timers.ElapsedEventArgs" /> instance containing the
        /// event data. </param>
        private void TimeoutTimer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {
            this.TryNotifyTimeout( EventArgs.Empty );
        }

        #endregion


        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public static void OpenSettingsEditor()
        {
            WindowsForms.EditConfiguration( "Serial Port Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( ListenerType.Display, sender.TraceShowLevel );
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
