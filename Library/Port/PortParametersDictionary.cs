using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Runtime.Serialization;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports.Serial
{

    /// <summary> Implements a key,value pair for storing the Port Parameters. </summary>
        /// <remarks>
        /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2018-06-16 </para>
        /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2240:ImplementISerializableCorrectly" )]
    [Serializable()]
    public partial class PortParametersDictionary : Dictionary<PortParameterKey, string>, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PortParametersDictionary() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
        /// <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
        public PortParametersDictionary( SerialPort serialPort, int receiveDelay ) : this()
        {
            if ( serialPort is object )
                this.PopulateThis( serialPort, receiveDelay );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="parameters"> Options for controlling the operation. </param>
        public PortParametersDictionary( PortParametersDictionary parameters ) : this()
        {
            if ( parameters is object )
                this.PopulateThis( parameters );
        }

        /// <summary> Creates the default. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The new default. </returns>
        public static PortParametersDictionary CreateDefault()
        {
            PortParametersDictionary result = null;
            try
            {
                result = new PortParametersDictionary() { { PortParameterKey.PortName, "COM1" }, { PortParameterKey.BaudRate, 9600.ToString() }, { PortParameterKey.DataBits, 8.ToString() }, { PortParameterKey.Parity, Parity.None.ToString() }, { PortParameterKey.StopBits, StopBits.One.ToString() }, { PortParameterKey.ReceivedBytesThreshold, 1.ToString() }, { PortParameterKey.ReceiveDelay, 1.ToString() }, { PortParameterKey.Handshake, Handshake.None.ToString() }, { PortParameterKey.RtsEnable, false.ToString() }, { PortParameterKey.ReadTimeout, 2000.ToString() }, { PortParameterKey.ReadBufferSize, 4096.ToString() }, { PortParameterKey.WriteBufferSize, 2048.ToString() } };
            }
            catch ( Exception )
            {
                if ( result is object )
                    result.Dispose();
                throw;
            }

            return result;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        public bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemovePropertyChangedEventHandlers();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~PortParametersDictionary()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="info">    The info. </param>
        /// <param name="context"> The context. </param>
        protected PortParametersDictionary( SerializationInfo info, StreamingContext context ) : base( info, context )
        {
        }

        #endregion

        #region " DICTIONARY MANAGER "

        /// <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="parameters"> Options for controlling the operation. </param>
        private void PopulateThis( PortParametersDictionary parameters )
        {
            if ( parameters is null )
                throw new ArgumentNullException( nameof( parameters ) );
            foreach ( PortParameterKey key in parameters.Keys )
                this.Replace( key, parameters[key] );
        }

        /// <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="parameters"> Options for controlling the operation. </param>
        public void Populate( PortParametersDictionary parameters )
        {
            this.PopulateThis( parameters );
        }

        /// <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
        /// <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
        private void PopulateThis( SerialPort serialPort, int receiveDelay )
        {
            this.Replace( PortParameterKey.PortName, serialPort.PortName );
            this.Replace( PortParameterKey.BaudRate, serialPort.BaudRate.ToString() );
            this.Replace( PortParameterKey.DataBits, serialPort.DataBits.ToString() );
            this.Replace( PortParameterKey.Parity, serialPort.Parity.ToString() );
            this.Replace( PortParameterKey.StopBits, serialPort.StopBits.ToString() );
            this.Replace( PortParameterKey.ReceivedBytesThreshold, serialPort.ReceivedBytesThreshold.ToString() );
            this.Replace( PortParameterKey.Handshake, serialPort.Handshake.ToString() );
            this.Replace( PortParameterKey.RtsEnable, serialPort.RtsEnable.ToString() );
            this.Replace( PortParameterKey.ReadTimeout, serialPort.ReadTimeout.ToString() );
            this.Replace( PortParameterKey.ReadBufferSize, serialPort.ReadBufferSize.ToString() );
            this.Replace( PortParameterKey.WriteBufferSize, serialPort.WriteBufferSize.ToString() );
            this.Replace( PortParameterKey.ReceiveDelay, receiveDelay.ToString() );
        }

        /// <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
        /// <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
        public void Populate( SerialPort serialPort, int receiveDelay )
        {
            if ( serialPort is null )
                throw new ArgumentNullException( nameof( serialPort ) );
            this.PopulateThis( serialPort, receiveDelay );
        }

        /// <summary> Returns the port parameters from the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
        /// <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// </returns>
        public static PortParametersDictionary ToPortParameters( SerialPort serialPort, int receiveDelay )
        {
            return serialPort is null ? new PortParametersDictionary( DefaultPortParameters() ) : new PortParametersDictionary( serialPort, receiveDelay );
        }

        /// <summary> Assigns port parameters to the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort">     The <see cref="SerialPort">serial port.</see> </param>
        /// <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
        /// parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see> </param>
        public static void FromPortParameters( SerialPort serialPort, PortParametersDictionary portParameters )
        {
            if ( serialPort is object && portParameters is object )
            {
                serialPort.PortName = portParameters.PortName;
                serialPort.BaudRate = portParameters.BaudRate;
                serialPort.Parity = portParameters.Parity;
                serialPort.DataBits = portParameters.DataBits;
                serialPort.StopBits = portParameters.StopBits;
                serialPort.ReceivedBytesThreshold = portParameters.ReceivedBytesThreshold;
                serialPort.Handshake = portParameters.Handshake;
                serialPort.RtsEnable = portParameters.RtsEnable;
                // 5000ms will never be reached because we read only bytes present in read buffer
                serialPort.ReadTimeout = portParameters.ReadTimeout;
                serialPort.ReadBufferSize = portParameters.ReadBufferSize;
                serialPort.WriteBufferSize = portParameters.WriteBufferSize;
            }
        }

        /// <summary> The default port parameters. </summary>
        private static PortParametersDictionary _DefaultPortParameters;

        /// <summary> Returns the default port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// </returns>
        public static PortParametersDictionary DefaultPortParameters()
        {
            if ( _DefaultPortParameters is null )
            {
                try
                {
                    _DefaultPortParameters = CreateDefault();
                }
                catch ( Exception )
                {
                    if ( _DefaultPortParameters is object )
                        _DefaultPortParameters.Dispose();
                    _DefaultPortParameters = null;
                    throw;
                }
            }

            return _DefaultPortParameters;
        }

        /// <summary> Replaces the specified key. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public void Replace( PortParameterKey key, string value )
        {
            if ( this.ContainsKey( key ) )
            {
                if ( !this[key].Equals( value, StringComparison.OrdinalIgnoreCase ) )
                {
                    _ = this.Remove( key );
                    this.Add( key, value );
                }
            }
            else
            {
                this.Add( key, value );
            }

            this.NotifyPropertyChanged( key.ToString() );
        }

        #endregion

        #region " FILE STORAGE "

        /// <summary> Restores the collection from the specified file name. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="fileName"> Name of the file. </param>
        public void Restore( string fileName )
        {
            using var sr = new StreamReader( fileName );
            while ( !sr.EndOfStream )
            {
                var values = sr.ReadLine().Split( ',' );
                if ( values.Length == 2 )
                {
                    if ( Enum.TryParse( values[0], out PortParameterKey key ) )
                    {
                        this.Replace( key, values[1] );
                    }
                }
            }
        }

        /// <summary> Store port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="fileName"> Name of the file. </param>
        public void Store( string fileName )
        {
            using var sw = new StreamWriter( fileName );
            foreach ( PortParameterKey key in Enum.GetValues( typeof( PortParameterKey ) ) )
            {
                if ( this.ContainsKey( key ) )
                {
                    sw.WriteLine( "{0},{1}", key, this[key] );
                }
            }
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the name of the port. </summary>
        /// <value> The name of the port. </value>
        public string PortName
        {
            get => this[PortParameterKey.PortName];

            set {
                if ( !string.Equals( value, this.PortName ) )
                {
                    this.Replace( PortParameterKey.PortName, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        public int BaudRate
        {
            get => Conversions.ToInteger( this[PortParameterKey.BaudRate] );

            set {
                if ( value != this.BaudRate )
                {
                    this.Replace( PortParameterKey.BaudRate, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the data bits. </summary>
        /// <value> The data bits. </value>
        public int DataBits
        {
            get => Conversions.ToInteger( this[PortParameterKey.DataBits] );

            set {
                if ( value != this.DataBits )
                {
                    this.Replace( PortParameterKey.DataBits, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Parse parity. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="parity"> The parity. </param>
        /// <returns> A Parity. </returns>
        public static Parity ParseParity( string parity )
        {
            return ( Parity ) Enum.Parse( typeof( Parity ), parity );
        }

        /// <summary> Gets or sets the parity. </summary>
        /// <value> The parity. </value>
        public Parity Parity
        {
            get => ParseParity( this[PortParameterKey.Parity] );

            set {
                if ( value != this.Parity )
                {
                    this.Replace( PortParameterKey.Parity, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Parse stop bits. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="stopBits"> The stop bits. </param>
        /// <returns> The StopBits. </returns>
        public static StopBits ParseStopBits( string stopBits )
        {
            return ( StopBits ) Enum.Parse( typeof( StopBits ), stopBits );
        }

        /// <summary> Gets or sets the stop bits. </summary>
        /// <value> The stop bits. </value>
        public StopBits StopBits
        {
            get => ParseStopBits( this[PortParameterKey.StopBits] );

            set {
                if ( ( int ) value != this.ReceiveDelay )
                {
                }

                this.Replace( PortParameterKey.StopBits, value.ToString() );
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Parse handshake. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="handshake"> The handshake. </param>
        /// <returns> A Handshake. </returns>
        public static Handshake ParseHandshake( string handshake )
        {
            return ( Handshake ) Enum.Parse( typeof( Handshake ), handshake );
        }

        /// <summary> Gets or sets the handshake. </summary>
        /// <value> The handshake. </value>
        public Handshake Handshake
        {
            get => ParseHandshake( this[PortParameterKey.Handshake] );

            set {
                if ( value != this.Handshake )
                {
                    this.Replace( PortParameterKey.Handshake, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Parse RTS enable. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="rtsEnable"> The RTS enable. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool ParseRtsEnable( string rtsEnable )
        {
            return bool.Parse( rtsEnable );
        }

        /// <summary> Gets or sets the RTS enable. </summary>
        /// <value> The RTS enable. </value>
        public bool RtsEnable
        {
            get => ParseRtsEnable( this[PortParameterKey.RtsEnable] );

            set {
                if ( value != this.RtsEnable )
                {
                    this.Replace( PortParameterKey.RtsEnable, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the read timeout. </summary>
        /// <value> The read timeout. </value>
        public int ReadTimeout
        {
            get => Conversions.ToInteger( this[PortParameterKey.ReadTimeout] );

            set {
                if ( value != this.ReadTimeout )
                {
                    this.Replace( PortParameterKey.ReadTimeout, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the size of the read buffer. </summary>
        /// <value> The size of the read buffer. </value>
        public int ReadBufferSize
        {
            get => Conversions.ToInteger( this[PortParameterKey.ReadBufferSize] );

            set {
                if ( value != this.ReadBufferSize )
                {
                    this.Replace( PortParameterKey.ReadBufferSize, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the receive delay. </summary>
        /// <value> The receive delay. </value>
        public int ReceiveDelay
        {
            get => Conversions.ToInteger( this[PortParameterKey.ReceiveDelay] );

            set {
                if ( value != this.ReceiveDelay )
                {
                    this.Replace( PortParameterKey.ReceiveDelay, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the received bytes threshold. </summary>
        /// <value> The received bytes threshold. </value>
        public int ReceivedBytesThreshold
        {
            get => Conversions.ToInteger( this[PortParameterKey.ReceivedBytesThreshold] );

            set {
                if ( value != this.ReceivedBytesThreshold )
                {
                    this.Replace( PortParameterKey.ReceivedBytesThreshold, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the size of the write buffer. </summary>
        /// <value> The size of the write buffer. </value>
        public int WriteBufferSize
        {
            get => Conversions.ToInteger( this[PortParameterKey.WriteBufferSize] );

            set {
                if ( value != this.WriteBufferSize )
                {
                    this.Replace( PortParameterKey.WriteBufferSize, value.ToString() );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Character bit count. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="startBitsCount"> Number of start bits. </param>
        /// <returns> A Double. </returns>
        public double CharacterBitCount( int startBitsCount )
        {
            double bitCount = this.DataBits + (this.Parity == Parity.None ? 0 : 1);
            switch ( this.StopBits )
            {
                case StopBits.None:
                    {
                        break;
                    }

                case StopBits.One:
                    {
                        bitCount += 1d;
                        break;
                    }

                case StopBits.OnePointFive:
                    {
                        bitCount += 1.5d;
                        break;
                    }

                case StopBits.Two:
                    {
                        bitCount += 2d;
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

            bitCount += startBitsCount;
            return bitCount;
        }

        /// <summary> Minimum time to transmit the number of character specified. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="startBitCount">  Number of start bits. </param>
        /// <param name="characterCount"> Number of characters. </param>
        /// <returns> A TimeSpan. </returns>
        public TimeSpan MinimumTransitTimespan( int startBitCount, int characterCount )
        {
            double bitCount = this.CharacterBitCount( startBitCount );
            return TimeSpan.FromTicks( ( long ) (characterCount * TimeSpan.TicksPerSecond * bitCount / this.BaudRate) );
        }

        /// <summary> Minimum time to transmit the number of character specified. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="characterCount"> Number of characters. </param>
        /// <returns> A TimeSpan. </returns>
        public TimeSpan MinimumTransitTimespan( int characterCount )
        {
            return this.MinimumTransitTimespan( 1, characterCount );
        }
        #endregion

    }
}
