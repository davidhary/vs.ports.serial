﻿using System.Collections.Generic;
using System.ComponentModel;

namespace isr.Ports.Serial
{

    /// <summary> Defines a message parser entity. </summary>
        /// <remarks>
        /// This entity receives the input buffer that the port accumulates. It then parses the message
        /// and returns true upon deciding that a valid message was received. <para>
        /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    public interface IMessageParser
    {

        /// <summary> Gets or sets the sentinel indicating that the parser is enabled. </summary>
        /// <value> The sentinel indicating that the parser is enabled. </value>
        bool ParseEnabled { get; set; }

        /// <summary> Parses the given values. </summary>
        /// <param name="values"> The values. </param>
        /// <returns> A MessageParserOutcome. </returns>
        MessageParserOutcome Parse( IEnumerable<byte> values );
    }

        /// <summary> Enumerates the message parser outcomes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
    public enum MessageParserOutcome
    {

        /// <summary> An enum constant representing the none option.
        /// Indicates that the message was not yet parsed</summary>
        [Description( "Value not set" )]
        None,

        /// <summary> An enum constant representing the complete option.
        /// Indicates that the message is complete</summary>
        [Description( "Complete" )]
        Complete,

        /// <summary> An enum constant representing the invalid option.
        /// Indicates that the message is invalid. This means that the provided buffer
        /// has invalid structure and needs to be cleared.</summary>
        [Description( "Invalid" )]
        Invalid,

        /// <summary> An enum constant representing the incomplete option.
        /// Indicates that the message is incomplete.
        /// This means that the message parser expects additional information.
        /// </summary>
        [Description( "Message incomplete" )]
        Incomplete
    }
}