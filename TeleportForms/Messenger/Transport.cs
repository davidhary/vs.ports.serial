using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Ports.Teleport.Forms
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary>
    /// Includes <see cref="IProtocolMessage">protocol messages</see> that are sent and received from
    /// the module and their status values.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Transport : Teleport.Transport
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        public Transport( IProtocolMessage templateProtocolMessage ) : base( templateProtocolMessage )
        {
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transportProtocolMessage"> Message describing the transport protocol. </param>
        /// <param name="sentProtocolMessage">      Message describing the sent protocol. </param>
        /// <param name="receivedProtocolMessage">  Message describing the received protocol. </param>
        public Transport( IProtocolMessage transportProtocolMessage, IProtocolMessage sentProtocolMessage, IProtocolMessage receivedProtocolMessage ) : base( transportProtocolMessage, sentProtocolMessage, receivedProtocolMessage )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transport"> The message. </param>
        public Transport( ITransport transport ) : base( transport )
        {
        }

        #endregion

        #region " PARSE GRID ROW "

        /// <summary> Parses the specified row. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="row"> The row. </param>
        public void Parse( DataGridViewRow row )
        {
            this.Clear();
            if ( row is null )
            {
                return;
            }

            foreach ( DataGridViewCell cell in row.Cells )
            {
                switch ( row.DataGridView.Columns[cell.ColumnIndex].Name ?? "" )
                {
                    case nameof( this.ItemNumber ):
                        {
                            this.ItemNumber = Conversions.ToInteger( cell.Value );
                            break;
                        }

                    case nameof( this.TransportStatus ):
                        {
                            if ( cell.Value is object )
                            {
                                this.TransportStatus = ( StatusCode ) Conversions.ToByte( cell.Value );
                            }

                            break;
                        }

                    case nameof( this.TransportHexMessage ):
                        {
                            string stringValue = cell.Value as string;
                            if ( !string.IsNullOrEmpty( stringValue ) )
                            {
                                _ = this.TransportProtocolMessage.ParseHexMessage( stringValue );
                            }

                            break;
                        }

                    case nameof( this.SentHexMessage ):
                        {
                            string stringValue = cell.Value as string;
                            if ( !string.IsNullOrEmpty( stringValue ) )
                            {
                                _ = this.SentProtocolMessage.ParseHexMessage( stringValue );
                            }

                            break;
                        }

                    case nameof( this.SendStatus ):
                        {
                            if ( cell.Value is object )
                            {
                                this.ReceiveStatus = ( StatusCode ) Conversions.ToByte( cell.Value );
                            }

                            break;
                        }

                    case nameof( this.ReceivedHexMessage ):
                        {
                            string stringValue = cell.Value as string;
                            if ( !string.IsNullOrEmpty( stringValue ) )
                            {
                                _ = this.ReceivedProtocolMessage.ParseHexMessage( stringValue );
                            }

                            break;
                        }

                    case nameof( this.ReceiveStatus ):
                        {
                            if ( cell.Value is object )
                            {
                                this.ReceiveStatus = ( StatusCode ) Conversions.ToByte( cell.Value );
                            }

                            break;
                        }
                    // ignored.
                    case nameof( this.CommandDescription ):
                        {
                            break;
                        }

                    default:
                        {
                            if ( row.DataGridView.Columns[cell.ColumnIndex].Visible )
                            {
                                // this no longer works because not all columns are displayed and the visible
                                // property does not seem to mark an invisible column
                                // Debug.Assert(False, "Unhandled Column Name")
                            }

                            break;
                        }
                }
            }
        }

        #endregion
    }

    /// <summary> Implements a collection of <see cref="Transport">Transport</see> entities. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public class TransportCollection : Teleport.TransportCollection
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCollection" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transport"> The transport. </param>
        public TransportCollection( ITransport transport ) : base( transport )
        {
        }

        #endregion

        #region " DISPLAY MANAGEMENT "

        /// <summary> Configure display values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid"> The grid. </param>
        /// <returns> An Integer. </returns>
        public int ConfigureDisplayValues( DataGridView grid )
        {
            if ( grid is null )
                throw new ArgumentNullException( nameof( grid ) );
            bool wasEnabled = grid.Enabled;
            grid.Enabled = false;
            grid.Enabled = wasEnabled;
            grid.Enabled = false;
            grid.DataSource = null;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSteelBlue;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grid.AutoGenerateColumns = false;
            grid.RowHeadersVisible = false;
            grid.ReadOnly = true;
            grid.DataSource = this;
            grid.Columns.Clear();
            grid.Refresh();
            int width = 0;
            DataGridViewTextBoxColumn column = null;
            int displayIndex;
            try
            {
                displayIndex = 0;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.ItemNumber ),
                    Name = "Item",
                    Width = 40,
                    DisplayIndex = displayIndex
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.CommandDescription ),
                    Name = "Description",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = 70
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.TransportStatus ),
                    Name = "Status",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = 50
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.TransportHexMessage ),
                    Name = "Message",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = 50
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.SendStatus ),
                    Name = "Send",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = 50
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.SentHexMessage ),
                    Name = "Sent",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = 50
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.ReceiveStatus ),
                    Name = "Receive",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = 50
                };
                _ = grid.Columns.Add( column );
                width += column.Width;
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            try
            {
                displayIndex += 1;
                column = new DataGridViewTextBoxColumn() {
                    DataPropertyName = nameof( Transport.ReceivedHexMessage ),
                    Name = "Received",
                    Visible = true,
                    DisplayIndex = displayIndex,
                    Width = grid.Width - width - grid.Columns.Count
                };
                _ = grid.Columns.Add( column );
            }
            catch
            {
                if ( column is object )
                    column.Dispose();
                throw;
            }

            grid.Enabled = true;
            return grid.Columns is object && grid.Columns.Count > 0 ? grid.Columns.Count : 0;
        }

        /// <summary> Displays the values described by grid. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid"> The grid. </param>
        /// <returns> An Integer. </returns>
        public int DisplayValues( DataGridView grid )
        {
            if ( grid is null )
                throw new ArgumentNullException( nameof( grid ) );
            bool wasEnabled = grid.Enabled;
            grid.Enabled = false;
            grid.Enabled = wasEnabled;
            if ( grid.DataSource is null )
            {
                _ = this.ConfigureDisplayValues( grid );
                Core.ApplianceBase.DoEvents();
            }

            grid.DataSource = this.ToList();
            Core.ApplianceBase.DoEvents();
            return grid.Columns.Count;
        }


        #endregion

    }

    /// <summary> A class handling the implementation of display ordering. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public class ColumnDisplayOrderCollection : List<KeyValuePair<string, int>>
    {

        /// <summary> Adds key. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public void Add( string key, int value )
        {
            this.Add( new KeyValuePair<string, int>( key, value ) );
        }
    }
}
