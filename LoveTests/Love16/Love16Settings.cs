﻿using isr.Core;

namespace isr.Ports.LoveTests
{

    /// <summary> Love protocol settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class Love16Settings : ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private Love16Settings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public static void OpenSettingsEditor()
        {
            WindowsForms.EditConfiguration($"{typeof(Love16Settings)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static Love16Settings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static Love16Settings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (Love16Settings)Synchronized(new Love16Settings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " PORT INFORMATION "

        /// <summary> Gets or sets the name of the port. </summary>
        /// <value> The name of the port. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("COM3")]
        public string PortName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("9600")]
        public int BaudRate
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the data bits. </summary>
        /// <value> The data bits. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("8")]
        public int DataBits
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the stop bits. </summary>
        /// <value> The stop bits. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("One")]
        public System.IO.Ports.StopBits StopBits
        {
            get
            {
                return AppSettingEnum<System.IO.Ports.StopBits>();
            }

            set
            {
                AppSettingSetter(value.ToString());
            }
        }

        /// <summary> Gets or sets the parity. </summary>
        /// <value> The parity. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("None")]
        public System.IO.Ports.Parity Parity
        {
            get
            {
                return AppSettingEnum<System.IO.Ports.Parity>();
            }

            set
            {
                AppSettingSetter(value.ToString());
            }
        }

        #endregion

        #region " MODULE INFORMATION "

        /// <summary> Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("02")]
        public string ModuleAddress
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " TEMPERATURE "

        /// <summary> Gets or sets the setpoint temperature. </summary>
        /// <value> The setpoint temperature. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("18")]
        public double SetpointTemperature
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}