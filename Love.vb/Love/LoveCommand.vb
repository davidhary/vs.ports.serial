﻿Imports System.ComponentModel

Imports isr.Core.EnumExtensions
Imports isr.Ports.Teleport

''' <summary> Defines the Love protocol command. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-16 </para>
''' </remarks>
Public Class LoveCommand
    Inherits ProtocolCommand

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The command code. </param>
    Public Sub New(ByVal commandCode As LoveCommandCode)
        MyBase.New(commandCode)
    End Sub

    ''' <summary>
    ''' Gets or sets the time it takes from the receipt of the command to when the module starts to
    ''' transmit a response.
    ''' </summary>
    ''' <value> The command timeout. </value>
    Public Overrides Property TurnaroundTime As TimeSpan
        Get
            Return LoveCommand.SelectTurnaroundTime(CType(Me.CommandCode, LoveCommandCode))
        End Get
        Set(value As TimeSpan)
            MyBase.TurnaroundTime = value
        End Set
    End Property

    ''' <summary> List of times of the turnarounds. </summary>
    Private Shared _TurnaroundTimes As Dictionary(Of LoveCommandCode, TimeSpan)

    ''' <summary> Gets a list of times of the turnarounds. </summary>
    ''' <value> A list of times of the turnarounds. </value>
    Public Shared ReadOnly Property TurnaroundTimes As Dictionary(Of LoveCommandCode, TimeSpan)
        Get
            If Love.LoveCommand._TurnaroundTimes Is Nothing Then
                Love.LoveCommand._TurnaroundTimes = New Dictionary(Of LoveCommandCode, TimeSpan) From {
                    {LoveCommandCode.ReadSetpoint, TimeSpan.FromMilliseconds(10)},
                    {LoveCommandCode.ReadTemperature, TimeSpan.FromMilliseconds(10)},
                    {LoveCommandCode.WriteSetpoint, TimeSpan.FromMilliseconds(10)},
                    {LoveCommandCode.WriteSetpointProcess2, TimeSpan.FromMilliseconds(10)}
                }
            End If
            Return Love.LoveCommand._TurnaroundTimes
        End Get
    End Property

    ''' <summary> The default turnaround time. </summary>
    ''' <value> The default turnaround time. </value>
    Public Shared Property DefaultTurnaroundTime As TimeSpan = TimeSpan.FromMilliseconds(10)

    ''' <summary> Select turnaround time. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The command code. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function SelectTurnaroundTime(ByVal commandCode As LoveCommandCode) As TimeSpan
        If LoveCommand.TurnaroundTimes.ContainsKey(commandCode) Then
            Return LoveCommand.TurnaroundTimes(commandCode)
        Else
            Return LoveCommand.DefaultTurnaroundTime
        End If
    End Function

    ''' <summary> The refractory periods. </summary>
    Private Shared _RefractoryPeriods As Dictionary(Of LoveCommandCode, TimeSpan)

    ''' <summary> Gets a list of Refractory periods. </summary>
    ''' <value> A list of Refractory periods. </value>
    Public Shared ReadOnly Property RefractoryPeriods As Dictionary(Of LoveCommandCode, TimeSpan)
        Get
            If Love.LoveCommand._RefractoryPeriods Is Nothing Then
                Love.LoveCommand._RefractoryPeriods = New Dictionary(Of LoveCommandCode, TimeSpan) From {
                    {LoveCommandCode.WriteSetpoint, TimeSpan.FromMilliseconds(750)},
                    {LoveCommandCode.WriteSetpointProcess2, TimeSpan.FromMilliseconds(750)}
                }
            End If
            Return Love.LoveCommand._RefractoryPeriods
        End Get
    End Property

    ''' <summary> The default refractory period. </summary>
    ''' <value> The default refractory period. </value>
    Public Shared Property DefaultRefractoryPeriod As TimeSpan = TimeSpan.Zero

    ''' <summary> Select Refractory Period. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The command code. </param>
    ''' <returns> A Timespan. </returns>
    Public Shared Function SelectRefractoryPeriod(ByVal commandCode As LoveCommandCode) As TimeSpan
        If LoveCommand.RefractoryPeriods.ContainsKey(commandCode) Then
            Return LoveCommand.RefractoryPeriods(commandCode)
        Else
            Return LoveCommand.DefaultRefractoryPeriod
        End If
    End Function

End Class

''' <summary> Dictionary of Love protocol commands. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-02 </para>
''' </remarks>
Public NotInheritable Class LoveCommandCollection
    Inherits ProtocolCommandCollection

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub New()
        MyBase.New(GetType(LoveCommandCode))
    End Sub

#End Region

#Region " SINGLETON "

    ''' <summary>
    ''' Gets or sets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets or sets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As LoveCommandCollection

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As LoveCommandCollection
        If LoveCommandCollection._Instance Is Nothing Then
            SyncLock _SyncLocker
                LoveCommandCollection._Instance = New LoveCommandCollection
            End SyncLock
        End If
        Return LoveCommandCollection._Instance
    End Function

#End Region

#Region " LOVE COMMAND "

    ''' <summary> Love command. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> A LoveCommand. </returns>
    Public Function LoveCommand(ByVal commandAscii As String) As LoveCommand
        Return New LoveCommand(CType(Me.Command(commandAscii).CommandCode, LoveCommandCode))
    End Function

#End Region

End Class

''' <summary> Defines the Love protocol response. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-16 </para>
''' </remarks>
Public Class LoveResponse
    Inherits ProtocolCommand

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The response code. </param>
    Public Sub New(ByVal commandCode As LoveResponseCode)
        MyBase.New(commandCode)
    End Sub

    ''' <summary>
    ''' Gets or sets the time it takes from the receipt of the command to when the module starts to
    ''' transmit a response.
    ''' </summary>
    ''' <value> The command timeout. </value>
    Public Overrides Property TurnaroundTime As TimeSpan
        Get
            Return LoveResponse.SelectTurnaroundTime(CType(Me.CommandCode, LoveResponseCode))
        End Get
        Set(value As TimeSpan)
            MyBase.TurnaroundTime = value
        End Set
    End Property

    ''' <summary> List of times of the turnarounds. </summary>
    Private Shared _TurnaroundTimes As Dictionary(Of LoveResponseCode, TimeSpan)

    ''' <summary> Gets a list of times of the turnarounds. </summary>
    ''' <value> A list of times of the turnarounds. </value>
    Public Shared ReadOnly Property TurnaroundTimes As Dictionary(Of LoveResponseCode, TimeSpan)
        Get
            If Love.LoveResponse._TurnaroundTimes Is Nothing Then
                Love.LoveResponse._TurnaroundTimes = New Dictionary(Of LoveResponseCode, TimeSpan) From {
                    {LoveResponseCode.Success, TimeSpan.FromMilliseconds(10)}}
            End If
            Return Love.LoveResponse._TurnaroundTimes
        End Get
    End Property

    ''' <summary> Gets or sets the default turnaround time. </summary>
    ''' <value> The default turnaround time. </value>
    Public Shared Property DefaultTurnaroundTime As TimeSpan = TimeSpan.FromMilliseconds(10)

    ''' <summary> Select turnaround time. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The response code. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function SelectTurnaroundTime(ByVal commandCode As LoveResponseCode) As TimeSpan
        If LoveResponse.TurnaroundTimes.ContainsKey(commandCode) Then
            Return LoveResponse.TurnaroundTimes(commandCode)
        Else
            Return LoveResponse.DefaultTurnaroundTime
        End If
    End Function

End Class

''' <summary> Dictionary of Love protocol responses. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-02 </para>
''' </remarks>
Public NotInheritable Class LoveResponseCollection
    Inherits ProtocolCommandCollection

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub New()
        MyBase.New(GetType(LoveResponseCode))
    End Sub

#End Region

#Region " SINGLETON "

    ''' <summary>
    ''' Gets or sets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets or sets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As LoveResponseCollection

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As LoveResponseCollection
        If LoveResponseCollection._Instance Is Nothing Then
            SyncLock _SyncLocker
                LoveResponseCollection._Instance = New LoveResponseCollection
            End SyncLock
        End If
        Return LoveResponseCollection._Instance
    End Function

#End Region

#Region " LOVE COMMAND "

    ''' <summary> Love Response. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="responseAscii"> The response ASCII. </param>
    ''' <returns> A LoveCommand. </returns>
    Public Function LoveResponse(ByVal responseAscii As String) As LoveResponse
        Return New LoveResponse(CType(Me.Command(responseAscii).CommandCode, LoveResponseCode))
    End Function

#End Region

End Class

''' <summary> Values that represent the Love protocol commands. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum LoveCommandCode

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("(None)")>
    None

    ''' <summary> An enum constant representing the read temperature option. </summary>
    <Description("Read Temperature (00)")>
    ReadTemperature

    ''' <summary> An enum constant representing the read setpoint option. </summary>
    <Description("Read Setpoint (0100)")>
    ReadSetpoint

    ''' <summary> An enum constant representing the write setpoint option. </summary>
    <Description("Write Setpoint (0200)")>
    WriteSetpoint

    ''' <summary> An enum constant representing the write setpoint process 2 option. </summary>
    <Description("Write Setpoint Process 2 (0201)")>
    WriteSetpointProcess2
End Enum

''' <summary> Values that represent the Love protocol responses. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum LoveResponseCode

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("(None)")>
    None

    ''' <summary> An enum constant representing the success option. </summary>
    <Description("Success (00)")>
    Success
End Enum

