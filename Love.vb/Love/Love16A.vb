﻿''' <summary> A Love 16A Temperature Controller Module class. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-02 </para>
''' </remarks>
Public Class Love16A
    Inherits LoveBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddressAscii"> The module address ASCII. </param>
    Public Sub New(moduleAddressAscii As String)
        MyBase.New(moduleAddressAscii)
    End Sub

#End Region

#Region " PRESETTABLE "

    ''' <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState(NameOf(Love16A))
        Me.PublishVerbose($"Done initializing {Me.ModuleName}.{Me.Messenger.ModuleAddress} known state")
    End Sub

#End Region

End Class
