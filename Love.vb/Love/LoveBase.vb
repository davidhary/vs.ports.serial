Imports isr.Core.Constructs
Imports isr.Core.EnumExtensions
Imports isr.Ports.Serial
Imports isr.Ports.Love.ExceptionExtensions
Imports isr.Ports.Teleport

''' <summary> A base class for the Love Protocol modules. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-10 </para>
''' </remarks>
Public MustInherit Class LoveBase
    Inherits Ports.Teleport.Scribe

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messenger"> The messenger. </param>
    Private Sub New(ByVal messenger As IMessenger)
        MyBase.New(messenger)
        MyBase.Messenger.ParseEnabled = False
        MyBase.Messenger.Transport.PopulateCommands(LoveCommandCollection.Get)
        MyBase.Messenger.OutputMessage = ProtocolMessage.Create
        MyBase.Messenger.InputMessage = ProtocolMessage.Create
        MyBase.Messenger.Transport.TransportProtocolMessage = ProtocolMessage.Create
        MyBase.Messenger.Transport.SentProtocolMessage = ProtocolMessage.Create
        MyBase.Messenger.Transport.ReceivedProtocolMessage = ProtocolMessage.Create
        Me.SetpointWindowTolerance = 0.01
        Me._SetpointWindow = New RangeR(0.2)
        Me._SetpointAccuracyRange = New RangeR(0.1)
        Me._TemperatureAccuracyRange = New RangeR(0.1)
        AddHandler My.Settings.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
    End Sub

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddressAscii"> The module address ASCII. </param>
    Protected Sub New(ByVal moduleAddressAscii As String)
        Me.New(Emitter.Create(moduleAddressAscii.ToBytes, Love.ProtocolMessage.Create()))
        Me._ModuleAddressAscii = moduleAddressAscii
    End Sub

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddressAscii"> The module address ASCII. </param>
    ''' <param name="port">               The port. </param>
    Protected Sub New(ByVal moduleAddressAscii As String, ByVal port As Serial.IPort)
        Me.New(Emitter.Create(moduleAddressAscii.ToBytes, Love.ProtocolMessage.Create(), port))
        Me._ModuleAddressAscii = moduleAddressAscii
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    RemoveHandler My.Settings.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PRESETTABLE "

    ''' <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    Public Overridable Sub InitKnownState()
    End Sub

    ''' <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    ''' <param name="moduleName"> The name of the module. </param>
    Public Sub InitKnownState(ByVal moduleName As String)
        Me.ModuleName = moduleName
        Me.PublishVerbose($"{Me.ModuleName}.{Me.Messenger.ModuleAddress} initializing known state")
        ' Me._AnalogInputPayload = New RealValuePayload() With {.Unit = Arebis.StandardUnits.ElectricUnits.Volt}
    End Sub

#End Region

#Region " PORT "

    ''' <summary> Configure port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="port">     The port. </param>
    ''' <param name="portName"> Name of the port. </param>
    ''' <param name="e">        Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryOpenPort(ByVal port As Serial.IPort, ByVal portName As String, ByVal e As Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If port Is Nothing Then Throw New ArgumentNullException(NameOf(port))
        Dim activity As String = $"opening port {portName}"
        Try
            activity = $"checking if {portName} is open"
            If port.IsOpen Then
                e.RegisterOutcomeEvent(TraceEventType.Information, $"{activity}; port {portName} is already open")
            Else
                activity = $"setting port {portName} parameters"
                port.PortParameters.PortName = portName
                port.PortParameters.BaudRate = isr.Ports.Love.My.MySettings.Default.BaudRate
                port.PortParameters.DataBits = isr.Ports.Love.My.MySettings.Default.DataBits
                port.PortParameters.Parity = isr.Ports.Love.My.MySettings.Default.Parity
                port.PortParameters.StopBits = isr.Ports.Love.My.MySettings.Default.StopBits
                port.PortParameters.ReceivedBytesThreshold = 1
                port.PortParameters.ReceiveDelay = 1
                activity = $"opening {portName}"
                port.TryOpen(e)
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Attempts to open port from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portName"> Name of the port. </param>
    ''' <param name="e">        Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryOpenPort(ByVal portName As String, ByVal e As Core.ActionEventArgs) As Boolean
        Return LoveBase.TryOpenPort(Me.Messenger.Port, portName, e)
    End Function

    ''' <summary> Attempts to close port from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryClosePort(ByVal e As Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e)) ''
        Dim activity As String = String.Empty
        Try
            activity = $"checking if port is open"
            If Me.IsPortOpen Then
                activity = $"closing {Me.Messenger.Port.SerialPort.PortName}"
                Me.Messenger.Port.TryClose(e)
            Else
                Me.Publish(TraceEventType.Information, $"{activity}; port already closed")
            End If
        Catch ex As Exception
            e.RegisterFailure(Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, $"Exception {activity};. {ex.ToFullBlownString}"))
        End Try
        Return Not e.Failed
    End Function

#End Region

#Region " COMMANDS "

    ''' <summary> Turnaround time. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> A timespan. </returns>
    Public Function TurnaroundTime(ByVal commandAscii As String) As TimeSpan
        Return Me.Messenger.Transport.SelectCommand(commandAscii).TurnaroundTime
    End Function

    ''' <summary> Converts a commandAscii to a command code. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> CommandAscii as a LoveCommandCode. </returns>
    Public Function ToCommandCode(ByVal commandAscii As String) As LoveCommandCode
        Return LoveBase.ToCommandCode(Me.Messenger.Transport.SelectCommand(commandAscii).CommandCode)
    End Function

    ''' <summary> Converts a commandAscii to a command code. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> CommandAscii as a LoveCommandCode. </returns>
    Public Shared Function ToCommandCode(ByVal value As Integer) As LoveCommandCode
        Return CType(value, LoveCommandCode)
    End Function


#End Region

#Region " MODULE INFO "

    ''' <summary> Gets or sets the module address ASCII. </summary>
    ''' <value> The module address ASCII. </value>
    Public ReadOnly Property ModuleAddressAscii As String

    ''' <summary> Name of the module. </summary>
    Private _ModuleName As String

    ''' <summary> Gets or sets the name of the module. </summary>
    ''' <value> The name of the module. </value>
    Public Property ModuleName As String
        Get
            Return Me._ModuleName
        End Get
        Set(value As String)
            If Not String.Equals(Me.ModuleName, value) Then
                Me._ModuleName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " TEMPERATURE "

    ''' <summary> The temperature. </summary>
    Private _Temperature As Double?

    ''' <summary> Gets or sets the temperature. </summary>
    ''' <value> The temperature. </value>
    Public Property Temperature As Double?
        Get
            Return Me._Temperature
        End Get
        Protected Set(value As Double?)
            If Not Nullable.Equals(value, Me.Temperature) Then
                Me._Temperature = value
                Me.NotifyPropertyChanged()
            End If
            Me.WithinSetpointWindow = Me.IsWithinAppliedSetpointWindow
        End Set
    End Property

    ''' <summary> The temperature accuracy range. </summary>
    Private _TemperatureAccuracyRange As Core.Constructs.RangeR

    ''' <summary> Gets or sets the Temperature accuracy range. </summary>
    ''' <value> The Temperature accuracy range. </value>
    Public Property TemperatureAccuracyRange As Core.Constructs.RangeR
        Get
            Return Me._TemperatureAccuracyRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If value <> Me.TemperatureAccuracyRange Then
                Me._TemperatureAccuracyRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Attempts to read temperature from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="retryCount">   Number of retries. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="e">            Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Function TryReadTemperature(ByVal retryCount As Integer, ByVal pollInterval As TimeSpan, ByVal e As Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = $"reading {Me.ModuleName} temperature"
        Try
            activity = $"checking if {Me.Messenger.Port.SerialPort.PortName} is open"
            If Me.IsPortOpen Then
                Dim status As StatusCode = StatusCode.ValueNotSet
                Do Until retryCount = 0 OrElse status = StatusCode.Okay
                    activity = $"reading {Me.ModuleName} temperature"
                    retryCount -= 1
                    status = Me.ReadTemperature()
                    If status = StatusCode.Okay Then
                        Me.Temperature = Me.ReadTemperaturePayload.Temperature
                        Me.TemperatureAccuracyRange = New RangeR(Math.Pow(10, -Me.ReadTemperaturePayload.Accuracy))
                        isr.Core.ApplianceBase.DoEvents()
                    Else
                        Threading.Thread.SpinWait(10)
                        isr.Core.ApplianceBase.DoEventsWait(pollInterval)
                    End If
                Loop
                If status <> StatusCode.Okay Then
                    e.RegisterFailure(Me.Publish(TraceEventType.Warning, $"Failed {activity} with status {CInt(status)}:{status.Description}"))
                End If
            Else
                Throw New InvalidOperationException($"Failed {activity}; Serial port to this device is not open")
            End If
        Catch ex As Exception
            e.RegisterFailure(Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}"))
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> The setpoint window tolerance. </summary>
    Private _SetpointWindowTolerance As Double

    ''' <summary> Gets or sets the setpoint window tolerance. </summary>
    ''' <value> The setpoint window tolerance. </value>
    Public Property SetpointWindowTolerance As Double
        Get
            Return Me._SetpointWindowTolerance
        End Get
        Set(value As Double)
            If value <> Me.SetpointWindowTolerance Then
                Me._SetpointWindowTolerance = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The setpoint window. </summary>
    Private _SetpointWindow As Core.Constructs.RangeR

    ''' <summary>
    ''' Gets or sets the setpoint window. The heater is at setpoint if its temperature is within the
    ''' <see cref="SetpointWindow"> setpoint window </see>with the specified
    ''' <see cref="SetpointWindowTolerance"/> tolerance.
    ''' </summary>
    ''' <value> The setpoint window. </value>
    Public Property SetpointWindow As Core.Constructs.RangeR
        Get
            Return Me._SetpointWindow
        End Get
        Set(value As Core.Constructs.RangeR)
            If value <> Me.SetpointWindow Then
                Me._SetpointWindow = value
                Me.NotifyPropertyChanged()
            End If
            Me.WithinSetpointWindow = Me.IsWithinAppliedSetpointWindow
        End Set
    End Property

    ''' <summary> Query if this object is within the applied setpoint window. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> <c>true</c> if within setpoint window; otherwise <c>false</c> </returns>
    Public Function IsWithinAppliedSetpointWindow() As Boolean
        Return Me.AppliedSetpointTemperature.HasValue AndAlso Me.SetpointTemperature.HasValue AndAlso
               Me.SetpointAccuracyRange.Contains(Me.SetpointTemperature.Value - Me.AppliedSetpointTemperature.Value) AndAlso
               Me.Temperature.HasValue AndAlso
               Me.SetpointWindow.Contains(Me.Temperature.Value - Me.SetpointTemperature.Value, Me.SetpointWindowTolerance)
    End Function

    ''' <summary> True to within setpoint window. </summary>
    Private _WithinSetpointWindow As Boolean

    ''' <summary> Gets or sets the within setpoint window. </summary>
    ''' <value> The within setpoint window. </value>
    Public Property WithinSetpointWindow As Boolean
        Get
            Return Me._WithinSetpointWindow
        End Get
        Set(value As Boolean)
            If value <> Me.WithinSetpointWindow Then
                Me._WithinSetpointWindow = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " TEMPERATURE: READ "

    ''' <summary> Gets or sets the read temperature payload. </summary>
    ''' <value> The temperature payload. </value>
    Public ReadOnly Property ReadTemperaturePayload As ReadTemperaturePayload

    ''' <summary> Reads the temperature. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The temperature. </returns>
    Public Function ReadTemperature() As StatusCode
        Me._ReadTemperaturePayload = New ReadTemperaturePayload()
        Dim activity As String = "building temperature query command"
        Using replyMessage As IProtocolMessage = New ProtocolMessage()
            replyMessage.ParseEnabled = True
            replyMessage.Prefix = ProtocolMessage.LovePrompt
            replyMessage.ModuleAddress = Me.Messenger.ModuleAddress
            Me.Messenger.Port.MessageParser = replyMessage
            replyMessage.MessageType = MessageType.Response
            MyBase.Messenger.InputMessage = replyMessage
            Using commandMessage As IProtocolMessage = New ProtocolMessage()
                commandMessage.ParseEnabled = False
                commandMessage.Prefix = ProtocolMessage.LovePrompt
                commandMessage.ModuleAddress = Me.Messenger.ModuleAddress
                commandMessage.CommandAscii = Me.Messenger.Transport.Commands(LoveCommandCode.ReadTemperature).CommandAscii
                commandMessage.MessageType = MessageType.Command
                activity = "reading temperature"
                MyBase.Messenger.OutputMessage = commandMessage
                Me.Query(commandMessage, My.Settings.ModuleReadTimeout)
            End Using
        End Using
        If Me.Transport.IsSuccess Then
            Me.Transport.TransportStatus = Me.ReadTemperaturePayload.Parse(Me.Transport.ReceivedProtocolMessage.Payload)
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReadTemperaturePayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReadTemperaturePayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReadTemperaturePayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReadTemperaturePayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Publish(TraceEventType.Verbose, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Publish(TraceEventType.Warning, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

    ''' <summary> Reply read temperature payload. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyReadTemperature() As StatusCode
        Me._ReadTemperaturePayload = New ReadTemperaturePayload()
        Dim activity As String = "building read module temperature reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.LovePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(LoveCommandCode.ReadTemperature).CommandAscii
            message.Payload = ReadTemperaturePayload.SimulatePayload().Payload
            activity = "replying read temperature"
            Me.Query(message, My.Settings.ModuleReadTimeout)
        End Using
        If Me.Transport.IsSuccess Then
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReadTemperaturePayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReadTemperaturePayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReadTemperaturePayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReadTemperaturePayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Publish(TraceEventType.Verbose, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Publish(TraceEventType.Warning, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " SETPOINT TEMPERATURE "

    ''' <summary> The applied setpoint temperature. </summary>
    Private _AppliedSetpointTemperature As Double?

    ''' <summary> Gets or sets the applied setpoint temperature. </summary>
    ''' <value> The applied setpoint temperature. </value>
    Public Property AppliedSetpointTemperature As Double?
        Get
            Return Me._AppliedSetpointTemperature
        End Get
        Protected Set(value As Double?)
            If Not Nullable.Equals(value, Me.AppliedSetpointTemperature) Then
            End If
            Me._AppliedSetpointTemperature = value
            Me.NotifyPropertyChanged()
            Me.WithinSetpointWindow = Me.IsWithinAppliedSetpointWindow
        End Set
    End Property

    ''' <summary> The setpoint accuracy range. </summary>
    Private _SetpointAccuracyRange As Core.Constructs.RangeR

    ''' <summary> Gets or sets the setpoint accuracy range. </summary>
    ''' <value> The setpoint accuracy range. </value>
    Public Property SetpointAccuracyRange As Core.Constructs.RangeR
        Get
            Return Me._SetpointAccuracyRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If value <> Me.SetpointAccuracyRange Then
                Me._SetpointAccuracyRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The setpoint temperature. </summary>
    Private _SetpointTemperature As Double?

    ''' <summary> Gets or sets the setpoint temperature. </summary>
    ''' <value> The setpoint temperature. </value>
    Public Property SetpointTemperature As Double?
        Get
            Return Me._SetpointTemperature
        End Get
        Protected Set(value As Double?)
            If Not Nullable.Equals(value, Me.SetpointTemperature) Then
            End If
            Me._SetpointTemperature = value
            Me.NotifyPropertyChanged()
            Me.WithinSetpointWindow = Me.IsWithinAppliedSetpointWindow
        End Set
    End Property

    ''' <summary>
    ''' Attempts to apply (write and then read) setpoint Temperature from the given data.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="temperature">  The temperature. </param>
    ''' <param name="retryCount">   Number of retries. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="e">            Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryApplySetpointTemperature(ByVal temperature As Double, ByVal retryCount As Integer, ByVal pollInterval As TimeSpan, ByVal e As Core.ActionEventArgs) As Boolean
        If Me.TryWriteSetpointTemperature(temperature, retryCount, pollInterval, e) Then
            isr.Core.ApplianceBase.DoEventsWait(Love.LoveCommand.SelectRefractoryPeriod(Love.LoveCommandCode.WriteSetpoint))
            Me.TryReadSetpointTemperature(retryCount, pollInterval, e)
        End If
        Return Not e.Failed
    End Function

    ''' <summary> Attempts to read setpoint Temperature from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="retryCount">   Number of retries. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="e">            Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryReadSetpointTemperature(ByVal retryCount As Integer, ByVal pollInterval As TimeSpan, ByVal e As Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = $"reading {Me.ModuleName} setpoint temperature"
        Try
            activity = $"checking if {Me.Messenger.Port.SerialPort.PortName} is open"
            If Me.IsPortOpen Then
                Dim status As StatusCode = StatusCode.ValueNotSet
                Do Until retryCount = 0 OrElse status = StatusCode.Okay
                    retryCount -= 1
                    activity = $"reading {Me.ModuleName} setpoint temperature"
                    status = Me.ReadSetpoint()
                    If status = StatusCode.Okay Then
                        Me.SetpointTemperature = Me.ReadSetpointPayload.Temperature
                        Me.SetpointAccuracyRange = New RangeR(Math.Pow(10, -Me.ReadSetpointPayload.Accuracy))
                        isr.Core.ApplianceBase.DoEvents()
                    Else
                        Threading.Thread.SpinWait(10)
                        isr.Core.ApplianceBase.DoEventsWait(pollInterval)
                    End If
                Loop
                If status <> StatusCode.Okay Then
                    e.RegisterFailure(Me.Publish(TraceEventType.Warning, $"Failed {activity} with status {CInt(status)}:{status.Description}"))
                End If
            Else
                Throw New InvalidOperationException($"Failed {activity}; Serial port to this device is not open")
            End If

        Catch ex As Exception
            e.RegisterFailure(Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}"))
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Attempts to write setpoint temperature from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="temperature">  The temperature. </param>
    ''' <param name="retryCount">   Number of retries. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="e">            Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryWriteSetpointTemperature(ByVal temperature As Double, ByVal retryCount As Integer, ByVal pollInterval As TimeSpan, ByVal e As Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = $"writing {Me.ModuleName} setpoint temperature"
        Try
            activity = $"checking if {Me.Messenger.Port.SerialPort.PortName} is open"
            If Me.IsPortOpen Then
                Dim status As StatusCode = StatusCode.ValueNotSet
                Do Until retryCount = 0 OrElse status = StatusCode.Okay
                    retryCount -= 1
                    activity = $"writing {Me.ModuleName} setpoint temperature"
                    status = Me.WriteSetpoint(temperature)
                    If status = StatusCode.Okay Then
                        Me.AppliedSetpointTemperature = Me.WriteSetpointPayload.Temperature
                        isr.Core.ApplianceBase.DoEvents()
                    Else
                        Threading.Thread.SpinWait(10)
                        isr.Core.ApplianceBase.DoEventsWait(pollInterval)
                    End If
                Loop
                If status <> StatusCode.Okay Then
                    e.RegisterFailure(Me.Publish(TraceEventType.Warning, $"Failed {activity} with status {CInt(status)}:{status.Description}"))
                End If
            Else
                Throw New InvalidOperationException($"Failed {activity}; Serial port to this device is not open")

            End If
        Catch ex As Exception
            e.RegisterFailure(Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}"))
        End Try
        Return Not e.Failed
    End Function

#End Region



#Region " SETPOINT: READ "

    ''' <summary> Gets or sets the Read Setpoint payload. </summary>
    ''' <value> The Setpoint payload. </value>
    Public ReadOnly Property ReadSetpointPayload As ReadSetpointPayload

    ''' <summary> Reads the setpoint. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The setpoint. </returns>
    Public Function ReadSetpoint() As StatusCode
        Me._ReadSetpointPayload = New ReadSetpointPayload()
        Dim activity As String = "building Setpoint query command"
        Using replyMessage As IProtocolMessage = New ProtocolMessage()
            replyMessage.ParseEnabled = True
            replyMessage.Prefix = ProtocolMessage.LovePrompt
            replyMessage.ModuleAddress = Me.Messenger.ModuleAddress
            Me.Messenger.Port.MessageParser = replyMessage
            replyMessage.MessageType = MessageType.Response
            MyBase.Messenger.InputMessage = replyMessage
            Using commandMessage As IProtocolMessage = New ProtocolMessage()
                commandMessage.ParseEnabled = False
                commandMessage.Prefix = ProtocolMessage.LovePrompt
                commandMessage.ModuleAddress = Me.Messenger.ModuleAddress
                commandMessage.CommandAscii = Me.Messenger.Transport.Commands(LoveCommandCode.ReadSetpoint).CommandAscii
                commandMessage.MessageType = MessageType.Command
                activity = "reading Setpoint"
                MyBase.Messenger.OutputMessage = commandMessage
                Me.Query(commandMessage, My.Settings.ModuleReadTimeout)
            End Using
        End Using
        If Me.Transport.IsSuccess Then
            Me.Transport.TransportStatus = Me.ReadSetpointPayload.Parse(Me.Transport.ReceivedProtocolMessage.Payload)
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReadSetpointPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReadSetpointPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReadSetpointPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReadSetpointPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Publish(TraceEventType.Verbose, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Publish(TraceEventType.Warning, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

    ''' <summary> Reply read setpoint. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyReadSetpoint() As StatusCode
        Me._ReadSetpointPayload = New ReadSetpointPayload()
        Dim activity As String = "building read module setpoint read reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.LovePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = Me.Messenger.Transport.Commands(LoveCommandCode.ReadSetpoint).CommandAscii
            message.Payload = ReadSetpointPayload.SimulatePayload().Payload
            activity = "replying read Setpoint"
            Me.Query(message, My.Settings.ModuleReadTimeout)
        End Using
        If Me.Transport.IsSuccess Then
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReadSetpointPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReadSetpointPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReadSetpointPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReadSetpointPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Publish(TraceEventType.Verbose, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Publish(TraceEventType.Warning, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " SETPOINT WRITE "

    ''' <summary> Gets or sets the reply payload. </summary>
    ''' <value> The reply payload. </value>
    Public ReadOnly Property ReplyPayload As ReplyPayload

    ''' <summary> Gets or sets the write setpoint payload. </summary>
    ''' <value> The setpoint payload. </value>
    Public ReadOnly Property WriteSetpointPayload As WriteSetpointPayload

    ''' <summary> Writes setpoint. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="setpoint"> The setpoint. </param>
    ''' <returns> A StatusCode. </returns>
    Public Function WriteSetpoint(ByVal setpoint As Double) As StatusCode
        Me._WriteSetpointPayload = New WriteSetpointPayload() With {.Temperature = setpoint}
        Me._ReplyPayload = New ReplyPayload()
        Dim activity As String = "building setpoint command message"
        Using replyMessage As IProtocolMessage = New ProtocolMessage()
            replyMessage.ParseEnabled = True
            replyMessage.Prefix = ProtocolMessage.LovePrompt
            replyMessage.ModuleAddress = Me.Messenger.ModuleAddress
            Me.Messenger.Port.MessageParser = replyMessage
            replyMessage.MessageType = MessageType.Response
            MyBase.Messenger.InputMessage = replyMessage
            Using commandMessage As IProtocolMessage = New ProtocolMessage()
                commandMessage.ParseEnabled = False
                commandMessage.Prefix = ProtocolMessage.LovePrompt
                commandMessage.ModuleAddress = Me.Messenger.ModuleAddress
                commandMessage.CommandAscii = Me.Messenger.Transport.Commands(LoveCommandCode.WriteSetpoint).CommandAscii
                commandMessage.MessageType = MessageType.Command
                commandMessage.Payload = Me._WriteSetpointPayload.Build
                activity = "Writing setpoint"
                MyBase.Messenger.OutputMessage = commandMessage
                Me.Query(commandMessage, My.Settings.ModuleReadTimeout)
            End Using
        End Using
        If Me.Transport.IsSuccess Then
            Me.Transport.TransportStatus = Me.ReplyPayload.Parse(Me.Transport.ReceivedProtocolMessage.Payload)
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReplyPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReplyPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReplyPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReplyPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Publish(TraceEventType.Verbose, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Publish(TraceEventType.Warning, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

    ''' <summary> Reply write setpoint. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Function ReplyWriteSetpoint() As StatusCode
        Me._ReplyPayload = New ReplyPayload()
        Dim activity As String = "building write module setup reply"
        Using message As IProtocolMessage = New ProtocolMessage()
            message.Prefix = ProtocolMessage.LovePrompt
            message.ModuleAddress = Me.Messenger.ModuleAddress
            message.CommandAscii = String.Empty
            message.Payload = Love.ReplyPayload.SimulatePayload().Payload
            activity = "replying write setpoint"
            Me.Query(message, My.Settings.ModuleReadTimeout)
        End Using
        If Me.Transport.IsSuccess Then
        ElseIf Me.Transport.ReceiveStatus <> StatusCode.Okay Then
            Me.ReplyPayload.StatusCode = Me.Transport.ReceiveStatus
        ElseIf Me.Transport.SendStatus <> StatusCode.Okay Then
            Me.ReplyPayload.StatusCode = Me.Transport.SendStatus
        ElseIf Me.Transport.TransportStatus <> StatusCode.Okay Then
            Me.ReplyPayload.StatusCode = Me.Transport.TransportStatus
        Else
            Me.ReplyPayload.StatusCode = StatusCode.ValueNotSet
        End If
        If Me.Transport.IsSuccess Then
            Me.Publish(TraceEventType.Verbose, $"Success {activity}: {Me.Transport.ReceivedHexMessage};. ")
        Else
            Me.Publish(TraceEventType.Warning, $"{Me.Transport.FailureMessage} error {activity};. ")
        End If
        Return Me.Transport.TransportStatus
    End Function

#End Region

#Region " COLLECTOR (listener) PROCESSING "

    ''' <summary> Listener message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Protocol event information. </param>
    Protected Sub ListenerMessageReceived(e As ProtocolEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Select Case Me.ToCommandCode(e.ProtocolMessage.CommandAscii)
            Case LoveCommandCode.ReadTemperature
                Me.ReplyReadTemperature()
            Case LoveCommandCode.ReadSetpoint
                Me.ReplyReadSetpoint()
            Case LoveCommandCode.WriteSetpoint
                Me.ReplyWriteSetpoint()
        End Select
    End Sub

    ''' <summary> Notifies a message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Protocol event information. </param>
    Protected Overrides Sub NotifyMessageReceived(e As ProtocolEventArgs)
        MyBase.NotifyMessageReceived(e)
        If Me.Messenger.MessengerRole = MessengerRole.Collector AndAlso Me.Transport.IsSuccess Then
            Me.ListenerMessageReceived(e)
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("Love Protocol Settings Editor", isr.Ports.Love.My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"{propertyName} changed to {sender.TraceShowLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub


#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

