Imports isr.Ports.Serial
Imports isr.Ports.Serial.Methods
Imports isr.Ports.Teleport

''' <summary> A protocol message for the Love modules. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-02 </para>
''' </remarks>
Public Class ProtocolMessage
    Inherits ProtocolMessageBase
    Implements IProtocolMessage

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New(New Text.ASCIIEncoding)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> The message. </param>
    Public Sub New(ByVal protocolMessage As IProtocolMessage)
        MyBase.New(protocolMessage)
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object
        Return ProtocolMessage.Clone(Me)
    End Function

    ''' <summary>
    ''' Creates a new <see cref="ProtocolMessageBase">serial protocol message</see> that is a copy of
    ''' the specified instance.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overloads Shared Function Clone(ByVal value As IProtocolMessage) As IProtocolMessage
        Return New ProtocolMessage(value)
    End Function

    ''' <summary> Creates a new IProtocolMessage. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> An IProtocolMessage. </returns>
    Public Shared Function Create() As IProtocolMessage
        Dim result As IProtocolMessage = Nothing
        Try
            result = New ProtocolMessage
        Catch
            If result IsNot Nothing Then
                result.Dispose()
            End If

            Throw
        End Try
        Return result
    End Function

#End Region

#Region " CONSTANTS AND SHARED "

    ''' <summary> The stream start value. </summary>
    Public Const StreamStartValue As Byte = 2

    ''' <summary> The command termination value. </summary>
    Public Const CommandTerminationValue As Byte = 3

    ''' <summary> The response termination value. </summary>
    Public Const ResponseTerminationValue As Byte = 6

    ''' <summary> The love prompt. </summary>
    Public Const LovePrompt As Byte = CByte(Asc("L"c) And &HFF)

    ''' <summary> Length of the prompt. </summary>
    Public Const PromptLength As Integer = 1

    ''' <summary> Length of the address. </summary>
    Public Const AddressLength As Integer = 2

    ''' <summary> Length of the checksum. </summary>
    Public Const ChecksumLength As Integer = 2

    ''' <summary> Length of the command. </summary>
    Public Const CommandLength As Integer = 2

    ''' <summary> The minimum length of the message. </summary>
    Public Const MinimumMessageLength As Integer = ProtocolMessage.PromptLength + ProtocolMessage.AddressLength + ProtocolMessage.CommandLength + ProtocolMessage.ChecksumLength

#End Region

#Region " CONTENTS "

    ''' <summary> Gets or sets the message type. </summary>
    ''' <remarks> Also sets the stream start and end values. </remarks>
    ''' <value> The message type. </value>
    Public Overrides Property MessageType As MessageType Implements IProtocolMessage.MessageType
        Get
            Return MyBase.MessageType
        End Get
        Set(value As MessageType)
            If Me.MessageType <> value Then
                Me.Prefix = ProtocolMessage.FromMessageType(value)
                MyBase.MessageType = value
                Me.StreamInitiation = New Byte() {ProtocolMessage.StreamStartValue}
                Me.StreamTermination = If(value = Teleport.MessageType.Command, New Byte() {ProtocolMessage.CommandTerminationValue}, New Byte() {ProtocolMessage.ResponseTerminationValue})
            End If
        End Set
    End Property

#End Region

#Region " I MESSAGE PARSER "

    ''' <summary> Query if 'values' is terminated. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> <c>true</c> if terminated; otherwise <c>false</c> </returns>
    Private Function IsInitiated(ByVal values As IEnumerable(Of Byte)) As Boolean
        Dim result As Boolean = False
        If values.Count > Me.StreamInitiation.Count Then
            Dim i As Integer = -1
            result = True
            For Each b As Byte In Me.StreamInitiation
                i += 1
                result = result AndAlso (b = values(i))
            Next
        End If
        Return result
    End Function

    ''' <summary> Query if 'values' is terminated. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> <c>true</c> if terminated; otherwise <c>false</c> </returns>
    Private Function IsTerminated(ByVal values As IEnumerable(Of Byte)) As Boolean
        Dim result As Boolean = False
        If values.Count > Me.StreamTermination.Count Then
            Dim i As Integer = values.Count
            result = True
            For Each b As Byte In Me.StreamTermination.Reverse
                i -= 1
                result = result AndAlso (b = values(i))
            Next
        End If
        Return result
    End Function

    ''' <summary> Parses the given values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A MessageParserOutcome. </returns>
    Public Overrides Function Parse(ByVal values As IEnumerable(Of Byte)) As Serial.MessageParserOutcome
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Serial.MessageParserOutcome = MessageParserOutcome.None
        ' check if value start with initiation characters.
        If Not values.Any Then
            result = MessageParserOutcome.Incomplete
        ElseIf Me.IsTerminated(values) Then
            Me.Status = Me.ParseStream(Me.MessageType, values)
            result = If(Me.Status = StatusCode.Okay, MessageParserOutcome.Complete, MessageParserOutcome.Invalid)
        Else
        End If
        Return result
    End Function

#End Region

#Region " PARSERS "

    ''' <summary> Initializes this object from the given from message type. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Byte. </returns>
    Public Shared Function FromMessageType(ByVal value As MessageType) As Byte
        Select Case value
            Case Teleport.MessageType.Command
                Return ProtocolMessage.LovePrompt
            Case Teleport.MessageType.Failure
                Return ProtocolMessage.LovePrompt
            Case Teleport.MessageType.Response
                Return ProtocolMessage.LovePrompt
            Case Else
                Return ProtocolMessage.LovePrompt
        End Select
    End Function

    ''' <summary> Enumerates trim start in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data">  The byte data. </param>
    ''' <param name="count"> Number of. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process trim start in this collection.
    ''' </returns>
    Private Shared Function TrimStart(ByVal data As IEnumerable(Of Byte), ByVal count As Integer) As IEnumerable(Of Byte)
        Dim dataSet As New Queue(Of Byte)(data)
        For i As Integer = 1 To count
            dataSet.Dequeue()
        Next
        Return dataSet
    End Function

    ''' <summary> Enumerates trim end in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data">  The byte data. </param>
    ''' <param name="count"> Number of. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process trim end in this collection.
    ''' </returns>
    Private Shared Function TrimEnd(ByVal data As IEnumerable(Of Byte), ByVal count As Integer) As IEnumerable(Of Byte)
        Dim dataSet As New Queue(Of Byte)(data.Reverse)
        For i As Integer = 1 To count
            dataSet.Dequeue()
        Next
        Return dataSet.Reverse
    End Function

    ''' <summary> Parse internal stream. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data"> The byte data. </param>
    ''' <returns> A StatusCode. </returns>
    Private Function ParseInternalStream(ByVal data As IEnumerable(Of Byte)) As StatusCode
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        Dim result As StatusCode = StatusCode.Okay

        Dim dataSet As New Queue(Of Byte)(data)
        ' get the prefix
        Me.Prefix = dataSet.Dequeue
        If dataSet.Count > 1 Then
            Me.ModuleAddress = New Byte() {dataSet.Dequeue, dataSet.Dequeue}
            If Me.ModuleAddressAscii = "00" Then
                result = StatusCode.InvalidModuleAddress
            ElseIf dataSet.Count > 2 Then
                dataSet = New Queue(Of Byte)(dataSet.Reverse)
                Dim checksumCache As Byte() = New Byte() {dataSet.Dequeue, dataSet.Dequeue}
                If data.Count > 0 Then
                    Me.Payload = dataSet.Reverse
                    Me.NotifyPropertyChanged(NameOf(ProtocolMessageBase.Payload))
                End If
                Me.Checksum = checksumCache.Reverse.ToArray
                If Not Serial.Methods.NullableSequenceEquals(Me.Checksum, Me.CalculateChecksum) Then
                    result = StatusCode.ChecksumInvalid
                End If
            Else
                result = StatusCode.MessageTooShort
            End If
        Else
            result = StatusCode.InvalidMessageLength
        End If

        Return result
    End Function

    ''' <summary> Parse hexadecimal message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexMessage"> A message describing the hexadecimal. </param>
    ''' <returns> A StatusCode. </returns>
    Public Overrides Function ParseHexMessage(ByVal hexMessage As String) As StatusCode
        Dim result As StatusCode = StatusCode.ValueNotSet
        Return result
    End Function

    ''' <summary>
    ''' Tries to parse the <paramref name="data">byte data</paramref> into a valid message.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' The Protocol <see cref="Command"/> of the collector (listener) must be set by the
    ''' emitter</para>
    ''' <para>
    '''       </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="messageType"> The message type. </param>
    ''' <param name="data">        The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Public Overrides Function ParseStream(ByVal messageType As MessageType, ByVal data As IEnumerable(Of Byte)) As StatusCode
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        Me.Clear()
        Dim result As StatusCode
        If Not data.Any OrElse data.Count < ProtocolMessage.MinimumMessageLength Then
            result = StatusCode.MessageTooShort
        ElseIf Not Me.IsInitiated(data) Then
            result = StatusCode.InvalidStreamStart
        ElseIf Not Me.IsTerminated(data) Then
            result = StatusCode.InvalidStreamEnd
        Else
            Me.MessageType = messageType
            Me.Stream = data
            data = ProtocolMessage.TrimStart(data, Me.StreamInitiation.Count)
            data = ProtocolMessage.TrimEnd(data, Me.StreamInitiation.Count)
            Me.InternalStream = data
            result = Me.ParseInternalStream(data)
        End If
        Me.Status = result
        Return result
    End Function

    ''' <summary> Validates the protocol message relative to this protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> The validated protocol message. </param>
    ''' <returns> A StatusCode. </returns>
    Public Overrides Function Validate(ByVal protocolMessage As IProtocolMessage) As StatusCode
        If protocolMessage Is Nothing Then
            Return StatusCode.Okay
        ElseIf Not Serial.Methods.NullableSequenceEquals(Me.ModuleAddress, protocolMessage.ModuleAddress) Then
            Return StatusCode.IncorrectModuleAddressReceived
        ElseIf Me.CommandAscii <> protocolMessage.CommandAscii Then
            Return StatusCode.IncorrectCommandReceived
        Else
            Return StatusCode.Okay
        End If
    End Function

#End Region

#Region " BUILDERS "

    ''' <summary> The checksum mask. </summary>
    Public Const ChecksumMask As Integer = &HFF

    ''' <summary> Calculates the checksum value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The calculated checksum hexadecimal. </returns>
    Public Overrides Function CalculateChecksumHex() As String
        Dim result As Long = 0
        ' the prefix counts only for messages received from the controller.
        If Me.MessageType = Teleport.MessageType.Response Then result += Me.Prefix
        result += Me.ModuleAddress.Sum
        If Me.MessageType = Teleport.MessageType.Command Then result += Me.Command.Sum
        result += Me.Payload.Sum
        result = result And ProtocolMessage.ChecksumMask
        Return result.ToString("X2")
    End Function

    ''' <summary> Enumerates build message in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messageType"> The message type. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Public Overrides Function BuildStream(ByVal messageType As MessageType) As IEnumerable(Of Byte)
        Me.MessageType = messageType
        Return Me.BuildStream
    End Function

    ''' <summary> Enumerates build internal stream in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build internal stream in this
    ''' collection.
    ''' </returns>
    Private Overloads Function BuildInternalStream() As IEnumerable(Of Byte)
        Me.InternalStreamValues.Clear()
        If Me.IsIncomplete Then
        ElseIf Me.Status = StatusCode.InvalidMessageLength Then
        Else
            Me.InternalStreamValues.Add(Me.Prefix)
            Me.InternalStreamValues.AddRange(Me.ModuleAddress)
            Me.InternalStreamValues.AddRange(Me.Command)
            If Me.Payload.Any Then Me.InternalStreamValues.AddRange(Me.Payload)
            Me.UpdateChecksum()
            Me.InternalStreamValues.AddRange(Me.Checksum)
        End If
        Return Me.InternalStreamValues
    End Function

    ''' <summary> Builds the stream. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Private Overloads Function BuildStream() As IEnumerable(Of Byte)
        Dim values As New List(Of Byte)
        If Me.IsIncomplete Then
        ElseIf Me.Status = StatusCode.InvalidMessageLength Then
        Else
            values.AddRange(Me.StreamInitiation)
            values.AddRange(Me.BuildInternalStream)
            values.AddRange(Me.StreamTermination)
        End If
        Return values
    End Function

#End Region

End Class

