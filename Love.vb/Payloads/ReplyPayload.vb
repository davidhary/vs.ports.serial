﻿Imports isr.Ports.Serial
Imports isr.Ports.Teleport

''' <summary> A reply payload. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-05 </para>
''' </remarks>
Public Class ReplyPayload
    Implements IPayload

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " SIMULATE A REPLY "

    ''' <summary> Simulate payload. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A SetupPayload. </returns>
    Public Shared Function SimulatePayload() As ReplyPayload
        Dim result As New ReplyPayload() With {.ReplyStatus = 0}
        Return result
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> The success reply. </summary>
    Public Const SuccessReply As Integer = 0

    ''' <summary> The reply status. </summary>
    Private _ReplyStatus As Integer

    ''' <summary> Gets or sets the reply status. </summary>
    ''' <value> The reply status. </value>
    Public Property ReplyStatus As Integer
        Get
            Return Me._ReplyStatus
        End Get
        Set(value As Integer)
            If Me.ReplyStatus <> value Then
                Me._ReplyStatus = value
                Me.ReplyAscii = value.ToString("X2")
            End If
        End Set
    End Property

    ''' <summary> The reply ASCII. </summary>
    Private _ReplyAscii As String

    ''' <summary> Gets or sets the reply ASCII. </summary>
    ''' <value> The reply ASCII. </value>
    Public Property ReplyAscii As String
        Get
            Return Me._ReplyAscii
        End Get
        Set(value As String)
            If Not String.Equals(Me.ReplyAscii, value) Then
                Me._ReplyAscii = value
                Me.ReplyStatus = Integer.Parse(value)
            End If
        End Set
    End Property

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary> Gets or sets the reading. </summary>
    ''' <value> The reading. </value>
    Public Property Reading As String
        Get
            Return Me._Reading
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Reading) Then
                Me._Reading = value
            End If
        End Set
    End Property

#End Region

#Region " I PAYLOAD IMPLEMENTATION "

    ''' <summary> Gets or sets a list of payloads. </summary>
    ''' <value> A list of payloads. </value>
    Private ReadOnly Property PayloadList As List(Of Byte)

    ''' <summary> Gets or sets the payload. </summary>
    ''' <value> The payload. </value>
    Public Property Payload As IEnumerable(Of Byte) Implements IPayload.Payload
        Get
            Return Me.PayloadList
        End Get
        Set(value As IEnumerable(Of Byte))
            Me._PayloadList = New List(Of Byte)(value)
        End Set
    End Property

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Public Property StatusCode As StatusCode Implements IPayload.StatusCode

    ''' <summary> Populates the payload from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="payload"> The payload. </param>
    ''' <returns> A StatusCode. </returns>
    Public Function Parse(ByVal payload As IEnumerable(Of Byte)) As StatusCode Implements IPayload.Parse
        Me.StatusCode = StatusCode.Okay
        Me.Payload = payload
        Dim values As New Queue(Of Byte)(payload)
        Dim encoding As Text.Encoding = New Text.ASCIIEncoding
        If Me.StatusCode = StatusCode.Okay Then
            If values.Any AndAlso values.Count >= 2 Then
                Me.ReplyAscii = encoding.GetString(New Byte() {values.Dequeue, values.Dequeue})
            Else
                Me.StatusCode = StatusCode.MessageIncomplete
            End If
        End If
        Me._Reading = $"{Me.ReplyAscii}"
        Return Me.StatusCode
    End Function

    ''' <summary> Converts the payload to bytes. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> This object as an IEnumerable(Of Byte) </returns>
    Public Function Build() As IEnumerable(Of Byte) Implements IPayload.Build
        Me.StatusCode = StatusCode.Okay
        Dim result As New List(Of Byte)
        result.AddRange(Me.ReplyAscii.ToBytes)
        Me.Payload = result
        Return Me.Payload()
    End Function

#End Region

End Class
