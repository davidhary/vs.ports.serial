﻿Imports isr.Ports.Serial
Imports isr.Ports.Teleport

''' <summary> A write setpoint payload. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-05 </para>
''' </remarks>
Public Class WriteSetpointPayload
    Implements IPayload

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Accuracy = 1
    End Sub

#End Region

#Region " SIMULATE A REPLY "

    ''' <summary> Simulate payload. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A SetupPayload. </returns>
    Public Shared Function SimulatePayload() As WriteSetpointPayload
        Dim result As New WriteSetpointPayload() With {.Temperature = 18.0}
        Return result
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> The accuracy. </summary>
    Private _Accuracy As Integer

    ''' <summary> Gets or sets the accuracy. </summary>
    ''' <value> The accuracy. </value>
    Public Property Accuracy As Integer
        Get
            Return Me._Accuracy
        End Get
        Set(value As Integer)
            If Me.Accuracy <> value Then
                Me._Accuracy = value
            End If
        End Set
    End Property

    ''' <summary> The temperature. </summary>
    Private _Temperature As Double

    ''' <summary> Gets or sets the temperature. </summary>
    ''' <value> The temperature. </value>
    Public Property Temperature As Double
        Get
            Return Me._Temperature
        End Get
        Set(value As Double)
            If value <> Me.Temperature Then
                Me._Temperature = value
                Dim wholeTemp As Integer = CInt(value * Math.Pow(10, Me.Accuracy))
                Me.Sign = If(value > 0, WriteSetpointPayload.PositiveSign, WriteSetpointPayload.NegativeSign)
                Me.TemperatureAscii = wholeTemp.ToString("0000")
            End If
        End Set
    End Property

    ''' <summary> The temperature ASCII. </summary>
    Private _TemperatureAscii As String

    ''' <summary> Gets or sets the temperature ASCII. </summary>
    ''' <value> The temperature ASCII. </value>
    Public Property TemperatureAscii As String
        Get
            Return Me._TemperatureAscii
        End Get
        Set(value As String)
            If Not String.Equals(Me.TemperatureAscii, value) Then
                Me._TemperatureAscii = value
                Dim wholeTemp As Integer = Integer.Parse(value)
                If Me.Sign = ReadTemperaturePayload.NegativeSign Then
                    wholeTemp = -wholeTemp
                End If
                Me.Temperature = wholeTemp / Math.Pow(10, Me.Accuracy)
            End If
        End Set
    End Property

    ''' <summary> The positive sign. </summary>
    Public Const PositiveSign As Integer = 0

    ''' <summary> The negative sign. </summary>
    Public Const NegativeSign As Integer = 255

    ''' <summary> The sign. </summary>
    Private _Sign As Integer

    ''' <summary> Gets or sets the Sign. </summary>
    ''' <value> The Sign. </value>
    Public Property Sign As Integer
        Get
            Return Me._Sign
        End Get
        Set(value As Integer)
            If Me.Sign <> value OrElse String.IsNullOrEmpty(Me.SignAscii) Then
                Me._Sign = value
                Me.SignAscii = value.ToString("X2")
            End If
        End Set
    End Property

    ''' <summary> The sign ASCII. </summary>
    Private _SignAscii As String

    ''' <summary> Gets or sets the Sign ASCII. </summary>
    ''' <value> The Sign ASCII. </value>
    Public Property SignAscii As String
        Get
            Return Me._SignAscii
        End Get
        Set(value As String)
            If Not String.Equals(Me.SignAscii, value) Then
                Me._SignAscii = value
                Me.Sign = Integer.Parse(value)
            End If
        End Set
    End Property

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary> Gets or sets the reading. </summary>
    ''' <value> The reading. </value>
    Public Property Reading As String
        Get
            Return Me._Reading
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Reading) Then
                Me._Reading = value
            End If
        End Set
    End Property

#End Region

#Region " I PAYLOAD IMPLEMENTATION "

    ''' <summary> Gets or sets a list of payloads. </summary>
    ''' <value> A list of payloads. </value>
    Private ReadOnly Property PayloadList As List(Of Byte)

    ''' <summary> Gets or sets the payload. </summary>
    ''' <value> The payload. </value>
    Public Property Payload As IEnumerable(Of Byte) Implements IPayload.Payload
        Get
            Return Me.PayloadList
        End Get
        Set(value As IEnumerable(Of Byte))
            Me._PayloadList = New List(Of Byte)(value)
        End Set
    End Property

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Public Property StatusCode As StatusCode Implements IPayload.StatusCode

    ''' <summary> Populates the payload from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="payload"> The payload. </param>
    ''' <returns> A StatusCode. </returns>
    Public Function Parse(ByVal payload As IEnumerable(Of Byte)) As StatusCode Implements IPayload.Parse
        Me.StatusCode = StatusCode.Okay
        Me.Payload = payload
        Dim values As New Queue(Of Byte)(payload)
        Dim encoding As Text.Encoding = New Text.ASCIIEncoding
        Me.TemperatureAscii = encoding.GetString(New Byte() {values.Dequeue, values.Dequeue, values.Dequeue, values.Dequeue})
        Me.SignAscii = encoding.GetString(New Byte() {values.Dequeue, values.Dequeue})
        Me._Reading = $"{Me.TemperatureAscii}{Me.SignAscii}"
        Return Me.StatusCode
    End Function

    ''' <summary> Converts the payload to bytes. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> This object as an IEnumerable(Of Byte) </returns>
    Public Function Build() As IEnumerable(Of Byte) Implements IPayload.Build
        Me.StatusCode = StatusCode.Okay
        Dim result As New List(Of Byte)
        result.AddRange(Me.TemperatureAscii.ToBytes)
        result.AddRange(Me.SignAscii.ToBytes)
        Me.Payload = result
        Return Me.Payload()
    End Function

#End Region

End Class
