''' <summary> Packs or unpacks a <see cref="T:Int32">unsigned value</see>. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
<CLSCompliant(False)>
Public NotInheritable Class PackedInt32

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt32" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Int32)
        Me.New()
        Me._Value = CType(value, Int32)
        Me.FromValueThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt32" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highPart"> The high part. </param>
    ''' <param name="lowPart">  The low part. </param>
    Public Sub New(ByVal highPart As PackedUInt16, ByVal lowPart As PackedUInt16)
        Me.New()
        Me._HighPart = highPart
        Me._LowPart = lowPart
        Me.ToValueThis()
    End Sub

#End Region

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As Int32

    ''' <summary> Gets or sets the high part. </summary>
    ''' <value> The high part. </value>
    Public Property HighPart As PackedUInt16

    ''' <summary> Gets or sets the low part. </summary>
    ''' <value> The low part. </value>
    Public Property LowPart As PackedUInt16

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub FromValueThis()
        Me._LowPart = PackedInt32.ToLowUnsignedPart(Me.Value)
        Me._HighPart = PackedInt32.ToHighUnsignedPart(Me.Value)
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub FromValue()
        Me.FromValueThis()
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub FromValue(ByVal value As Int32)
        Me.Value = value
        Me.FromValue()
    End Sub

    ''' <summary> Builds the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ToValueThis()
        Me._Value = CInt(Me._HighPart.Value And &HFFFF)
        Me._Value = CInt(Me._LowPart.Value Or (&HFFFF0000 And (Me._Value << 16)))
    End Sub

    ''' <summary> Builds the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overloads Sub ToValue()
        Me.ToValueThis()

    End Sub

    ''' <summary> Parses the high unsigned part. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a PackedUInt16. </returns>
    Public Shared Function ToHighUnsignedPart(ByVal value As Int32) As PackedUInt16
        Return New PackedUInt16(CType((value And &HFFFF0000) >> 16, UInt16))
    End Function

    ''' <summary> Parses the low unsigned part. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a PackedUInt16. </returns>
    Public Shared Function ToLowUnsignedPart(ByVal value As Int32) As PackedUInt16
        Return New PackedUInt16(CType(value And &HFFFF, UInt16))
    End Function

    ''' <summary> Combines the unsigned parts into a signed value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="highPart"> The high part. </param>
    ''' <param name="lowPart">  The low part. </param>
    ''' <returns> The given data converted to an Int32. </returns>
    Public Overloads Shared Function ToValue(ByVal highPart As PackedUInt16, ByVal lowPart As PackedUInt16) As Int32
        If highPart Is Nothing Then Throw New ArgumentNullException(NameOf(highPart))
        If lowPart Is Nothing Then Throw New ArgumentNullException(NameOf(lowPart))
        Dim result As Int32 = CInt(highPart.Value And &HFFFF)
        result = CInt(lowPart.Value Or (&HFFFF0000 And (result << 16)))
        Return result
    End Function

End Class


