Imports System.Collections.Generic

''' <summary>
''' Packs or unpacks two <see cref="T:Byte">byte nibble value (0-15), into a single byte</see>.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class PackedInt8

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt8" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Byte)
        Me.New()
        Me.FromValueThis(value)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt8" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highNibble"> The high nibble. </param>
    ''' <param name="lowNibble">  The low nibble. </param>
    Public Sub New(ByVal highNibble As Byte, ByVal lowNibble As Byte)
        Me.New()
        Me.FromValueThis(PackedInt8.ToValue(highNibble, lowNibble))
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt8" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As IEnumerable(Of Byte))
        Me.New()
        If values IsNot Nothing AndAlso values.Count >= 2 Then
            Me.FromValueThis(PackedInt8.ToValue(values(0), values(1)))
        End If
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary> Gets or sets the packed value. </summary>
    ''' <value> The value. </value>
    Public Property Value As Byte

    ''' <summary> Gets or sets the high byte value. </summary>
    ''' <value> The high nibble. </value>
    Public Property HighNibble As Byte

    ''' <summary> Gets or sets the low byte value. </summary>
    ''' <value> The low nibble. </value>
    Public Property LowNibble As Byte

#End Region

#Region " PARSERS "

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub FromValueThis()
        Me._HighNibble = PackedInt8.ToHighNibble(Me.Value)
        Me._LowNibble = PackedInt8.ToLowNibble(Me.Value)
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub FromValue()
        Me.FromValueThis()
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub FromValueThis(ByVal value As Byte)
        Me._Value = value
        Me.FromValueThis()
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub FromValue(ByVal value As Byte)
        Me.Value = value
        Me.FromValueThis()
    End Sub

    ''' <summary> Parses the high nibble. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToHighNibble(ByVal value As Byte) As Byte
        Return CByte((value And &HF0) >> 4)
    End Function

    ''' <summary> From High nibble. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Byte. </returns>
    Public Shared Function FromHighNibble(ByVal value As Byte) As Byte
        Return CByte((value And &HF) << 4)
    End Function

    ''' <summary> Parses the low nibble. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToLowNibble(ByVal value As Byte) As Byte
        Return CByte(value And &HF)
    End Function

    ''' <summary> Initializes this object from the given from low nibble. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Byte. </returns>
    Public Shared Function FromLowNibble(ByVal value As Byte) As Byte
        Return CByte(value And &HF)
    End Function

    ''' <summary> Converts a value to a nibble. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToNibble(ByVal value As Byte) As Byte
        Return CByte(value And &HF)
    End Function

    ''' <summary> Combines the bytes into a word. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Overloads Sub ToValueThis()
        Me.Value = ToValue(Me.HighNibble, Me.LowNibble)
    End Sub

    ''' <summary> Combines the bytes into a byte value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overloads Sub ToValue()

        Me.ToValueThis()
    End Sub

    ''' <summary> Combines the bytes into a word. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="highNibble"> The high nibble. </param>
    ''' <param name="lowNibble">  The low nibble. </param>
    ''' <returns> The given data converted to a Byte. </returns>
    Public Overloads Shared Function ToValue(ByVal highNibble As Byte, ByVal lowNibble As Byte) As Byte
        If highNibble >= &H10 Then Throw New ArgumentException($"value {highNibble} must be lower than {&H10}", NameOf(highNibble))
        If lowNibble >= &H10 Then Throw New ArgumentException($"value {lowNibble} must be lower than {&H10}", NameOf(lowNibble))
        Return PackedInt8.FromHighNibble(highNibble) + PackedInt8.FromLowNibble(lowNibble)
    End Function

#End Region

End Class



