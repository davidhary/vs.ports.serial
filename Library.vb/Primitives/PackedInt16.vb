Imports System.Collections.Generic

''' <summary> Packs or unpacks a <see cref="T:Short">signed value</see>. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class PackedInt16

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Int16)
        Me.New()
        Me._Value = value
        Me.FromValueThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highByte"> The high byte. </param>
    ''' <param name="lowByte">  The low byte. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="byte")>
    Public Sub New(ByVal highByte As Byte, ByVal lowByte As Byte)
        Me.New()
        Me._LowByte = lowByte
        Me._HighByte = highByte
        Me.ToValueThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As IEnumerable(Of Byte))
        Me.New()
        If values IsNot Nothing AndAlso values.Count >= 2 Then
            Me._LowByte = values(1)
            Me._HighByte = values(0)
            Me.ToValueThis()
        End If
    End Sub

#End Region

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As Int16

    ''' <summary> Gets or sets the high byte. </summary>
    ''' <value> The high byte. </value>
    Public Property HighByte As Byte

    ''' <summary> Gets or sets the low byte. </summary>
    ''' <value> The low byte. </value>
    Public Property LowByte As Byte

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub FromValueThis()
        Me._HighByte = PackedInt16.ToHighByte(Me.Value)
        Me._LowByte = PackedInt16.ToLowByte(Me.Value)
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub FromValue()
        Me.FromValueThis()
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub FromValue(ByVal value As Int16)
        Me.Value = value
        Me.FromValue()
    End Sub

    ''' <summary> Parses the high byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToHighByte(ByVal value As Int16) As Byte
        Return CByte((value And &HFF00) >> 8)
    End Function

    ''' <summary> Parses the low byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToLowByte(ByVal value As Int16) As Byte
        Return CByte(value And &HFF)
    End Function

    ''' <summary> Combines the bytes into a value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Overloads Sub ToValueThis()
        Me.Value = ToValue(Me.HighByte, Me.LowByte)
    End Sub

    ''' <summary> Combines the bytes into a value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overloads Sub ToValue()
        Me.ToValueThis()
    End Sub

    ''' <summary> Combines the bytes into a value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highPart"> The high byte. </param>
    ''' <param name="lowPart">  The low byte. </param>
    ''' <returns> The given data converted to an Int16. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="byte")>
    Public Overloads Shared Function ToValue(ByVal highPart As Byte, ByVal lowPart As Byte) As Int16
        Dim result As Int16 = CShort(highPart And &HFF)
        result = CShort(lowPart Or (&HFF00 And (result << 8)))
        Return result
    End Function

End Class


