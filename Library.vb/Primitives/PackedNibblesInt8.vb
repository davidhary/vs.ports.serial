﻿Imports System.Collections.Generic

''' <summary>
''' Packs or unpacks two <see cref="T:Byte">bytes, each of nibble value (0-15), into a single
''' byte</see>.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class PackedNibblesInt8

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedNibblesInt8" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Byte)
        Me.New()
        Me.PackedValue = New PackedInt8(value)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedNibblesInt8" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highNibble"> The high nibble. </param>
    ''' <param name="lowNibble">  The low nibble. </param>
    Public Sub New(ByVal highNibble As Byte, ByVal lowNibble As Byte)
        Me.New()
        Me.PackedValue = New PackedInt8(highNibble, lowNibble)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedNibblesInt8" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As IEnumerable(Of Byte))
        Me.New()
        Me.PackedValue = New PackedInt8(values)
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary> Gets or sets the packed hexadecimal value. </summary>
    ''' <value> The packed hexadecimal value. </value>
    Public Property PackedHexValue As String
        Get
            Return PackedNibblesInt8.ToHex(Me.PackedValue)
        End Get
        Set(value As String)
            Me.PackedValue = New PackedInt8(PackedNibblesInt8.FromHex(value))
        End Set
    End Property

    ''' <summary> Gets or sets the packed value. </summary>
    ''' <value> The packed value. </value>
    Public Property PackedValue As PackedInt8

#End Region

#Region " PARSERS "

    ''' <summary> Converts this object to a hexadecimal. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="highNibble"> The high nibble. </param>
    ''' <param name="lowNibble">  The low nibble. </param>
    ''' <returns> The given data converted to a String. </returns>
    Public Overloads Shared Function ToHex(ByVal highNibble As Byte, ByVal lowNibble As Byte) As String
        If highNibble >= &H10 Then Throw New ArgumentException($"value {highNibble} must be lower than {&H10}", NameOf(highNibble))
        If lowNibble >= &H10 Then Throw New ArgumentException($"value {lowNibble} must be lower than {&H10}", NameOf(lowNibble))
        Return $"{highNibble.ToHex}{lowNibble.ToHex}"
    End Function

    ''' <summary> Converts this object to a hexadecimal. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> The given data converted to a String. </returns>
    Public Overloads Shared Function ToHex(ByVal value As PackedInt8) As String
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Return $"{value.HighNibble.ToHex}{value.LowNibble.ToHex}"
    End Function

    ''' <summary> From hexadecimal. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highNibble"> The high nibble. </param>
    ''' <param name="lowNibble">  The low nibble. </param>
    ''' <returns> A Byte. </returns>
    Public Overloads Shared Function FromHex(ByVal highNibble As String, ByVal lowNibble As String) As Byte
        Return PackedInt8.ToValue(Byte.Parse(highNibble), Byte.Parse(lowNibble))
    End Function

    ''' <summary> From hexadecimal. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Byte. </returns>
    Public Overloads Shared Function FromHex(ByVal value As String) As Byte
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        If value.Count <> 2 Then Throw New ArgumentException($"Invalid value length {value.Count}; must be 2", NameOf(value))
        Return PackedNibblesInt8.FromHex(value.Substring(0, 1), value.Substring(1, 1))
    End Function
#End Region

End Class


