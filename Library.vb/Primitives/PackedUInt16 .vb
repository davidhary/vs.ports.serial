Imports System.Collections.Generic

''' <summary>
''' Packs or unpacks two <see cref="T:Byte">bytes</see> into an <see cref="T:UInt16">unsigned
''' short</see>.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
<CLSCompliant(False)>
Public NotInheritable Class PackedUInt16

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As UInt16)
        Me.New()
        Me._Value = value
        Me.FromValueThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Int16)
        Me.New()
        Me._Value = CType(value, UInt16)
        Me.FromValueThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highByte"> The high byte. </param>
    ''' <param name="lowByte">  The low byte. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="byte")>
    Public Sub New(ByVal highByte As Byte, ByVal lowByte As Byte)
        Me.New()
        Me._LowByte = lowByte
        Me._HighByte = highByte
        Me.ToValueThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PackedUInt16" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As IEnumerable(Of Byte))
        Me.New()
        If values IsNot Nothing AndAlso values.Count >= 2 Then
            Me._LowByte = values(1)
            Me._HighByte = values(0)
            Me.ToValueThis()
        End If
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As UInt16

    ''' <summary> Gets or sets the high byte. </summary>
    ''' <value> The high byte. </value>
    Public Property HighByte As Byte

    ''' <summary> Gets or sets the low byte. </summary>
    ''' <value> The low byte. </value>
    Public Property LowByte As Byte

#End Region

#Region " PARSERS "

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub FromValueThis()
        Me._HighByte = PackedUInt16.ToHighByte(Me.Value)
        Me._LowByte = PackedUInt16.ToLowByte(Me.Value)
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub FromValue()
        Me.FromValueThis()
    End Sub

    ''' <summary> Parses the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub FromValue(ByVal value As UInt16)
        Me.Value = value
        Me.FromValue()
    End Sub

    ''' <summary> Parses the high byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToHighByte(ByVal value As UInt16) As Byte
        Return CByte((value And &HFF00) >> 8)
    End Function

    ''' <summary> Parses the low byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    Public Shared Function ToLowByte(ByVal value As UInt16) As Byte
        Return CByte(value And &HFF)
    End Function

    ''' <summary> Combines the bytes into the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Overloads Sub ToValueThis()
        Me.Value = ToValue(Me.HighByte, Me.LowByte)
    End Sub

    ''' <summary> Combines the bytes into the value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overloads Sub ToValue()
        Me.ToValueThis()
    End Sub

    ''' <summary> Combines the bytes into a value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="highByte"> The high byte. </param>
    ''' <param name="lowByte">  The low byte. </param>
    ''' <returns> The given data converted to an UInt16. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="byte")>
    Public Overloads Shared Function ToValue(ByVal highByte As Byte, ByVal lowByte As Byte) As UInt16
        Dim w As UInt16 = CType(highByte And &HFF, UInt16)
        w = CType(lowByte Or (&HFF00 And (w << 8)), UInt16)
        Return w
    End Function

    ''' <summary> Converts the 1-dimension byte array to 2-dimensions array. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values">       The values. </param>
    ''' <param name="startIndex">   The start index. </param>
    ''' <param name="rowsCount">    The rows count. </param>
    ''' <param name="columnsCount"> The columns count. </param>
    ''' <returns> A Short(,) </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Return")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Body")>
    Public Shared Function FromBytes(ByVal values As IEnumerable(Of Byte), ByVal startIndex As Integer, ByVal rowsCount As Integer, ByVal columnsCount As Integer) As Short(,)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim t As Short(,) = New Short(rowsCount - 1, columnsCount - 1) {}
        Dim n As Integer = startIndex
        Dim packedValue As PackedUInt16
        For row As Integer = 0 To rowsCount - 1
            ' store in LUT row
            packedValue = New PackedUInt16(values(n), values(n + 1)) : n += 2
            t(row, 0) = CShort(packedValue.Value)
            packedValue = New PackedUInt16(values(n), values(n + 1)) : n += 2
            t(row, 1) = CShort(packedValue.Value)
        Next row
        Return t

    End Function

    ''' <summary> Returns the data as a 1-Dimension byte array. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The array of values. </param>
    ''' <returns> Values as an IEnumerable(Of Byte) </returns>
    Public Shared Function ToBytes(ByVal values As Short(,)) As IEnumerable(Of Byte)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim bytes As New List(Of Byte)
        Dim packedValue As PackedUInt16
        For row As Integer = 0 To values.GetLength(0) - 1
            ' store in LUT row
            For col As Integer = 0 To values.GetLength(1) - 1
                packedValue = New PackedUInt16(CUShort(values(row, col)))
                bytes.Add(packedValue.HighByte)
                bytes.Add(packedValue.LowByte)
            Next
        Next row
        Return bytes.ToArray
    End Function

#End Region

End Class


