ISR Ports Serial Library<sub>&trade;</sub>
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*2.1.6667 2018-04-03*  
2018 release.

*2.0.6611 2018-02-06*  
Uses core library elements.

*1.0.4710 2012-11-23*  
Removes .VB tags from assemblies.

*1.0.4657 2012-10-01*  
Hide bias console properties.

*1.0.4655 2012-09-29*  
Fixes a crush due to handling of the port disposed
event.

*1.0.4651 2012-09-25*  
Adds a message parser interface. Rearranges Port code.
Removes history comments. Handles the data sent event to display the
event data.

*1.0.4650 2012-09-24*  
Validates Sync Context. Restore unsafe sync if sync
context is nothing. Clear the input buffer on a resync. Assign receive
delay.

*0.5.4645 2012-09-19*  
Defaults to hex display and enter hex. Handles the
device error and dispose commands.

*0.4.4643 2012-09-17*  
Implements safe invokes and view message events.

*0.3.4640 2012-09-14*  
Fixes and adds extensions. Adds try-catch clauses to
event handlers. Adds construction to message events arguments.

*0.2.4635 2012-09-09*  
Updates connection parameters on 'Connected'. Exposes
Serial Port to the calling application, Removes threshold parameters.
Documents the event methods. Changes connect and disconnect to
functions. Renames panel to Port Terminal. Improve handling of
characters. Makes some control properties hidden from the designer.
Updates panel based on current port parameters. Fixes To Hex. Adds
circular buffer. Applies code analysis changes.

*0.1.4626 2012-08-31*  
Version 0.1

\(C\) 2012 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

The source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Serial Libraries](http://git%20clone%20git@bitbucket.org:davidhary/vs.ports.serial.git)  
[Serial Port Sample in VB.NET
(C\#)](http://code.MSDN.microsoft.com/SerialPort-Sample-in-VBNET-fb040fb2)  
[Serial Port Windows Forms Application
II](http://code.MSDN.microsoft.com/SerialPort-Windows-Forms-a43f208e)  
[Extended Serial Port Windows Forms
Sample](http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37)  
[Circular Collection](http://www.INAV.NET)  
[Core Libraries](http://git%20clone%20git@bitbucket.org:davidhary/vs.core.git)
