﻿Imports System.ComponentModel
Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Enum ProjectTraceEventId

        ''' <summary> An enum constant representing the none option. </summary>
        <Description("Not specified")>
        None

        ''' <summary> An enum constant representing the serial option. </summary>
        <Description("Serial")>
        Serial = isr.Core.ProjectTraceEventId.Serial

        ''' <summary> An enum constant representing the rooster option. </summary>
        <Description("Rooster")>
        Rooster = ProjectTraceEventId.Serial + &H2

        ''' <summary> An enum constant representing the teleport option. </summary>
        <Description("Teleport")>
        Teleport = ProjectTraceEventId.Serial + &H2

        ''' <summary> An enum constant representing the 1000 option. </summary>
        <Description("D1000")>
        D1000 = ProjectTraceEventId.Serial + &H3

        ''' <summary> An enum constant representing the love option. </summary>
        <Description("Love")>
        Love = ProjectTraceEventId.Serial + &H4

        ''' <summary> An enum constant representing the switchboard option. </summary>
        <Description("Switchboard")>
        Switchboard = ProjectTraceEventId.Serial + &HA

        ''' <summary> An enum constant representing the serial terminal option. </summary>
        <Description("Serial Terminal")>
        SerialTerminal = ProjectTraceEventId.Serial + &HB

        ''' <summary> An enum constant representing the port forms option. </summary>
        <Description("Port Forms")>
        PortForms = ProjectTraceEventId.Serial + &HC

        ''' <summary> An enum constant representing the teleport forms option. </summary>
        <Description("Teleport Forms")>
        TeleportForms = ProjectTraceEventId.Serial + &HD
    End Enum


End Namespace
