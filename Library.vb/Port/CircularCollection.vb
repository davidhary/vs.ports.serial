Imports System
Imports System.Collections
Imports System.Threading
Imports System.Reflection

''' <summary>
''' <para>Implements a <b>Circular Buffer</b>. </para>
''' The Circular Buffer is a memory queue where memory locations are reused when the data
''' producer (writer) overwrites (modulo the buffer size) previously used locations. The Circular
''' Buffer operates in two modes of operation: the synchronous mode and the asynchronous mode.
''' The synchronous mode is useful where one process, either the producer (writer) or the
''' consumer (reader), operates much faster than the other. The faster process would otherwise
''' waste time waiting for the slower process. Instead the faster process can "burst" off the
''' data into the Circular Buffer and continue. The slower process will read the data at its own
''' rate. However, in the synchronous mode, the producer and consumer of the Circular Buffer must
''' access the queue at the same average rate over time or an overflow or underflow of data will
''' occur. In the asynchronous mode, we are only interested in the last data items written by the
''' producer--earlier data will be lost. This is useful in debug tracing where debug information
''' is continuously written into the Circular Buffer over a period of time until the error
''' condition is detected. The Circular Buffer is then examined for the sequence of states,
''' commands, etc. written just before the error was detected to help isolate the bug. The actual
''' capacity of the Circular Buffer is one less (N-1) than the total length of the Buffer(N) so
''' that a full buffer and an empty buffer can be differentiated. The default length is N=256 for
''' a default capacity of 255. A client process can be notified that the Circular Buffer has
''' reached a specified count (WaterMark) using the CBEventHandler delegate
'''   <para>
''' Example
'''   </para><para><code>
''' ' notify every time circular buffer is at count 8 theCB.SetWaterMark(8);
'''   </code></para><para><code>
''' theCB.WaterMarkNotify += new QueueCB.CBEventHandler(OnWaterMarkEvent);
'''   </code></para>
''' </summary>
''' <remarks>
''' Copyright (C) 2002 Robert HinRichs. All rights reserved.<para>
''' https://www.codeproject.com/articles/2880/circular-buffer-2</para><para>
''' BOBH@INAV.NET</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface")>
Public Class CircularCollection(Of T)
    Implements IEnumerable, IDisposable

#Region " CONSTRUCTION "

    ''' <summary> Default Constructor creates N=256 element Circular Buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(256)
    End Sub

    ''' <summary> Constructor creates a Circular Buffer with the specified length. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="length"> Circular Buffer Length (# of elements) </param>
    Public Sub New(ByVal length As System.Int32)
        MyBase.New()
        Me._NumberToAdd = 0
        Me._Buffer = New T(length - 1) {}
        Me.Capacity = length
        Me._Add = 1 - Me.Capacity
        Me._Len = Me.Capacity
        Me.ClearThis()
        Me._SynchronousMode = True
        Me._WatermarkCheckingEnabled = True
        Me._CountEvent = New CountEvent()
    End Sub

#Region " IDisposable Support "

    ''' <summary> Gets a value indicating whether this instance is disposed. </summary>
    ''' <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Releases unmanaged and - optionally - managed resources. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.WatermarkNotifyEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.WatermarkNotifyEvent.GetInvocationList
                        RemoveHandler Me.WatermarkNotify, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                    Next
                End If
                If Me._CountEvent IsNot Nothing Then Me._CountEvent.Dispose() : Me._CountEvent = Nothing
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

    ''' <summary>
    ''' Occurs when the read pointer hits the watermark count.
    ''' </summary>
    Public Event WatermarkNotify As EventHandler(Of System.EventArgs)

    ''' <summary> Number of. </summary>
    Private _Count As System.Int32

    ''' <summary>
    ''' Returns current number of items in the Circular Buffer. In the asynchronous mode, count will
    ''' saturate at N-1 if an overflow conditions exists. The read index will then follow the write
    ''' index so that the latest items are always available.
    ''' </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count() As System.Int32
        Get
            Return Me._Count
        End Get
    End Property

    ''' <summary> Zero-based index of the read. </summary>
    Private _ReadIndex As System.Int32

    ''' <summary> Returns next Read Index. </summary>
    ''' <value> The read index. </value>
    Public ReadOnly Property ReadIndex() As System.Int32
        Get
            Return Me._ReadIndex
        End Get
    End Property

    ''' <summary> Zero-based index of the write. </summary>
    Private _WriteIndex As System.Int32

    ''' <summary> Returns next Write Index. </summary>
    ''' <value> The write index. </value>
    Public ReadOnly Property WriteIndex() As System.Int32
        Get
            Return Me._WriteIndex
        End Get
    End Property

    ''' <summary> True to enable synchronous mode, false to disable it. </summary>
    Private _SynchronousMode As System.Boolean

    ''' <summary>
    ''' Used to set synchronous or asynchronous modes of operation Modes differ in how they handle
    ''' the buffer overflow condition.
    ''' </summary>
    ''' <remarks>
    ''' 1> Synchronous Mode -- In the synchronous mode the emptying of the 
    ''' Circular Buffer must occur at the same average rate as the filling 
    ''' of the Circular Buffer so that the overrun condition never occurs. 
    ''' That is, as the tail index chases the head index around the circle 
    ''' of the buffer, the head index always stays ahead (in a modulo N sense)
    ''' of the tail index. No data is lost. An exception will be thrown when 
    ''' a buffer overrun condition occurs.
    ''' <para>
    ''' 2> Asynchronous Mode -- In the asynchronous mode, data will be lost 
    ''' as the Circular Buffer is overwritten. Usually in this mode, only 
    ''' the last item written are of interest. When the Write index 
    ''' (head index) catches the Read Index (tail index) the Read Index is 
    ''' automatically incremented and the Circular Buffer count will indicate 
    ''' N-1 items in the Buffer.
    ''' The synchronous mode is pretty straight forward as most of the 
    ''' responsibility for avoiding buffer overflow is a system issue for the 
    ''' producer and consumer of the data. The circular buffer just checks 
    ''' for error conditions.
    ''' The asynchronous mode is more difficult as buffer overruns are allowed.
    ''' So things operate as normal until the maximum occupancy is reached in 
    ''' the asynchronous mode. From then on, while the buffer is still at 
    ''' maximum occupancy, the read index follows the write index. This 
    ''' simulates synchronous operation. I call this the saturation
    ''' mode. While saturation exists, an additional complication must be 
    ''' handled when the data is read. The read index is stored ahead of valid 
    ''' data (write index so an adjustment must be made before it is used to 
    ''' output data from the circular buffer. 
    ''' Capacity is N-1
    ''' </para>
    ''' </remarks>		
    ''' <value> Property <c>SynchronousMode</c>set to true of false. </value>
    Public Property SynchronousMode() As System.Boolean
        Get
            Return Me._SynchronousMode
        End Get
        Set(ByVal value As System.Boolean)
            SyncLock Me
                Me._SynchronousMode = value
            End SyncLock
        End Set
    End Property

    ''' <summary> Number of watermarks. </summary>
    Private _WatermarkCount As System.Int32

    ''' <summary>
    ''' Gets or sets the level (queue count) at which the WaterMarkNotify event will fire.
    ''' </summary>
    ''' <value> Property <c>Watermark</c>Pos. integer at which event fires. </value>
    Public Property WatermarkCount() As System.Int32
        Get
            Return Me._WatermarkCount
        End Get
        Set(ByVal value As System.Int32)
            SyncLock Me
                Me._WatermarkCount = value
            End SyncLock
        End Set
    End Property

    ''' <summary> True to enable, false to disable the watermark checking. </summary>
    Private _WatermarkCheckingEnabled As System.Boolean

    ''' <summary> Gets or sets a value indicating whether watermark checking is enabled. </summary>
    ''' <value> <c>True</c> if watermark checking is enabled; otherwise, <c>False</c>. </value>
    Public Property WatermarkCheckingEnabled() As System.Boolean
        Get
            Return Me._WatermarkCheckingEnabled
        End Get
        Set(ByVal value As System.Boolean)
            SyncLock Me
                Me._WatermarkCheckingEnabled = value
            End SyncLock
        End Set
    End Property

    ''' <summary> Returns assembly version string. </summary>
    ''' <value> The get version. </value>
    Public ReadOnly Property GetVersion() As String
        Get
            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim asmn As AssemblyName = asm.GetName()
            Dim ver As Version = asmn.Version
            Return ver.ToString()
        End Get
    End Property

    ''' <summary> The add. </summary>
    Private _Add As System.Int32

    ''' <summary> The length. </summary>
    Private _Len As System.Int32

    ''' <summary> Number of to adds. </summary>
    Private _NumberToAdd As System.Int32

    ''' <summary> The capacity. </summary>
    Private _Capacity As System.Int32

    ''' <summary> Gets or sets the capacity. Client must set through method. </summary>
    ''' <value> The capacity. </value>
    Public Property Capacity() As System.Int32
        Get
            Return Me._Capacity
        End Get
        Set(ByVal value As System.Int32)
            Me._Capacity = value
            Me._Add = 1 - Me.Capacity
            Me._Len = Me.Capacity
            Me.Clear()
        End Set
    End Property

    ''' <summary> The buffer. </summary>
    Private ReadOnly _Buffer() As T

    ''' <summary>
    ''' Resynchronizes the buffer pointers to beginning of buffer, i.e., it sets the Read pointer to
    ''' the write pointer. This is equivalent to dumping all the data in the buffer.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Resync()
        Me.ClearThis()
    End Sub

    ''' <summary> Clears the Circular Buffer and initializes with null. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ClearThis()
        Me._ReadIndex = 0
        Me._WriteIndex = 0
        Me._Count = 0
        Array.Clear(Me._Buffer, 0, Me.Capacity)
    End Sub

    ''' <summary> Clears the Circular Buffer and initializes with null. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Clear()
        Me.ClearThis()
    End Sub

    ''' <summary> Enable Water Mark (queue count) checking. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub WatermarkEnable()
        Me._WatermarkCheckingEnabled = True
    End Sub

    ''' <summary> Disable Water Mark (queue count) checking. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub WatermarkDisable()
        Me._WatermarkCheckingEnabled = False
    End Sub

    ''' <summary> Sets Asynchronous Mode of Circular Buffer operation-- see class summary. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub SetAsynchronousMode()
        Me.SynchronousMode = False
    End Sub

    ''' <summary> Sets Synchronous Mode of Circular Buffer operation-- see class summary. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub SetSynchronousMode()
        Me.SynchronousMode = True
    End Sub

    ''' <summary> Sets WaterMark (queue count) value for WaterMark event operations. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="mark"> Positive Integer WaterMark event value. </param>
    Public Sub SetWatermark(ByVal mark As System.Int32)
        If mark <= (Me.Capacity - 1) Or (mark < 0) Then
            Me.WatermarkCount = mark
        Else
            Throw New IndexRangeException("Watermark out of range")
        End If
    End Sub

    ''' <summary> Returns item at read index without modifying the queue. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <returns>
    ''' Returns the object at the read index
    ''' <c>without</c> removing it (modifying the index)
    ''' </returns>
    Public Function Peek() As T
        SyncLock Me
            Dim temp As T

            If Me.Count >= 1 Then
                temp = Me._Buffer(Me._ReadIndex)
                Return (temp)
            Else
                Throw New IndexRangeException("Too few items in circular buffer")
            End If

        End SyncLock ' unlock
    End Function

    ''' <summary> Add single item to the Circular Buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="item"> Generic item. </param>
    Public Sub Enqueue(ByVal item As T)
        If (Me.Count + 1 > Me.Capacity - 1) AndAlso (Me.SynchronousMode = True) Then
            Throw New IndexRangeException("Circular Buffer capacity exceeded--Synchronous Mode")
        Else ' Async mode
            Me._NumberToAdd = 1
            Me.WriteThis(item)
        End If
    End Sub

    ''' <summary> Add entire Array[] to the Circular Buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="items"> Array of items. </param>
    Public Sub Enqueue(ByVal items As Array)

        If items Is Nothing Then Return
        If (Me.Count + items.Length > Me.Capacity - 1) AndAlso (Me.SynchronousMode = True) Then
            Throw New IndexRangeException("Circular Buffer capacity exceeded--Synchronous Mode")
        Else ' Async mode
            Me._NumberToAdd = items.Length
            Me.WriteThis(items)
        End If

    End Sub

    ''' <summary> The count event. </summary>
    Private _CountEvent As CountEvent

    ''' <summary>
    ''' Add a single item to the Circular Buffer but block if no room. Used with synchronous mode
    ''' only.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="item"> Generic item. </param>
    Public Sub EnqueueBlocking(ByVal item As T)
        If Me.SynchronousMode = True Then
            If Me.Count <= (Me.Capacity - 1) Then ' if room is available
                Me.WriteThis(item) ' write the item
            Else
                Me._CountEvent.WaitLoad(1)
                Me.WriteThis(item) ' write the item
            End If

            Me._CountEvent.SetCopy(1) ' in case waiting for items

        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Sub

    ''' <summary>
    ''' Add entire Array[] to the Circular Buffer but block if no room. Used with synchronous mode
    ''' only.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="items"> Array of items. </param>
    Public Sub EnqueueBlocking(ByVal items As Array)
        If items Is Nothing Then Return
        If Me.SynchronousMode = True Then
            If (Me.Count + items.Length) <= (Me.Capacity - 1) Then ' if room is available
                Me.WriteThis(items) ' write the item
            Else
                Me._CountEvent.WaitLoad(items.Length)
                Me.WriteThis(items) ' write the item
            End If

            Me._CountEvent.SetCopy(items.Length) ' in case waiting for items
        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Sub

    ''' <summary>
    ''' Return/Remove single item from the Circular Buffer Will throw exception if no items are in
    ''' the Circular Buffer regardless of mode. This and CopyTo(Array, index, number) are the only
    ''' queue item removal methods that will check for underflow and throw an exception. Others will
    ''' either block or return the number of items they were able to successfully remove.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <returns> returns/removes a single object from the Circular Buffer. </returns>
    Public Function Dequeue() As T
        If Me.Count > 0 Then
            Return Me.ReadThis()
        Else
            Throw New IndexRangeException("No items in circular buffer")
        End If
    End Function

    ''' <summary>
    ''' Return/Remove single item from the Circular Buffer but block if empty. Must be in synchronous
    ''' mode.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <returns> returns/removes a single object from the Circular Buffer. </returns>
    Public Function DequeueBlocking() As T
        Dim tempo As T

        If Me.SynchronousMode = True Then
            If Me.Count <= 0 Then
                Me._CountEvent.WaitCopy(1)
            End If

            tempo = Me.ReadThis()
            Me._CountEvent.SetLoad(1) ' this is for a blocked enqueue
            Return tempo
        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of index.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="items"> Target Array[]. </param>
    ''' <param name="index"> Target Array[] offset. </param>
    ''' <returns> Number of items copied/removed from Circular Buffer. </returns>
    Public Function CopyTo(ByVal items As Array, ByVal index As System.Int32) As System.Int32

        If items Is Nothing Then Return Me.Count
        Dim cacheCount As System.Int32
        SyncLock Me
            cacheCount = Me.Count
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                Me.ReadThis(items, index, items.Length)
                Return cacheCount
            End If
        End SyncLock
    End Function

    ''' <summary>
    ''' Copy/Remove items from Circular Buffer to an Array[] with Array offset of index. This and
    ''' <see cref="Dequeue">dequeue</see> are the only queue item removal methods that will check for
    ''' underflow and throw an exception. Others will either block or return the number of items they
    ''' were able to successfully remove.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="items">     Target Array[]. </param>
    ''' <param name="index">     Target Array[] offset. </param>
    ''' <param name="itemCount"> Number of items to copy/remove. </param>
    ''' <returns> Number of items copied/removed from Circular Buffer. </returns>
    Public Function CopyTo(ByVal items As Array, ByVal index As System.Int32, ByVal itemCount As System.Int32) As System.Int32
        If items Is Nothing Then Return Me.Count
        Dim cacheCount As System.Int32
        SyncLock Me
            cacheCount = Me.Count
            If cacheCount >= itemCount Then
                Me.ReadThis(items, index, itemCount)
            Else
                Throw New IndexRangeException("Not enough items in circular buffer")
            End If
            Return cacheCount
        End SyncLock
    End Function

    ''' <summary> Copy/Remove items from entire Circular Buffer to an Array[]. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="items"> Target Array[]. </param>
    ''' <returns> Number if items copied/removed from Circular Buffer. </returns>
    Public Function CopyTo(ByVal items As Array) As System.Int32
        If items Is Nothing Then Return Me.Count
        Dim cacheCount As System.Int32
        SyncLock Me
            cacheCount = Me.Count
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                Me.ReadThis(items, 0, cacheCount)
                Return cacheCount
            End If
        End SyncLock
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of index but
    ''' block if empty. Must be in synchronous mode.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <exception cref="ArgumentException">   Thrown when one or more arguments have unsupported or
    '''                                        illegal values. </exception>
    ''' <param name="items"> Target Array[]. </param>
    ''' <param name="index"> Target Array[] offset. </param>
    ''' <returns> Number if items copied/removed from Circular Buffer. </returns>
    Public Function CopyToBlocking(ByVal items As Array, ByVal index As System.Int32) As System.Int32
        If items Is Nothing Then Return Me.Count
        Dim cacheCount As System.Int32
        If Me.SynchronousMode = True Then
            ' returns all available items from the circular buffer 
            ' to caller's array
            SyncLock Me
                cacheCount = Me.Count
            End SyncLock
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                If cacheCount > 0 Then
                    Me.ReadThis(items, index, cacheCount)
                    ' this is the only reason method is called blocking
                    ' this if for a blocked enqueue
                    Me._CountEvent.SetLoad(cacheCount)
                End If
            End If
            Return cacheCount
        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of index but
    ''' block if empty. Must be in synchronous mode.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="items">     Target Array[]. </param>
    ''' <param name="index">     Target Array[] offset. </param>
    ''' <param name="itemCount"> number to copy. </param>
    ''' <returns> Number if items copied/removed from Circular Buffer. </returns>
    Public Function CopyToBlocking(ByVal items As Array, ByVal index As System.Int32, ByVal itemCount As System.Int32) As System.Int32
        If items Is Nothing Then Return Me.Count
        Dim cacheCount As System.Int32
        If Me.SynchronousMode = True Then
            If Me.Count < itemCount Then
                ' since a specific number was asked for.
                Me._CountEvent.WaitCopy(itemCount)
                cacheCount = itemCount
                Me.ReadThis(items, index, itemCount)
            Else
                cacheCount = itemCount
                Me.ReadThis(items, index, itemCount)
            End If

            ' this if for a blocked enqueue
            Me._CountEvent.SetLoad(itemCount)
            Return cacheCount
        Else
            Throw New ArgumentException("Must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of zero but
    ''' block if empty. Must be in synchronous mode.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <exception cref="ArgumentException">   Thrown when one or more arguments have unsupported or
    '''                                        illegal values. </exception>
    ''' <param name="items"> Target Array[]. </param>
    ''' <returns> Number if items copied/removed from Circular Buffer. </returns>
    Public Function CopyToBlocking(ByVal items As Array) As System.Int32
        If items Is Nothing Then Return Me.Count
        Dim cacheCount As System.Int32
        If Me.SynchronousMode = True Then
            ' returns all available items from the circular buffer 
            ' to caller's array
            cacheCount = Me.Count
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                If cacheCount > 0 Then
                    Me.ReadThis(items, 0, items.Length)
                    ' this is the only reason method is 
                    ' called "blocking"
                    ' this if for a blocked enqueue
                    Me._CountEvent.SetLoad(cacheCount)
                End If
            End If

            Return cacheCount
        Else
            Throw New ArgumentException("Must be in synchronous mode")
        End If
    End Function

    ''' <summary> Displays Circular Buffer contents and or state information. Overridable. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overridable Sub DumpCircularBuffer()
    End Sub

    ''' <summary> Raise the watermark event by invoking the delegates. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub OnWatermarkNotify()
        Try
            ' fire the generic event
            Dim evt As EventHandler(Of EventArgs) = Me.WatermarkNotifyEvent
            evt?.Invoke(Me, System.EventArgs.Empty)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached)
        End Try
    End Sub

#Region " PRIVATE SUPPORT FUNCTIONS "

    ''' <summary> Writes the item to the circular collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="item"> The item. </param>
    Private Sub WriteThis(ByVal item As T)
        SyncLock Me
            Dim i As System.Int32 = Me._WriteIndex
            Dim tempCount As System.Int32
            Me._Buffer(i) = item
            ' for speed, we only update the count after the operation
            ' but if watermark check is on, we must do it here also
            ' but don't bother if nobody has registered for the event
            If (Me.WatermarkCheckingEnabled = True) AndAlso (Me.WatermarkNotifyEvent IsNot Nothing) Then
                tempCount = i - Me._ReadIndex
                If tempCount < 0 Then
                    tempCount += Me._Capacity ' modulo buffer size
                End If

                If tempCount = Me.WatermarkCount Then
                    Me.OnWatermarkNotify()
                End If
            End If

            i += Me._Add
            If i < 0 Then
                i += Me._Len
            End If

            Me._WriteIndex = i
            Me.UpdateCount()
        End SyncLock ' unlock
    End Sub

    ''' <summary> Writes the items from the array into the buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="items"> The items. </param>
    Private Sub WriteThis(ByVal items As Array)
        If items Is Nothing Then Return
        SyncLock Me
            Dim i As System.Int32 = Me._WriteIndex
            Dim n As System.Int32 = items.Length
            Dim offset As System.Int32 = 0
            Dim tempCount As System.Int32

            Dim tempVar As Boolean = n > 0
            n -= 1
            Do While tempVar
                Me._Buffer(i) = CType(items.GetValue(offset), T)
                offset += 1

                ' for speed, we only update the count after the operation
                ' but if watermark check is on, we must do it here also
                ' but don't bother if nobody has registered for the event
                If (Me.WatermarkCheckingEnabled = True) AndAlso (Me.WatermarkNotifyEvent IsNot Nothing) Then
                    tempCount = i - Me._ReadIndex
                    If tempCount < 0 Then
                        tempCount += Me._Capacity ' modulo buffer size
                    End If

                    If tempCount = Me.WatermarkCount Then
                        Me.OnWatermarkNotify()
                    End If
                End If

                i += Me._Add
                If i < 0 Then
                    i += Me._Len
                End If
                tempVar = n > 0
                n -= 1
            Loop
            Me._WriteIndex = i
            Me.UpdateCount()
        End SyncLock ' unlock
    End Sub

    ''' <summary> Reads an item from the circular collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> this. </returns>
    Private Function ReadThis() As T

        SyncLock Me
            Dim i As System.Int32 = Me._ReadIndex
            Dim temp As T
            Dim tempCount As System.Int32

            ' A modification to the read index may be required
            ' if we have been operating in asynchronous saturated mode
            If Me.SynchronousMode = False Then
                If Me.Count >= (Me.Capacity - 1) Then
                    i = (Me._ReadIndex - 1) Mod Me.Capacity
                End If
            End If

            temp = Me._Buffer(i)

            ' for speed, we only update the count after the operation
            ' but if watermark check is on, we must do it here also
            ' but don't bother if nobody has registered for the event
            If (Me.WatermarkCheckingEnabled = True) AndAlso (Me.WatermarkNotifyEvent IsNot Nothing) Then
                tempCount = Me._ReadIndex - i ' change
                If tempCount < 0 Then
                    tempCount += Me._Capacity ' modulo buffer size
                End If

                If tempCount = Me.WatermarkCount Then
                    Me.OnWatermarkNotify()
                End If
            End If

            i += Me._Add
            If i < 0 Then
                i += Me._Len
            End If
            Me._ReadIndex = i
            Me.UpdateCount()
            Return temp
        End SyncLock ' unlock
    End Function

    ''' <summary> Reads the specified items from the circular buffer into the array. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="IndexRangeException"> Thrown when an Index Range error condition occurs. </exception>
    ''' <param name="items">      The items. </param>
    ''' <param name="index">      The index. </param>
    ''' <param name="itemsCount"> The items count. </param>
    Private Sub ReadThis(ByVal items As Array, ByVal index As System.Int32, ByVal itemsCount As System.Int32)

        If items Is Nothing Then Return
        SyncLock Me
            Dim i As System.Int32 = Me._ReadIndex
            Dim n As System.Int32 = itemsCount - index
            Dim offset As System.Int32 = index
            Dim tempCount As System.Int32

            ' A modification to the read index may be required
            ' if we have been operating in asynchronous saturated mode
            'if(SynchronousMode==false)
            '	if(Count >= (capacity-1))
            '		I = (n.r.p-1)%capacity;
            If Me.Count >= n Then
                Dim tempVar As Boolean = n > 0
                n -= 1
                Do While tempVar
                    items.SetValue(Me._Buffer(i), offset)
                    offset += 1
                    ' for speed, we only update the count after the 
                    ' operation but if watermark check is on, we must 
                    ' do it here also but don't bother if nobody has
                    ' registered for the event
                    If (Me.WatermarkCheckingEnabled = True) AndAlso (Me.WatermarkNotifyEvent IsNot Nothing) Then
                        tempCount = Me._ReadIndex - i ' change
                        If tempCount < 0 Then
                            tempCount += Me._Capacity ' modulo buffer size
                        End If

                        If tempCount = Me.WatermarkCount Then
                            Me.OnWatermarkNotify()
                        End If
                    End If
                    i += Me._Add
                    If i < 0 Then
                        i += Me._Len
                    End If
                    tempVar = n > 0
                    n -= 1
                Loop
            Else
                Throw New IndexRangeException("Too few items in circular buffer")
            End If
            Me._ReadIndex = i
            Me.UpdateCount()
        End SyncLock ' unlock
    End Sub

    ''' <summary> Updates the count. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub UpdateCount()
        If Me.SynchronousMode = True Then
            Me._Count = Me._WriteIndex - Me._ReadIndex
            If Me.Count < 0 Then
                Me._Count += Me._Capacity ' modulo buffer size
            End If
            ' if asynchronousMode, adjust the read index to follow the 
            ' write index if buffer is full (saturation)
        Else
            ' check to see if we are in saturation
            If Me._Count + Me._NumberToAdd >= (Me.Capacity - 1) Then
                Me._ReadIndex = (Me._WriteIndex + Me._NumberToAdd) Mod Me.Capacity
                ' and Count remains at max capacity
                Me._Count = Me.Capacity - 1
            Else
                Me._Count = Me._WriteIndex - Me._ReadIndex
                If Me.Count < 0 Then
                    Me._Count += Me._Capacity ' modulo buffer size
                End If
            End If
        End If

    End Sub
#End Region

#Region " IEnumerator Support "

    ''' <summary> IEnumerator support. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The enumerator. </returns>
    Public Function GetEnumerator() As CircularCollectionEnumerator
        Return New CircularCollection(Of T).CircularCollectionEnumerator(Me)
    End Function

    ''' <summary> Enumerable get enumerator. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> An IEnumerator. </returns>
    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return Me.GetEnumerator()
    End Function

    ''' <summary> Enumerator class for the circular Collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Class CircularCollectionEnumerator
        Implements IEnumerator

        ''' <summary> The buffer. </summary>
        Private ReadOnly _Buffer As CircularCollection(Of T)

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="buffer"> The buffer. </param>
        Friend Sub New(ByVal buffer As CircularCollection(Of T))
            Me._Buffer = buffer
        End Sub

        ' IEnumerator support

        ''' <summary> IEnumerator support. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <returns>
        ''' <see langword="true" /> if the enumerator was successfully advanced to the next element;
        ''' <see langword="false" /> if the enumerator has passed the end of the collection.
        ''' </returns>
        Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext
            SyncLock Me
                Return Me._Buffer.Count <> 0
            End SyncLock
        End Function

        ''' <summary> IEnumerator support. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        Public Sub Reset() Implements IEnumerator.Reset
        End Sub

        ''' <summary> Gets the current element in the collection. </summary>
        ''' <remarks> Client must set through method. </remarks>
        ''' <value> The current element in the collection. </value>
        Private ReadOnly Property _Current() As Object Implements IEnumerator.Current
            Get
                SyncLock Me
                    Dim i As System.Int32 = Me._Buffer._ReadIndex
                    Dim temp As T

                    ' A modification to the read index may be required
                    ' if we have been operating in asynchronous 
                    ' saturated mode
                    If Me._Buffer.SynchronousMode = False Then
                        If Me._Buffer.Count >= (Me._Buffer.Capacity - 1) Then
                            i = (Me._Buffer._ReadIndex - 1) Mod Me._Buffer.Capacity
                        End If
                    End If

                    temp = Me._Buffer._Buffer(i)
                    i += Me._Buffer._Add
                    If i < 0 Then
                        i += Me._Buffer._Len
                    End If
                    Me._Buffer._ReadIndex = i
                    Me._Buffer.UpdateCount()
                    Return (temp)
                End SyncLock ' unlock
            End Get
        End Property

        ''' <summary> Gets the current element in the collection. </summary>
        ''' <value> The current element in the collection. </value>
        Public ReadOnly Property Current As T
            Get
                Return CType(Me._Current, T)
            End Get
        End Property

    End Class ' end class CBEnumerator
#End Region


End Class

''' <summary> Blocking Support Class. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Friend Class CountEvent
    Implements IDisposable

    ''' <summary> Initializes a new instance of the <see cref="CountEvent" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Friend Sub New()
        MyBase.New()
        Me._LoadAutoEvent = New AutoResetEvent(False)
        Me._CopyAutoEvent = New AutoResetEvent(False)
        Me._CopyEventCount = 0
        Me._LoadEventCount = 0
    End Sub

    ''' <summary> The load automatic event. </summary>
    Private _LoadAutoEvent As AutoResetEvent

    ''' <summary> The copy automatic event. </summary>
    Private _CopyAutoEvent As AutoResetEvent

    ''' <summary> Number of load events. </summary>
    Private _LoadEventCount As System.Int32 = 0

    ''' <summary> Number of copy events. </summary>
    Private _CopyEventCount As System.Int32 = 0

    ''' <summary> Requests to continue when the number specified can be written. </summary>
    ''' <remarks> WaitLoad() and SetLoad() are part of a blocked enqueue operation. </remarks>
    ''' <param name="requestedCount"> The requested count. </param>
    Friend Sub WaitLoad(ByVal requestedCount As System.Int32)
        SyncLock Me
            Me._LoadEventCount = requestedCount ' wait for count to reach this value
        End SyncLock
        Me._LoadAutoEvent.WaitOne()
    End Sub

    ''' <summary> Updates the count as items are removed from the circular buffer. </summary>
    ''' <remarks> WaitLoad() and SetLoad() are part of a blocked enqueue operation. </remarks>
    ''' <param name="requestedCount"> The requested count. </param>
    Friend Sub SetLoad(ByVal requestedCount As System.Int32)
        SyncLock Me
            If Me._LoadEventCount > 0 Then
                Me._LoadEventCount -= requestedCount ' queue has been emptied by this amount
                If Me._LoadEventCount <= 0 Then
                    Me._LoadAutoEvent.Set()
                End If
            End If
        End SyncLock
    End Sub

    ''' <summary> Requests to continue when the number specified can be read. </summary>
    ''' <remarks> WaitCopy() and SetCopy() are part of a blocked dequeue operation. </remarks>
    ''' <param name="requestedCount"> The requested count. </param>
    Friend Sub WaitCopy(ByVal requestedCount As System.Int32)
        SyncLock Me
            Me._CopyEventCount = requestedCount ' countdown this value
        End SyncLock

        Me._CopyAutoEvent.WaitOne()
    End Sub

    ''' <summary> Updates the count as items are written into the circular buffer. </summary>
    ''' <remarks> WaitCopy() and SetCopy() are part of a blocked dequeue operation. </remarks>
    ''' <param name="requestedCount"> The requested count. </param>
    Friend Sub SetCopy(ByVal requestedCount As System.Int32)
        SyncLock Me
            If Me._CopyEventCount > 0 Then
                Me._CopyEventCount -= requestedCount ' queue has been emptied by this amount
                If Me._CopyEventCount <= 0 Then
                    Me._CopyAutoEvent.Set()
                End If
            End If
        End SyncLock
    End Sub

#Region "IDisposable Support"

    ''' <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
    ''' <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Releases unmanaged and - optionally - managed resources. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._LoadAutoEvent IsNot Nothing Then
                        Me._LoadAutoEvent.Dispose()
                        Me._LoadAutoEvent = Nothing
                    End If
                    If Me._CopyAutoEvent IsNot Nothing Then
                        Me._CopyAutoEvent.Dispose()
                        Me._CopyAutoEvent = Nothing
                    End If
                End If
                ' TO_DO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TO_DO: set large fields to null.
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class


