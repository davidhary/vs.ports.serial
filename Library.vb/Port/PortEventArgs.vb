﻿Imports System.Collections.Generic

''' <summary> Defines an event arguments class for <see cref="Port">port messages</see>. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class PortEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(True, Array.Empty(Of Byte)())
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="okay"> if set to <c>True</c> [okay]. </param>
    Public Sub New(ByVal okay As Boolean)
        Me.New(okay, Array.Empty(Of Byte)())
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Public Sub New(ByVal e As PortEventArgs)
        Me.New(PortEventArgs.Validated(e).StatusOkay, PortEventArgs.Validated(e).DataBuffer)
    End Sub

    ''' <summary> Validated the given e. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Port event information. </param>
    ''' <returns> The PortEventArgs. </returns>
    Public Shared Function Validated(ByVal e As PortEventArgs) As PortEventArgs
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Return e
    End Function

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="okay"> if set to <c>True</c> [okay]. </param>
    ''' <param name="data"> The data. </param>
    Public Sub New(ByVal okay As Boolean, ByVal data As IEnumerable(Of Byte))
        MyBase.New()
        Me._StatusOkay = okay
        Me._DataBuffer = data
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets a value indicating whether status is okay, true; Otherwise, False.
    ''' </summary>
    ''' <value> The status okay. </value>
    Public ReadOnly Property StatusOkay As Boolean

    ''' <summary> Gets or sets the data buffer. </summary>
    ''' <value> A buffer for data data. </value>
    Public ReadOnly Property DataBuffer As IEnumerable(Of Byte)

    ''' <summary> Gets a string. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="encoding"> The encoding. </param>
    ''' <returns> The string. </returns>
    Public Function GetString(ByVal encoding As Text.Encoding) As String
        If encoding Is Nothing Then Throw New ArgumentNullException(NameOf(encoding))
        Return encoding.GetString(Me.DataBuffer.ToArray, 0, Me.DataBuffer.Count)
    End Function
#End Region

End Class
