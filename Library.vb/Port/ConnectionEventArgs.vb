﻿''' <summary>
''' Defines an event arguments class for <see cref="Port">port connection messages</see>.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class ConnectionEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PortEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(False)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ConnectionEventArgs" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="isPortOpen"> if set to <c>True</c> [is connected]. </param>
    Public Sub New(ByVal isPortOpen As Boolean)
        MyBase.New()
        Me._IsPortOpen = isPortOpen
    End Sub

#End Region

    ''' <summary>
    ''' Gets or sets a value indicating whether the port is open, true; Otherwise, False.
    ''' </summary>
    ''' <value> The is port open. </value>
    Public ReadOnly Property IsPortOpen As Boolean

End Class
