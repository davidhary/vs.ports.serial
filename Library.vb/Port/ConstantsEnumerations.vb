Imports System.ComponentModel

''' <summary> Enumerates the key into the port parameters dictionary. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum PortParameterKey

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("None")>
    None

    ''' <summary> An enum constant representing the port name option. </summary>
    <Description("Port Name")>
    PortName

    ''' <summary> An enum constant representing the baud rate option. </summary>
    <Description("Baud Rate")>
    BaudRate

    ''' <summary> An enum constant representing the data bits option. </summary>
    <Description("Data Bits")>
    DataBits

    ''' <summary> An enum constant representing the parity option. </summary>
    <Description("Parity")>
    Parity

    ''' <summary> An enum constant representing the stop bits option. </summary>
    <Description("Stop Bits")>
    StopBits

    ''' <summary> An enum constant representing the received bytes threshold option. </summary>
    <Description("Received Bytes Threshold")>
    ReceivedBytesThreshold

    ''' <summary> An enum constant representing the receive delay option. </summary>
    <Description("Receive Delay")>
    ReceiveDelay

    ''' <summary> An enum constant representing the handshake option. </summary>
    <Description("Handshake")>
    Handshake

    ''' <summary> An enum constant representing the RTS (request to send) enable option. </summary>
    <Description("RtsEnable")>
    RtsEnable

    ''' <summary> An enum constant representing the read timeout option. </summary>
    <Description("Read Timeout")>
    ReadTimeout

    ''' <summary> An enum constant representing the read buffer size option. </summary>
    <Description("Read Buffer Size")>
    ReadBufferSize

    ''' <summary> An enum constant representing the write buffer size option. </summary>
    <Description("Write Buffer Size")>
    WriteBufferSize
End Enum

''' <summary> Enumerates the data buffering option. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum DataBufferingOption

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not defined")>
    None

    ''' <summary> An enum constant representing the linear buffer option. </summary>
    <Description("Linear Buffer")>
    LinearBuffer

    ''' <summary> An enum constant representing the circular buffer option. </summary>
    <Description("Circular Buffer")>
    CircularBuffer
End Enum
