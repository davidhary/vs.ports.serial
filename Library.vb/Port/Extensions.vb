Imports System.Collections.Generic
Public Module Methods

#Region " ALPHA "

    ''' <summary> Query if 'value' is alpha numeric. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if alpha numeric; otherwise <c>false</c> </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function IsAlphanumeric(ByVal value As String) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim result As Boolean = True
        For Each c As Char In value.ToCharArray
            If Not Char.IsLetterOrDigit(c) Then
                result = False
                Exit For
            End If
        Next
        Return result
    End Function

#End Region

#Region " DELIMITER "

    ''' <summary> Builds the hex string by adding delimiter between each pair of nibbles. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> The given data converted to a String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToByteFormatted(ByVal value As String, ByVal delimiter As String) As String
        Return value.RemoveDelimiter(delimiter).InsertDelimiter(2, delimiter)
    End Function

    ''' <summary> Inserts the delimiter. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value">       The value. </param>
    ''' <param name="everyLength"> Length of the every. </param>
    ''' <param name="delimiter">   The delimiter. </param>
    ''' <returns> A String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function InsertDelimiter(ByVal value As String, ByVal everyLength As Integer, ByVal delimiter As String) As String
        If String.IsNullOrWhiteSpace(value) Then Return value
        If String.IsNullOrEmpty(delimiter) Then Return value
        Dim sb As New System.Text.StringBuilder
        For i As Integer = 0 To value.Length - 1 Step everyLength
            If sb.Length > 0 Then sb.Append(delimiter)
            Dim maxLength As Integer = Math.Min(value.Length - i, everyLength)
            sb.Append(value.Substring(i, maxLength))
        Next
        Return sb.ToString
    End Function

    ''' <summary> Removes the delimiter. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> A String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function RemoveDelimiter(ByVal value As String, ByVal delimiter As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return value
        Else
            If String.IsNullOrEmpty(delimiter) Then
                Return value
            Else
                Dim values() As String = value.Split(New String() {delimiter}, StringSplitOptions.RemoveEmptyEntries)
                Dim sb As New System.Text.StringBuilder
                For Each v As String In values
                    sb.Append(v.Trim)
                Next
                Return sb.ToString
            End If
        End If
    End Function

    ''' <summary> Convert a HEX string to its representing binary values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexText"> The hex values. </param>
    ''' <returns> The converted bytes. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHexBytes(ByVal hexText As String) As IEnumerable(Of Byte)
        If String.IsNullOrWhiteSpace(hexText) Then
            Return Array.Empty(Of Byte)()
        Else
            Dim d As String = " "
            Return ToHexBytes(InsertDelimiter(hexText, 2, d), d)
        End If
    End Function

    ''' <summary> The parse error value. </summary>
    Public Const ParseErrorValue As Byte = 255

    ''' <summary> Convert a delimited HEX string to its representing binary values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexText">   The hex values. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns>
    ''' The converted bytes. If error, the first byte is set to <see cref="ParseErrorValue"/> and the
    ''' second byte is set to the ASCII value of the offending character.
    ''' </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHexBytes(ByVal hexText As String, ByVal delimiter As String) As IEnumerable(Of Byte)
        Dim data As New List(Of Byte)
        If Not (String.IsNullOrWhiteSpace(hexText) OrElse String.IsNullOrEmpty(delimiter)) Then
            Dim s() As String = hexText.Split(New String() {delimiter}, StringSplitOptions.RemoveEmptyEntries)
            Dim result As Byte = 0
            For Each value As String In s
                If Byte.TryParse(value, Globalization.NumberStyles.HexNumber, Globalization.CultureInfo.CurrentCulture, result) Then
                    data.Add(result)
                Else
                    ' if error, tag the first byte at the Parse Error Values and point to the location of the error.
                    data = New List(Of Byte)({Methods.ParseErrorValue, CByte(Asc(CChar(value)))})
                    Exit For
                End If
            Next
        End If
        Return data
    End Function

#End Region

#Region " HEX "

    ''' <summary> Determines whether the specified value is hex type. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if the specified value is allowed; otherwise, <c>False</c>. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function IsHexadecimal(ByVal value As Char) As Boolean
        Dim allowedChars As String = "0123456789ABCDEFabcdef"
        Return Not String.IsNullOrWhiteSpace(value) AndAlso allowedChars.Contains(value)
    End Function

    ''' <summary> Returns an HEX string using 2 nibble presentation. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal value As Byte) As String
        Return $"{value:X2}"
    End Function

    ''' <summary> Returns an HEX string using 2 nibble presentation. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> Values as a String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal values As IEnumerable(Of Byte)) As String
        If values Is Nothing Then
            Return String.Empty
        Else
            Dim sb As New System.Text.StringBuilder
            If values IsNot Nothing Then
                For Each b As Byte In values
                    sb.Append(b.ToHex)
                Next
            End If
            Return sb.ToString
        End If
    End Function

    ''' <summary> Returns an HEX string using 2 nibble presentation. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values">     The values. </param>
    ''' <param name="startIndex"> The start index. </param>
    ''' <param name="length">     The length. </param>
    ''' <returns> The given data converted to a String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal values As IEnumerable(Of Byte), ByVal startIndex As Integer, ByVal length As Integer) As String
        If values Is Nothing Then
            Return String.Empty
        Else
            Dim sb As New System.Text.StringBuilder
            If values IsNot Nothing Then
                If values.Count > startIndex Then
                    length = Math.Min(length, values.Count - startIndex)
                    For i As Integer = startIndex To startIndex + length - 1
                        sb.Append(values(i).ToHex)
                    Next
                End If
            End If
            Return sb.ToString
        End If
    End Function

    ''' <summary> Returns an HEX string. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value">       The value. </param>
    ''' <param name="nibbleCount"> Number of nibbles. </param>
    ''' <returns> The given data converted to a String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal value As Byte, ByVal nibbleCount As Byte) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
    End Function

    ''' <summary> Returns a byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToByte(ByVal value As String) As Byte
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        Return Byte.Parse(value, Globalization.NumberStyles.HexNumber)
    End Function

    ''' <summary> Gets a string. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="encoding"> The encoding. </param>
    ''' <param name="value">    The value. </param>
    ''' <returns> The string. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function GetString(ByVal encoding As Text.Encoding, ByVal value As Byte) As String
        If encoding Is Nothing Then Throw New ArgumentNullException(NameOf(encoding))
        Return encoding.GetString(New Byte() {value})
    End Function

#End Region

#Region " BYTES "

    ''' <summary> Nullable sequence equals. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function NullableSequenceEquals(ByVal left As IEnumerable(Of Byte), ByVal right As IEnumerable(Of Byte)) As Boolean
        Return left IsNot Nothing AndAlso right IsNot Nothing AndAlso Enumerable.SequenceEqual(Of Byte)(left, right)
    End Function

    ''' <summary> Converts a value to the bytes. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Byte() </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToBytes(ByVal value As String) As Byte()
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        Dim encoding As New System.Text.ASCIIEncoding
        ' Store the source string in a byte array         
        Return encoding.GetBytes(value)
    End Function

    ''' <summary> Adds values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values to add. </param>
    ''' <returns> An Integer. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function Sum(ByVal values As IEnumerable(Of Byte)) As Integer
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Integer = 0
        For Each value As Byte In values
            result += value
        Next
        Return result
    End Function

    ''' <summary> Get the check sum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function Checksum(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        Dim sum As Long = value.ToBytes.Sum
        Return CByte(sum And &HFF).ToHex
    End Function

    ''' <summary> Query if 'value' has a valid checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if valid checksum; otherwise <c>false</c> </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function IsValidChecksum(ByVal value As String) As Boolean
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        Dim expected As String = value.Substring(value.Length - 3, 2)
        Dim actual As String = value.Remove(value.Length - 3, 2).Checksum
        Return String.Equals(expected, actual, StringComparison.OrdinalIgnoreCase)
    End Function

#End Region

#Region " INTEGER "

    ''' <summary> Returns an integer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as an Integer. </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToInteger(ByVal value As String) As Integer
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        Return Integer.Parse(value, Globalization.NumberStyles.HexNumber)
    End Function

#End Region

End Module

