Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

Imports isr.Core
Imports isr.Ports.Serial.ExceptionExtensions

''' <summary>
''' Defines a wrapper around the Visual Studio <see cref="SerialPort">serial port</see>
''' </summary>
''' <remarks>
''' Based on Extended Serial Port Windows Forms Sample
''' http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37 <para>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Port
    Inherits isr.Core.Models.ViewModelTalkerBase
    Implements IPort

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> initialize a new instance. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(New System.IO.Ports.SerialPort())
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Port" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="serialPort"> The <see cref="SerialPort">serial port.</see> </param>
    Public Sub New(ByVal serialPort As System.IO.Ports.SerialPort)
        MyBase.New()

        Me._PortParametersFileName = Path.Combine(My.Application.Info.DirectoryPath, $"{My.Application.Info.AssemblyName}.ini")

        Me._TimeoutTimer = New Timers.Timer

        ' create a new instance of the serial port.
        Me._SerialPort = serialPort

        ' initialize the default port parameters.
        Me._PortParameters = New PortParametersDictionary(Me.SerialPort, 1) With {.PortName = Port.DefaultPortName}

        Me._InputBufferingOption = Serial.DataBufferingOption.LinearBuffer

        Me.ReceivedBytes = New List(Of Byte)
        Me.TransmittedBytes = New List(Of Byte)
        AddHandler My.Settings.PropertyChanged, AddressOf Me.MySettings_PropertyChanged

    End Sub

    ''' <summary> Gets the is port that owns this item. </summary>
    ''' <value> The is port owner. </value>
    Public Property IsPortOwner As Boolean

    ''' <summary> Creates a new Port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A Port. </returns>
    Public Shared Function Create() As Port
        Dim port As Port = Nothing
        Try
            port = New Port
        Catch
            If port IsNot Nothing Then port.Dispose() : 
            Throw
        End Try
        Return port
    End Function

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                RemoveHandler My.Settings.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
                Me.RemoveConnectionChangedEventHandlers()
                Me.RemoveDataReceivedEventHandlers()
                Me.RemoveDataSentEventHandlers()
                Me.RemoveSerialPortDisposedEventHandlers()
                Me.RemoveTimeoutEventHandlers()
                If Me._TimeoutTimer IsNot Nothing Then Me._TimeoutTimer.Enabled = False : Me._TimeoutTimer.Dispose() : Me._TimeoutTimer = Nothing
                If Me.IsPortOwner AndAlso Me._SerialPort IsNot Nothing Then Me._SerialPort.Dispose()
                Me._SerialPort = Nothing
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " SHARED "

    ''' <summary> Default port name. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A String. </returns>
    Public Shared Function DefaultPortName() As String
        Dim portNames As String() = SerialPort.GetPortNames
        Return If(portNames.Any AndAlso Not portNames.Contains(My.Settings.PortName, StringComparer.OrdinalIgnoreCase),
            portNames(0),
            My.Settings.PortName)
    End Function

    ''' <summary> Queries if a given port exists. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portName"> Name of the port. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function PortExists(ByVal portName As String) As Boolean
        Return Port.PortExists(SerialPort.GetPortNames, portName)
    End Function

    ''' <summary> Queries if a given port exists. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portNames"> List of names of the ports. </param>
    ''' <param name="portName">  Name of the port. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function PortExists(ByVal portNames As String(), ByVal portName As String) As Boolean
        Return portNames IsNot Nothing AndAlso portNames.Contains(portName, StringComparer.CurrentCultureIgnoreCase)
    End Function

#End Region

#Region " PORT PARAMETERS "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _PortParameters As PortParametersDictionary
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the port parameters. </summary>
    ''' <value> The port properties. </value>
    Public ReadOnly Property PortParameters As PortParametersDictionary Implements IPort.PortParameters
        Get
            Return Me._PortParameters
        End Get
    End Property

    ''' <summary> Port properties property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PortProperties_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _PortParameters.PropertyChanged
        Dim activity As String = String.Empty
        Try
            If sender IsNot Nothing AndAlso e IsNot Nothing Then
                activity = $"handling {NameOf(PortParametersDictionary)}.{e.PropertyName} change"
                Me.NotifyPropertyChanged($"{NameOf(Port.PortParameters)}.{e.PropertyName}")
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Returns the port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portName"> Name of the port. </param>
    ''' <returns>
    ''' A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
    ''' <see cref="PortParameterKey">parameter key</see>
    ''' </returns>
    Public Function ToPortParameters(ByVal portName As String) As PortParametersDictionary
        Dim p As PortParametersDictionary = PortParametersDictionary.ToPortParameters(Me.SerialPort, Me.PortParameters.ReceiveDelay)
        p.PortName = portName
        Return p
    End Function

    ''' <summary> Returns the port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
    ''' <see cref="PortParameterKey">parameter key</see>
    ''' </returns>
    Public Function ToPortParameters() As PortParametersDictionary Implements IPort.ToPortParameters
        Return PortParametersDictionary.ToPortParameters(Me.SerialPort, Me.PortParameters.ReceiveDelay)
    End Function

    ''' <summary> Assign port parameters to the port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
    '''                               parameters keyed by the
    '''                               <see cref="PortParameterKey">parameter key</see> </param>
    Public Sub FromPortParameters(ByVal portParameters As PortParametersDictionary) Implements IPort.FromPortParameters
        If Me._SerialPort IsNot Nothing AndAlso portParameters IsNot Nothing Then
            PortParametersDictionary.FromPortParameters(Me.SerialPort, portParameters)
        End If
    End Sub

    ''' <summary> Filename of the port parameters file. </summary>
    Private _PortParametersFileName As String

    ''' <summary> Gets or sets the filename of the port parameters file. </summary>
    ''' <value> The filename of the port parameters file. </value>
    Public Property PortParametersFileName As String Implements IPort.PortParametersFileName
        Get
            Return Me._PortParametersFileName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.PortParametersFileName) Then
                Me._PortParametersFileName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Attempts to store port parameters from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryStorePortParameters(ByVal e As Core.ActionEventArgs) As Boolean Implements IPort.TryStorePortParameters
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = $"storing port parameters to {Me.PortParametersFileName}"
            Me.Publish(Diagnostics.TraceEventType.Verbose, activity)
            Me.PortParameters.Store(Me.PortParametersFileName)
        Catch ex As System.IO.IOException
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Attempts to restore port parameters from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryRestorePortParameters(ByVal e As Core.ActionEventArgs) As Boolean Implements IPort.TryRestorePortParameters
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = $"restoring port parameters from {Me.PortParametersFileName}"
            Me.Publish(Diagnostics.TraceEventType.Verbose, activity)
            Me.PortParameters.Restore(Me.PortParametersFileName)
            activity = $"applying port parameters to port {Me.SerialPort.PortName}"
            PortParametersDictionary.FromPortParameters(Me.SerialPort, Me.PortParameters)
            Me.Publish(Diagnostics.TraceEventType.Verbose, activity)
        Catch ex As System.IO.IOException
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> The supported baud rates. </summary>
    Private _SupportedBaudRates As List(Of Integer)

    ''' <summary> Gets the supported baud rates. </summary>
    ''' <value> The supported baud rates. </value>
    Public ReadOnly Property SupportedBaudRates As IEnumerable(Of Integer) Implements IPort.SupportedBaudRates
        Get
            If Me._SupportedBaudRates Is Nothing Then
                Me._SupportedBaudRates = New List(Of Integer) From {300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600}
            End If
            Return Me._SupportedBaudRates
        End Get
    End Property

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Gets the is open. </summary>
    ''' <value> The is open. </value>
    Public ReadOnly Property IsOpen As Boolean Implements IPort.IsOpen
        Get
            Return Me.SerialPort IsNot Nothing AndAlso Me.SerialPort.IsOpen
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _SerialPort As New SerialPort
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the <see cref="SerialPort">Serial Port</see>. </summary>
    ''' <value> The serial port. </value>
    Public Property SerialPort As SerialPort Implements IPort.SerialPort
        Get
            Return Me._SerialPort
        End Get
        Set(value As SerialPort)
            Me._SerialPort = value
            If value Is Nothing Then
            Else
                Me.TryNotifyConnectionChanged()
            End If
        End Set
    End Property

    ''' <summary> Opens the port using current port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    Public Function TryOpen(ByVal e As Core.ActionEventArgs) As Boolean Implements IPort.TryOpen
        Return Me.TryOpen(Me.PortParameters, e)
    End Function

    ''' <summary> Opens the port using current port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="portName"> Name of the port. </param>
    ''' <param name="e">        Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    Public Function TryOpen(ByVal portName As String, ByVal e As Core.ActionEventArgs) As Boolean Implements IPort.TryOpen
        If String.IsNullOrWhiteSpace(portName) Then Throw New ArgumentNullException(NameOf(portName))
        Return Me.TryOpen(Me.ToPortParameters(portName), e)
    End Function

    ''' <summary> Opens the port using specified port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
    '''                               parameters keyed by the
    '''                               <see cref="PortParameterKey">parameter key</see> </param>
    ''' <param name="e">              Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryOpen(ByVal portParameters As PortParametersDictionary, ByVal e As Core.ActionEventArgs) As Boolean Implements IPort.TryOpen
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If portParameters Is Nothing Then Throw New ArgumentNullException(NameOf(portParameters))
        Dim activity As String = String.Empty
        Dim wasOpen As Boolean = Me.IsOpen
        Try
            activity = $"setting {portParameters.PortName} port parameters"
            Me.Publish(TraceEventType.Verbose, activity)
            Me.FromPortParameters(portParameters)
            ' open and check device if is available?
            activity = $"opening {Me.SerialPort.PortName}..."
            Me.Publish(TraceEventType.Verbose, activity)
            Me.SerialPort.Open()
            If Me.IsOpen Then
                Me.Publish(TraceEventType.Verbose, $"done {activity}")
                Me.PortParameters.Populate(Me.SerialPort, portParameters.ReceiveDelay)
            Else
                Me.Publish(TraceEventType.Warning, $"failed {activity}; not open")
                e.RegisterOutcomeEvent(TraceEventType.Warning, $"failed {activity}; not open")
            End If
        Catch ex As Exception
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            ' if port was closed and is now open
            If wasOpen <> Me.IsOpen Then Me.TryNotifyConnectionChanged()
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> C;pses the port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryClose(ByVal e As Core.ActionEventArgs) As Boolean Implements IPort.TryClose
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Dim isOpen As Boolean = Me.IsOpen
        Try
            activity = $"closing {Me.SerialPort.PortName}..."
            Me.Publish(TraceEventType.Verbose, activity)
            activity = $"discarding {Me.SerialPort.PortName} input buffer"
            Me.SerialPort.DiscardInBuffer()
            activity = $"closing {Me.SerialPort.PortName}"
            Me.SerialPort.Close()
            If Me.IsOpen Then
                Me.Publish(TraceEventType.Warning, $"failed {activity}; still open")
                e.RegisterOutcomeEvent(TraceEventType.Warning, $"failed {activity}; still open")
            Else
                Me.Publish(TraceEventType.Verbose, $"done {activity}")
            End If
        Catch ex As Exception
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            If isOpen <> Me.IsOpen Then Me.TryNotifyConnectionChanged()
        End Try
        Return Not e.Failed
    End Function

#End Region

#Region " BUFFERS "

    ''' <summary> Gets the received bytes. </summary>
    ''' <value> The received bytes. </value>
    Private ReadOnly Property ReceivedBytes As List(Of Byte)

    ''' <summary> Holds the received values. </summary>
    ''' <value> The received values. </value>
    Public ReadOnly Property ReceivedValues() As IEnumerable(Of Byte) Implements IPort.ReceivedValues
        Get
            Return If(Me.ReceivedBytes Is Nothing, Array.Empty(Of Byte)(), Me.ReceivedBytes.ToArray)
        End Get
    End Property

    ''' <summary> Gets the number of bytes received. </summary>
    ''' <value> The number of bytes received. </value>
    Public ReadOnly Property ReceivedBytesCount As Integer
        Get
            Return Me.ReceivedBytes.Count
        End Get
    End Property

    ''' <summary> Discard input buffers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub DiscardInputBuffers()
        Me.ReceivedBytes.Clear()
        Me.SerialPort?.DiscardInBuffer()
    End Sub

    ''' <summary> Attempts to wait for the receive count from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="count">      Number of. </param>
    ''' <param name="trialCount"> Number of trials. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryWaitReceiveCount(ByVal count As Integer, ByVal trialCount As Integer) As Boolean Implements IPort.TryWaitReceiveCount
        ' wait for the data to come in: 
        Dim minimumTransitTime As TimeSpan = Me.PortParameters.MinimumTransitTimespan(count)
        ' timeout after number of trial of transit time
        Dim timeout As TimeSpan = Me.PortParameters.MinimumTransitTimespan(trialCount * count)
        Return Me.TryWaitReceiveCount(count, minimumTransitTime, timeout)
    End Function

    ''' <summary> Attempts to wait for the receive count from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="count">        Number of. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="timeout">      The timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryWaitReceiveCount(ByVal count As Integer, ByVal pollInterval As TimeSpan, ByVal timeout As TimeSpan) As Boolean Implements IPort.TryWaitReceiveCount
        Dim sw As Stopwatch = Stopwatch.StartNew
        isr.Core.ApplianceBase.DoEventsWaitUntil(pollInterval, timeout, Function() Me.ReceivedBytesCount >= count)
        ' failed if timeout before reaching the desired count
        Return Me.ReceivedBytesCount < count
    End Function

    ''' <summary> Returns the encoded string from the byte values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> A String. </returns>
    Public Function Decode(ByVal values As IEnumerable(Of Byte)) As String

        Return Port.Decode(values, Me.SerialPort.Encoding)
    End Function

    ''' <summary> Returns the encoded string from the byte values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values">   The values. </param>
    ''' <param name="encoding"> The encoding. </param>
    ''' <returns> A String. </returns>
    Public Shared Function Decode(ByVal values As IEnumerable(Of Byte), ByVal encoding As Text.Encoding) As String
        If encoding Is Nothing Then Throw New ArgumentNullException(NameOf(encoding))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Return If(values.Any, encoding.GetString(values.ToArray, 0, values.Count), String.Empty)
    End Function

    ''' <summary> Gets the transmitted bytes. </summary>
    ''' <value> The transmitted bytes. </value>
    Private ReadOnly Property TransmittedBytes As List(Of Byte)

    ''' <summary> Holds the transmitted values. </summary>
    ''' <value> The transmitted values. </value>
    Public ReadOnly Property TransmittedValues() As IEnumerable(Of Byte) Implements IPort.TransmittedValues
        Get
            Return If(Me.TransmittedBytes Is Nothing, Array.Empty(Of Byte)(), Me.TransmittedBytes.ToArray)
        End Get
    End Property

    ''' <summary> Discard output buffers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub DiscardOutputBuffers()
        Me.TransmittedBytes.Clear()
        Me.SerialPort?.DiscardOutBuffer()
    End Sub

    ''' <summary> Resynchronizes the circular buffer or clears the receive buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Resync() Implements IPort.Resync
        If Me.InputBufferingOption = DataBufferingOption.CircularBuffer Then
            Me.ReceiveBuffer.Resync()
        Else
            Me.SerialPort.ReadExisting()
        End If
    End Sub

#End Region

#Region " CIRCULAR BUFFER "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ReceiveBuffer As CircularCollection(Of Byte)
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the buffer for receive data. </summary>
    ''' <value> A Buffer for receive data. </value>
    Public ReadOnly Property ReceiveBuffer As CircularCollection(Of Byte)
        Get
            Return Me._ReceiveBuffer
        End Get
    End Property

    ''' <summary> Returns the data count in the circular buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function DataCount() As Integer Implements IPort.DataCount
        Return Me.ReceiveBuffer.Count
    End Function

    ''' <summary> Reads the next byte from the circular buffer. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The next. </returns>
    Public Function ReadNext() As Byte Implements IPort.ReadNext
        Return Me.ReceiveBuffer.Dequeue
    End Function

    ''' <summary> Notify of buffer overflow. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Buffer_WaterMarkNotify(sender As Object, e As System.EventArgs) Handles _ReceiveBuffer.WatermarkNotify
    End Sub

    ''' <summary> The input buffering option. </summary>
    Private _InputBufferingOption As DataBufferingOption

    ''' <summary> Gets or sets the data buffering option. </summary>
    ''' <value> The data buffering option. </value>
    Public Property InputBufferingOption As DataBufferingOption Implements IPort.InputBufferingOption
        Get

            Return Me._InputBufferingOption
        End Get
        Set(value As DataBufferingOption)
            Me._InputBufferingOption = value
            If value = DataBufferingOption.CircularBuffer AndAlso (Me._ReceiveBuffer Is Nothing OrElse 0 > Me._ReceiveBuffer.Capacity) Then
                Me._ReceiveBuffer = New CircularCollection(Of Byte)(1024)
                Me.ReceivedBytes.Clear()
            End If
        End Set
    End Property

#End Region

#Region " SERIAL PORT EVENTS "

    ''' <summary> The send lock. </summary>
    Private _SendLock As Object

    ''' <summary> Sends data. </summary>
    ''' <remarks> Clears the input buffer. </remarks>
    ''' <param name="data"> byte array data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub SendData(ByVal data As IEnumerable(Of Byte)) Implements IPort.SendData
        If Me._SendLock Is Nothing Then Me._SendLock = New Object
        Dim activity As String = String.Empty
        Dim portEventArgs As PortEventArgs = Nothing
        Try
            activity = "setting the send lock"
            SyncLock Me._SendLock
                activity = "clearing input buffers"
                ' clear the input buffers in preparation from the returned message.
                Me.DiscardInputBuffers()
                ' clear the output buffers.
                activity = "clearing output buffers"
                Me.DiscardOutputBuffers()
                If data?.Any Then
                    activity = "setting output buffer to new data"
                    Me.TransmittedBytes.AddRange(data)
                    activity = $"{Me.SerialPort.PortName} sending {Me.TransmittedValues.Count} byes; {Me.TransmittedValues.ToHex(0, 10)}... "
                    Me.Publish(TraceEventType.Verbose, activity)
                    Me.SerialPort.Write(Me.TransmittedValues.ToArray, 0, Me.TransmittedValues.Count)
                    activity = $"{Me.SerialPort.PortName} sent {Me.TransmittedValues.Count} byes"
                    Me.Publish(TraceEventType.Information, activity)
                    activity = "setting new port event arguments"
                    portEventArgs = New PortEventArgs(True, Me.TransmittedValues)
                Else
                    activity = "attempt at sending data ignored -- nothing to send"
                    Me.Publish(TraceEventType.Information, activity)
                    activity = "setting new port event arguments"
                    portEventArgs = New PortEventArgs(False, Me.TransmittedValues)
                End If
            End SyncLock
        Catch ex As Exception
            portEventArgs = New PortEventArgs(False, Me.TransmittedValues)
            Me.PublishException(activity, ex)
        Finally
            Me.TryNotifyDataSent(portEventArgs)
        End Try

    End Sub

    ''' <summary> Sends a hexadecimal data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="port"> The port. </param>
    ''' <param name="data"> byte array data. </param>
    Public Shared Sub SendHexData(ByVal port As IPort, ByVal data As String)
        If port Is Nothing OrElse String.IsNullOrWhiteSpace(data) Then Return
        If Not String.IsNullOrWhiteSpace(data) Then
            Dim bytes As IEnumerable(Of Byte) = data.ToHexBytes
            If bytes(0) = 255 Then Throw New OperationFailedException($"failed converting '{data}'; '{Chr(bytes(1))}' is not a valid HEX character")
            port.SendData(bytes)
        End If
    End Sub

    ''' <summary> Sends a hexadecimal data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data"> byte array data. </param>
    Public Sub SendHexData(ByVal data As String)
        Port.SendHexData(Me, data)
    End Sub

    ''' <summary> Sends an ASCII data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="port"> The port. </param>
    ''' <param name="data"> byte array data. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process send ASCII data in this collection.
    ''' </returns>
    Public Shared Function SendAsciiData(ByVal port As IPort, ByVal data As String) As IEnumerable(Of Byte)
        Dim bytes As IEnumerable(Of Byte) = Array.Empty(Of Byte)()
        If port Is Nothing OrElse String.IsNullOrWhiteSpace(data) Then Return bytes
        If Not String.IsNullOrWhiteSpace(data) Then
            bytes = Text.Encoding.ASCII.GetBytes(data)
            port.SendData(bytes)
        End If
        Return bytes
    End Function

    ''' <summary> Sends an ASCII data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data"> byte array data. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process send ASCII data in this collection.
    ''' </returns>
    Public Function SendAsciiData(ByVal data As String) As IEnumerable(Of Byte)
        Return Port.SendAsciiData(Me, data)
    End Function

    ''' <summary> Try receive data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="serialPort"> The <see cref="SerialPort">serial port.</see> </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryReceiveData(ByVal serialPort As SerialPort)
        Dim activity As String = String.Empty
        Dim portEventArgs As PortEventArgs = Nothing
        Try
            If serialPort.IsOpen Then
                activity = $"awaiting {serialPort.PortName} receive delay {Me.PortParameters.ReceiveDelay} ms"
                isr.Core.ApplianceBase.Delay(Me.PortParameters.ReceiveDelay)
                Dim len As Integer = serialPort.BytesToRead
                If len > 0 Then
                    activity = $"reading {len} bytes from {serialPort.PortName}..."
                    Me.Publish(TraceEventType.Verbose, activity)
                    Dim values As Byte() = Array.Empty(Of Byte)()
                    Array.Resize(values, len)
                    serialPort.Read(values, 0, len)
                    Me.ReceivedBytes.AddRange(values)
                    activity = $"received {values.Length} bytes {values.ToHex(0, 10)}... from {serialPort.PortName}"
                    Me.Publish(TraceEventType.Information, activity)
                    If Me.MessageParser IsNot Nothing AndAlso Me.MessageParser.ParseEnabled Then
                        ' if the parser is defined, use it to parse the message. 
                        Dim outcome As MessageParserOutcome = Me.MessageParser.Parse(Me.ReceivedValues)
                        If outcome = MessageParserOutcome.Complete Then
                            portEventArgs = New PortEventArgs(True, Me.ReceivedValues)
                            activity = $"{serialPort.PortName} message completed"
                            Me.Publish(TraceEventType.Information, activity)
                        ElseIf outcome = MessageParserOutcome.Invalid Then
                            portEventArgs = New PortEventArgs(False, Me.ReceivedValues)
                            activity = $"{serialPort.PortName} failed parsing the input message -- message is invalid"
                            Me.Publish(TraceEventType.Warning, activity)
                        Else
                            ' if not complete, wait for more characters.
                            activity = $"{serialPort.PortName} message incomplete; awaiting more characters"
                            Me.Publish(TraceEventType.Information, activity)
                        End If
                    Else
                        activity = $"{serialPort.PortName} message received"
                        Me.Publish(TraceEventType.Verbose, activity)
                        portEventArgs = New PortEventArgs(True, Me.ReceivedValues)
                    End If
                End If
            Else
                activity = $"{serialPort.PortName} data received event aborted; port no longer open"
                Me.Publish(TraceEventType.Warning, activity)
                portEventArgs = New PortEventArgs(False, Me.ReceivedValues)
            End If
        Catch ex As Exception
            portEventArgs = New PortEventArgs(False, Me.ReceivedValues)
            Me.PublishException(activity, ex)
        Finally
            If portEventArgs IsNot Nothing Then Me.TryNotifyDataReceived(portEventArgs)
        End Try

    End Sub

    ''' <summary> Handles the DataReceived event of the _serialPort control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.IO.Ports.SerialDataReceivedEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub SerialPort_DataReceived(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles _SerialPort.DataReceived
        Dim port As SerialPort = TryCast(sender, SerialPort)
        If port IsNot Nothing AndAlso e IsNot Nothing Then
            Me.Publish(TraceEventType.Verbose, $"{e.EventType} received")
            Me.TryReceiveData(port)
        End If
    End Sub

#End Region


#Region " MESSAGE PARSER "

    ''' <summary> Gets or sets the message parser. </summary>
    ''' <value> The message parser. </value>
    Public Property MessageParser As IMessageParser Implements IPort.MessageParser

#End Region

#Region " EVENT MANAGEMENT "

#Region " SERIAL PORT ERROR RECEIVED "

    ''' <summary> Raises the serial port error received event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub NotifySerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        Me.SyncNotifySerialPortErrorReceived(e)
    End Sub

    ''' <summary>
    ''' Removes the  <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveSerialPortErrorReceivedEventHandlers()
        Me._SerialPortErrorReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="SerialPortErrorReceived">Serial port error receivd </see>> event handlers. </summary>
    Private ReadOnly _SerialPortErrorReceivedEventHandlers As New EventHandlerContextCollection(Of SerialErrorReceivedEventArgs)

    ''' <summary> Event queue for all listeners interested in
    ''' <see cref="SerialPortErrorReceived">Serial port error receivd </see>> events.
    ''' Serial Port Error Received status is reported with the
    ''' <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
    ''' </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SerialPortErrorReceived As EventHandler(Of SerialErrorReceivedEventArgs) Implements IPort.SerialPortErrorReceived
        AddHandler(value As EventHandler(Of SerialErrorReceivedEventArgs))
            Me._SerialPortErrorReceivedEventHandlers.Add(New EventHandlerContext(Of SerialErrorReceivedEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of SerialErrorReceivedEventArgs))
            Me._SerialPortErrorReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As SerialErrorReceivedEventArgs)
            Me._SerialPortErrorReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="SerialPortErrorReceived">Serial Port Error Received Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="SerialErrorReceivedEventArgs"/> instance containing the event
    '''                  data. </param>
    Protected Sub SyncNotifySerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        Me._SerialPortErrorReceivedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Handles the Error Received event of the _serialPort control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="SerialErrorReceivedEventArgs" /> instance containing the
    '''                       event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SerialPort_ErrorReceived(sender As Object, e As System.IO.Ports.SerialErrorReceivedEventArgs) Handles _SerialPort.ErrorReceived
        Dim activity As String = String.Empty
        Try
            Dim port As SerialPort = TryCast(sender, SerialPort)
            If port IsNot Nothing Then
                activity = $"notifying {port.PortName} {NameOf(Me.SerialPort.ErrorReceived)} event"
                Me.NotifySerialPortErrorReceived(e)
            End If

        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SERIAL PORT DISPOSED "

    ''' <summary>
    ''' Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    Private Sub NotifySerialPortDisposed(ByVal e As EventArgs)
        Me.SyncNotifySerialPortDisposed(e)
    End Sub

    ''' <summary>
    ''' Removes the <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveSerialPortDisposedEventHandlers()
        Me._SerialPortDisposedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers. </summary>
    Private ReadOnly _SerialPortDisposedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port Disposed</see> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SerialPortDisposed As EventHandler(Of System.EventArgs) Implements IPort.SerialPortDisposed
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._SerialPortDisposedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._SerialPortDisposedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._SerialPortDisposedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="SerialPortDisposed">Serial Port Disposed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifySerialPortDisposed(ByVal e As System.EventArgs)
        Me._SerialPortDisposedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Try notify serial port disposed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifySerialPortDisposed(ByVal e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = "notifying serial port disposed"
            Me.Publish(TraceEventType.Information, activity)
            Me.NotifySerialPortDisposed(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Serial port disposed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SerialPort_Disposed(sender As Object, e As System.EventArgs) Handles _SerialPort.Disposed
        Me.TryNotifySerialPortDisposed(e)
    End Sub

#End Region

#Region " CONNECTION CHANGED "

    ''' <summary> Raises the connection event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub NotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Dim activity As String = $"notifying port {Me.SerialPort?.PortName} {If(Me.IsOpen, "open", "closed")}"
        Me.Publish(TraceEventType.Information, activity)
        Me.SyncNotifyConnectionChanged(e)
    End Sub

    ''' <summary> Removes the Connection Changed event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveConnectionChangedEventHandlers()
        Me._ConnectionChangedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Connection Changed event handlers. </summary>
    Private ReadOnly _ConnectionChangedEventHandlers As New EventHandlerContextCollection(Of ConnectionEventArgs)

    ''' <summary> Event queue for all listeners interested in Connection Changed events. 
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ConnectionChanged As EventHandler(Of ConnectionEventArgs) Implements IPort.ConnectionChanged
        AddHandler(value As EventHandler(Of ConnectionEventArgs))
            Me._ConnectionChangedEventHandlers.Add(New EventHandlerContext(Of ConnectionEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ConnectionEventArgs))
            Me._ConnectionChangedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ConnectionEventArgs)
            Me._ConnectionChangedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="ConnectionChanged">Connection Changed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ConnectionEventArgs"/> instance containing the event
    '''                  Connection. </param>
    Protected Sub SyncNotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Me._ConnectionChangedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Raises the connection event within a try catch clause. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"notifying port {Me.SerialPort?.PortName} {If(e.IsPortOpen, "open", "closed")}"
            Me.NotifyConnectionChanged(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Safely Invokes the <see cref="ConnectionChanged">connection changed event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub TryNotifyConnectionChanged()
        Me.TryNotifyConnectionChanged(New ConnectionEventArgs(Me.IsOpen))
    End Sub

#End Region

#Region " DATA RECEIVED "

    ''' <summary> Notifies a data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Private Sub NotifyDataReceived(ByVal e As PortEventArgs)
        Dim activity As String = $"notifying {Me.SerialPort?.PortName} data received"
        Me.Publish(TraceEventType.Verbose, activity)
        If Me._TimeoutTimer IsNot Nothing Then Me._TimeoutTimer.Enabled = False
        Me.SyncNotifyDataReceived(e)
    End Sub

    ''' <summary>
    ''' Removes the <see cref="DataReceived">data received</see>/&gt; event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveDataReceivedEventHandlers()
        Me._DataReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="DataReceived">data received</see>/> event handlers. </summary>
    Private ReadOnly _DataReceivedEventHandlers As New EventHandlerContextCollection(Of PortEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DataReceived">data received</see>/> events
    ''' Receipt status is reported along with the received data in the receive buffer
    ''' using the <see cref="PortEventArgs">port event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DataReceived As EventHandler(Of PortEventArgs) Implements IPort.DataReceived
        AddHandler(value As EventHandler(Of PortEventArgs))
            Me._DataReceivedEventHandlers.Add(New EventHandlerContext(Of PortEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of PortEventArgs))
            Me._DataReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As PortEventArgs)
            Me._DataReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DataReceived">Data Received Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
    Protected Sub SyncNotifyDataReceived(ByVal e As PortEventArgs)
        Me._DataReceivedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Try notify data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifyDataReceived(ByVal e As PortEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"notifying {Me.SerialPort?.PortName} data received"
            Me.NotifyDataReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DATA SENT "

    ''' <summary> Notifies a data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Private Sub NotifyDataSent(ByVal e As PortEventArgs)
        If e.StatusOkay AndAlso Me.SerialPort.ReadTimeout > 0 Then
            Me._TimeoutTimer.AutoReset = False
            Me._TimeoutTimer.Interval = Me.SerialPort.ReadTimeout
            Me._TimeoutTimer.Enabled = True
        End If
        Dim activity As String = $"notifying {Me.SerialPort?.PortName} data sent"
        Me.Publish(TraceEventType.Verbose, activity)
        Me.SyncNotifyDataSent(e)
    End Sub

    ''' <summary> Removes the <see cref="DataSent">Data Sent event</see> handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveDataSentEventHandlers()
        Me._DataSentEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="DataSent">Data Sent event</see> handlers. </summary>
    Private ReadOnly _DataSentEventHandlers As New EventHandlerContextCollection(Of PortEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
    ''' Receipt status is reported along with the Sent data in the receive buffer
    ''' using the <see cref="PortEventArgs">port event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DataSent As EventHandler(Of PortEventArgs) Implements IPort.DataSent
        AddHandler(value As EventHandler(Of PortEventArgs))
            Me._DataSentEventHandlers.Add(New EventHandlerContext(Of PortEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of PortEventArgs))
            Me._DataSentEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As PortEventArgs)
            Me._DataSentEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DataSent">Data Sent Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
    Protected Sub SyncNotifyDataSent(ByVal e As PortEventArgs)
        Me._DataSentEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Try notify data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifyDataSent(ByVal e As PortEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"notifying {Me.SerialPort?.PortName} data sent"
            Me.NotifyDataSent(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#End Region

#Region " TIMEOUT MANAGER "

    ''' <summary> Notifies a timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    Private Sub NotifyTimeout(ByVal e As EventArgs)
        Me.SyncNotifyTimeout(e)
    End Sub

    ''' <summary> Removes the <see cref="Timeout"/> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveTimeoutEventHandlers()
        Me._TimeoutEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Timeout"/> event handlers. </summary>
    Private ReadOnly _TimeoutEventHandlers As New EventHandlerContextCollection(Of EventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Timeout As EventHandler(Of EventArgs) Implements IPort.Timeout
        AddHandler(value As EventHandler(Of EventArgs))
            Me._TimeoutEventHandlers.Add(New EventHandlerContext(Of EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of EventArgs))
            Me._TimeoutEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As EventArgs)
            Me._TimeoutEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Timeout">Timeout Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyTimeout(ByVal e As EventArgs)
        Me._TimeoutEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Try notify data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifyTimeout(ByVal e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"notifying {Me.SerialPort.PortName} timeout"
            Me.Publish(TraceEventType.Verbose, activity)
            Me.NotifyTimeout(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Handles the timeout management
    ''' </summary>
    Private WithEvents _TimeoutTimer As Timers.Timer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Handles the Elapsed event of the _timeoutTimer control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Timers.ElapsedEventArgs" /> instance containing the
    '''                       event data. </param>
    Private Sub TimeoutTimer_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles _TimeoutTimer.Elapsed
        Me.TryNotifyTimeout(EventArgs.Empty)
    End Sub

#End Region


#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("Serial Port Settings Editor", isr.Ports.Serial.My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ApplySettings() Implements IPort.ApplySettings
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"{propertyName} changed to {sender.TraceShowLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)


        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

