Imports System.Runtime.Serialization

''' <summary> Reports index exception for the Circular buffer. </summary>
''' <remarks>
''' Based on O/R mapping exception template. <para>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<Serializable()> Public Class IndexRangeException
    Inherits isr.Core.ExceptionBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New("Index exception occurred")
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message"> Specifies the exception message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>
    ''' and <paramref name="innerException"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message">        Specifies the exception message. </param>
    ''' <param name="innerException"> Specifies the InnerException. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and
    ''' <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="info">    Specifies <see cref="SerializationInfo">serialization
    '''                        information</see>. </param>
    ''' <param name="context"> Specifies <see cref="StreamingContext">streaming context</see> for
    '''                        the exception. </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class

