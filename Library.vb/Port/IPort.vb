Imports System.Collections.Generic

''' <summary> Defines the interface for the <see cref="Port">serial port</see> </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Interface IPort
    Inherits IDisposable, ComponentModel.INotifyPropertyChanged, Core.ITalker

#Region " PORT PARAMETERS "

    ''' <summary> Gets or sets the port parameters. </summary>
    ''' <value> The port properties. </value>
    ReadOnly Property PortParameters As PortParametersDictionary

    ''' <summary> Returns the port parameters. </summary>
    ''' <returns>
    ''' A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
    ''' <see cref="PortParameterKey">parameter key</see>
    ''' Includes, in addition to the standard parameters, the Threshold count, Delay time in
    ''' milliseconds and timeout in milliseconds.
    ''' </returns>
    Function ToPortParameters() As PortParametersDictionary

    ''' <summary> Assign port parameters to the port. </summary>
    ''' <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
    '''                               parameters keyed by the
    '''                               <see cref="PortParameterKey">parameter key</see> </param>
    Sub FromPortParameters(ByVal portParameters As PortParametersDictionary)

    ''' <summary> Gets or sets the filename of the port parameters file. </summary>
    ''' <value> The filename of the port parameters file. </value>
    Property PortParametersFileName As String

    ''' <summary> Attempts to store port parameters from the given data. </summary>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Function TryStorePortParameters(ByVal e As Core.ActionEventArgs) As Boolean

    ''' <summary> Attempts to restore port parameters from the given data. </summary>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Function TryRestorePortParameters(ByVal e As Core.ActionEventArgs) As Boolean

    ''' <summary> Gets or sets the supported baud rates. </summary>
    ''' <value> The supported baud rates. </value>
    ReadOnly Property SupportedBaudRates As IEnumerable(Of Integer)

#End Region

#Region " PORT OPEN STATUS "

    ''' <summary> Gets or sets the is open. </summary>
    ''' <value> The is open. </value>
    ReadOnly Property IsOpen As Boolean

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Gets or sets the <see cref="System.IO.Ports.SerialPort">Serial Port</see>. </summary>
    ''' <value> The serial port. </value>
    Property SerialPort As System.IO.Ports.SerialPort

    ''' <summary> Opens the port using specified port parameters. </summary>
    ''' <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
    '''                               parameters keyed by the
    '''                               <see cref="PortParameterKey">parameter key</see> </param>
    ''' <param name="e">              Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    Function TryOpen(ByVal portParameters As PortParametersDictionary, ByVal e As Core.ActionEventArgs) As Boolean

    ''' <summary> Opens the port using current port parameters. </summary>
    ''' <param name="portName"> Name of the port. </param>
    ''' <param name="e">        Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    Function TryOpen(ByVal portName As String, ByVal e As Core.ActionEventArgs) As Boolean

    ''' <summary> Opens the port using current port parameters. </summary>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    Function TryOpen(ByVal e As Core.ActionEventArgs) As Boolean

    ''' <summary> Closes the port. </summary>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> True if success; otherwise, false. </returns>
    Function TryClose(ByVal e As Core.ActionEventArgs) As Boolean

#End Region

#Region " DATA MANAGEMENT "

    ''' <summary> Returns the data count in the circular buffer. </summary>
    ''' <returns> An Integer. </returns>
    Function DataCount() As Integer

    ''' <summary> Reads the next byte from the circular buffer. </summary>
    ''' <returns> The next. </returns>
    Function ReadNext() As Byte

    ''' <summary> Resynchronizes the circular buffer. </summary>
    Sub Resync()

    ''' <summary> Sends data. </summary>
    ''' <param name="data"> byte array data. </param>
    Sub SendData(ByVal data As IEnumerable(Of Byte))

    ''' <summary> Gets or sets the input (receive) data buffering option. </summary>
    ''' <value> The data buffering option. </value>
    Property InputBufferingOption As DataBufferingOption

    ''' <summary> Gets or sets the received values. </summary>
    ''' <value> The received values. </value>
    ReadOnly Property ReceivedValues() As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the transmitted values. </summary>
    ''' <value> The transmitted values. </value>
    ReadOnly Property TransmittedValues() As IEnumerable(Of Byte)

    ''' <summary> Attempts to wait for the receive count from the given data. </summary>
    ''' <param name="count">      Number of. </param>
    ''' <param name="trialCount"> Number of trials. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Function TryWaitReceiveCount(ByVal count As Integer, ByVal trialCount As Integer) As Boolean

    ''' <summary> Attempts to wait for the receive count from the given data. </summary>
    ''' <param name="count">        Number of. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="timeout">      The timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Function TryWaitReceiveCount(ByVal count As Integer, ByVal pollInterval As TimeSpan, ByVal timeout As TimeSpan) As Boolean

#End Region

#Region " MESSAGE PARSER "

    ''' <summary> Gets or sets the message parser. </summary>
    ''' <value> The message parser. </value>
    Property MessageParser As IMessageParser

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Occurs when connection changed.
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
    ''' </summary>
    Event ConnectionChanged As EventHandler(Of ConnectionEventArgs)

    ''' <summary>
    ''' Occurs when data is received.
    ''' Reception status is report along with the received data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataReceived As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when data is Sent.
    ''' Reception status is report along with the Sent data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataSent As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when Serial Port Error Received.
    ''' SerialPortErrorReceived status is reported with the 
    ''' <see cref="System.IO.Ports.SerialErrorReceivedEventArgs">SerialPortErrorReceived event arguments.</see>
    ''' </summary>
    Event SerialPortErrorReceived As EventHandler(Of System.IO.Ports.SerialErrorReceivedEventArgs)

    ''' <summary> Event queue for all listeners interested in SerialPortDisposed events. </summary>
    Event SerialPortDisposed As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Occurs when timeout.
    ''' </summary>
    Event Timeout As EventHandler(Of System.EventArgs)

#End Region

#Region " MY SETTINGS "

    ''' <summary> Applies the settings. </summary>
    Sub ApplySettings()

#End Region

End Interface

