Imports System.IO
Imports System.IO.Ports
Imports System.Runtime.Serialization

''' <summary> Implements a key,value pair for storing the Port Parameters. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-06-16 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly")>
<Serializable()>
Public Class PortParametersDictionary
    Inherits Collections.Generic.Dictionary(Of PortParameterKey, String)
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
    ''' <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
    Public Sub New(ByVal serialPort As SerialPort, ByVal receiveDelay As Integer)
        Me.New
        If serialPort IsNot Nothing Then Me.PopulateThis(serialPort, receiveDelay)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="parameters"> Options for controlling the operation. </param>
    Public Sub New(ByVal parameters As PortParametersDictionary)
        Me.New
        If parameters IsNot Nothing Then Me.PopulateThis(parameters)
    End Sub

    ''' <summary> Creates the default. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The new default. </returns>
    Public Shared Function CreateDefault() As PortParametersDictionary
        Dim result As PortParametersDictionary = Nothing
        Try
            result = New PortParametersDictionary From {
                {PortParameterKey.PortName, "COM1"},
                {PortParameterKey.BaudRate, 9600.ToString},
                {PortParameterKey.DataBits, 8.ToString},
                {PortParameterKey.Parity, IO.Ports.Parity.None.ToString},
                {PortParameterKey.StopBits, IO.Ports.StopBits.One.ToString},
                {PortParameterKey.ReceivedBytesThreshold, 1.ToString},
                {PortParameterKey.ReceiveDelay, 1.ToString},
                {PortParameterKey.Handshake, Handshake.None.ToString},
                {PortParameterKey.RtsEnable, False.ToString},
                {PortParameterKey.ReadTimeout, 2000.ToString},
                {PortParameterKey.ReadBufferSize, 4096.ToString},
                {PortParameterKey.WriteBufferSize, 2048.ToString}
            }
        Catch ex As Exception
            If result IsNot Nothing Then result.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Public Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemovePropertyChangedEventHandlers()
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="info">    The info. </param>
    ''' <param name="context"> The context. </param>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

#Region " DICTIONARY MANAGER "

    ''' <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="parameters"> Options for controlling the operation. </param>
    Private Sub PopulateThis(ByVal parameters As PortParametersDictionary)
        If parameters Is Nothing Then Throw New ArgumentNullException(NameOf(parameters))
        For Each key As PortParameterKey In parameters.Keys
            Me.Replace(key, parameters.Item(key))
        Next
    End Sub

    ''' <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="parameters"> Options for controlling the operation. </param>
    Public Sub Populate(ByVal parameters As PortParametersDictionary)
        Me.PopulateThis(parameters)

    End Sub

    ''' <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
    ''' <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
    Private Sub PopulateThis(ByVal serialPort As SerialPort, ByVal receiveDelay As Integer)
        Me.Replace(PortParameterKey.PortName, serialPort.PortName)
        Me.Replace(PortParameterKey.BaudRate, serialPort.BaudRate.ToString)
        Me.Replace(PortParameterKey.DataBits, serialPort.DataBits.ToString)
        Me.Replace(PortParameterKey.Parity, serialPort.Parity.ToString)
        Me.Replace(PortParameterKey.StopBits, serialPort.StopBits.ToString)
        Me.Replace(PortParameterKey.ReceivedBytesThreshold, serialPort.ReceivedBytesThreshold.ToString)
        Me.Replace(PortParameterKey.Handshake, serialPort.Handshake.ToString)
        Me.Replace(PortParameterKey.RtsEnable, serialPort.RtsEnable.ToString)

        Me.Replace(PortParameterKey.ReadTimeout, serialPort.ReadTimeout.ToString)
        Me.Replace(PortParameterKey.ReadBufferSize, serialPort.ReadBufferSize.ToString)
        Me.Replace(PortParameterKey.WriteBufferSize, serialPort.WriteBufferSize.ToString)
        Me.Replace(PortParameterKey.ReceiveDelay, receiveDelay.ToString)
    End Sub

    ''' <summary> Populates the port parameters from the <see cref="SerialPort"/>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
    ''' <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
    Public Sub Populate(ByVal serialPort As SerialPort, ByVal receiveDelay As Integer)
        If serialPort Is Nothing Then Throw New ArgumentNullException(NameOf(serialPort))
        Me.PopulateThis(serialPort, receiveDelay)
    End Sub

    ''' <summary> Returns the port parameters from the port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="serialPort">   The <see cref="SerialPort">serial port.</see> </param>
    ''' <param name="receiveDelay"> Gets or sets the time in ms the Data Received handler waits. </param>
    ''' <returns>
    ''' A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
    ''' <see cref="PortParameterKey">parameter key</see>
    ''' </returns>
    Public Shared Function ToPortParameters(ByVal serialPort As SerialPort, ByVal receiveDelay As Integer) As PortParametersDictionary
        Return If(serialPort Is Nothing,
            New PortParametersDictionary(PortParametersDictionary.DefaultPortParameters),
            New PortParametersDictionary(serialPort, receiveDelay))
    End Function

    ''' <summary> Assigns port parameters to the port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="serialPort">     The <see cref="SerialPort">serial port.</see> </param>
    ''' <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
    '''                               parameters keyed by the
    '''                               <see cref="PortParameterKey">parameter key</see> </param>
    Public Shared Sub FromPortParameters(ByVal serialPort As SerialPort, ByVal portParameters As PortParametersDictionary)
        If serialPort IsNot Nothing AndAlso portParameters IsNot Nothing Then
            serialPort.PortName = portParameters.PortName
            serialPort.BaudRate = portParameters.BaudRate
            serialPort.Parity = portParameters.Parity
            serialPort.DataBits = portParameters.DataBits
            serialPort.StopBits = portParameters.StopBits
            serialPort.ReceivedBytesThreshold = portParameters.ReceivedBytesThreshold
            serialPort.Handshake = portParameters.Handshake
            serialPort.RtsEnable = portParameters.RtsEnable
            ' 5000ms will never be reached because we read only bytes present in read buffer
            serialPort.ReadTimeout = portParameters.ReadTimeout
            serialPort.ReadBufferSize = portParameters.ReadBufferSize
            serialPort.WriteBufferSize = portParameters.WriteBufferSize
        End If
    End Sub

    ''' <summary> The default port parameters. </summary>
    Private Shared _DefaultPortParameters As PortParametersDictionary

    ''' <summary> Returns the default port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
    ''' <see cref="PortParameterKey">parameter key</see>
    ''' </returns>
    Public Shared Function DefaultPortParameters() As PortParametersDictionary
        If PortParametersDictionary._DefaultPortParameters Is Nothing Then
            Try
                PortParametersDictionary._DefaultPortParameters = PortParametersDictionary.CreateDefault
            Catch ex As Exception
                If PortParametersDictionary._DefaultPortParameters IsNot Nothing Then PortParametersDictionary._DefaultPortParameters.Dispose()
                PortParametersDictionary._DefaultPortParameters = Nothing
                Throw
            End Try
        End If
        Return PortParametersDictionary._DefaultPortParameters
    End Function

    ''' <summary> Replaces the specified key. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="key">   The key. </param>
    ''' <param name="value"> The value. </param>
    Public Sub Replace(ByVal key As PortParameterKey, ByVal value As String)
        If Me.ContainsKey(key) Then
            If Not Me.Item(key).Equals(value, StringComparison.OrdinalIgnoreCase) Then
                Me.Remove(key)
                Me.Add(key, value)
            End If
        Else
            Me.Add(key, value)
        End If
        Me.NotifyPropertyChanged(key.ToString)
    End Sub

#End Region

#Region " FILE STORAGE "

    ''' <summary> Restores the collection from the specified file name. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="fileName"> Name of the file. </param>
    Public Sub Restore(ByVal fileName As String)
        Using sr As New StreamReader(fileName)
            Do Until sr.EndOfStream
                Dim values() As String = sr.ReadLine().Split(","c)
                If values.Length = 2 Then
                    Dim key As PortParameterKey
                    If [Enum].TryParse(Of PortParameterKey)(values(0), key) Then
                        Me.Replace(key, values(1))
                    End If
                End If
            Loop
        End Using
    End Sub

    ''' <summary> Store port parameters. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="fileName"> Name of the file. </param>
    Public Sub Store(ByVal fileName As String)
        Using sw As New StreamWriter(fileName)
            For Each key As PortParameterKey In [Enum].GetValues(GetType(PortParameterKey))
                If Me.ContainsKey(key) Then
                    sw.WriteLine("{0},{1}", key, Me.Item(key))
                End If
            Next
        End Using
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the name of the port. </summary>
    ''' <value> The name of the port. </value>
    Public Property PortName As String
        Get
            Return Me(PortParameterKey.PortName)
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.PortName) Then
                Me.Replace(PortParameterKey.PortName, value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the baud rate. </summary>
    ''' <value> The baud rate. </value>
    Public Property BaudRate As Integer
        Get
            Return CInt(Me(PortParameterKey.BaudRate))
        End Get
        Set(value As Integer)
            If value <> Me.BaudRate Then
                Me.Replace(PortParameterKey.BaudRate, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the data bits. </summary>
    ''' <value> The data bits. </value>
    Public Property DataBits As Integer
        Get
            Return CInt(Me.Item(PortParameterKey.DataBits))
        End Get
        Set(value As Integer)
            If value <> Me.DataBits Then
                Me.Replace(PortParameterKey.DataBits, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Parse parity. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="parity"> The parity. </param>
    ''' <returns> A Parity. </returns>
    Public Shared Function ParseParity(ByVal parity As String) As Parity
        Return DirectCast([Enum].Parse(GetType(Parity), parity), Parity)
    End Function

    ''' <summary> Gets or sets the parity. </summary>
    ''' <value> The parity. </value>
    Public Property Parity As IO.Ports.Parity
        Get
            Return PortParametersDictionary.ParseParity(Me.Item(PortParameterKey.Parity))
        End Get
        Set(value As IO.Ports.Parity)
            If value <> Me.Parity Then
                Me.Replace(PortParameterKey.Parity, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Parse stop bits. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="stopBits"> The stop bits. </param>
    ''' <returns> The StopBits. </returns>
    Public Shared Function ParseStopBits(ByVal stopBits As String) As StopBits
        Return DirectCast([Enum].Parse(GetType(StopBits), stopBits), StopBits)
    End Function

    ''' <summary> Gets or sets the stop bits. </summary>
    ''' <value> The stop bits. </value>
    Public Property StopBits As IO.Ports.StopBits
        Get
            Return PortParametersDictionary.ParseStopBits(Me.Item(PortParameterKey.StopBits))
        End Get
        Set(value As IO.Ports.StopBits)
            If value <> Me.ReceiveDelay Then
            End If
            Me.Replace(PortParameterKey.StopBits, value.ToString)
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Parse handshake. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="handshake"> The handshake. </param>
    ''' <returns> A Handshake. </returns>
    Public Shared Function ParseHandshake(ByVal handshake As String) As Handshake
        Return DirectCast([Enum].Parse(GetType(Handshake), handshake), Handshake)
    End Function

    ''' <summary> Gets or sets the handshake. </summary>
    ''' <value> The handshake. </value>
    Public Property Handshake As IO.Ports.Handshake
        Get
            Return PortParametersDictionary.ParseHandshake(Me.Item(PortParameterKey.Handshake))
        End Get
        Set(value As IO.Ports.Handshake)
            If value <> Me.Handshake Then
                Me.Replace(PortParameterKey.Handshake, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Parse RTS enable. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="rtsEnable"> The RTS enable. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function ParseRtsEnable(ByVal rtsEnable As String) As Boolean
        Return Boolean.Parse(rtsEnable)
    End Function

    ''' <summary> Gets or sets the RTS enable. </summary>
    ''' <value> The RTS enable. </value>
    Public Property RtsEnable As Boolean
        Get
            Return PortParametersDictionary.ParseRtsEnable(Me(PortParameterKey.RtsEnable))
        End Get
        Set(value As Boolean)
            If value <> Me.RtsEnable Then
                Me.Replace(PortParameterKey.RtsEnable, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the read timeout. </summary>
    ''' <value> The read timeout. </value>
    Public Property ReadTimeout As Integer
        Get
            Return CInt(Me.Item(PortParameterKey.ReadTimeout))
        End Get
        Set(value As Integer)
            If value <> Me.ReadTimeout Then
                Me.Replace(PortParameterKey.ReadTimeout, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the size of the read buffer. </summary>
    ''' <value> The size of the read buffer. </value>
    Public Property ReadBufferSize As Integer
        Get
            Return CInt(Me.Item(PortParameterKey.ReadBufferSize))
        End Get
        Set(value As Integer)
            If value <> Me.ReadBufferSize Then
                Me.Replace(PortParameterKey.ReadBufferSize, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the receive delay. </summary>
    ''' <value> The receive delay. </value>
    Public Property ReceiveDelay As Integer
        Get
            Return CInt(Me(PortParameterKey.ReceiveDelay))
        End Get
        Set(value As Integer)
            If value <> Me.ReceiveDelay Then
                Me.Replace(PortParameterKey.ReceiveDelay, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the received bytes threshold. </summary>
    ''' <value> The received bytes threshold. </value>
    Public Property ReceivedBytesThreshold As Integer
        Get
            Return CInt(Me.Item(PortParameterKey.ReceivedBytesThreshold))
        End Get
        Set(value As Integer)
            If value <> Me.ReceivedBytesThreshold Then
                Me.Replace(PortParameterKey.ReceivedBytesThreshold, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the size of the write buffer. </summary>
    ''' <value> The size of the write buffer. </value>
    Public Property WriteBufferSize As Integer
        Get
            Return CInt(Me.Item(PortParameterKey.WriteBufferSize))
        End Get
        Set(value As Integer)
            If value <> Me.WriteBufferSize Then
                Me.Replace(PortParameterKey.WriteBufferSize, value.ToString)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Character bit count. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="startBitsCount"> Number of start bits. </param>
    ''' <returns> A Double. </returns>
    Public Function CharacterBitCount(ByVal startBitsCount As Integer) As Double
        Dim bitCount As Double = Me.DataBits + If(Me.Parity = Parity.None, 0, 1)
        Select Case Me.StopBits
            Case StopBits.None
            Case StopBits.One
                bitCount += 1
            Case StopBits.OnePointFive
                bitCount += 1.5
            Case StopBits.Two
                bitCount += 2
            Case Else
        End Select
        bitCount += startBitsCount
        Return bitCount
    End Function

    ''' <summary> Minimum time to transmit the number of character specified. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="startBitCount">  Number of start bits. </param>
    ''' <param name="characterCount"> Number of characters. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function MinimumTransitTimespan(ByVal startBitCount As Integer, ByVal characterCount As Integer) As TimeSpan
        Dim bitCount As Double = Me.CharacterBitCount(startBitCount)
        Return TimeSpan.FromTicks(CLng(characterCount * TimeSpan.TicksPerSecond * bitCount / Me.BaudRate))
    End Function

    ''' <summary> Minimum time to transmit the number of character specified. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="characterCount"> Number of characters. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function MinimumTransitTimespan(ByVal characterCount As Integer) As TimeSpan
        Return Me.MinimumTransitTimespan(1, characterCount)
    End Function
#End Region

End Class

