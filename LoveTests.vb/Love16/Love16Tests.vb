Imports isr.Ports.Serial
Imports isr.Ports.Serial.Methods

''' <summary> Tests the Love 16A Temperature Controller Module . </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-26 </para>
''' </remarks>
<TestClass()>
Public Class Love16Tests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Ports.Serial.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
	Public Shared Sub MyClassCleanup()
		_TestSite?.Dispose()
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
		Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
		Dim expectedUpperLimit As Double = 12
		Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
		TestInfo.ClearMessageQueue()
		Assert.IsTrue(Love16Settings.Get.Exists, $"{GetType(Love16Settings)} settings should exist")
		TestInfo.ClearMessageQueue()
	End Sub

	''' <summary> Cleans up after each test has run. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	''' <summary>
	''' Gets the test context which provides information about and functionality for the current test
	''' run.
	''' </summary>
	''' <value> The test context. </value>
	Public Property TestContext() As TestContext
	''' <summary> The test site. </summary>
	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " CHECK SUM TESTS "

	''' <summary> (Unit Test Method) tests ASCII bytes. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub AsciiBytesTest()
		Dim asciiValue As String = "00"
		Dim expectedBytes As Byte() = New Byte() {48, 48}
		Dim encoding As Text.Encoding = New Text.ASCIIEncoding
		Dim actualBytes As Byte() = encoding.GetBytes(asciiValue)
		Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes), $"Expected.SequenceEquals values for text={asciiValue}")
		actualBytes = asciiValue.ToBytes
		Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes), $"{asciiValue}.ToBytes values for text={asciiValue}")
	End Sub

    ''' <summary> (Unit Test Method) tests read command checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub ReadCommandChecksumTest()
        Using loveProtocol As New Love.ProtocolMessage() With {.MessageType = Teleport.MessageType.Command,
                                                               .CommandAscii = Love.LoveCommandCollection.Get.Item(CInt(Love.LoveCommandCode.ReadTemperature)).CommandAscii,
                                                               .ModuleAddressAscii = "01"}
            Dim expectedChecksum As String = "C1"
            Dim actualChecksum As String = loveProtocol.CalculateChecksumHex
            loveProtocol.BuildStream(Teleport.MessageType.Command)
            Assert.AreEqual(expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests temperature response checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub TemperatureResponseChecksumTest()
        Dim payload As Love.ReadTemperaturePayload = Love.ReadTemperaturePayload.SimulatePayload
        Using loveProtocol As New Love.ProtocolMessage() With {
            .MessageType = Teleport.MessageType.Response,
            .CommandAscii = String.Empty,
            .ModuleAddressAscii = "01",
            .Payload = payload.Build}
            Dim expectedChecksum As String = "46"
            Dim actualChecksum As String = loveProtocol.CalculateChecksumHex
            loveProtocol.BuildStream(Teleport.MessageType.Response)
            Assert.AreEqual(expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests setpoint command checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub SetpointCommandChecksumTest()
        Dim payload As Love.WriteSetpointPayload = Love.WriteSetpointPayload.SimulatePayload
        Using loveProtocol As New Love.ProtocolMessage() With {
            .MessageType = Teleport.MessageType.Command,
            .CommandAscii = Love.LoveCommandCollection.Get.Item(CInt(Love.LoveCommandCode.WriteSetpoint)).CommandAscii,
            .ModuleAddressAscii = "01",
            .Payload = payload.Build}
            Dim expectedChecksum As String = "4C"
            Dim actualChecksum As String = loveProtocol.CalculateChecksumHex
            loveProtocol.BuildStream(Teleport.MessageType.Command)
            Assert.AreEqual(expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests setpoint command address 02 checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub SetpointCommandAddress02ChecksumTest()
        Dim payload As Love.WriteSetpointPayload = Love.WriteSetpointPayload.SimulatePayload
        Using loveProtocol As New Love.ProtocolMessage() With {
            .MessageType = Teleport.MessageType.Command,
            .CommandAscii = Love.LoveCommandCollection.Get.Item(CInt(Love.LoveCommandCode.WriteSetpoint)).CommandAscii,
            .ModuleAddressAscii = "02",
            .Payload = payload.Build}
            Dim expectedChecksum As String = "4D"
            Dim actualChecksum As String = loveProtocol.CalculateChecksumHex
            loveProtocol.BuildStream(Teleport.MessageType.Command)
            Assert.AreEqual(expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests setpoint process 2 address 02 checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub SetpointProcess2Address02ChecksumTest()
        Dim payload As Love.WriteSetpointPayload = Love.WriteSetpointPayload.SimulatePayload
        Using loveProtocol As New Love.ProtocolMessage() With {
            .MessageType = Teleport.MessageType.Command,
            .CommandAscii = Love.LoveCommandCollection.Get.Item(CInt(Love.LoveCommandCode.WriteSetpointProcess2)).CommandAscii,
            .ModuleAddressAscii = "02",
            .Payload = payload.Build}
            Dim expectedChecksum As String = "4E"
            Dim actualChecksum As String = loveProtocol.CalculateChecksumHex
            loveProtocol.BuildStream(Teleport.MessageType.Command)
            Assert.AreEqual(expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests setpoint process 2 address 01 checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod()>
    Public Sub SetpointProcess2Address01ChecksumTest()
        Dim payload As Love.WriteSetpointPayload = Love.WriteSetpointPayload.SimulatePayload
        Using loveProtocol As New Love.ProtocolMessage() With {
            .MessageType = Teleport.MessageType.Command,
            .CommandAscii = Love.LoveCommandCollection.Get.Item(CInt(Love.LoveCommandCode.WriteSetpointProcess2)).CommandAscii,
            .ModuleAddressAscii = "01",
            .Payload = payload.Build}
            Dim expectedChecksum As String = "4D"
            Dim actualChecksum As String = loveProtocol.CalculateChecksumHex
            loveProtocol.BuildStream(Teleport.MessageType.Command)
            Assert.AreEqual(expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}")
        End Using
    End Sub

#End Region

#Region " PAYLOAD TESTS "

    ''' <summary> Reads temperature payload. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Shared Sub ReadTemperaturePayload()
		Dim expectedPayload As Love.ReadTemperaturePayload = Love.ReadTemperaturePayload.SimulatePayload()
		Dim actualPayload As Love.ReadTemperaturePayload = New Love.ReadTemperaturePayload
		actualPayload.Parse(expectedPayload.Build)
		Assert.AreEqual(expectedPayload.AlarmStatus, actualPayload.AlarmStatus, "Alarm status")
		Assert.AreEqual(expectedPayload.Sign, actualPayload.Sign, "temperature sign")
		Assert.AreEqual(expectedPayload.Accuracy, actualPayload.Accuracy, "temperature accuracy")
		Assert.AreEqual(expectedPayload.Temperature, actualPayload.Temperature, "temperature value")
	End Sub

	''' <summary> (Unit Test Method) tests read temperature payload. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub ReadTemperaturePayloadTest()
		Love16Tests.ReadTemperaturePayload()
	End Sub

	''' <summary> Reads Setpoint payload. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	Private Shared Sub ReadSetpointPayload()
		Dim expectedPayload As Love.ReadSetpointPayload = Love.ReadSetpointPayload.SimulatePayload()
		Dim actualPayload As Love.ReadSetpointPayload = New Love.ReadSetpointPayload
		actualPayload.Parse(expectedPayload.Build)
		Assert.AreEqual(expectedPayload.Sign, actualPayload.Sign, "Setpoint sign")
		Assert.AreEqual(expectedPayload.Accuracy, actualPayload.Accuracy, "Setpoint accuracy")
		Assert.AreEqual(expectedPayload.Temperature, actualPayload.Temperature, "Setpoint value")
	End Sub

	''' <summary> (Unit Test Method) tests read Setpoint payload. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub ReadSetpointPayloadTest()
		Love16Tests.ReadSetpointPayload()
	End Sub

	''' <summary> Writes the setpoint payload. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	Private Shared Sub WriteSetpointPayload()
		Dim expectedPayload As Love.WriteSetpointPayload = Love.WriteSetpointPayload.SimulatePayload()
		Dim actualPayload As Love.WriteSetpointPayload = New Love.WriteSetpointPayload
		actualPayload.Parse(expectedPayload.Build)
		Assert.AreEqual(expectedPayload.Temperature, actualPayload.Temperature, "Setpoint temperature")
		Assert.AreEqual(expectedPayload.Sign, actualPayload.Sign, "Setpoint sign")
	End Sub

	''' <summary> (Unit Test Method) tests read temperature payload. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	<TestMethod()>
	Public Sub WriteSetpointPayloadTest()
		Love16Tests.WriteSetpointPayload()
	End Sub

#End Region

#Region " PORT FUNCTIONS "

	''' <summary> Configure port. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	''' <param name="port"> The port. </param>
	Private Shared Sub ConfigurePort(ByVal port As isr.Ports.Serial.IPort)
		port.PortParameters.PortName = Love16Settings.Get.PortName
		Assert.AreEqual(Love16Settings.Get.PortName, port.PortParameters.PortName, "Port name")
		port.PortParameters.BaudRate = Love16Settings.Get.BaudRate
		Assert.AreEqual(Love16Settings.Get.BaudRate, port.PortParameters.BaudRate, "Baud Rate")
		port.PortParameters.DataBits = Love16Settings.Get.DataBits
		port.PortParameters.Parity = Love16Settings.Get.Parity
		port.PortParameters.StopBits = Love16Settings.Get.StopBits
		port.PortParameters.ReceivedBytesThreshold = 1
		port.PortParameters.ReceiveDelay = 1
	End Sub

	''' <summary> Opens port. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	''' <param name="port"> The port. </param>
	Private Shared Sub OpenPort(ByVal port As isr.Ports.Serial.IPort)
		Dim e As New Core.ActionEventArgs
		port.TryOpen(e)
		Assert.IsFalse(e.Failed, e.Details)
		Assert.AreEqual(Love16Settings.Get.PortName, port.SerialPort.PortName, "Serial Port name")
		Assert.AreEqual(Love16Settings.Get.BaudRate, port.SerialPort.BaudRate, "Serial Port Baud Rate")
	End Sub

	''' <summary> Closes a port. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	''' <param name="love16"> The love 16. </param>
	Private Shared Sub ClosePort(ByVal love16 As Love.Love16A)
		love16.Messenger.Port.SerialPort.Close()
		Assert.IsFalse(love16.Messenger.Port.SerialPort.IsOpen, $"Serial port is { love16.Messenger.Port.PortParameters.PortName} close")
	End Sub

    ''' <summary> (Unit Test Method) tests open port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod(), TestCategory("Love16")>
    Public Sub OpenPortTest()
		Using love16a As Ports.Love.Love16A = New Ports.Love.Love16A(Love16Settings.Get.ModuleAddress)
			Try
				Love16Tests.ConfigurePort(love16a.Messenger.Port)
				Love16Tests.OpenPort(love16a.Messenger.Port)
			Catch
				Throw
			Finally
				Love16Tests.ClosePort(love16a)
			End Try
		End Using
	End Sub

#End Region

#Region " MODULE TEMPERATURE READ TESTS "

	''' <summary> Reads a temperature. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	''' <param name="love16"> The love 16. </param>
	Private Shared Sub ReadTemperature(ByVal love16 As Love.Love16A)
		Dim statusCode As Teleport.StatusCode = love16.ReadTemperature
		Assert.AreEqual(Teleport.StatusCode.Okay, statusCode, "Read temperature status code")
		Assert.AreEqual(1, love16.ReadTemperaturePayload.Accuracy, "Temperature accuracy")
		Assert.AreEqual(4, love16.ReadTemperaturePayload.Sign, "Temperature sign")
		Assert.AreEqual(48, love16.ReadTemperaturePayload.AlarmStatus, "Temperature alarm status")
		Assert.AreEqual(24, love16.ReadTemperaturePayload.Temperature, 4, "Temperature")
	End Sub

    ''' <summary> (Unit Test Method) tests read temperature. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod(), TestCategory("Love16")>
    Public Sub ReadTemperatureTest()
		Using love16a As Ports.Love.Love16A = New Ports.Love.Love16A(Love16Settings.Get.ModuleAddress)
			Try
				Love16Tests.ConfigurePort(love16a.Messenger.Port)
				Love16Tests.OpenPort(love16a.Messenger.Port)
				Love16Tests.ReadTemperature(love16a)
			Catch
				Throw
			Finally
				Love16Tests.ClosePort(love16a)
			End Try
		End Using
	End Sub

#End Region

#Region " MODULE SETPOINT WRITE TESTS "

	''' <summary> Writes a setpoint. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	''' <param name="love16">      The love 16. </param>
	''' <param name="temperature"> The temperature. </param>
	Private Shared Sub WriteSetpoint(ByVal love16 As Love.Love16A, ByVal temperature As Double)
		Dim statusCode As Teleport.StatusCode = love16.WriteSetpoint(temperature)
		Assert.AreEqual(Teleport.StatusCode.Okay, statusCode, "Write setpoint status code")
		Assert.AreEqual(0, love16.ReplyPayload.ReplyStatus, "Reply status")
	End Sub

	''' <summary> Reads a setpoint. </summary>
	''' <remarks> David, 2020-10-22. </remarks>
	''' <param name="love16">              The love 16. </param>
	''' <param name="expectedTemperature"> The expected temperature. </param>
	Private Shared Sub ReadSetpoint(ByVal love16 As Love.Love16A, ByVal expectedTemperature As Double)
		Dim statusCode As Teleport.StatusCode = love16.ReadSetpoint
		Assert.AreEqual(Teleport.StatusCode.Okay, statusCode, "Read setpoint status code")
		Assert.AreEqual(1, love16.ReadSetpointPayload.Accuracy, "Setpoint accuracy")
		Assert.AreEqual(Love.ReadSetpointPayload.PositiveSign, love16.ReadSetpointPayload.Sign, "Setpoint sign")
		Assert.AreEqual(expectedTemperature, love16.ReadSetpointPayload.Temperature, 0.1, "Setpoint Temperature")
	End Sub

    ''' <summary> (Unit Test Method) writes the setpoint test. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    <TestMethod(), TestCategory("Love16")>
    Public Sub WriteSetpointTest()
		Using love16a As Ports.Love.Love16A = New Ports.Love.Love16A(Love16Settings.Get.ModuleAddress)
			Try
				Love16Tests.ConfigurePort(love16a.Messenger.Port)
				Love16Tests.OpenPort(love16a.Messenger.Port)
				Dim temperature As Double = DateTimeOffset.Now.Second + 0.1 * DateTimeOffset.Now.Second
				Love16Tests.WriteSetpoint(love16a, temperature)
                isr.Core.ApplianceBase.DoEventsWait(Love.LoveCommand.SelectRefractoryPeriod(Love.LoveCommandCode.WriteSetpoint))
                Love16Tests.ReadSetpoint(love16a, temperature)
			Catch
				Throw
			Finally
				Love16Tests.ClosePort(love16a)

			End Try
		End Using
	End Sub

#End Region

End Class
