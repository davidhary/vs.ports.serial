using System;

using isr.Core.MSTest;
using isr.Ports.Serial;

using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Ports.SerialTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Tests the <see cref="isr.Ports.Serial.Port"/> port. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    public class FrameworkTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Serial.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( FrameworkSettings.Get().Exists, $"{typeof( FrameworkSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " EXTENSION: DELIMITER TESTS "

        /// <summary> A test for InsertDelimiter. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void InsertDelimiterTest()
        {
            string value = "000003";
            int everyLength = 2;
            string delimiter = " ";
            string expected = "00 00 03";
            string actual;
            actual = value.InsertDelimiter( everyLength, delimiter );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expected, value.InsertDelimiter( everyLength, delimiter ) );
        }

        /// <summary> A test for RemoveDelimiter. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void RemoveDelimiterTest()
        {
            string value = "00 00 03";
            string delimiter = " ";
            string expected = "000003";
            string actual;
            actual = value.RemoveDelimiter( delimiter );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expected, value.RemoveDelimiter( delimiter ) );
        }

        /// <summary> A test for  Remove and then insert Delimiter. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void RemoveInsertDelimiterTest()
        {
            string value = "00 00 03";
            int everyLength = 2;
            string delimiter = " ";
            string expected = "00 00 03";
            string actual;
            actual = value.RemoveDelimiter( delimiter ).InsertDelimiter( everyLength, delimiter );
            Assert.AreEqual( expected, actual );
        }
        #endregion

        #region " CHECK SUM TESTS "

        /// <summary> (Unit Test Method) tests checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void ChecksumTest()
        {
            string payload = "#1DOFF";
            string expected = "73";
            string actual = payload.Checksum();
            Assert.AreEqual( expected, actual, $"Check sum of {payload}" );
        }

        /// <summary> (Unit Test Method) validates the checksum test. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void ValidateChecksumTest()
        {
            string payload = "*1RD+00072.10A4";
            string expected = "A4";
            string message = payload.Remove( payload.Length - 2, 2 );
            string expectedMessage = "*1RD+00072.10";
            Assert.AreEqual( expectedMessage, message, $"Stripped message" );
            string actual = message.Checksum();
            Assert.AreEqual( expected, actual, $"Validated check sum of {payload}" );
        }

        #endregion

        #region " TRANSIT TIME "

        /// <summary> (Unit Test Method) tests default bit count. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void DefaultBitCountTest()
        {
            var dix = PortParametersDictionary.DefaultPortParameters();
            Assert.AreEqual( FrameworkSettings.Get().DefaultCharacterBitCount, dix.CharacterBitCount( 1 ), "Bits per character" );
        }

        /// <summary> (Unit Test Method) tests default minimum transit time. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void DefaultMinimumTransitTimeTest()
        {
            var dix = PortParametersDictionary.DefaultPortParameters();
            Asserts.Instance.AreEqual( FrameworkSettings.Get().DefaultMinimumTransitTime, dix.MinimumTransitTimespan( 1 ), TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * 0.01d) ), "Minimum transit time" );
        }

        #endregion

    }
}
