using System;

using isr.Core;

namespace isr.Ports.SerialTests
{

    /// <summary> A serial port framework settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode( "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0" )]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
    internal class FrameworkSettings : ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private FrameworkSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public static void OpenSettingsEditor()
        {
            WindowsForms.EditConfiguration( $"{typeof( FrameworkSettings )} Editor", Get() );
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static FrameworkSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static FrameworkSettings Get()
        {
            if ( _Instance is null )
            {
                lock ( _SyncLocker )
                    _Instance = ( FrameworkSettings ) Synchronized( new FrameworkSettings() );
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( _SyncLocker )
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool Exists
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public bool Verbose
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool Enabled
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool All
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

        #region " FRAMEWORK INFORMATION "

        /// <summary> Gets or sets the default minimum transit time. </summary>
        /// <value> The default minimum transit time. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "00:00:00.00104" )]
        public TimeSpan DefaultMinimumTransitTime
        {
            get {
                return AppSettingGetter( TimeSpan.Zero );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the default character bit count. </summary>
        /// <value> The default character bit count. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public double DefaultCharacterBitCount
        {
            get {
                return AppSettingGetter( 0d );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

    }
}
