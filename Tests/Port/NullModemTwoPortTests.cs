using System;

using isr.Ports.Serial;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.SerialTests
{

    /// <summary> Tests the <see cref="isr.Ports.Serial.Port"/> port using a two port null model. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "NullModemTwoPorts" )]
    public class NullModemTwoPortTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Serial.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( NullModemSettings.Get().Exists, $"{typeof( NullModemSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " NULL MODEM TESTS "

        /// <summary> Check connected port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        private static void CheckConnectedPort( IPort port )
        {
            Assert.AreEqual( true, port.IsOpen, $"Port {port.PortParameters.PortName} is open" );
        }

        /// <summary> (Unit Test Method) tests null modem using two ports. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void TwoPortNullModemTest()
        {
            if ( !NullModemSettings.Get().CheckPort1Exists() )
                Assert.Inconclusive( $"{NullModemSettings.Get().Port1Name} not found" );
            var e = new Core.ActionEventArgs();
            using IPort port1 = Port.Create();
            try
            {
                port1.AddListener( TestInfo.TraceMessagesQueueListener );
                _ = port1.TryOpen( NullModemSettings.Get().Port1Name, e );
                Assert.AreEqual( false, e.Failed, $"Failed connecting {NullModemSettings.Get().Port1Name}: {e.Details}" );
                Assert.AreEqual( NullModemSettings.Get().Port1Name, port1.SerialPort.PortName, $"Serial port name should match" );
                CheckConnectedPort( port1 );
            }
            catch
            {
                throw;
            }
            finally
            {
                e.Clear();
            }

            using IPort port2 = Port.Create();
            try
            {
                port2.AddListener( TestInfo.TraceMessagesQueueListener );
                _ = port2.TryOpen( NullModemSettings.Get().Port2Name, e );
                Assert.AreEqual( false, e.Failed, $"Failed connecting {NullModemSettings.Get().Port2Name}: {e.Details}" );
                Assert.AreEqual( NullModemSettings.Get().Port2Name, port2.SerialPort.PortName, $"Serial port name should match" );
                CheckConnectedPort( port2 );
            }
            catch
            {
                throw;
            }
            finally
            {
                e.Clear();
            }

            string message = "ABC";
            _ = Port.SendAsciiData( port2, message );

            // wait for the data to go out
            Core.ApplianceBase.DoEventsWait( port2.PortParameters.MinimumTransitTimespan( message.Length ) );
            string sentMessage = Port.Decode( port2.TransmittedValues, port2.SerialPort.Encoding );

            // wait for the characters to come in.
            _ = port1.TryWaitReceiveCount( message.Length, 10 );
            string receivedMessage = Port.Decode( port1.ReceivedValues, port1.SerialPort.Encoding );
            Assert.AreEqual( message, sentMessage, $"sent message should match" );
            Assert.AreEqual( message, receivedMessage, $"received message should match" );
        }

        #endregion

    }
}
