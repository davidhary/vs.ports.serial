Imports isr.Core
Imports isr.Ports.Serial

''' <summary> Tests the D1141 Module. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/26/2018 </para></remarks>
<TestClass()>
Public Class D1141Tests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Ports.Serial.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(D1141Settings.Get.Exists, $"{GetType(D1141Settings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CHECK SUM TESTS "

    ''' <summary> (Unit Test Method) tests checksum. </summary>
    ''' <remarks> David, 2020-11-16. </remarks>
    <TestMethod()>
    Public Sub ChecksumTest()
        Dim payload As String = "#1DOFF"
        Dim expected As String = "73"
        Dim actual As String = payload.Checksum
        Assert.AreEqual(expected, actual, $"Check sum of {payload}")
    End Sub

    ''' <summary> (Unit Test Method) validates the checksum test. </summary>
    ''' <remarks> David, 2020-11-16. </remarks>
    <TestMethod()>
    Public Sub ValidateChecksumTest()
        Dim payload As String = "*1RD+00072.10A4"
        Dim expected As String = "A4"
        Dim message As String = payload.Remove(payload.Length - 2, 2)
        Dim expectedMessage As String = "*1RD+00072.10"
        Assert.AreEqual(expectedMessage, message, $"Stripped message")
        Dim actual As String = message.Checksum
        Assert.AreEqual(expected, actual, $"Validated check sum of {payload}")
    End Sub

#End Region

End Class
