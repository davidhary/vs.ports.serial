using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.EscapeSequencesExtensions;

using Microsoft.VisualBasic;

namespace isr.Ports.Serial.Forms
{

    /// <summary>   An extensions. </summary>
    /// <remarks>   David, 2020-11-17. </remarks>
    public static class Extensions
    {

        #region " LABEL "

        /// <summary> Displays a ruler on a <see cref="System.Windows.Forms.Label">label</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="label">          The <see cref="System.Windows.Forms.RichTextBox">Rich Text Box</see>. </param>
        /// <param name="width">          The width. </param>
        /// <param name="characterWidth"> Width of the character. </param>
        /// <param name="displayOption">  The display option. </param>
        /// <returns> length of ruler. </returns>
        public static int DisplayRuler( this System.Windows.Forms.Label label, int width, float characterWidth, DisplayOptions displayOption )
        {
            if ( label is null )
                return 0;
            var s = new System.Text.StringBuilder();
            int anzMarks;
            if ( DisplayOptions.Hex == (displayOption & DisplayOptions.Hex) )
            {
                anzMarks = ( int ) Math.Floor( width / characterWidth / 3f );
                for ( int i = 1, loopTo = anzMarks; i <= loopTo; i++ )
                    _ = s.Append( $".{i:00}" );
            }
            else
            {
                anzMarks = ( int ) Math.Floor( width / characterWidth / 5f );
                for ( int i = 1, loopTo1 = anzMarks; i <= loopTo1; i++ )
                {
                    if ( i < 2 )
                    {
                        _ = s.Append( $"....{i * 5:0}" );
                    }
                    else
                    {
                        _ = i < 20 ? s.Append( $"...{i * 5:00}" ) : s.Append( $"..{i * 5:000}" );
                    }
                }
            }

            label.Text = s.ToString();

            // coloring ruler
            label.BackColor = System.Drawing.Color.LightGray;
            return s.Length;
        }

        #endregion

        #region " RICH TEXT BOX "

        /// <summary>
        /// Displays a ruler on a <see cref="System.Windows.Forms.RichTextBox">Rich Text Box</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="textBox">        The <see cref="System.Windows.Forms.RichTextBox">Rich Text Box</see>. </param>
        /// <param name="characterWidth"> Width of the character. </param>
        /// <param name="displayOption">  The display option. </param>
        /// <returns> length of ruler. </returns>
        public static int DisplayRuler( this System.Windows.Forms.RichTextBox textBox, float characterWidth, DisplayOptions displayOption )
        {
            if ( textBox is null )
                return 0;
            int rbWidth = textBox.Width;
            var s = new System.Text.StringBuilder();
            int anzMarks;
            if ( DisplayOptions.Hex == (displayOption & DisplayOptions.Hex) )
            {
                anzMarks = ( int ) (rbWidth / characterWidth / 3f);
                for ( int i = 1, loopTo = anzMarks; i <= loopTo; i++ )
                    _ = s.Append( $" {i:00}" );
            }
            else
            {
                anzMarks = ( int ) (rbWidth / characterWidth / 5f);
                for ( int i = 1, loopTo1 = anzMarks; i <= loopTo1; i++ )
                {
                    if ( i < 2 )
                    {
                        _ = s.Append( $"    {i * 5:0}" );
                    }
                    else
                    {
                        _ = i < 20 ? s.Append( $"   {i * 5:00}" ) : s.Append( $"  {i * 5:000}" );
                    }
                }
            }

            // coloring ruler
            var cl = textBox.BackColor;
            textBox.Select( 0, textBox.Lines[0].Length );
            textBox.SelectionBackColor = System.Drawing.Color.LightGray;
            textBox.SelectedText = s.ToString();
            if ( textBox.Lines.Length == 1 )
                textBox.AppendText( Constants.vbCr );
            textBox.SelectionBackColor = cl;
            textBox.SelectionLength = 0;
            return s.Length;
        }

        /// <summary> Builds hexadecimal string. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> data frame. </param>
        /// <returns> A String. </returns>
        private static string BuildHexString( IEnumerable<byte> data )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( byte value in data )
                _ = builder.Append( $" {value:X2}" );
            return builder.ToString();
        }

        /// <summary> Builds character string. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data">     data frame. </param>
        /// <param name="encoding"> The encoding. </param>
        /// <returns> A String. </returns>
        private static string BuildCharacterString( IEnumerable<byte> data, System.Text.Encoding encoding )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( byte value in data )
            {
                if ( value > 31 )
                {
                    _ = builder.Append( $"  {encoding.GetString( value )}" );
                }
                else
                {
                    _ = value.IsEscapeValue() ? builder.Append( $" {value.ToEscapeSequence()}" ) : builder.Append( "  ." );
                }
            }

            return builder.ToString();
        }

        /// <summary> Builds ASCII string. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data">     data frame. </param>
        /// <param name="encoding"> The encoding. </param>
        /// <returns> A String. </returns>
        private static string BuildAsciiString( IEnumerable<byte> data, System.Text.Encoding encoding )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( byte value in data )
            {
                if ( value > 31 )
                {
                    _ = builder.Append( $"{encoding.GetString( value )}" );
                }
                else
                {
                    _ = value.IsEscapeValue() ? builder.Append( $"{value.ToEscapeSequence()}" ) : builder.Append( "." );
                }
            }

            return builder.ToString();
        }

        /// <summary> Adds an object onto the end of this queue. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value">  The value. </param>
        /// <param name="length"> The length. </param>
        /// <returns> A Queue(Of String) </returns>
        private static Queue<string> Enqueue( string value, int length )
        {
            var result = new Queue<string>();
            int startIndex = 0;
            while ( startIndex < value.Length )
            {
                string s = value.Substring( startIndex, Math.Min( length, value.Length - startIndex ) );
                result.Enqueue( s );
                startIndex += length;
            }

            return result;
        }

        /// <summary>
        /// Append data bytes to the <see cref="System.Windows.Forms.TextBoxBase">Text Box</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="textBox">       The <see cref="System.Windows.Forms.TextBoxBase">Text Box</see>. </param>
        /// <param name="data">          data frame. </param>
        /// <param name="encoding">      The encoding. </param>
        /// <param name="currentLength"> Max chars in box. </param>
        /// <param name="displayOption"> The display option. </param>
        public static void AppendBytes( this System.Windows.Forms.TextBoxBase textBox, IEnumerable<byte> data, System.Text.Encoding encoding, int currentLength, DisplayOptions displayOption )
        {
            if ( textBox is null )
                return;
            if ( data is null || !data.Any() )
                return;
            if ( DisplayOptions.Ascii == displayOption )
            {
                string asciiString = BuildAsciiString( data, encoding );
                var q = Enqueue( asciiString, currentLength );
                while ( q.Any() )
                {
                    textBox.ScrollToCaret();
                    textBox.AppendText( $"{q.Dequeue()}{Environment.NewLine}" );
                }
            }
            else
            {
                string hexString = BuildHexString( data );
                var hexQ = Enqueue( hexString, currentLength );
                if ( DisplayOptions.Ascii == (displayOption & DisplayOptions.Ascii) )
                {
                    string charString = BuildCharacterString( data, encoding );
                    var charQ = Enqueue( charString, currentLength );
                    while ( hexQ.Any() || charQ.Any() )
                    {
                        if ( hexQ.Any() )
                        {
                            textBox.ScrollToCaret();
                            textBox.AppendText( $"{hexQ.Dequeue()}{Environment.NewLine}" );
                        }

                        if ( charQ.Any() )
                        {
                            textBox.ScrollToCaret();
                            textBox.AppendText( $"{charQ.Dequeue()}{Environment.NewLine}" );
                        }
                    }
                }
                else
                {
                    while ( hexQ.Any() )
                    {
                        textBox.ScrollToCaret();
                        textBox.AppendText( $"{hexQ.Dequeue()}{Environment.NewLine}" );
                    }
                }
            }
        }

        #endregion

        #region " COMBO BOX EXTENSIUONS "

        /// <summary> List port names. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control">         The control. </param>
        /// <param name="currentPortName"> The current port name. </param>
        /// <returns> A String() </returns>
        public static string[] ListPortNames( this System.Windows.Forms.ComboBox control, string currentPortName )
        {
            if ( control is null )
                throw new ArgumentNullException( nameof( control ) );
            var portNames = System.IO.Ports.SerialPort.GetPortNames();
            control.Items.Clear();
            control.Items.AddRange( portNames );
            string portName;
            if ( portNames.Any() )
            {
                if ( portNames.Contains( Serial.My.MySettings.Default.PortName, StringComparer.CurrentCultureIgnoreCase ) )
                {
                }

                if ( portNames.Contains( currentPortName, StringComparer.CurrentCultureIgnoreCase ) )
                {
                    portName = currentPortName;
                }
                else
                {
                    portName = portNames.Contains( Serial.My.MySettings.Default.PortName, StringComparer.CurrentCultureIgnoreCase )
                        ? Serial.My.MySettings.Default.PortName
                        : portNames[0];
                }
            }
            else
            {
                portName = "NO PORTS";
            }

            control.Text = portName;
            return portNames;
        }

        #endregion

    }

    /// <summary> A bit-field of flags for specifying display options. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    [Flags]
    public enum DisplayOptions
    {

        /// <summary> An enum constant representing the Hexadecimal option. </summary>
        Hex = 1,

        /// <summary> An enum constant representing the ASCII option. </summary>
        Ascii = 2,

        /// <summary> An enum constant representing the both option. </summary>
        Both = 3
    }
}
