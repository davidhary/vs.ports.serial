﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Ports.Serial.Forms
{
    [DesignerGenerated()]
    public partial class PortConsole
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(PortConsole));
            _ToolStrip = new System.Windows.Forms.ToolStrip();
            __RefreshPortListButton = new System.Windows.Forms.ToolStripButton();
            __RefreshPortListButton.Click += new EventHandler(RefreshPortListButton_Click);
            _PortNamesCombo = new System.Windows.Forms.ToolStripComboBox();
            __LoadConfigButton = new System.Windows.Forms.ToolStripButton();
            __LoadConfigButton.Click += new EventHandler(LoadConfigButton_Click);
            __SaveConfigButton = new System.Windows.Forms.ToolStripButton();
            __SaveConfigButton.Click += new EventHandler(SaveConfigButton_Click);
            _BaudRateComboLabel = new System.Windows.Forms.ToolStripLabel();
            _BaudRateCombo = new System.Windows.Forms.ToolStripComboBox();
            _DataBitsComboLabel = new System.Windows.Forms.ToolStripLabel();
            _DataBitsCombo = new System.Windows.Forms.ToolStripComboBox();
            _ParityComboLabel = new System.Windows.Forms.ToolStripLabel();
            _ParityCombo = new System.Windows.Forms.ToolStripComboBox();
            _StopBitsComboLabel = new System.Windows.Forms.ToolStripLabel();
            _StopBitsCombo = new System.Windows.Forms.ToolStripComboBox();
            _ReceiveDelayComboLabel = new System.Windows.Forms.ToolStripLabel();
            _ReceiveDelayCombo = new System.Windows.Forms.ToolStripComboBox();
            _ReceiveThresholdComboLabel = new System.Windows.Forms.ToolStripLabel();
            _ReceiveThresholdCombo = new System.Windows.Forms.ToolStripComboBox();
            __ConnectButton = new System.Windows.Forms.ToolStripButton();
            __ConnectButton.Click += new EventHandler(ConnectButton_Click);
            _StatusStrip = new System.Windows.Forms.StatusStrip();
            _StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _FontDialog = new System.Windows.Forms.FontDialog();
            _SplitContainer = new System.Windows.Forms.SplitContainer();
            _ReceiveTextBox = new System.Windows.Forms.RichTextBox();
            _ReceiveRulerLabel = new System.Windows.Forms.Label();
            _ReceiveToolStrip = new System.Windows.Forms.ToolStrip();
            _ReceiveTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _Separator2 = new System.Windows.Forms.ToolStripSeparator();
            __ClearReceiveBoxButton = new System.Windows.Forms.ToolStripButton();
            __ClearReceiveBoxButton.Click += new EventHandler(ClearReceiveBoxButton_Click);
            _Separator = new System.Windows.Forms.ToolStripSeparator();
            __SaveReceiveTextBoxButton = new System.Windows.Forms.ToolStripButton();
            __SaveReceiveTextBoxButton.Click += new EventHandler(SaveReceiveTextBoxButton_Click);
            _Separator7 = new System.Windows.Forms.ToolStripSeparator();
            _ReceiveCountLabel = new System.Windows.Forms.ToolStripLabel();
            _Separator8 = new System.Windows.Forms.ToolStripSeparator();
            _ReceiveStatusLabel = new System.Windows.Forms.ToolStripLabel();
            _Separator9 = new System.Windows.Forms.ToolStripSeparator();
            __ReceiveDisplayOptionComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReceiveDisplayOptionComboBox.SelectedIndexChanged += new EventHandler(ReceiveDisplayOptionComboBox_SelectedIndexChanged);
            __FontSizeComboBox = new System.Windows.Forms.ToolStripComboBox();
            __FontSizeComboBox.SelectedIndexChanged += new EventHandler(FontSizeComboBox_SelectedIndexChanged);
            __TransmitTextBox = new System.Windows.Forms.RichTextBox();
            __TransmitTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(TransmitTextBox_MouseClick);
            _TransmitContextMenu = new System.Windows.Forms.ContextMenuStrip(components);
            __TransmitCopyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __TransmitCopyMenuItem.Click += new EventHandler(TransmitCopyMenuItem_Click);
            __TransmitPasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __TransmitPasteMenuItem.Click += new EventHandler(TransmitPasteMenuItem_Click);
            __TransmitCutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __TransmitCutMenuItem.Click += new EventHandler(TransmitCutMenuItem_Click);
            __TransmitSendMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __TransmitSendMenuItem.Click += new EventHandler(TransmitSendMenuItem_Click);
            __TransmitSendSelectionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __TransmitSendSelectionMenuItem.Click += new EventHandler(TransmitSendSelectionMenuItem_Click);
            _TransmitRulerLabel = new System.Windows.Forms.Label();
            _SendDataToolStrip = new System.Windows.Forms.ToolStrip();
            __TransmitMessageCombo = new System.Windows.Forms.ToolStripComboBox();
            __TransmitMessageCombo.TextUpdate += new EventHandler(TransmitMessageCombo_TextUpdate);
            __TransmitMessageCombo.KeyDown += new System.Windows.Forms.KeyEventHandler(SuppressDingHandler);
            __TransmitMessageCombo.KeyUp += new System.Windows.Forms.KeyEventHandler(SuppressDingHandler);
            __TransmitMessageCombo.KeyDown += new System.Windows.Forms.KeyEventHandler(SuppressDingHandler);
            __TransmitMessageCombo.KeyUp += new System.Windows.Forms.KeyEventHandler(SuppressDingHandler);
            __TransmitMessageCombo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TransmitMessageCombo_KeyPress);
            _TransmitEnterOptionComboBox = new System.Windows.Forms.ToolStripComboBox();
            _TransmitToolStrip = new System.Windows.Forms.ToolStrip();
            _TransmitTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _TransmitSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            __ClearTransmitBoxButton = new System.Windows.Forms.ToolStripButton();
            __ClearTransmitBoxButton.Click += new EventHandler(ClearTransmitBoxButton_Click);
            _TransmitSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            __LoadTransmitFileButton = new System.Windows.Forms.ToolStripButton();
            __LoadTransmitFileButton.Click += new EventHandler(LoadTransmitFileButton_Click);
            _TransmitSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            _TransmitCountLabel = new System.Windows.Forms.ToolStripLabel();
            _TransmitSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            _TransmitStatusLabel = new System.Windows.Forms.ToolStripLabel();
            _TransmitSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            __TransmitDisplayOptionComboBox = new System.Windows.Forms.ToolStripComboBox();
            __TransmitDisplayOptionComboBox.SelectedIndexChanged += new EventHandler(TransmitDisplayOptionComboBox_CheckedChanged);
            _TransmitSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            _TransmitTerminationComboBox = new System.Windows.Forms.ToolStripComboBox();
            _TransmitSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            _TransmitRepeatCountTextBox = new System.Windows.Forms.ToolStripTextBox();
            _TransmitDelayMillisecondsTextBox = new System.Windows.Forms.ToolStripTextBox();
            __TransmitPlayButton = new System.Windows.Forms.ToolStripButton();
            __TransmitPlayButton.CheckedChanged += new EventHandler(TransmitPlayButton_CheckedChanged);
            _TransmitSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            _ToolTip = new System.Windows.Forms.ToolTip(components);
            _ToolStrip.SuspendLayout();
            _StatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_SplitContainer).BeginInit();
            _SplitContainer.Panel1.SuspendLayout();
            _SplitContainer.Panel2.SuspendLayout();
            _SplitContainer.SuspendLayout();
            _ReceiveToolStrip.SuspendLayout();
            _TransmitContextMenu.SuspendLayout();
            _SendDataToolStrip.SuspendLayout();
            _TransmitToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            _ToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __RefreshPortListButton, _PortNamesCombo, __ConnectButton, __LoadConfigButton, __SaveConfigButton, _BaudRateComboLabel, _BaudRateCombo, _DataBitsComboLabel, _DataBitsCombo, _ParityComboLabel, _ParityCombo, _StopBitsComboLabel, _StopBitsCombo, _ReceiveDelayComboLabel, _ReceiveDelayCombo, _ReceiveThresholdComboLabel, _ReceiveThresholdCombo });
            _ToolStrip.Location = new System.Drawing.Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new System.Drawing.Size(889, 25);
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "ToolStrip1";
            // 
            // _RefreshPortListButton
            // 
            __RefreshPortListButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __RefreshPortListButton.Image = My.Resources.Resources.refresh;
            __RefreshPortListButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __RefreshPortListButton.Name = "__RefreshPortListButton";
            __RefreshPortListButton.Size = new System.Drawing.Size(23, 22);
            __RefreshPortListButton.Text = "Refresh";
            __RefreshPortListButton.ToolTipText = "Refreshes list of available ports";
            // 
            // _PortNamesCombo
            // 
            _PortNamesCombo.AutoSize = false;
            _PortNamesCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PortNamesCombo.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            _PortNamesCombo.Name = "_PortNamesCombo";
            _PortNamesCombo.Size = new System.Drawing.Size(69, 23);
            _PortNamesCombo.ToolTipText = "Port number";
            // 
            // _LoadConfigButton
            // 
            __LoadConfigButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __LoadConfigButton.Image = My.Resources.Resources.Disk;
            __LoadConfigButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LoadConfigButton.Name = "__LoadConfigButton";
            __LoadConfigButton.Size = new System.Drawing.Size(23, 22);
            __LoadConfigButton.Text = "Load Port Configuration";
            __LoadConfigButton.ToolTipText = "Loads port parameters from file";
            // 
            // _SaveConfigButton
            // 
            __SaveConfigButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __SaveConfigButton.Image = My.Resources.Resources.DiskDownload;
            __SaveConfigButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __SaveConfigButton.Name = "__SaveConfigButton";
            __SaveConfigButton.Size = new System.Drawing.Size(23, 22);
            __SaveConfigButton.Text = "Save Port Configuration";
            __SaveConfigButton.ToolTipText = "Saves port parameters to file";
            // 
            // _BaudRateComboLabel
            // 
            _BaudRateComboLabel.Name = "_BaudRateComboLabel";
            _BaudRateComboLabel.Size = new System.Drawing.Size(37, 22);
            _BaudRateComboLabel.Text = " Baud";
            _BaudRateComboLabel.ToolTipText = "Baud Rate";
            // 
            // _BaudRateCombo
            // 
            _BaudRateCombo.AutoSize = false;
            _BaudRateCombo.DropDownWidth = 50;
            _BaudRateCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _BaudRateCombo.Items.AddRange(new object[] { "2400", "4800", "9600", "19200", "38400", "115200" });
            _BaudRateCombo.Name = "_BaudRateCombo";
            _BaudRateCombo.Size = new System.Drawing.Size(77, 23);
            _BaudRateCombo.Text = "9600";
            _BaudRateCombo.ToolTipText = "Baud Rate";
            // 
            // _DataBitsComboLabel
            // 
            _DataBitsComboLabel.AutoToolTip = true;
            _DataBitsComboLabel.Name = "_DataBitsComboLabel";
            _DataBitsComboLabel.Size = new System.Drawing.Size(34, 22);
            _DataBitsComboLabel.Text = " Data";
            _DataBitsComboLabel.ToolTipText = "Data bits";
            // 
            // _DataBitsCombo
            // 
            _DataBitsCombo.AutoSize = false;
            _DataBitsCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _DataBitsCombo.Items.AddRange(new object[] { "7", "8" });
            _DataBitsCombo.Name = "_DataBitsCombo";
            _DataBitsCombo.Size = new System.Drawing.Size(34, 23);
            _DataBitsCombo.Text = "8";
            _DataBitsCombo.ToolTipText = "Data bits";
            // 
            // _ParityComboLabel
            // 
            _ParityComboLabel.Name = "_ParityComboLabel";
            _ParityComboLabel.Size = new System.Drawing.Size(40, 22);
            _ParityComboLabel.Text = " Parity";
            _ParityComboLabel.ToolTipText = "Parity";
            // 
            // _ParityCombo
            // 
            _ParityCombo.AutoSize = false;
            _ParityCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _ParityCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ParityCombo.Items.AddRange(new object[] { "None", "Even", "Mark", "Odd", "Space" });
            _ParityCombo.Name = "_ParityCombo";
            _ParityCombo.Size = new System.Drawing.Size(58, 23);
            _ParityCombo.ToolTipText = "Parity";
            // 
            // _StopBitsComboLabel
            // 
            _StopBitsComboLabel.Name = "_StopBitsComboLabel";
            _StopBitsComboLabel.Size = new System.Drawing.Size(34, 22);
            _StopBitsComboLabel.Text = " Stop";
            _StopBitsComboLabel.ToolTipText = "Stop bits";
            // 
            // _StopBitsCombo
            // 
            _StopBitsCombo.AutoSize = false;
            _StopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _StopBitsCombo.DropDownWidth = 50;
            _StopBitsCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _StopBitsCombo.Items.AddRange(new object[] { "None", "One", "Two" });
            _StopBitsCombo.Name = "_StopBitsCombo";
            _StopBitsCombo.Size = new System.Drawing.Size(58, 23);
            _StopBitsCombo.ToolTipText = "Stop bits";
            // 
            // _ReceiveDelayComboLabel
            // 
            _ReceiveDelayComboLabel.Name = "_ReceiveDelayComboLabel";
            _ReceiveDelayComboLabel.Size = new System.Drawing.Size(36, 22);
            _ReceiveDelayComboLabel.Text = "Delay";
            _ReceiveDelayComboLabel.ToolTipText = "Delay in ms";
            // 
            // _ReceiveDelayCombo
            // 
            _ReceiveDelayCombo.AutoSize = false;
            _ReceiveDelayCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReceiveDelayCombo.Items.AddRange(new object[] { "1", "2", "5", "10", "20", "50", "100", "200", "500", "1000" });
            _ReceiveDelayCombo.Name = "_ReceiveDelayCombo";
            _ReceiveDelayCombo.Size = new System.Drawing.Size(52, 23);
            _ReceiveDelayCombo.Text = "1";
            _ReceiveDelayCombo.ToolTipText = "Data received handle delay in ms";
            // 
            // _ReceiveThresholdComboLabel
            // 
            _ReceiveThresholdComboLabel.Name = "_ReceiveThresholdComboLabel";
            _ReceiveThresholdComboLabel.Size = new System.Drawing.Size(30, 22);
            _ReceiveThresholdComboLabel.Text = "Lim.";
            // 
            // _ReceiveThresholdCombo
            // 
            _ReceiveThresholdCombo.AutoSize = false;
            _ReceiveThresholdCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold);
            _ReceiveThresholdCombo.Items.AddRange(new object[] { "1", "2", "5", "10", "20", "50", "100", "200", "500", "1000" });
            _ReceiveThresholdCombo.Name = "_ReceiveThresholdCombo";
            _ReceiveThresholdCombo.Size = new System.Drawing.Size(52, 23);
            _ReceiveThresholdCombo.Text = "1";
            _ReceiveThresholdCombo.ToolTipText = "received bytes threshold property";
            // 
            // _ConnectButton
            // 
            __ConnectButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            __ConnectButton.CheckOnClick = true;
            __ConnectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __ConnectButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ConnectButton.Image = My.Resources.Resources.LedCornerGray;
            __ConnectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ConnectButton.Name = "__ConnectButton";
            __ConnectButton.Size = new System.Drawing.Size(23, 22);
            __ConnectButton.Text = "*CONNECT*";
            __ConnectButton.ToolTipText = "Connect/Disconnect";
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _StatusLabel });
            _StatusStrip.Location = new System.Drawing.Point(0, 760);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            _StatusStrip.Size = new System.Drawing.Size(889, 22);
            _StatusStrip.TabIndex = 1;
            _StatusStrip.Text = "Status Strip";
            // 
            // _StatusLabel
            // 
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Size = new System.Drawing.Size(16, 17);
            _StatusLabel.Text = "...";
            // 
            // _SplitContainer
            // 
            _SplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            _SplitContainer.Location = new System.Drawing.Point(0, 25);
            _SplitContainer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _SplitContainer.Name = "_SplitContainer";
            _SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // _SplitContainer.Panel1
            // 
            _SplitContainer.Panel1.Controls.Add(_ReceiveTextBox);
            _SplitContainer.Panel1.Controls.Add(_ReceiveRulerLabel);
            _SplitContainer.Panel1.Controls.Add(_ReceiveToolStrip);
            // 
            // _SplitContainer.Panel2
            // 
            _SplitContainer.Panel2.Controls.Add(__TransmitTextBox);
            _SplitContainer.Panel2.Controls.Add(_TransmitRulerLabel);
            _SplitContainer.Panel2.Controls.Add(_SendDataToolStrip);
            _SplitContainer.Panel2.Controls.Add(_TransmitToolStrip);
            _SplitContainer.Size = new System.Drawing.Size(889, 735);
            _SplitContainer.SplitterDistance = 455;
            _SplitContainer.SplitterWidth = 5;
            _SplitContainer.TabIndex = 3;
            // 
            // _ReceiveTextBox
            // 
            _ReceiveTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            _ReceiveTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReceiveTextBox.Font = new System.Drawing.Font("Lucida Console", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReceiveTextBox.Location = new System.Drawing.Point(0, 42);
            _ReceiveTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _ReceiveTextBox.Name = "_ReceiveTextBox";
            _ReceiveTextBox.ReadOnly = true;
            _ReceiveTextBox.Size = new System.Drawing.Size(887, 411);
            _ReceiveTextBox.TabIndex = 3;
            _ReceiveTextBox.Text = "*";
            _ReceiveTextBox.WordWrap = false;
            // 
            // _ReceiveRulerLabel
            // 
            _ReceiveRulerLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _ReceiveRulerLabel.Font = new System.Drawing.Font("Lucida Console", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReceiveRulerLabel.Location = new System.Drawing.Point(0, 25);
            _ReceiveRulerLabel.Name = "_ReceiveRulerLabel";
            _ReceiveRulerLabel.Size = new System.Drawing.Size(887, 17);
            _ReceiveRulerLabel.TabIndex = 4;
            _ReceiveRulerLabel.Text = "Ruler";
            _ReceiveRulerLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _ReceiveToolStrip
            // 
            _ReceiveToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _ReceiveToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _ReceiveToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _ReceiveTextBoxLabel, _Separator2, __ClearReceiveBoxButton, _Separator, __SaveReceiveTextBoxButton, _Separator7, _ReceiveCountLabel, _Separator8, _ReceiveStatusLabel, _Separator9, __ReceiveDisplayOptionComboBox, __FontSizeComboBox });
            _ReceiveToolStrip.Location = new System.Drawing.Point(0, 0);
            _ReceiveToolStrip.Name = "_ReceiveToolStrip";
            _ReceiveToolStrip.Size = new System.Drawing.Size(887, 25);
            _ReceiveToolStrip.TabIndex = 0;
            _ReceiveToolStrip.Text = "ToolStrip2";
            // 
            // _ReceiveTextBoxLabel
            // 
            _ReceiveTextBoxLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReceiveTextBoxLabel.Name = "_ReceiveTextBoxLabel";
            _ReceiveTextBoxLabel.Size = new System.Drawing.Size(51, 22);
            _ReceiveTextBoxLabel.Text = "RECEIVE";
            // 
            // _Separator2
            // 
            _Separator2.Name = "_Separator2";
            _Separator2.Size = new System.Drawing.Size(6, 25);
            // 
            // _ClearReceiveBoxButton
            // 
            __ClearReceiveBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __ClearReceiveBoxButton.Image = My.Resources.Resources.DocDelete;
            __ClearReceiveBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearReceiveBoxButton.Name = "__ClearReceiveBoxButton";
            __ClearReceiveBoxButton.Size = new System.Drawing.Size(23, 22);
            __ClearReceiveBoxButton.ToolTipText = "clear receive box";
            // 
            // _Separator
            // 
            _Separator.Name = "_Separator";
            _Separator.Size = new System.Drawing.Size(6, 25);
            // 
            // _SaveReceiveTextBoxButton
            // 
            __SaveReceiveTextBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __SaveReceiveTextBoxButton.Image = My.Resources.Resources.DiskDownload;
            __SaveReceiveTextBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __SaveReceiveTextBoxButton.Name = "__SaveReceiveTextBoxButton";
            __SaveReceiveTextBoxButton.Size = new System.Drawing.Size(23, 22);
            __SaveReceiveTextBoxButton.ToolTipText = "save received data to text file";
            // 
            // _Separator7
            // 
            _Separator7.Name = "_Separator7";
            _Separator7.Size = new System.Drawing.Size(6, 25);
            // 
            // _ReceiveCountLabel
            // 
            _ReceiveCountLabel.Name = "_ReceiveCountLabel";
            _ReceiveCountLabel.Size = new System.Drawing.Size(37, 22);
            _ReceiveCountLabel.Text = "00000";
            _ReceiveCountLabel.ToolTipText = "number of bytes received";
            // 
            // _Separator8
            // 
            _Separator8.Name = "_Separator8";
            _Separator8.Size = new System.Drawing.Size(6, 25);
            // 
            // _ReceiveStatusLabel
            // 
            _ReceiveStatusLabel.Image = My.Resources.Resources.LedCornerGray;
            _ReceiveStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            _ReceiveStatusLabel.Name = "_ReceiveStatusLabel";
            _ReceiveStatusLabel.Size = new System.Drawing.Size(16, 22);
            _ReceiveStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _ReceiveStatusLabel.ToolTipText = "receive status";
            // 
            // _Separator9
            // 
            _Separator9.Name = "_Separator9";
            _Separator9.Size = new System.Drawing.Size(6, 25);
            // 
            // _ReceiveDisplayOptionComboBox
            // 
            __ReceiveDisplayOptionComboBox.AutoSize = false;
            __ReceiveDisplayOptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __ReceiveDisplayOptionComboBox.Items.AddRange(new object[] { "HEX", "ASCII", "BOTH" });
            __ReceiveDisplayOptionComboBox.Name = "__ReceiveDisplayOptionComboBox";
            __ReceiveDisplayOptionComboBox.Size = new System.Drawing.Size(61, 23);
            __ReceiveDisplayOptionComboBox.ToolTipText = "Receive display option";
            // 
            // _FontSizeComboBox
            // 
            __FontSizeComboBox.AutoSize = false;
            __FontSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __FontSizeComboBox.Items.AddRange(new object[] { "Small", "Medium", "Large" });
            __FontSizeComboBox.Name = "__FontSizeComboBox";
            __FontSizeComboBox.Size = new System.Drawing.Size(71, 23);
            __FontSizeComboBox.ToolTipText = "Font size";
            // 
            // _TransmitTextBox
            // 
            __TransmitTextBox.BackColor = System.Drawing.SystemColors.Info;
            __TransmitTextBox.ContextMenuStrip = _TransmitContextMenu;
            __TransmitTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            __TransmitTextBox.Font = new System.Drawing.Font("Lucida Console", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TransmitTextBox.Location = new System.Drawing.Point(0, 67);
            __TransmitTextBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            __TransmitTextBox.Name = "__TransmitTextBox";
            __TransmitTextBox.ReadOnly = true;
            __TransmitTextBox.Size = new System.Drawing.Size(887, 206);
            __TransmitTextBox.TabIndex = 4;
            __TransmitTextBox.Text = "*";
            _ToolTip.SetToolTip(__TransmitTextBox, "press right mouse button");
            __TransmitTextBox.WordWrap = false;
            // 
            // _TransmitContextMenu
            // 
            _TransmitContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __TransmitCopyMenuItem, __TransmitPasteMenuItem, __TransmitCutMenuItem, __TransmitSendMenuItem, __TransmitSendSelectionMenuItem });
            _TransmitContextMenu.Name = "MenuTxBox";
            _TransmitContextMenu.Size = new System.Drawing.Size(150, 114);
            // 
            // _TransmitCopyMenuItem
            // 
            __TransmitCopyMenuItem.Image = My.Resources.Resources.Copy;
            __TransmitCopyMenuItem.Name = "__TransmitCopyMenuItem";
            __TransmitCopyMenuItem.Size = new System.Drawing.Size(149, 22);
            __TransmitCopyMenuItem.Text = "Copy";
            // 
            // _TransmitPasteMenuItem
            // 
            __TransmitPasteMenuItem.Image = My.Resources.Resources.Paste;
            __TransmitPasteMenuItem.Name = "__TransmitPasteMenuItem";
            __TransmitPasteMenuItem.Size = new System.Drawing.Size(149, 22);
            __TransmitPasteMenuItem.Text = "Paste";
            // 
            // _TransmitCutMenuItem
            // 
            __TransmitCutMenuItem.Image = My.Resources.Resources.ClipboardCut;
            __TransmitCutMenuItem.Name = "__TransmitCutMenuItem";
            __TransmitCutMenuItem.Size = new System.Drawing.Size(149, 22);
            __TransmitCutMenuItem.Text = "Cut";
            // 
            // _TransmitSendMenuItem
            // 
            __TransmitSendMenuItem.Image = My.Resources.Resources.Arrow1Right;
            __TransmitSendMenuItem.Name = "__TransmitSendMenuItem";
            __TransmitSendMenuItem.Size = new System.Drawing.Size(149, 22);
            __TransmitSendMenuItem.Text = "send line";
            // 
            // _TransmitSendSelectionMenuItem
            // 
            __TransmitSendSelectionMenuItem.Image = My.Resources.Resources.Arrow2Right;
            __TransmitSendSelectionMenuItem.Name = "__TransmitSendSelectionMenuItem";
            __TransmitSendSelectionMenuItem.Size = new System.Drawing.Size(149, 22);
            __TransmitSendSelectionMenuItem.Text = "send selection";
            // 
            // _TransmitRulerLabel
            // 
            _TransmitRulerLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _TransmitRulerLabel.Font = new System.Drawing.Font("Lucida Console", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TransmitRulerLabel.Location = new System.Drawing.Point(0, 50);
            _TransmitRulerLabel.Name = "_TransmitRulerLabel";
            _TransmitRulerLabel.Size = new System.Drawing.Size(887, 17);
            _TransmitRulerLabel.TabIndex = 5;
            _TransmitRulerLabel.Text = "Ruler";
            _TransmitRulerLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _SendDataToolStrip
            // 
            _SendDataToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _SendDataToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _SendDataToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __TransmitMessageCombo, _TransmitEnterOptionComboBox });
            _SendDataToolStrip.Location = new System.Drawing.Point(0, 25);
            _SendDataToolStrip.Name = "_SendDataToolStrip";
            _SendDataToolStrip.Size = new System.Drawing.Size(887, 25);
            _SendDataToolStrip.TabIndex = 3;
            _SendDataToolStrip.Text = "ToolStrip1";
            // 
            // _TransmitMessageCombo
            // 
            __TransmitMessageCombo.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TransmitMessageCombo.Name = "__TransmitMessageCombo";
            __TransmitMessageCombo.Size = new System.Drawing.Size(583, 25);
            __TransmitMessageCombo.Text = "enter message to send and press<enter>";
            __TransmitMessageCombo.ToolTipText = "enter message to send and press<enter>";
            // 
            // _TransmitEnterOptionComboBox
            // 
            _TransmitEnterOptionComboBox.AutoSize = false;
            _TransmitEnterOptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _TransmitEnterOptionComboBox.Items.AddRange(new object[] { "HEX", "ASCII" });
            _TransmitEnterOptionComboBox.Name = "_TransmitEnterOptionComboBox";
            _TransmitEnterOptionComboBox.Size = new System.Drawing.Size(61, 23);
            _TransmitEnterOptionComboBox.ToolTipText = "Enter Hex or ASCII";
            // 
            // _TransmitToolStrip
            // 
            _TransmitToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _TransmitToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _TransmitToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _TransmitTextBoxLabel, _TransmitSeparator1, __ClearTransmitBoxButton, _TransmitSeparator2, __LoadTransmitFileButton, _TransmitSeparator3, _TransmitCountLabel, _TransmitSeparator4, _TransmitStatusLabel, _TransmitSeparator5, __TransmitDisplayOptionComboBox, _TransmitSeparator6, _TransmitTerminationComboBox, _TransmitSeparator7, _TransmitRepeatCountTextBox, _TransmitDelayMillisecondsTextBox, __TransmitPlayButton, _TransmitSeparator9 });
            _TransmitToolStrip.Location = new System.Drawing.Point(0, 0);
            _TransmitToolStrip.Name = "_TransmitToolStrip";
            _TransmitToolStrip.Size = new System.Drawing.Size(887, 25);
            _TransmitToolStrip.TabIndex = 0;
            _TransmitToolStrip.Text = "ToolStrip3";
            // 
            // _TransmitTextBoxLabel
            // 
            _TransmitTextBoxLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TransmitTextBoxLabel.Name = "_TransmitTextBoxLabel";
            _TransmitTextBoxLabel.Size = new System.Drawing.Size(62, 22);
            _TransmitTextBoxLabel.Text = "TRANSMIT";
            // 
            // _TransmitSeparator1
            // 
            _TransmitSeparator1.Name = "_TransmitSeparator1";
            _TransmitSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // _ClearTransmitBoxButton
            // 
            __ClearTransmitBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __ClearTransmitBoxButton.Image = My.Resources.Resources.DocDelete;
            __ClearTransmitBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearTransmitBoxButton.Name = "__ClearTransmitBoxButton";
            __ClearTransmitBoxButton.Size = new System.Drawing.Size(23, 22);
            __ClearTransmitBoxButton.ToolTipText = "Clear transmit box";
            // 
            // _TransmitSeparator2
            // 
            _TransmitSeparator2.Name = "_TransmitSeparator2";
            _TransmitSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // _LoadTransmitFileButton
            // 
            __LoadTransmitFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __LoadTransmitFileButton.Image = My.Resources.Resources.Disk;
            __LoadTransmitFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LoadTransmitFileButton.Name = "__LoadTransmitFileButton";
            __LoadTransmitFileButton.Size = new System.Drawing.Size(23, 22);
            __LoadTransmitFileButton.ToolTipText = "load file into transmit text box";
            // 
            // _TransmitSeparator3
            // 
            _TransmitSeparator3.Name = "_TransmitSeparator3";
            _TransmitSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // _TransmitCountLabel
            // 
            _TransmitCountLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TransmitCountLabel.Name = "_TransmitCountLabel";
            _TransmitCountLabel.Size = new System.Drawing.Size(37, 22);
            _TransmitCountLabel.Text = "00000";
            _TransmitCountLabel.ToolTipText = "number of bytes sent";
            // 
            // _TransmitSeparator4
            // 
            _TransmitSeparator4.Name = "_TransmitSeparator4";
            _TransmitSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // _TransmitStatusLabel
            // 
            _TransmitStatusLabel.Image = My.Resources.Resources.LedCornerGray;
            _TransmitStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            _TransmitStatusLabel.Name = "_TransmitStatusLabel";
            _TransmitStatusLabel.Size = new System.Drawing.Size(16, 22);
            _TransmitStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _TransmitStatusLabel.ToolTipText = "send status";
            // 
            // _TransmitSeparator5
            // 
            _TransmitSeparator5.Name = "_TransmitSeparator5";
            _TransmitSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // _TransmitDisplayOptionComboBox
            // 
            __TransmitDisplayOptionComboBox.AutoSize = false;
            __TransmitDisplayOptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __TransmitDisplayOptionComboBox.Items.AddRange(new object[] { "HEX", "ASCII", "BOTH" });
            __TransmitDisplayOptionComboBox.Name = "__TransmitDisplayOptionComboBox";
            __TransmitDisplayOptionComboBox.Size = new System.Drawing.Size(61, 23);
            __TransmitDisplayOptionComboBox.ToolTipText = "Transmit display option";
            // 
            // _TransmitSeparator6
            // 
            _TransmitSeparator6.Name = "_TransmitSeparator6";
            _TransmitSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // _TransmitTerminationComboBox
            // 
            _TransmitTerminationComboBox.AutoSize = false;
            _TransmitTerminationComboBox.Items.AddRange(new object[] { "", @"\n", @"\r", @"\r\n" });
            _TransmitTerminationComboBox.Name = "_TransmitTerminationComboBox";
            _TransmitTerminationComboBox.Size = new System.Drawing.Size(51, 23);
            _TransmitTerminationComboBox.ToolTipText = "Append end-of-line characters";
            // 
            // _TransmitSeparator7
            // 
            _TransmitSeparator7.Name = "_TransmitSeparator7";
            _TransmitSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // _TransmitRepeatCountTextBox
            // 
            _TransmitRepeatCountTextBox.AutoSize = false;
            _TransmitRepeatCountTextBox.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TransmitRepeatCountTextBox.Name = "_TransmitRepeatCountTextBox";
            _TransmitRepeatCountTextBox.Size = new System.Drawing.Size(58, 25);
            _TransmitRepeatCountTextBox.Text = "100";
            _TransmitRepeatCountTextBox.ToolTipText = "Repeat the transmitted message.";
            _TransmitRepeatCountTextBox.Visible = false;
            // 
            // _TransmitDelayMillisecondsTextBox
            // 
            _TransmitDelayMillisecondsTextBox.AutoSize = false;
            _TransmitDelayMillisecondsTextBox.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TransmitDelayMillisecondsTextBox.Name = "_TransmitDelayMillisecondsTextBox";
            _TransmitDelayMillisecondsTextBox.Size = new System.Drawing.Size(58, 23);
            _TransmitDelayMillisecondsTextBox.Text = "5000";
            _TransmitDelayMillisecondsTextBox.ToolTipText = "Transmit delay time milliseconds. Applies to repeat messages only. ";
            _TransmitDelayMillisecondsTextBox.Visible = false;
            // 
            // _TransmitPlayButton
            // 
            __TransmitPlayButton.AutoToolTip = false;
            __TransmitPlayButton.CheckOnClick = true;
            __TransmitPlayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __TransmitPlayButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TransmitPlayButton.Image = (System.Drawing.Image)resources.GetObject("_TransmitPlayButton.Image");
            __TransmitPlayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __TransmitPlayButton.Name = "__TransmitPlayButton";
            __TransmitPlayButton.Size = new System.Drawing.Size(38, 22);
            __TransmitPlayButton.Text = "PLAY";
            __TransmitPlayButton.ToolTipText = "Send the messages loaded in the transmit box, as many times as identified in the " + "Repeat box.";
            __TransmitPlayButton.Visible = false;
            // 
            // _TransmitSeparator9
            // 
            _TransmitSeparator9.Name = "_TransmitSeparator9";
            _TransmitSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // PortConsole
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_SplitContainer);
            Controls.Add(_StatusStrip);
            Controls.Add(_ToolStrip);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            MinimumSize = new System.Drawing.Size(835, 327);
            Name = "PortConsole";
            Size = new System.Drawing.Size(889, 782);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            _SplitContainer.Panel1.ResumeLayout(false);
            _SplitContainer.Panel1.PerformLayout();
            _SplitContainer.Panel2.ResumeLayout(false);
            _SplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_SplitContainer).EndInit();
            _SplitContainer.ResumeLayout(false);
            _ReceiveToolStrip.ResumeLayout(false);
            _ReceiveToolStrip.PerformLayout();
            _TransmitContextMenu.ResumeLayout(false);
            _SendDataToolStrip.ResumeLayout(false);
            _SendDataToolStrip.PerformLayout();
            _TransmitToolStrip.ResumeLayout(false);
            _TransmitToolStrip.PerformLayout();
            Load += new EventHandler(Form_Load);
            Resize += new EventHandler(Form_ResizeEnd);
            VisibleChanged += new EventHandler(MessengerConsole_VisibleChanged);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.ToolStrip _ToolStrip;
        private System.Windows.Forms.StatusStrip _StatusStrip;
        private System.Windows.Forms.FontDialog _FontDialog;
        private System.Windows.Forms.SplitContainer _SplitContainer;
        private System.Windows.Forms.ToolStrip _ReceiveToolStrip;
        private System.Windows.Forms.ToolStripLabel _ReceiveTextBoxLabel;
        private System.Windows.Forms.ToolStrip _TransmitToolStrip;
        private System.Windows.Forms.ToolStripLabel _TransmitTextBoxLabel;
        private System.Windows.Forms.ToolStripButton __ClearTransmitBoxButton;

        private System.Windows.Forms.ToolStripButton _ClearTransmitBoxButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearTransmitBoxButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearTransmitBoxButton != null)
                {
                    __ClearTransmitBoxButton.Click -= ClearTransmitBoxButton_Click;
                }

                __ClearTransmitBoxButton = value;
                if (__ClearTransmitBoxButton != null)
                {
                    __ClearTransmitBoxButton.Click += ClearTransmitBoxButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _Separator2;
        private System.Windows.Forms.ToolStripButton __ClearReceiveBoxButton;

        private System.Windows.Forms.ToolStripButton _ClearReceiveBoxButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearReceiveBoxButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearReceiveBoxButton != null)
                {
                    __ClearReceiveBoxButton.Click -= ClearReceiveBoxButton_Click;
                }

                __ClearReceiveBoxButton = value;
                if (__ClearReceiveBoxButton != null)
                {
                    __ClearReceiveBoxButton.Click += ClearReceiveBoxButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _Separator;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator1;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator2;
        private System.Windows.Forms.ToolStripButton __LoadTransmitFileButton;

        private System.Windows.Forms.ToolStripButton _LoadTransmitFileButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadTransmitFileButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadTransmitFileButton != null)
                {
                    __LoadTransmitFileButton.Click -= LoadTransmitFileButton_Click;
                }

                __LoadTransmitFileButton = value;
                if (__LoadTransmitFileButton != null)
                {
                    __LoadTransmitFileButton.Click += LoadTransmitFileButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator3;
        private System.Windows.Forms.ToolStripLabel _TransmitCountLabel;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator4;
        private System.Windows.Forms.ToolStripButton __SaveReceiveTextBoxButton;

        private System.Windows.Forms.ToolStripButton _SaveReceiveTextBoxButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveReceiveTextBoxButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveReceiveTextBoxButton != null)
                {
                    __SaveReceiveTextBoxButton.Click -= SaveReceiveTextBoxButton_Click;
                }

                __SaveReceiveTextBoxButton = value;
                if (__SaveReceiveTextBoxButton != null)
                {
                    __SaveReceiveTextBoxButton.Click += SaveReceiveTextBoxButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _Separator7;
        private System.Windows.Forms.ToolStripLabel _ReceiveCountLabel;
        private System.Windows.Forms.ToolStripSeparator _Separator8;
        private System.Windows.Forms.ToolStripLabel _ReceiveStatusLabel;
        private System.Windows.Forms.ToolStripSeparator _Separator9;
        private System.Windows.Forms.ToolStripLabel _TransmitStatusLabel;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator5;
        private System.Windows.Forms.RichTextBox _ReceiveTextBox;
        private System.Windows.Forms.ToolStrip _SendDataToolStrip;
        private System.Windows.Forms.ToolStripComboBox __TransmitMessageCombo;

        private System.Windows.Forms.ToolStripComboBox _TransmitMessageCombo
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitMessageCombo;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitMessageCombo != null)
                {
                    __TransmitMessageCombo.TextUpdate -= TransmitMessageCombo_TextUpdate;
                    __TransmitMessageCombo.KeyDown -= SuppressDingHandler;
                    __TransmitMessageCombo.KeyUp -= SuppressDingHandler;
                    __TransmitMessageCombo.KeyPress -= TransmitMessageCombo_KeyPress;
                }

                __TransmitMessageCombo = value;
                if (__TransmitMessageCombo != null)
                {
                    __TransmitMessageCombo.TextUpdate += TransmitMessageCombo_TextUpdate;
                    __TransmitMessageCombo.KeyDown += SuppressDingHandler;
                    __TransmitMessageCombo.KeyUp += SuppressDingHandler;
                    __TransmitMessageCombo.KeyPress += TransmitMessageCombo_KeyPress;
                }
            }
        }

        private System.Windows.Forms.RichTextBox __TransmitTextBox;

        private System.Windows.Forms.RichTextBox _TransmitTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitTextBox != null)
                {
                    __TransmitTextBox.MouseClick -= TransmitTextBox_MouseClick;
                }

                __TransmitTextBox = value;
                if (__TransmitTextBox != null)
                {
                    __TransmitTextBox.MouseClick += TransmitTextBox_MouseClick;
                }
            }
        }

        private System.Windows.Forms.ToolStripStatusLabel _StatusLabel;
        private System.Windows.Forms.ToolStripComboBox _PortNamesCombo;
        private System.Windows.Forms.ToolStripLabel _BaudRateComboLabel;
        private System.Windows.Forms.ToolStripComboBox _BaudRateCombo;
        private System.Windows.Forms.ToolStripLabel _ParityComboLabel;
        private System.Windows.Forms.ToolStripComboBox _ParityCombo;
        private System.Windows.Forms.ToolStripLabel _StopBitsComboLabel;
        private System.Windows.Forms.ToolStripComboBox _StopBitsCombo;
        private System.Windows.Forms.ToolStripButton __ConnectButton;

        private System.Windows.Forms.ToolStripButton _ConnectButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConnectButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConnectButton != null)
                {
                    __ConnectButton.Click -= ConnectButton_Click;
                }

                __ConnectButton = value;
                if (__ConnectButton != null)
                {
                    __ConnectButton.Click += ConnectButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.ContextMenuStrip _TransmitContextMenu;
        private System.Windows.Forms.ToolStripMenuItem __TransmitCopyMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _TransmitCopyMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitCopyMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitCopyMenuItem != null)
                {
                    __TransmitCopyMenuItem.Click -= TransmitCopyMenuItem_Click;
                }

                __TransmitCopyMenuItem = value;
                if (__TransmitCopyMenuItem != null)
                {
                    __TransmitCopyMenuItem.Click += TransmitCopyMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __TransmitPasteMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _TransmitPasteMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitPasteMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitPasteMenuItem != null)
                {
                    __TransmitPasteMenuItem.Click -= TransmitPasteMenuItem_Click;
                }

                __TransmitPasteMenuItem = value;
                if (__TransmitPasteMenuItem != null)
                {
                    __TransmitPasteMenuItem.Click += TransmitPasteMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __TransmitCutMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _TransmitCutMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitCutMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitCutMenuItem != null)
                {
                    __TransmitCutMenuItem.Click -= TransmitCutMenuItem_Click;
                }

                __TransmitCutMenuItem = value;
                if (__TransmitCutMenuItem != null)
                {
                    __TransmitCutMenuItem.Click += TransmitCutMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _DataBitsComboLabel;
        private System.Windows.Forms.ToolStripComboBox _DataBitsCombo;
        private System.Windows.Forms.ToolStripMenuItem __TransmitSendMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _TransmitSendMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitSendMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitSendMenuItem != null)
                {
                    __TransmitSendMenuItem.Click -= TransmitSendMenuItem_Click;
                }

                __TransmitSendMenuItem = value;
                if (__TransmitSendMenuItem != null)
                {
                    __TransmitSendMenuItem.Click += TransmitSendMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __TransmitSendSelectionMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _TransmitSendSelectionMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitSendSelectionMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitSendSelectionMenuItem != null)
                {
                    __TransmitSendSelectionMenuItem.Click -= TransmitSendSelectionMenuItem_Click;
                }

                __TransmitSendSelectionMenuItem = value;
                if (__TransmitSendSelectionMenuItem != null)
                {
                    __TransmitSendSelectionMenuItem.Click += TransmitSendSelectionMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _ReceiveDelayComboLabel;
        private System.Windows.Forms.ToolStripComboBox _ReceiveDelayCombo;
        private System.Windows.Forms.ToolStripLabel _ReceiveThresholdComboLabel;
        private System.Windows.Forms.ToolStripComboBox _ReceiveThresholdCombo;
        private System.Windows.Forms.ToolStripTextBox _TransmitRepeatCountTextBox;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator6;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator7;
        private System.Windows.Forms.ToolStripButton __TransmitPlayButton;

        private System.Windows.Forms.ToolStripButton _TransmitPlayButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitPlayButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitPlayButton != null)
                {
                    __TransmitPlayButton.CheckedChanged -= TransmitPlayButton_CheckedChanged;
                }

                __TransmitPlayButton = value;
                if (__TransmitPlayButton != null)
                {
                    __TransmitPlayButton.CheckedChanged += TransmitPlayButton_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripTextBox _TransmitDelayMillisecondsTextBox;
        private System.Windows.Forms.ToolStripSeparator _TransmitSeparator9;
        private System.Windows.Forms.ToolStripButton __RefreshPortListButton;

        private System.Windows.Forms.ToolStripButton _RefreshPortListButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshPortListButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshPortListButton != null)
                {
                    __RefreshPortListButton.Click -= RefreshPortListButton_Click;
                }

                __RefreshPortListButton = value;
                if (__RefreshPortListButton != null)
                {
                    __RefreshPortListButton.Click += RefreshPortListButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripComboBox _TransmitEnterOptionComboBox;
        private System.Windows.Forms.ToolStripComboBox __TransmitDisplayOptionComboBox;

        private System.Windows.Forms.ToolStripComboBox _TransmitDisplayOptionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TransmitDisplayOptionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TransmitDisplayOptionComboBox != null)
                {
                    __TransmitDisplayOptionComboBox.SelectedIndexChanged -= TransmitDisplayOptionComboBox_CheckedChanged;
                }

                __TransmitDisplayOptionComboBox = value;
                if (__TransmitDisplayOptionComboBox != null)
                {
                    __TransmitDisplayOptionComboBox.SelectedIndexChanged += TransmitDisplayOptionComboBox_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripComboBox _TransmitTerminationComboBox;
        private System.Windows.Forms.ToolStripComboBox __ReceiveDisplayOptionComboBox;

        private System.Windows.Forms.ToolStripComboBox _ReceiveDisplayOptionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReceiveDisplayOptionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReceiveDisplayOptionComboBox != null)
                {
                    __ReceiveDisplayOptionComboBox.SelectedIndexChanged -= ReceiveDisplayOptionComboBox_SelectedIndexChanged;
                }

                __ReceiveDisplayOptionComboBox = value;
                if (__ReceiveDisplayOptionComboBox != null)
                {
                    __ReceiveDisplayOptionComboBox.SelectedIndexChanged += ReceiveDisplayOptionComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.Label _ReceiveRulerLabel;
        private System.Windows.Forms.Label _TransmitRulerLabel;
        private System.Windows.Forms.ToolStripButton __LoadConfigButton;

        private System.Windows.Forms.ToolStripButton _LoadConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadConfigButton != null)
                {
                    __LoadConfigButton.Click -= LoadConfigButton_Click;
                }

                __LoadConfigButton = value;
                if (__LoadConfigButton != null)
                {
                    __LoadConfigButton.Click += LoadConfigButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __SaveConfigButton;

        private System.Windows.Forms.ToolStripButton _SaveConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveConfigButton != null)
                {
                    __SaveConfigButton.Click -= SaveConfigButton_Click;
                }

                __SaveConfigButton = value;
                if (__SaveConfigButton != null)
                {
                    __SaveConfigButton.Click += SaveConfigButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripComboBox __FontSizeComboBox;

        private System.Windows.Forms.ToolStripComboBox _FontSizeComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FontSizeComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FontSizeComboBox != null)
                {
                    __FontSizeComboBox.SelectedIndexChanged -= FontSizeComboBox_SelectedIndexChanged;
                }

                __FontSizeComboBox = value;
                if (__FontSizeComboBox != null)
                {
                    __FontSizeComboBox.SelectedIndexChanged += FontSizeComboBox_SelectedIndexChanged;
                }
            }
        }
    }
}