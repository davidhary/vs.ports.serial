using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EscapeSequencesExtensions;
using isr.Ports.Serial.Forms.ExceptionExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Ports.Serial.Forms
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Hosts the <see cref="IPort">port</see>. </summary>
    /// <remarks>
    /// Based on Extended Serial Port Windows Forms Sample
    /// http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37 <para>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public partial class PortConsole : Core.Forma.ModelViewTalkerBase
    {

        /// <summary> Initializes a new instance of the <see cref="PortConsole" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        public PortConsole( IPort port ) : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this._ErrorProvider = new ErrorProvider();
            this.InitializingComponents = false;
            this.BindPortThis( port );
            this._ToolStrip.Renderer = new CustomProfessionalRenderer();
            this.ToggleTransmissionRepeatControlEnabled( false );
            this.__RefreshPortListButton.Name = "_RefreshPortListButton";
            this.__LoadConfigButton.Name = "_LoadConfigButton";
            this.__SaveConfigButton.Name = "_SaveConfigButton";
            this.__ConnectButton.Name = "_ConnectButton";
            this.__ClearReceiveBoxButton.Name = "_ClearReceiveBoxButton";
            this.__SaveReceiveTextBoxButton.Name = "_SaveReceiveTextBoxButton";
            this.__ReceiveDisplayOptionComboBox.Name = "_ReceiveDisplayOptionComboBox";
            this.__FontSizeComboBox.Name = "_FontSizeComboBox";
            this.__TransmitTextBox.Name = "_TransmitTextBox";
            this.__TransmitCopyMenuItem.Name = "_TransmitCopyMenuItem";
            this.__TransmitPasteMenuItem.Name = "_TransmitPasteMenuItem";
            this.__TransmitCutMenuItem.Name = "_TransmitCutMenuItem";
            this.__TransmitSendMenuItem.Name = "_TransmitSendMenuItem";
            this.__TransmitSendSelectionMenuItem.Name = "_TransmitSendSelectionMenuItem";
            this.__TransmitMessageCombo.Name = "_TransmitMessageCombo";
            this.__ClearTransmitBoxButton.Name = "_ClearTransmitBoxButton";
            this.__LoadTransmitFileButton.Name = "_LoadTransmitFileButton";
            this.__TransmitDisplayOptionComboBox.Name = "_TransmitDisplayOptionComboBox";
            this.__TransmitPlayButton.Name = "_TransmitPlayButton";
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="PortConsole" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PortConsole() : this( new Port() )
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( this.components is object )
                        this.components.Dispose();
                    if ( this.Port is object )
                    {
                        if ( this.IsPortOwner )
                            this.Port.Dispose();
                        this.Port = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SHOW IN FORM "

        /// <summary> Shows the control in a modeless form. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="formCaption">    The form caption. </param>
        /// <param name="controlCaption"> The control caption. </param>
        /// <param name="listener">       The listener. </param>
        public static void ShowModeless( string formCaption, string controlCaption, IMessageListener listener )
        {
            PortConsole control = null;
            Core.Forma.ConsoleForm f = null;
            try
            {
                f = new Core.Forma.ConsoleForm();
                control = new PortConsole();
                f.Text = formCaption;
                f.AddTalkerControl( controlCaption, control, true );
                f.AddListener( listener );
                f.Show();
            }
            catch
            {
                control?.Dispose();
                f?.Dispose();
                throw;
            }
        }

        /// <summary> Shows the control in a modal form. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="formCaption">    The form caption. </param>
        /// <param name="controlCaption"> The control caption. </param>
        /// <param name="listener">       The listener. </param>
        public static void ShowModal( string formCaption, string controlCaption, IMessageListener listener )
        {
            using var f = new Core.Forma.ConsoleForm();
            using var control = new PortConsole();
            f.Text = formCaption;
            f.AddTalkerControl( controlCaption, control, true );
            f.AddListener( listener );
            _ = f.ShowDialog();
        }

        /// <summary> Shows the simple read write console. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="listener"> The listener. </param>
        public static void ShowSimpleReadWriteDialog( IMessageListener listener )
        {
            ShowModal( "Serial Port Dialog", "Serial", listener );
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary> Handles the Load event of the form control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {
                this.OnFormShown();
            }
            catch ( Exception ex )
            {
                _ = WindowsForms.ShowDialog( ex );
            }
        }

        /// <summary>
        /// Handles the ResizeEnd event of the form control. Adjusts the character counts.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_ResizeEnd( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "displaying the receive text box ruler";
                this.ReceiveTextBoxCharactersPerLine = this._ReceiveRulerLabel.DisplayRuler( this._ReceiveTextBox.Width, this.ReceiveTextBoxCharacterWidth, this.ReceiveDisplayOption );
                activity = "displaying the transmit text box ruler";
                this.TransmitTextBoxCharactersPerLine = this._TransmitRulerLabel.DisplayRuler( this._TransmitTextBox.Width, this.TransmitTextBoxCharacterWidth, this.TransmitDisplayOption );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Clears the selections. A workaround to prevent highlights. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearSelections()
        {
            this._PortNamesCombo.ComboBox.Select( 0, 0 );
            this._BaudRateCombo.ComboBox.Select( 0, 0 );
            this._DataBitsCombo.ComboBox.Select( 0, 0 );
            this._ParityCombo.ComboBox.Select( 0, 0 );
            this._StopBitsCombo.ComboBox.Select( 0, 0 );
            this._ReceiveDelayCombo.ComboBox.Select( 0, 0 );
            this._ReceiveThresholdCombo.ComboBox.Select( 0, 0 );
            this._TransmitDisplayOptionComboBox.ComboBox.Select( 0, 0 );
            this._TransmitEnterOptionComboBox.ComboBox.Select( 0, 0 );
            this._ReceiveDisplayOptionComboBox.ComboBox.Select( 0, 0 );
        }

        /// <summary> Messenger console visible changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MessengerConsole_VisibleChanged( object sender, EventArgs e )
        {
            this.ClearSelections();
        }

        /// <summary> Handles the display of the controls. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void OnFormShown()
        {

            // set the form caption
            this.Text = ApplicationInfo.BuildApplicationDescriptionCaption( "" );
            this.Text += string.Format( System.Globalization.CultureInfo.CurrentCulture, " Version {0}.{1:00}", My.MyProject.Application.Info.Version.Major, My.MyProject.Application.Info.Version.Minor );
            this._StatusLabel.Text = "closed";
            this._TransmitTerminationComboBox.Text = Serial.My.MySettings.Default.Termination;
            this.TransmitEnterOption = ( DisplayOptions ) Conversions.ToInteger( Serial.My.MySettings.Default.EnterOption );
            this.TransmitDisplayOption = ( DisplayOptions ) Conversions.ToInteger( Serial.My.MySettings.Default.TransmitDisplayOption );
            this.ClearTransmitBox();
            this.ReceiveDisplayOption = ( DisplayOptions ) Conversions.ToInteger( Serial.My.MySettings.Default.ReceiveDisplayOption );
            this.ClearReceiveBox();
            this.SelectFontSize( Serial.My.MySettings.Default.FontSize );

            // list the port names.
            _ = this.ListPortNames();

            // clear the selections
            this.ClearSelections();
            this.OnConnectionChanged( this.Port.IsOpen );
        }

        #endregion

        #region " TRANSMIT MANAGEMENT "

        /// <summary> The delimiter. </summary>
        private const string _Delimiter = " ";

        /// <summary>
        /// Handles the Click event of the _TransmitCopyMenuItem control. Copies a transmit box selection.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void TransmitCopyMenuItem_Click( object sender, EventArgs e )
        {
            this._TransmitTextBox.Copy();
        }

        /// <summary>
        /// Handles the Click event of the _TransmitPasteMenuItem control. Pastes a transmit box
        /// selection.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void TransmitPasteMenuItem_Click( object sender, EventArgs e )
        {
            this._TransmitTextBox.Paste();
        }

        /// <summary>
        /// Handles the Click event of the _TransmitCutMenuItem control. Cuts the transmit text box item.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void TransmitCutMenuItem_Click( object sender, EventArgs e )
        {
            this._TransmitTextBox.Cut();
        }

        /// <summary>
        /// Handles the Click event of the _TransmitSendMenuItem control. Sends position of caret line
        /// from transmit text box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitSendMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "setting transmission data from text line";
                int loc = this._TransmitTextBox.GetFirstCharIndexOfCurrentLine();
                int ln = this._TransmitTextBox.GetLineFromCharIndex( loc );
                if ( 0 <= ln && ln < this._TransmitTextBox.Lines.Count() )
                {
                    activity = "transmitting text line";
                    this.SendData( this._TransmitTextBox.Lines[ln], this.EnterTransmitHexData );
                    activity = "all text sent";
                    this._TransmitMessageCombo.Text = string.Empty;
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Sends only selection in transmit box to com. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitSendSelectionMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "setting transmission data from selected text";
                if ( this._TransmitTextBox.SelectionLength > 0 )
                {
                    activity = "sending selected text";
                    this.SendData( this._TransmitTextBox.SelectedText, this.EnterTransmitHexData );
                    activity = "select text sent";
                    this._TransmitMessageCombo.Text = string.Empty;
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> fetches a line from the transmit box to the transmit message box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Mouse event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitTextBox_MouseClick( object sender, MouseEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "fetching transmission data from text box";
                if ( sender is RichTextBox rb )
                {
                    int loc = rb.GetFirstCharIndexOfCurrentLine();
                    int ln = rb.GetLineFromCharIndex( loc );
                    if ( 0 <= ln && ln < rb.Lines.Count() )
                    {
                        activity = "updating transmission combo box";
                        this._TransmitMessageCombo.Text = rb.Lines[ln];
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Clears the transmit box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearTransmitBox()
        {
            string activity = string.Empty;
            try
            {
                activity = "clearing the Transmit text box";
                this._TransmitTextBox.Clear();
                activity = "displaying the Transmit text box ruler";
                this.TransmitTextBoxCharactersPerLine = this._TransmitRulerLabel.DisplayRuler( this._TransmitTextBox.Width, this.TransmitTextBoxCharacterWidth, this.TransmitDisplayOption );
                this.TransmitCount = 0;
                activity = "setting Transmit status image";
                this._TransmitStatusLabel.Image = this.IsPortOpen ? My.Resources.Resources.LedCornerOrange : My.Resources.Resources.LedCornerGray;
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _ClearTransmitBoxButton control. Clears the transmit box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearTransmitBoxButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "clearing transmission text box";
                this.ClearTransmitBox();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Build hex string in the transmit box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TransmitMessageCombo_TextUpdate( object sender, EventArgs e )
        {
            if ( this.HasNewHexCharacter && this.EnterTransmitHexData )
            {
                if ( sender is ToolStripComboBox cb )
                {
                    cb.Text = cb.Text.ToByteFormatted( _Delimiter );
                    cb.SelectionStart = cb.Text.Length;
                }
            }
        }

        /// <summary> Gets or sets the transmit enter option. </summary>
        /// <value> The transmit enter option. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DisplayOptions TransmitEnterOption
        {
            get => ( DisplayOptions ) Conversions.ToInteger( this._TransmitEnterOptionComboBox.SelectedIndex + 1 );

            set {
                if ( value != this.TransmitEnterOption )
                {
                    this._TransmitEnterOptionComboBox.SelectedIndex = ( int ) value - 1;
                }
            }
        }

        /// <summary> Gets information describing the enter transmit hexadecimal. </summary>
        /// <value> Information describing the enter transmit hexadecimal. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool EnterTransmitHexData => DisplayOptions.Hex == (this.TransmitEnterOption & DisplayOptions.Hex);

        /// <summary> Gets or sets the has new hexadecimal character. </summary>
        /// <value> The has new hexadecimal character. </value>
        private bool HasNewHexCharacter { get; set; }

        /// <summary>
        /// Event handler to suppress ding sound on enter. Called by _LotNumberTextBox for key down
        /// events.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SuppressDingHandler( object sender, KeyEventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "handling transmit message combo down";
                if ( e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return )
                {
                    // this suppresses the bell.
                    e.Handled = true;
                    // do not suppress the key press, just the key bell.
                    e.SuppressKeyPress = false;
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the KeyPress event of the cboEnterMessage control. Enter only allowed keys in hex
        /// mode.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="KeyPressEventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitMessageCombo_KeyPress( object sender, KeyPressEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "building data";
                this.HasNewHexCharacter = false;
                if ( Conversions.ToString( e.KeyChar ) != Constants.vbCr )
                {
                    if ( this.EnterTransmitHexData )
                    {
                        this.HasNewHexCharacter = e.KeyChar.IsHexadecimal();
                        if ( char.IsControl( e.KeyChar ) )
                        {
                        }
                        else if ( char.IsLetterOrDigit( e.KeyChar ) && !e.KeyChar.IsHexadecimal() )
                        {
                            e.Handled = true;
                        }
                        else if ( char.IsPunctuation( e.KeyChar ) )
                        {
                            e.Handled = true;
                        }
                    }
                }
                else
                {
                    activity = "sending data";
                    this.SendData( this._TransmitMessageCombo.Text, this.EnterTransmitHexData );
                    this._TransmitMessageCombo.Text = string.Empty;
                    e.Handled = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the CheckedChanged event of the _TransmitShowHEXToolStripMenuItem control. Toggles
        /// showing the transmit data in hex.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitDisplayOptionComboBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "displaying the transmit text box ruler";
                this.TransmitTextBoxCharactersPerLine = this._TransmitRulerLabel.DisplayRuler( this._TransmitTextBox.Width, this.TransmitTextBoxCharacterWidth, this.TransmitDisplayOption );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets or sets the transmit display option. </summary>
        /// <value> The transmit display option. </value>
        public DisplayOptions TransmitDisplayOption
        {
            get => ( DisplayOptions ) Conversions.ToInteger( this._TransmitDisplayOptionComboBox.SelectedIndex + 1 );

            set => this._TransmitDisplayOptionComboBox.SelectedIndex = ( int ) value - 1;
        }

        /// <summary> Gets information describing the display transmit hexadecimal. </summary>
        /// <value> Information describing the display transmit hexadecimal. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool DisplayTransmitHexData => DisplayOptions.Hex == (this.TransmitDisplayOption & DisplayOptions.Hex);

        /// <summary> Number of transmits. </summary>
        private int _TransmitCount;

        /// <summary> Gets or sets the transmit count. </summary>
        /// <value> The transmit count. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int TransmitCount
        {
            get => this._TransmitCount;

            set {
                this._TransmitCount = value;
                this._TransmitCountLabel.Text = $"{this.TransmitCount:D6}";
            }
        }

        /// <summary> Sends the data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="textData">  The text data. </param>
        /// <param name="isHexData"> True if is hexadecimal data, false if not. </param>
        public void SendData( string textData, bool isHexData )
        {
            if ( string.IsNullOrWhiteSpace( textData ) )
            {
            }
            else if ( !this.IsPortOpen )
            {
                throw new InvalidOperationException( "Port not connected" );
            }
            else
            {
                var data = new List<byte>();
                if ( isHexData )
                {
                    data.AddRange( textData.RemoveDelimiter( _Delimiter ).ToHexBytes() );
                    if ( data[0] == Methods.ParseErrorValue )
                        _ = this.PublishWarning( $"failed converting '{textData}'; '{( char ) data[1]}' is not a valid HEX character" );
                }
                else
                {
                    data.AddRange( Encoding.ASCII.GetBytes( textData ) );
                }

                data.AddRange( this._TransmitTerminationComboBox.Text.CommonEscapeValues() );
                this.Port.SendData( data );
            }
            // Store the data in the transmit combo box.
            _ = this._TransmitMessageCombo.Items.Add( textData );
        }

        #endregion

        #region " RECEIVE HANDLERS "

        /// <summary> Number of receives. </summary>
        private int _ReceiveCount;

        /// <summary> Gets or sets the Receive count. </summary>
        /// <value> The Receive count. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int ReceiveCount
        {
            get => this._ReceiveCount;

            set {
                this._ReceiveCount = value;
                this._ReceiveCountLabel.Text = $"{this.ReceiveCount:D6}";
            }
        }

        /// <summary> Clears the transmit box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearReceiveBox()
        {
            string activity = string.Empty;
            try
            {
                activity = "clearing the receive text box";
                this._ReceiveTextBox.Clear();
                activity = "displaying the receive text box ruler";
                this.ReceiveTextBoxCharactersPerLine = this._ReceiveRulerLabel.DisplayRuler( this._ReceiveTextBox.Width, this.ReceiveTextBoxCharacterWidth, this.ReceiveDisplayOption );
                this.ReceiveCount = 0;
                activity = "setting receive status image";
                this._ReceiveStatusLabel.Image = this.IsPortOpen ? My.Resources.Resources.LedCornerOrange : My.Resources.Resources.LedCornerGray;
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _ClearReceiveBoxButton control. clear receive box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void ClearReceiveBoxButton_Click( object sender, EventArgs e )
        {
            this.ClearReceiveBox();
        }

        /// <summary> Gets or sets the receive display option. </summary>
        /// <value> The receive display option. </value>
        public DisplayOptions ReceiveDisplayOption
        {
            get => ( DisplayOptions ) Conversions.ToInteger( this._ReceiveDisplayOptionComboBox.SelectedIndex + 1 );

            set => this._ReceiveDisplayOptionComboBox.SelectedIndex = ( int ) value - 1;
        }

        /// <summary> Gets information describing the display receive hexadecimal. </summary>
        /// <value> Information describing the display receive hexadecimal. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool DisplayReceiveHexData => DisplayOptions.Hex == (this.ReceiveDisplayOption & DisplayOptions.Hex);

        /// <summary> Receive display option combo box index changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReceiveDisplayOptionComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "displaying the receive text box ruler";
                this.ReceiveTextBoxCharactersPerLine = this._ReceiveRulerLabel.DisplayRuler( this._ReceiveTextBox.Width, this.ReceiveTextBoxCharacterWidth, this.ReceiveDisplayOption );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _SaveReceiveTextBoxButton control. Save receive box to file.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveReceiveTextBoxButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "instantiating the save received data dialog";
                using var saveFileDialog = new System.Windows.Forms.SaveFileDialog {
                    DefaultExt = "*.TXT",
                    Filter = "Text files (*.txt)|*.txt"
                };
                activity = "showing the save received data dialog";
                if ( saveFileDialog.ShowDialog() == DialogResult.OK )
                {
                    string fullpath = saveFileDialog.FileName;
                    activity = $"saving received data to {fullpath}";
                    this._ReceiveTextBox.SaveFile( fullpath, RichTextBoxStreamType.PlainText );
                    this._StatusLabel.Text = System.IO.Path.GetFileName( fullpath ) + " written";
                }
                else
                {
                    this._StatusLabel.Text = "no data chosen";
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _LoadTransmitFileButton control. load file into transmit box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadTransmitFileButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "instantiating open dialog for transmit data";
                using var openFileDialog = new System.Windows.Forms.OpenFileDialog {
                    DefaultExt = "*.TXT",
                    Filter = "Text files (*.txt)|*.txt"
                };
                activity = "opening transmit data file dialog";
                if ( openFileDialog.ShowDialog() == DialogResult.OK )
                {
                    string fullpath = openFileDialog.FileName;
                    this._TransmitTextBox.Clear();
                    activity = $"loading transmit data from {fullpath}";
                    this._TransmitTextBox.LoadFile( fullpath, RichTextBoxStreamType.PlainText );
                }
                else
                {
                    this._StatusLabel.Text = "no data chosen";
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Handles the Click event of the _ConnectButton control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConnectButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "handing connect button click";
                this.OnConnecting( this.IsPortOpen );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Applies the port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A PortParametersDictionary. </returns>
        private PortParametersDictionary BuildPortParamters()
        {
            var result = this.Port.ToPortParameters();
            result.PortName = this._PortNamesCombo.Text;
            result.BaudRate = Conversions.ToInteger( this._BaudRateCombo.Text );
            result.DataBits = Conversions.ToInteger( this._DataBitsCombo.Text );
            result.Parity = PortParametersDictionary.ParseParity( this._ParityCombo.Text );
            result.StopBits = PortParametersDictionary.ParseStopBits( this._StopBitsCombo.Text );
            result.ReceiveDelay = Conversions.ToInteger( this._ReceiveDelayCombo.Text );
            result.ReceivedBytesThreshold = Conversions.ToInteger( this._ReceiveThresholdCombo.Text );
            return result;
        }

        /// <summary> Process connecting. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnConnecting( bool isConnected )
        {
            string activity = string.Empty;
            this._ErrorProvider.Clear();
            var e = new ActionEventArgs();
            try
            {
                if ( isConnected )
                {
                    if ( this.Port is object )
                    {
                        activity = $"disconnecting {this.Port.SerialPort?.PortName}";
                        _ = this.Port.TryClose( e );
                    }
                }
                else
                {
                    activity = $"connecting {this._PortNamesCombo.Text}";
                    if ( this.Port is null )
                    {
                        _ = this.Publish( TraceEventType.Warning, $"Failed {activity}; Port class is nothing" );
                    }
                    else
                    {
                        activity = $"building port parameters for {this._PortNamesCombo.Text}";
                        activity = $"connecting {this._PortNamesCombo.Text}";
                        _ = this.Port.TryOpen( this.BuildPortParamters(), e );
                    }
                }

                if ( e.Failed )
                    _ = this.Publish( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the change of connection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
        private void OnConnectionChanged( bool isConnected )
        {
            this._ErrorProvider.Clear();
            if ( isConnected )
            {
                this._ParityCombo.Enabled = false;
                this._StopBitsCombo.Enabled = false;
                this._PortNamesCombo.Enabled = false;
                this._BaudRateCombo.Enabled = false;
                this._DataBitsCombo.Enabled = false;
                this._ReceiveDelayCombo.Enabled = false;
                this._ReceiveThresholdCombo.Enabled = false;
                this._ConnectButton.Image = My.Resources.Resources.LedCornerGreen;
                this._ReceiveStatusLabel.Image = My.Resources.Resources.LedCornerOrange;
                this._TransmitStatusLabel.Image = My.Resources.Resources.LedCornerOrange;
                if ( this.Port is object )
                    this.ShowPortParameters( this.Port.ToPortParameters() );
            }
            else
            {
                this._ParityCombo.Enabled = true;
                this._StopBitsCombo.Enabled = true;
                this._PortNamesCombo.Enabled = true;
                this._BaudRateCombo.Enabled = true;
                this._DataBitsCombo.Enabled = true;
                this._ReceiveDelayCombo.Enabled = true;
                this._ReceiveThresholdCombo.Enabled = true;
                this._ConnectButton.Image = My.Resources.Resources.LedCornerGray;
                this._ReceiveStatusLabel.Image = My.Resources.Resources.LedCornerGray;
                this._TransmitStatusLabel.Image = My.Resources.Resources.LedCornerGray;
                this._StatusLabel.Text = "Disconnected";
            }
        }

        #endregion

        #region " PORT PARAMETERS FILE MANAGEMENT "

        /// <summary> load configuration. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadConfigButton_Click( object sender, EventArgs e )
        {
            string activity = $"loading port parameters from file";
            try
            {
                var args = new ActionEventArgs();
                if ( !this.Port.TryRestorePortParameters( args ) )
                    _ = this.Publish( args );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> save port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveConfigButton_Click( object sender, EventArgs e )
        {
            string activity = $"saving port parameters to file";
            try
            {
                var args = new ActionEventArgs();
                if ( !this.Port.TryStorePortParameters( args ) )
                    _ = this.Publish( args );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }


        #endregion

        #region " COM PORT MANAGEMENT "

        /// <summary> Gets the port. </summary>
        /// <value> The port. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public IPort Port { get; private set; }

        /// <summary> Gets the is port that owns this item. </summary>
        /// <value> The is port owner. </value>
        private bool IsPortOwner { get; set; }

        /// <summary> Bind port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        public void BindPort( IPort port )
        {
            this.BindPortThis( port );
            this.IsPortOwner = false;
        }

        /// <summary> Bind port this. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        private void BindPortThis( IPort port )
        {
            if ( this.Port is object )
            {
                this.Port.PropertyChanged -= this.PortPropertyChanged;
                this.Port.ConnectionChanged -= this.Port_ConnectionChanged;
                this.Port.DataReceived -= this.Port_DataReceived;
                this.Port.DataSent -= this.Port_DataSent;
                this.Port.SerialPortErrorReceived -= this.Port_SerialPortErrorReceived;
                this.Port.Timeout -= this.Port_Timeout;
                if ( this.IsPortOwner )
                    this.Port.Dispose();
                this.Port = null;
            }

            this.Port = port;
            if ( this.Port is null )
            {
                this.OnConnectionChanged( this.IsPortOpen );
            }
            else
            {
                this.Port.PropertyChanged += this.PortPropertyChanged;
                this.Port.ConnectionChanged += this.Port_ConnectionChanged;
                this.Port.DataReceived += this.Port_DataReceived;
                this.Port.DataSent += this.Port_DataSent;
                this.Port.SerialPortErrorReceived += this.Port_SerialPortErrorReceived;
                this.Port.Timeout += this.Port_Timeout;
                this.HandlePropertyChanged( this.Port, nameof( IPort.IsOpen ) );
                this.HandlePropertyChanged( this.Port, nameof( IPort.PortParametersFileName ) );
                this.HandlePropertyChanged( this.Port, nameof( IPort.SupportedBaudRates ) );
                this.HandlePropertyChanged( this.Port, nameof( IPort.InputBufferingOption ) );
            }

            this.IsPortOwner = true;
            this.BindPortParameters( port );
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( IPort sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( IPort.InputBufferingOption ):
                    {
                        break;
                    }

                case nameof( IPort.IsOpen ):
                    {
                        this.OnConnectionChanged( sender.IsOpen );
                        break;
                    }

                case nameof( IPort.SupportedBaudRates ):
                    {
                        this._BaudRateCombo.ComboBox.DataSource = null;
                        this._BaudRateCombo.Items.Clear();
                        this._BaudRateCombo.ComboBox.DataSource = sender.SupportedBaudRates;
                        break;
                    }
            }
        }

        /// <summary> Port property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.PropertyChanged )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.PortPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as IPort, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets the is port open. </summary>
        /// <value> The is port open. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsPortOpen => this.Port is object && this.Port.IsOpen;

        /// <summary> Gets or sets options for controlling the port. </summary>
        /// <value> Options that control the port. </value>
        private PortParametersDictionary PortParameters { get; set; }

        /// <summary> Bind port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        private void BindPortParameters( IPort port )
        {
            if ( this.PortParameters is object )
            {
                this.PortParameters.PropertyChanged -= this.PortPropertyChanged;
                this.PortParameters = null;
            }

            if ( port is object )
            {
                this.PortParameters = port.PortParameters;
                this.PortParameters.PropertyChanged += this.PortParametersPropertyChanged;
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.BaudRate ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.DataBits ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.Handshake ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.Parity ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.PortName ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReadBufferSize ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReadTimeout ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReceivedBytesThreshold ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReceiveDelay ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.RtsEnable ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.StopBits ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.WriteBufferSize ) );
            }

            this.IsPortOwner = true;
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( PortParametersDictionary sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( PortParametersDictionary.BaudRate ):
                    {
                        this._BaudRateCombo.Text = sender.BaudRate.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.DataBits ):
                    {
                        this._DataBitsCombo.Text = sender.DataBits.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.Handshake ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.Parity ):
                    {
                        this._ParityCombo.Text = sender.Parity.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.PortName ):
                    {
                        this._PortNamesCombo.Text = sender.PortName;
                        break;
                    }

                case nameof( PortParametersDictionary.ReadBufferSize ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.ReadTimeout ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.ReceivedBytesThreshold ):
                    {
                        this._ReceiveThresholdCombo.Text = sender.ReceivedBytesThreshold.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.ReceiveDelay ):
                    {
                        this._ReceiveDelayCombo.Text = sender.ReceiveDelay.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.RtsEnable ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.StopBits ):
                    {
                        this._StopBitsCombo.Text = sender.StopBits.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.WriteBufferSize ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Port parameters property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortParametersPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( PortParametersDictionary )}.{nameof( PortParametersDictionary.PropertyChanged )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.PortPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as PortParametersDictionary, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Port event information. </param>
        private void HandleDataReceived( IPort sender, PortEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            if ( e.StatusOkay )
            {
                this._ReceiveStatusLabel.Image = My.Resources.Resources.LedCornerGreen;
                if ( e.DataBuffer is object )
                {
                    this.ReceiveCount += e.DataBuffer.Count();
                    this._ReceiveTextBox.AppendBytes( e.DataBuffer, this.Port.SerialPort.Encoding, this.ReceiveTextBoxCharactersPerLine, this.ReceiveDisplayOption );
                }
            }
            else
            {
                this._ReceiveStatusLabel.Image = My.Resources.Resources.LedCornerRed;
            }
        }

        /// <summary>
        /// Handles the Data Received event of the _serialPort control. Updates the data boxes and the
        /// received status.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="PortEventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_DataReceived( object sender, PortEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.DataReceived )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PortEventArgs>( this.Port_DataReceived ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleDataReceived( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the data sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Port event information. </param>
        private void HandleDataSent( IPort sender, PortEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            if ( e.StatusOkay )
            {
                this._TransmitStatusLabel.Image = My.Resources.Resources.LedCornerGreen;
                this.TransmitCount += e.DataBuffer.Count();
                this._TransmitStatusLabel.Image = My.Resources.Resources.LedCornerGray;
                this._TransmitTextBox.AppendBytes( e.DataBuffer, this.Port.SerialPort.Encoding, this.TransmitTextBoxCharactersPerLine, this.TransmitDisplayOption );
            }
            else
            {
                this._TransmitStatusLabel.Image = My.Resources.Resources.LedCornerRed;
            }
        }

        /// <summary>
        /// Handles the Data Sent event of the _serialPort control. Updates controls and data.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="PortEventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_DataSent( object sender, PortEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.DataSent )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PortEventArgs>( this.Port_DataSent ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleDataSent( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the connection changed described by e. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Connection event information. </param>
        private void HandleConnectionChanged( ConnectionEventArgs e )
        {
            if ( e is null )
                return;
            this.OnConnectionChanged( e.IsPortOpen );
        }

        /// <summary> Handles the Connection Changed event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="ConnectionEventArgs" /> instance containing the event
        /// data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_ConnectionChanged( object sender, ConnectionEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.ConnectionChanged )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, ConnectionEventArgs>( this.Port_ConnectionChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleConnectionChanged( e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the serial port error received described by e. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Serial error received event information. </param>
        private void HandleSerialPortErrorReceived( IPort sender, SerialErrorReceivedEventArgs e )
        {
            if ( sender is null || sender.SerialPort is null || e is null )
                return;
            string message = $"Serial port error received from port {sender.SerialPort.PortName}: {e}";
            _ = this.Talker is null
                ? My.MyLibrary.Appliance.LogUnpublishedMessage( TraceEventType.Warning, My.MyLibrary.TraceEventId, message )
                : this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, message );
        }

        /// <summary> Port serial port error received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Serial error received event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_SerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( Serial.Port.SerialPortErrorReceived )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, SerialErrorReceivedEventArgs>( this.Port_SerialPortErrorReceived ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleSerialPortErrorReceived( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void HandleTimeout( IPort sender, EventArgs e )
        {
            if ( sender is null || sender.SerialPort is null || e is null )
                return;
            string message = $"Serial port {sender.SerialPort.PortName} timeout";
            _ = this.Talker is null
                ? My.MyLibrary.Appliance.LogUnpublishedMessage( TraceEventType.Warning, My.MyLibrary.TraceEventId, message )
                : this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, message );
        }

        /// <summary> Port timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_Timeout( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( Serial.Port.Timeout )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, EventArgs>( this.Port_Timeout ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleTimeout( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> List port names. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String() </returns>
        private string[] ListPortNames()
        {
            var portNames = this._PortNamesCombo.ComboBox.ListPortNames( this.Port.SerialPort.PortName );
            this._PortNamesCombo.Items.Clear();
            this._PortNamesCombo.Items.AddRange( portNames );
            // read available ports on system
            if ( portNames.Length > 0 )
            {
                this._PortNamesCombo.Text = portNames.Contains( Serial.My.MySettings.Default.PortName, StringComparer.OrdinalIgnoreCase ) ? Serial.My.MySettings.Default.PortName : portNames[0];
            }
            else
            {
                this._StatusLabel.Text = "no ports detected";
                this._PortNamesCombo.Text = "NO PORTS";
                this._ConnectButton.Text = "NO PORTS";
                this._ConnectButton.Enabled = false;
            }

            return portNames;
        }

        /// <summary> Refresh port list button click. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshPortListButton_Click( object sender, EventArgs e )
        {
            _ = this.ListPortNames();
        }


        #endregion

        #region " DISPLAY MANAGEMENT "

        /// <summary> Width of the receive text box character. </summary>
        private float _ReceiveTextBoxCharacterWidth;

        /// <summary> Gets or sets the average width of the receive text box character. </summary>
        /// <value> The average width of the receive text box character. </value>
        private float ReceiveTextBoxCharacterWidth
        {
            get {
                if ( this._ReceiveTextBoxCharacterWidth == 0f )
                {
                    this._ReceiveTextBoxCharacterWidth = EstimateCharacterWidth( this._ReceiveTextBox );
                }

                return this._ReceiveTextBoxCharacterWidth;
            }

            set => this._ReceiveTextBoxCharacterWidth = value;
        }

        /// <summary> Width of the transmit text box character. </summary>
        private float _TransmitTextBoxCharacterWidth;

        /// <summary> Gets or sets the average width of the Transmit text box character. </summary>
        /// <value> The average width of the Transmit text box character. </value>
        private float TransmitTextBoxCharacterWidth
        {
            get {
                if ( this._TransmitTextBoxCharacterWidth == 0f )
                {
                    this._TransmitTextBoxCharacterWidth = EstimateCharacterWidth( this._TransmitTextBox );
                }

                return this._TransmitTextBoxCharacterWidth;
            }

            set => this._TransmitTextBoxCharacterWidth = value;
        }

        /// <summary> Estimate character width. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="textBox"> The text box control. </param>
        /// <returns> A Single. </returns>
        private static float EstimateCharacterWidth( RichTextBox textBox )
        {
            float result = 10f;
            using ( var g = textBox.CreateGraphics() )
            {
                // measure with a test string
                var szF = g.MeasureString( "0123456789", textBox.Font );
                result = 0.1f * szF.Width;
            }

            return result;
        }

        /// <summary> Gets or sets the transmit panel characters per line. </summary>
        /// <value> The transmit panel characters per line. </value>
        private int TransmitTextBoxCharactersPerLine { get; set; }

        /// <summary> Gets or sets the receive panel characters per line. </summary>
        /// <value> The receive panel characters per line. </value>
        private int ReceiveTextBoxCharactersPerLine { get; set; }

        /// <summary> Selected font. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Font. </returns>
        private static Font SelectedFont( FontSize value )
        {
            Font result;
            switch ( value )
            {
                case FontSize.Large:
                    {
                        result = new Font( Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.LargeFontSize, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
                        break;
                    }

                case FontSize.Medium:
                    {
                        result = new Font( Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.MediumFontSize, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
                        break;
                    }

                case FontSize.Small:
                    {
                        result = new Font( Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.SmallFontSize, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
                        break;
                    }

                default:
                    {
                        result = new Font( Serial.My.MySettings.Default.FontName, Serial.My.MySettings.Default.MediumFontSize, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
                        break;
                    }
            }

            return result;
        }

        /// <summary> Values that represent font sizes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private enum FontSize
        {

            /// <summary> An enum constant representing the small option. </summary>
            Small,

            /// <summary> An enum constant representing the medium option. </summary>
            Medium,

            /// <summary> An enum constant representing the large option. </summary>
            Large
        }

        /// <summary> Select font size. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void SelectFontSize( int value )
        {
            if ( Enum.IsDefined( typeof( FontSize ), value ) )
            {
                this.SelectFontSize( ( FontSize ) Conversions.ToInteger( value ) );
            }
        }

        /// <summary> Select font size. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void SelectFontSize( FontSize value )
        {
            this._ReceiveTextBox.Font = SelectedFont( value );
            this._ReceiveRulerLabel.Font = this._ReceiveTextBox.Font;
            this._TransmitTextBox.Font = this._ReceiveTextBox.Font;
            this._TransmitRulerLabel.Font = this._ReceiveTextBox.Font;
            this.ReceiveTextBoxCharacterWidth = EstimateCharacterWidth( this._ReceiveTextBox );
            this.TransmitTextBoxCharacterWidth = EstimateCharacterWidth( this._TransmitTextBox );
            Serial.My.MySettings.Default.FontSize = ( int ) value;
            if ( this._FontSizeComboBox.SelectedIndex != ( int ) value )
            {
                this.InitializingComponents = true;
                this._FontSizeComboBox.SelectedIndex = ( int ) value;
                this.InitializingComponents = false;
            }
        }

        /// <summary> Font size combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FontSizeComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = "selecting font size";
            try
            {
                if ( sender is ToolStripComboBox combo && combo.SelectedIndex > 0 )
                {
                    this.SelectFontSize( combo.SelectedIndex );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Shows the Messenger parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portParameters"> Options for controlling the port. </param>
        private void ShowPortParameters( PortParametersDictionary portParameters )
        {
            var s = new StringBuilder();
            _ = s.Append( "Connected using " );
            _ = s.Append( portParameters[PortParameterKey.PortName] );
            _ = s.Append( ";" );
            _ = s.Append( portParameters[PortParameterKey.BaudRate] );
            _ = s.Append( ";" );
            _ = s.Append( portParameters[PortParameterKey.DataBits] );
            _ = s.Append( ";" );
            _ = s.Append( portParameters[PortParameterKey.StopBits] );
            _ = s.Append( ";" );
            _ = s.Append( portParameters[PortParameterKey.Parity] );
            this._StatusLabel.Text = s.ToString();
        }

        #endregion

        #region " TRANSMIT REPEAT "

        /// <summary> Toggle transmission repeat control enabled. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToggleTransmissionRepeatControlEnabled()
        {
            this.ToggleTransmissionRepeatControlEnabled( !this._TransmitPlayButton.Visible );
        }

        /// <summary> Toggle transmission repeat control enabled. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="enabled"> True to enable, false to disable. </param>
        public void ToggleTransmissionRepeatControlEnabled( bool enabled )
        {
            this._TransmitPlayButton.Visible = enabled;
            this._TransmitDelayMillisecondsTextBox.Visible = enabled;
            this._TransmitRepeatCountTextBox.Visible = enabled;
            this._TransmitSeparator9.Visible = enabled;
        }

        /// <summary> Transmit play button checked changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TransmitPlayButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( this._TransmitPlayButton.Enabled && this._TransmitPlayButton.Visible )
            {
                this._TransmitPlayButton.Text = this._TransmitPlayButton.Checked ? "STOP" : "PLAY";
            }
        }

        #endregion

        #region " MESSAGE NOTIFICATIONS "

        private readonly ErrorProvider _ErrorProvider;


        /// <summary> Enunciates the specified control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="control">            The control. </param>
        /// <param name="errorIconAlignment"> The error icon alignment. </param>
        /// <param name="padding">            The padding. </param>
        /// <param name="e">                  The <see cref="TraceMessageEventArgs" /> instance containing
        /// the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void Enunciate( Control control, ErrorIconAlignment errorIconAlignment, int padding, TraceMessage e )
        {
            if ( e.EventType <= TraceEventType.Warning )
            {
                this._ErrorProvider.SetIconAlignment( control, errorIconAlignment );
                this._ErrorProvider.SetIconPadding( control, padding );
                this._ErrorProvider.SetError( control, e.Details );
            }
            else
            {
                this._ErrorProvider.Clear();
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds a listener to the device manager devices. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="listener"> The value. </param>
        public override void AddListener( IMessageListener listener )
        {
            this.Port.AddListener( listener );
            base.AddListener( listener );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

        #region " TOOL STRIP RENDERER "

        /// <summary> A custom professional renderer. </summary>
        /// <remarks>
        /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2017-10-30 </para>
        /// </remarks>
        private class CustomProfessionalRenderer : ToolStripProfessionalRenderer
        {

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
            /// event.
            /// </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
            /// contains the event data. </param>
            protected override void OnRenderLabelBackground( ToolStripItemRenderEventArgs e )
            {
                if ( e is object && e.Item.BackColor != SystemColors.ControlDark )
                {
                    using var brush = new SolidBrush( e.Item.BackColor );
                    e.Graphics.FillRectangle( brush, e.Item.ContentRectangle );
                }
            }
        }

        #endregion

    }
}
