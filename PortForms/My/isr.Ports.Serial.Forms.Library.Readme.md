ISR Ports Serial Forms Library<sub>&trade;</sub>

* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*2.2.6969 2019-01-30*  
Split off from the serial library

\(C\) 2012 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

The source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Serial Libraries](http://git%20clone%20git@bitbucket.org:davidhary/vs.ports.serial.git)  
[Serial Port Sample in VB.NET
(C\#)](http://code.MSDN.microsoft.com/SerialPort-Sample-in-VBNET-fb040fb2)  
[Serial Port Windows Forms Application
II](http://code.MSDN.microsoft.com/SerialPort-Windows-Forms-a43f208e)  
[Extended Serial Port Windows Forms
Sample](http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37)  
[Circular Collection](http://www.INAV.NET)  
[Core Libraries](http://git%20clone%20git@bitbucket.org:davidhary/vs.core.git)
