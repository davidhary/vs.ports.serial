﻿''' <summary> Collection of bytes. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-07-06 </para>
''' </remarks>
Public Class ByteCollection
    Inherits Collections.ObjectModel.Collection(Of Byte)

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> An IEnumerable(OfByte) of items to append to this. </param>
    Public Sub New(ByVal values As IEnumerable(Of Byte))
        Me.New()
        Me.AddRange(values)
    End Sub

    ''' <summary> Adds a range. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> An IEnumerable(OfByte) of items to append to this. </param>
    Public Sub AddRange(ByVal values As IEnumerable(Of Byte))
        If values?.Any Then
            For Each v As Byte In values
                Me.Add(v)
            Next
        End If
    End Sub

#End Region


End Class
