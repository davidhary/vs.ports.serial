﻿Imports isr.Ports.Serial

''' <summary>
''' A payload consisting of a single real value of <see cref="T:Double"/> type.
''' </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Class RealValuePayload
    Implements IPayload

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        Me.New(isr.Core.Constructs.RangeR.Full, New isr.Core.Constructs.RangeR(-99999.99, 99999.99))
    End Sub

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="range">    The range. </param>
    ''' <param name="overflow"> The positive overflow. </param>
    Public Sub New(ByVal range As Core.Constructs.RangeR, ByVal overflow As Core.Constructs.RangeR)
        MyBase.New()
        Me.Range = New isr.Core.Constructs.RangeR(range)
        Me.Overflow = New isr.Core.Constructs.RangeR(overflow)
        Me.Format = "0.00"
        Me.StatusCode = StatusCode.ValueNotSet
        Me.Unit = Arebis.StandardUnits.ElectricUnits.Volt
    End Sub

#End Region

#Region " SIMULATE "

    ''' <summary> Returns an emulated value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="range"> The range. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function SimulateValue(ByVal range As isr.Core.Constructs.RangeR) As Double
        If range Is Nothing Then Throw New ArgumentNullException(NameOf(range))
        Dim rand As New Random(Now.Second)
        Return range.Min + range.Span * rand.NextDouble
    End Function

    ''' <summary> Gets the simulated value. </summary>
    ''' <value> The simulated value. </value>
    Public ReadOnly Property SimulatedValue As Double
        Get
            Return RealValuePayload.SimulateValue(Me.Range)
        End Get
    End Property

    ''' <summary> Gets the simulated payload. </summary>
    ''' <value> The simulated payload. </value>
    Public ReadOnly Property SimulatedPayload As IEnumerable(Of Byte)
        Get
            Return String.Format(Me.Format, RealValuePayload.SimulateValue(Me.Range)).ToBytes
        End Get
    End Property

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range. </summary>
    ''' <value> The range. </value>
    Public Property Range As isr.Core.Constructs.RangeR

    ''' <summary> Gets or sets the overflow range. </summary>
    ''' <value> The positive overflow. </value>
    Public Property Overflow As isr.Core.Constructs.RangeR

#End Region

#Region " VALUE "

    ''' <summary> Gets or sets the real value unit. </summary>
    ''' <value> The real value. </value>
    Public Property Unit As Arebis.TypedUnits.Unit

    ''' <summary> Gets or sets the analog input read. </summary>
    ''' <value> The analog input read. </value>
    Public Property Amount As Arebis.TypedUnits.Amount

    ''' <summary> The real value. </summary>
    Private _RealValue As Double

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The real value. </value>
    Public Property RealValue As Double
        Get
            Return Me._RealValue
        End Get
        Set(value As Double)
            If value <> Me.RealValue Then
                Me._RealValue = value
                Me.Reading = String.Format(Me.Format, value)
                Me.Amount = New Arebis.TypedUnits.Amount(value, Me.Unit)
            End If
        End Set
    End Property

    ''' <summary> Gets the format to use. </summary>
    ''' <value> The format. </value>
    Public Property Format As String

    ''' <summary> Gets the has value. </summary>
    ''' <value> The has value. </value>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.StatusCode = StatusCode.Okay
        End Get
    End Property

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary> Gets or sets the reading. </summary>
    ''' <value> The reading. </value>
    Public Property Reading As String
        Get
            Return Me._Reading
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Reading) Then
                Me._Reading = value
                Me.Payload = value.ToBytes
                Me.StatusCode = If(Double.TryParse(value, Me.RealValue), StatusCode.Okay, StatusCode.PayloadParseError)
            End If
        End Set
    End Property

#End Region

#Region " I PAYLOAD IMPLEMENTATION "

    ''' <summary> Gets or sets a list of payloads. </summary>
    ''' <value> A list of payloads. </value>
    Private ReadOnly Property PayloadList As List(Of Byte)

    ''' <summary> Gets or sets the payload. </summary>
    ''' <value> The payload. </value>
    Public Property Payload As IEnumerable(Of Byte) Implements IPayload.Payload
        Get
            Return Me.PayloadList
        End Get
        Set(value As IEnumerable(Of Byte))
            Me._PayloadList = New List(Of Byte)(value)
            If value?.Any Then
                Dim encoding As New System.Text.ASCIIEncoding
                Me.Reading = encoding.GetString(value.ToArray)
            Else
                Me.Reading = String.Empty
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Public Property StatusCode As StatusCode Implements IPayload.StatusCode

    ''' <summary> Populates the payload from the given data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="payload"> The payload. </param>
    ''' <returns> A StatusCode. </returns>
    Public Function Parse(ByVal payload As IEnumerable(Of Byte)) As StatusCode Implements IPayload.Parse
        Me.Payload = payload
        Return Me.StatusCode
    End Function

    ''' <summary> Converts the payload to bytes. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> This object as an IEnumerable(Of Byte) </returns>
    Public Function Build() As IEnumerable(Of Byte) Implements IPayload.Build
        Return Me.Payload()
    End Function

#End Region

End Class

