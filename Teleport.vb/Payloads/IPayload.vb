﻿''' <summary> Defines the interface for the payload. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Interface IPayload

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Property StatusCode As StatusCode

    ''' <summary> Gets or sets the payload. </summary>
    ''' <value> The payload. </value>
    ReadOnly Property Payload As IEnumerable(Of Byte)

    ''' <summary> Converts the payload to bytes. </summary>
    ''' <returns> This object as an IEnumerable(Of Byte) </returns>
    Function Build() As IEnumerable(Of Byte)

    ''' <summary> Populates the payload from the given data. </summary>
    ''' <param name="payload"> The payload. </param>
    ''' <returns> A StatusCode. </returns>
    Function Parse(ByVal payload As IEnumerable(Of Byte)) As StatusCode

End Interface
