﻿''' <summary> Defines an event arguments class for protocol messages. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class ProtocolEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ProtocolEventArgs" />
    ''' using an empty <see cref="Teleport.IProtocolMessage">serial protocol message</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ProtocolEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="status">          The <see cref="StatusCode">status</see>. </param>
    ''' <param name="protocolMessage"> The <see cref="IProtocolMessage">protocol message</see>. </param>
    Public Sub New(ByVal status As StatusCode, ByVal protocolMessage As IProtocolMessage)
        MyBase.New()
        Me._TransportStatus = status
        Me._ProtocolMessage = protocolMessage
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Gets or sets the <see cref="StatusCode">transport status</see>. This status is different from
    ''' the status of the sent or received message.
    ''' </summary>
    ''' <value> The transport status. </value>
    Public ReadOnly Property TransportStatus As StatusCode

    ''' <summary> Gets or sets the <see cref="IProtocolMessage">protocol message</see>. </summary>
    ''' <value> A message describing the protocol. </value>
    Public ReadOnly Property ProtocolMessage As IProtocolMessage

#End Region

End Class
