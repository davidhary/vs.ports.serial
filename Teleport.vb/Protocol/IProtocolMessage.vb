﻿Imports System.Collections.ObjectModel

''' <summary> Defines an interface to the protocol message. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IProtocolMessage
    Inherits ICloneable, IDisposable, ComponentModel.INotifyPropertyChanged, Serial.IMessageParser

#Region " CONTENTS "

    ''' <summary> Gets or sets the address mode. </summary>
    ''' <value> The address mode. </value>
    Property AddressMode As AddressModes

    ''' <summary> Gets or sets the module address. </summary>
    ''' <value> The module address. </value>
    Property ModuleAddress As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the module address in Ascii. </summary>
    ''' <value> The controller address hexadecimal. </value>
    Property ModuleAddressAscii As String

    ''' <summary> Gets or sets the message type. </summary>
    ''' <value> The message type. </value>
    Property MessageType As MessageType

    ''' <summary>
    ''' Gets or sets the length of the message including the address byte, checksum, command and
    ''' payload.
    ''' </summary>
    ''' <value> The length of the message. </value>
    Property TotalMessageLength As Integer

    ''' <summary> Gets or sets the command in ASCII. </summary>
    ''' <value> The command in Ascii. </value>
    Property CommandAscii As String

    ''' <summary> Gets or sets the command. </summary>
    ''' <value> The command. </value>
    Property Command As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the command timeout. </summary>
    ''' <value> The command timeout. </value>
    Property CommandTimeout As TimeSpan

    ''' <summary>
    ''' Gets or sets the payload.  The represents the message excluding the command and checksum. Not
    ''' every message has a payload.
    ''' </summary>
    ''' <value> The payload. </value>
    Property Payload() As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the payload in hex. </summary>
    ''' <value> The payload hexadecimal. </value>
    ReadOnly Property PayloadHex As String

    ''' <summary> Gets or sets the checksum. </summary>
    ''' <value> The checksum. </value>
    Property Checksum As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the checksum hexadecimal. </summary>
    ''' <value> The checksum hexadecimal. </value>
    Property ChecksumHex As String

    ''' <summary> Gets or sets a value indicating whether this instance has status value. </summary>
    ''' <value> The has status value. </value>
    ReadOnly Property HasStatusValue As Boolean

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Property Status As StatusCode

#End Region

#Region " STREAM DELINEATION "

    ''' <summary> Gets or sets the stream initiation byte(s). </summary>
    ''' <value> The stream initiation byte(s). </value>
    Property StreamInitiation As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the stream termination bytes(s). </summary>
    ''' <value> The stream termination bytes(s). </value>
    Property StreamTermination As IEnumerable(Of Byte)

#End Region

#Region " PREFIX "

    ''' <summary> Gets or sets the command, response or error prefix. </summary>
    ''' <value> The command, response or error prompt. </value>
    Property Prefix As Byte

    ''' <summary> Gets or sets the command, response or error prefix in ASCII. </summary>
    ''' <value> The command, response or error prefix in ASCII. </value>
    Property PrefixAscii As String

#End Region

#Region " CONTENT MANAGEMENT "

    ''' <summary> Clears the contents of an existing <see cref="IProtocolMessage" />. </summary>
    Sub Clear()

    ''' <summary> Copies the contents of an existing <see cref="IProtocolMessage" />. </summary>
    ''' <param name="protocolMessage"> The validated protocol message. </param>
    Sub CopyFrom(ByVal protocolMessage As IProtocolMessage)

#End Region

#Region " PAYLOAD MANAGEMENT "

    ''' <summary> Gets or sets the payload values. </summary>
    ''' <value> The payload values. </value>
    ReadOnly Property PayloadValues As ByteCollection

    ''' <summary> Notifies the payload changed. </summary>
    Sub NotifyPayloadChanged()

#End Region

#Region " STREAM "

    ''' <summary> Gets or sets a list of streams. </summary>
    ''' <value> A List of streams. </value>
    ReadOnly Property StreamValues As ByteCollection

    ''' <summary> Gets or sets the stream. </summary>
    ''' <value> The stream. </value>
    Property Stream As IEnumerable(Of Byte)

    ''' <summary> Gets or sets the encoding. </summary>
    ''' <value> The encoding. </value>
    Property Encoding As Text.Encoding

    ''' <summary> Gets or sets a list of internal streams. </summary>
    ''' <value> A List of internal streams. </value>
    ReadOnly Property InternalStreamValues As ByteCollection

    ''' <summary>
    ''' Gets or sets the internal stream excluding the initiation and termination characters.
    ''' </summary>
    ''' <value> The internal stream. </value>
    Property InternalStream As IEnumerable(Of Byte)

    ''' <summary> Gets or sets a message describing the internal ASCII. </summary>
    ''' <value> A message describing the internal ASCII. </value>
    ReadOnly Property InternalAsciiMessage As String

    ''' <summary> Gets or sets a message as hexadecimal. </summary>
    ''' <value> A message describing the hexadecimal. </value>
    ReadOnly Property HexMessage As String

#End Region

#Region " PARSERS "

    ''' <summary> Determines whether the message status signifies an incomplete message. </summary>
    ''' <returns>
    ''' <c>true</c> if the specified status is incomplete; otherwise, <c>false</c>.
    ''' </returns>
    Function IsIncomplete() As Boolean

    ''' <summary> Determines whether the message status signifies an invalid message. </summary>
    ''' <returns> <c>true</c> if the message is invalid; otherwise, <c>false</c>. </returns>
    Function IsInvalid() As Boolean

    ''' <summary> Parse hexadecimal message. </summary>
    ''' <param name="hexMessage"> A message describing the hexadecimal. </param>
    ''' <returns> A StatusCode. </returns>
    Function ParseHexMessage(ByVal hexMessage As String) As StatusCode

    ''' <summary>
    ''' Parses the <paramref name="data">byte data</paramref> into a protocol message.
    ''' </summary>
    ''' <param name="messageType"> The message type. </param>
    ''' <param name="data">        The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Function ParseStream(ByVal messageType As MessageType, ByVal data As IEnumerable(Of Byte)) As StatusCode

    ''' <summary>
    ''' Parses the <paramref name="data">byte data</paramref> into a protocol message.
    ''' </summary>
    ''' <param name="data"> The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Function ParseStream(ByVal data As IEnumerable(Of Byte)) As StatusCode

    ''' <summary>
    ''' Parses the <paramref name="data">byte data</paramref> into a protocol message.
    ''' </summary>
    ''' <param name="messengerRole"> The messenger Role. </param>
    ''' <param name="data">          The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Function ParseStream(ByVal messengerRole As MessengerRole, ByVal data As IEnumerable(Of Byte)) As StatusCode

    ''' <summary> Parses the stream into a protocol message. </summary>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Function ParseStream() As StatusCode

    ''' <summary> Validates the protocol message relative to this protocol message. </summary>
    ''' <param name="protocolMessage"> The validated protocol message. </param>
    ''' <returns> A StatusCode. </returns>
    Function Validate(ByVal protocolMessage As IProtocolMessage) As StatusCode

#End Region

#Region " BUILDERS "

    ''' <summary> Calculates the checksum hexadecimal. </summary>
    ''' <returns> The calculated checksum hexadecimal. </returns>
    Function CalculateChecksumHex() As String

    ''' <summary> Calculates the checksum. </summary>
    ''' <returns> The calculated checksum. </returns>
    Function CalculateChecksum() As IEnumerable(Of Byte)

    ''' <summary> Calculates and sets the checksum value. </summary>
    Sub UpdateChecksum()

    ''' <summary> Builds the message stream including termination and initiation bytes. </summary>
    ''' <param name="messageType"> The message type. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Function BuildStream(ByVal messageType As MessageType) As IEnumerable(Of Byte)

    ''' <summary> Builds the message stream including termination and initiation bytes. </summary>
    ''' <param name="messengerRole"> The messenger role. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Function BuildStream(ByVal messengerRole As MessengerRole) As IEnumerable(Of Byte)

    ''' <summary> Converts a value to a message type. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a MessageType. </returns>
    Function ToMessageType(ByVal value As MessengerRole) As MessageType

#End Region

End Interface

