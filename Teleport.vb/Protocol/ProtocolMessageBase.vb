Imports isr.Ports.Serial
Imports isr.Ports.Serial.Methods

''' <summary> Defines the protocol message. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public MustInherit Class ProtocolMessageBase
    Inherits isr.Core.Models.ViewModelBase
    Implements IProtocolMessage

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="encoding"> The encoding. </param>
    Protected Sub New(ByVal encoding As Text.Encoding)
        MyBase.New()
        Me.Encoding = encoding
        Me.Initialize()
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub Initialize()
        Me._Prefix = 0
        Me._PrefixAscii = String.Empty
        Me._StreamInitiationValues = New List(Of Byte)()
        Me._StreamTerminationValues = New List(Of Byte)()
        Me._ModuleAddressValues = New List(Of Byte)()
        Me._CommandAscii = String.Empty
        Me._PayloadValues = New ByteCollection()
        Me._ChecksumValues = New List(Of Byte)()
        Me._Status = StatusCode.ValueNotSet
        Me._MessageType = MessageType.None
        Me._StreamValues = New ByteCollection()
        Me._InternalStreamValues = New ByteCollection()
        Me._Encoding = New Text.ASCIIEncoding
    End Sub

    ''' <summary> Clears the contents of an existing <see cref="ProtocolMessageBase" />. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overridable Sub Clear() Implements IProtocolMessage.Clear
        Me.Checksum = New List(Of Byte)
        Me.CommandAscii = String.Empty
        Me.InternalStream = New List(Of Byte)
        Me.Payload = New List(Of Byte)
        Me.PrefixAscii = String.Empty
        Me.Stream = New List(Of Byte)
        Me.Status = StatusCode.ValueNotSet
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessageBase"> The message. </param>
    Protected Sub New(ByVal protocolMessageBase As IProtocolMessage)
        Me.New(Teleport.ProtocolMessageBase.ValidateInstance(protocolMessageBase).Encoding)
        Me.CopyFromThis(protocolMessageBase)
    End Sub

    ''' <summary> Copies the contents of an existing <see cref="IProtocolMessage" />. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> The message. </param>
    Private Sub CopyFromThis(ByVal protocolMessage As IProtocolMessage)
        Me._Prefix = protocolMessage.Prefix
        Me._StreamInitiationValues = New List(Of Byte)(protocolMessage.StreamInitiation)
        Me._StreamTerminationValues = New List(Of Byte)(protocolMessage.StreamTermination)
        Me._ModuleAddressValues = New List(Of Byte)(protocolMessage.ModuleAddress)
        Me._CommandAscii = protocolMessage.CommandAscii
        Me._PayloadValues = New ByteCollection(protocolMessage.Payload)
        Me._ChecksumValues = New List(Of Byte)(protocolMessage.Checksum)
        Me._Status = protocolMessage.Status
        Me._MessageType = protocolMessage.MessageType
        Me._StreamValues = New ByteCollection(protocolMessage.Stream)

        Me._InternalStreamValues = New ByteCollection(protocolMessage.InternalStream)
        Me._Encoding = protocolMessage.Encoding
    End Sub

    ''' <summary> Copies the contents of an existing <see cref="IProtocolMessage" />. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="protocolMessage"> The validated protocol message. </param>
    Public Overridable Sub CopyFrom(ByVal protocolMessage As IProtocolMessage) Implements IProtocolMessage.CopyFrom
        If protocolMessage Is Nothing Then Throw New ArgumentNullException(NameOf(protocolMessage))
        Me.CopyFromThis(protocolMessage)
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> A copy of this object. </returns>
    Public MustOverride Function Clone() As Object Implements System.ICloneable.Clone

    ''' <summary> Validated the given protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="protocolMessage"> The message. </param>
    ''' <returns> An IProtocolMessage. </returns>
    Public Shared Function ValidateInstance(ByVal protocolMessage As IProtocolMessage) As IProtocolMessage
        If protocolMessage Is Nothing Then Throw New ArgumentNullException(NameOf(protocolMessage))
        Return protocolMessage
    End Function

#Region "IDISPOSABLE SUPPORT"

    ''' <summary> Gets or sets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Private Property IsDisposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                End If
                Me.StreamInitiationValues?.Clear()
                Me.StreamValues?.Clear()
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' removes this object from the finalization(Queue); uncomment 
        ' the following line if Finalize() is overridden.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " START / END TEXT "

    ''' <summary> Gets or sets the stream initiation values. </summary>
    ''' <value> The stream initiation values. </value>
    Private ReadOnly Property StreamInitiationValues As List(Of Byte)

    ''' <summary> Gets or sets the stream initiation byte(s). </summary>
    ''' <remarks> Typically \x02, ^B. </remarks>
    ''' <value> The stream initiation byte(s). </value>
    Public Overridable Property StreamInitiation As IEnumerable(Of Byte) Implements IProtocolMessage.StreamInitiation
        Get
            Return Me.StreamInitiationValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(Me.StreamInitiation, value) Then
                Me._StreamInitiationValues = New List(Of Byte)(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the stream termination values. </summary>
    ''' <value> The stream termination values. </value>
    Private ReadOnly Property StreamTerminationValues As List(Of Byte)

    ''' <summary> Gets or sets the stream termination bytes(s). </summary>
    ''' <remarks>
    ''' Typically \x03 ^C or Acknowledge \x06 ^F (on the return message), or \x13 \r ^M or \x10 \n ^J
    ''' or both \r \n.
    ''' </remarks>
    ''' <value> The stream termination bytes(s). </value>
    Public Overridable Property StreamTermination As IEnumerable(Of Byte) Implements IProtocolMessage.StreamTermination
        Get
            Return Me.StreamTerminationValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(Me.StreamTermination, value) Then
                Me._StreamTerminationValues = New List(Of Byte)(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " PREFIX "

    ''' <summary> The prefix. </summary>
    Private _Prefix As Byte

    ''' <summary> Gets or sets the command prefix. </summary>
    ''' <value> The command prompt. </value>
    Public Overridable Property Prefix As Byte Implements IProtocolMessage.Prefix
        Get
            Return Me._Prefix
        End Get
        Set(value As Byte)
            If Me.Prefix <> value Then
                Me._Prefix = value
                Me.NotifyPropertyChanged()
                If value <> 0 Then
                    ' was Unicode Encoding
                    Me._PrefixAscii = Me.Encoding.GetString(New Byte() {Me.Prefix})
                Else
                    Me._PrefixAscii = String.Empty
                End If
            End If
        End Set
    End Property

    ''' <summary> The prefix ASCII. </summary>
    Private _PrefixAscii As String

    ''' <summary> Gets or sets the Prompt. </summary>
    ''' <value> The Prompt. </value>
    Public Property PrefixAscii As String Implements IProtocolMessage.PrefixAscii
        Get
            Return Me._PrefixAscii
        End Get
        Set(value As String)
            If Not String.Equals(Me.PrefixAscii, value) Then
                Me._PrefixAscii = value
                Me.NotifyPropertyChanged()
                Me.Prefix = If(String.IsNullOrWhiteSpace(value), CByte(0), Me._PrefixAscii.ToByte)
            End If
        End Set
    End Property

#End Region

#Region " CONTENTS "

    ''' <summary> The address mode. </summary>
    Private _AddressMode As AddressModes

    ''' <summary> Gets or sets the address mode. </summary>
    ''' <value> The address mode. </value>
    Public Property AddressMode As AddressModes Implements IProtocolMessage.AddressMode
        Get
            Return Me._AddressMode
        End Get
        Set(value As AddressModes)
            If Me._AddressMode <> value Then
                Me._AddressMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the module address values. </summary>
    ''' <value> The module address values. </value>
    Private ReadOnly Property ModuleAddressValues As List(Of Byte)

    ''' <summary> Gets or sets the module address. </summary>
    ''' <value> The module address. </value>
    Public Property ModuleAddress As IEnumerable(Of Byte) Implements IProtocolMessage.ModuleAddress
        Get
            Return Me.ModuleAddressValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(Me.ModuleAddress, value) Then
                Me._ModuleAddressValues = New List(Of Byte)(value)
                Me.NotifyPropertyChanged()
                If Me.ModuleAddress.Any Then
                    ' was Unicode Encoding
                    Dim encoding As New System.Text.ASCIIEncoding
                    Me._ModuleAddressAscii = encoding.GetString(Me.ModuleAddress.ToArray)
                Else
                    Me._ModuleAddressAscii = String.Empty
                End If
            End If
        End Set
    End Property

    ''' <summary> The module address ASCII. </summary>
    Private _ModuleAddressAscii As String

    ''' <summary> Gets or sets the ModuleAddress. </summary>
    ''' <value> The ModuleAddress. </value>
    Public Property ModuleAddressAscii As String Implements IProtocolMessage.ModuleAddressAscii
        Get
            Return Me._ModuleAddressAscii
        End Get
        Set(value As String)
            If Not String.Equals(Me.ModuleAddressAscii, value) Then
                Me._ModuleAddressAscii = value
                Me.NotifyPropertyChanged()
                Me.ModuleAddress = If(String.IsNullOrWhiteSpace(value), New List(Of Byte), DirectCast(Me._ModuleAddressAscii.ToBytes, IEnumerable(Of Byte)))
            End If
        End Set
    End Property

    ''' <summary> Length of the total message. </summary>
    Private _TotalMessageLength As Integer

    ''' <summary>
    ''' Gets or sets the length of the message including the address byte, checksum, command and
    ''' payload.
    ''' </summary>
    ''' <value> The length of the message. </value>
    Public Property TotalMessageLength As Integer Implements IProtocolMessage.TotalMessageLength
        Get
            Return Me._TotalMessageLength
        End Get
        Set(value As Integer)
            If Me.TotalMessageLength <> value Then
                Me._TotalMessageLength = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a message describing the error. </summary>
    ''' <value> A message describing the error. </value>
    Public ReadOnly Property ErrorMessage As String
        Get
            Return If(Me.MessageType = MessageType.Failure, Me.Encoding.GetString(Me.Payload.ToArray), String.Empty)
        End Get
    End Property

    ''' <summary> Type of the message. </summary>
    Private _MessageType As MessageType

    ''' <summary> Gets or sets the message type. </summary>
    ''' <value> The message type. </value>
    Public Overridable Property MessageType As MessageType Implements IProtocolMessage.MessageType
        Get
            Return Me._MessageType
        End Get
        Set(value As MessageType)
            If Me.MessageType <> value Then
                Me._MessageType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The command ASCII. </summary>
    Private _CommandAscii As String

    ''' <summary> Gets or sets the command. </summary>
    ''' <value> The command. </value>
    Public Property CommandAscii As String Implements IProtocolMessage.CommandAscii
        Get
            Return Me._CommandAscii
        End Get
        Set(value As String)
            If Not String.Equals(Me.CommandAscii, value) Then
                Me._CommandAscii = value
                Me.NotifyPropertyChanged()
                Me.Command = If(String.IsNullOrWhiteSpace(value), New List(Of Byte), DirectCast(Me._CommandAscii.ToBytes, IEnumerable(Of Byte)))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the command values. </summary>
    ''' <value> The command values. </value>
    Private ReadOnly Property CommandValues As List(Of Byte)

    ''' <summary> Gets or sets Command. </summary>
    ''' <value> The command. </value>
    Public Property Command() As IEnumerable(Of Byte) Implements IProtocolMessage.Command
        Get
            Return Me.CommandValues
        End Get
        Set(value As IEnumerable(Of Byte))
            Me._CommandValues = New List(Of Byte)(value)
            Me.NotifyPropertyChanged()
            Me._CommandAscii = If(value?.Any, Me.Encoding.GetString(Me.CommandValues.ToArray), String.Empty)
        End Set
    End Property

    ''' <summary> The command timeout. </summary>
    Private _CommandTimeout As TimeSpan

    ''' <summary> Gets or sets the command timeout. </summary>
    ''' <value> The command timeout. </value>
    Public Property CommandTimeout As TimeSpan Implements IProtocolMessage.CommandTimeout
        Get
            Return Me._CommandTimeout
        End Get
        Set(value As TimeSpan)
            If Me.CommandTimeout <> value Then
                Me._CommandTimeout = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a list of payloads. </summary>
    ''' <value> A List of payloads. </value>
    Public ReadOnly Property PayloadValues As ByteCollection Implements IProtocolMessage.PayloadValues

    ''' <summary> Notifies the payload changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub NotifyPayloadChanged() Implements IProtocolMessage.NotifyPayloadChanged
        Me.NotifyPropertyChanged(NameOf(ProtocolMessageBase.Payload))
    End Sub

    ''' <summary> Gets or sets payload. </summary>
    ''' <value> The payload. </value>
    Public Property Payload() As IEnumerable(Of Byte) Implements IProtocolMessage.Payload
        Get
            Return Me.PayloadValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(value, Me.Payload) Then
                Me._PayloadValues = New ByteCollection(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the payload in hex. </summary>
    ''' <value> The payload hexadecimal. </value>
    Public ReadOnly Property PayloadHex As String Implements IProtocolMessage.PayloadHex
        Get
            Return If(Me.Payload?.Any, Me.Payload.ToHex, String.Empty)
        End Get
    End Property

    ''' <summary> Gets or sets the checksum values. </summary>
    ''' <value> The checksum values. </value>
    Private ReadOnly Property ChecksumValues As List(Of Byte)

    ''' <summary> Gets or sets the checksum. </summary>
    ''' <value> The checksum. </value>
    Public Property Checksum As IEnumerable(Of Byte) Implements IProtocolMessage.Checksum
        Get
            Return Me.ChecksumValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(Me.Checksum, value) Then
                Me._ChecksumValues = New List(Of Byte)(value)
                Me.NotifyPropertyChanged()
                Me.ChecksumHex = value.ToHex
            End If
        End Set
    End Property

    ''' <summary> The checksum hexadecimal. </summary>
    Private _ChecksumHex As String

    ''' <summary> Gets or sets the Checksum in hex. </summary>
    ''' <value> The checksum hexadecimal. </value>
    Public Property ChecksumHex As String Implements IProtocolMessage.ChecksumHex
        Get
            Return Me._ChecksumHex
        End Get
        Set(value As String)
            If Not String.Equals(Me.ChecksumHex, value) Then
                Me._ChecksumHex = value
                Me.NotifyPropertyChanged()
                Me.Checksum = If(String.IsNullOrWhiteSpace(value), New List(Of Byte), value.ToHexBytes)
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether this instance has status value. </summary>
    ''' <value> The has status value. </value>
    Public ReadOnly Property HasStatusValue As Boolean Implements IProtocolMessage.HasStatusValue
        Get
            Return Me.Status <> StatusCode.ValueNotSet
        End Get
    End Property

    ''' <summary> The status. </summary>
    Private _Status As StatusCode

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As StatusCode Implements IProtocolMessage.Status
        Get
            Return Me._Status
        End Get
        Set(value As StatusCode)
            If Me.Status <> value Then
                Me._Status = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(ProtocolMessageBase.HasStatusValue))
            End If
        End Set
    End Property

#End Region

#Region " STREAM "

    ''' <summary> Gets or sets a stream of bytes. </summary>
    ''' <value> A stream of bytes. </value>
    Public ReadOnly Property StreamValues As ByteCollection Implements IProtocolMessage.StreamValues

    ''' <summary> Gets or sets the stream. </summary>
    ''' <value> The stream. </value>
    Public Property Stream As IEnumerable(Of Byte) Implements IProtocolMessage.Stream
        Get
            Return Me.StreamValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(value, Me.Stream) Then
                Me._StreamValues = New ByteCollection(value)
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(ProtocolMessageBase.HexMessage))
            End If
        End Set
    End Property

    ''' <summary> Gets a message as hexadecimal. </summary>
    ''' <value> A message describing the hexadecimal. </value>
    Public ReadOnly Property HexMessage As String Implements IProtocolMessage.HexMessage
        Get
            Return Me.Stream.ToHex
        End Get
    End Property

    ''' <summary> Gets or sets the Internal Stream of bytes. </summary>
    ''' <value> A InternalStream of bytes. </value>
    Public ReadOnly Property InternalStreamValues As ByteCollection Implements IProtocolMessage.InternalStreamValues

    ''' <summary> Gets or sets the InternalStream. </summary>
    ''' <value> The InternalStream. </value>
    Public Property InternalStream As IEnumerable(Of Byte) Implements IProtocolMessage.InternalStream
        Get
            Return Me.InternalStreamValues
        End Get
        Set(value As IEnumerable(Of Byte))
            If Not Serial.Methods.NullableSequenceEquals(value, Me.InternalStreamValues) Then
                Me._InternalStreamValues = New ByteCollection(value)
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(ProtocolMessageBase.InternalAsciiMessage))
            End If
        End Set
    End Property

    ''' <summary> Gets the encoding. </summary>
    ''' <value> The encoding. </value>
    Public Property Encoding As Text.Encoding Implements IProtocolMessage.Encoding

    ''' <summary> Gets a message describing the internal ASCII. </summary>
    ''' <value> A message describing the internal ASCII. </value>
    Public ReadOnly Property InternalAsciiMessage As String Implements IProtocolMessage.InternalAsciiMessage
        Get
            Return Me.Encoding.GetString(Me.InternalStream.ToArray)
        End Get
    End Property

#End Region

#Region " I PARSER PARSER "

    ''' <summary> True to enable, false to disable the parse. </summary>
    Private _ParseEnabled As Boolean

    ''' <summary> Gets or sets the parse enabled. </summary>
    ''' <value> The parse enabled. </value>
    Public Property ParseEnabled As Boolean Implements IMessageParser.ParseEnabled
        Get
            Return Me._ParseEnabled
        End Get
        Set(value As Boolean)
            If Me.ParseEnabled <> value Then
                Me._ParseEnabled = True
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Parses the given values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Serial.MessageParserOutcome. </returns>
    Public MustOverride Function Parse(ByVal values As IEnumerable(Of Byte)) As Serial.MessageParserOutcome Implements IMessageParser.Parse

#End Region

#Region " PARSERS "

    ''' <summary> Determines whether the message status signifies an incomplete message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' <c>true</c> if the specified status is incomplete; otherwise, <c>false</c>.
    ''' </returns>
    Public Function IsIncomplete() As Boolean Implements IProtocolMessage.IsIncomplete
        Return IsIncomplete(CType(Me.Status, StatusCode))
    End Function

    ''' <summary> Determines whether the specified status signifies an incomplete message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns>
    ''' <c>true</c> if the specified status is incomplete; otherwise, <c>false</c>.
    ''' </returns>
    Public Shared Function IsIncomplete(ByVal status As StatusCode) As Boolean
        Return status = StatusCode.MessageTooShort
    End Function

    ''' <summary> Determines whether the message status signifies an invalid message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> <c>true</c> if the message is invalid; otherwise, <c>false</c>. </returns>
    Public Function IsInvalid() As Boolean Implements IProtocolMessage.IsInvalid
        Return ProtocolMessageBase.IsInvalid(Me.Status)
    End Function

    ''' <summary> Determines whether the specified status signifies an invalid message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns>
    ''' <c>true</c> if [is invalid message] [the specified status]; otherwise, <c>false</c>.
    ''' </returns>
    Public Shared Function IsInvalid(ByVal status As StatusCode) As Boolean
        Return status = StatusCode.InvalidModuleAddress OrElse
               status = StatusCode.InvalidMessageLength OrElse
               status = StatusCode.UnknownStatusCode OrElse
               status = StatusCode.InvalidCommandCode OrElse
               status = StatusCode.ChecksumInvalid
    End Function

    ''' <summary> Parse hexadecimal message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexMessage"> A message describing the hexadecimal. </param>
    ''' <returns> A StatusCode. </returns>
    Public MustOverride Function ParseHexMessage(ByVal hexMessage As String) As StatusCode Implements IProtocolMessage.ParseHexMessage

    ''' <summary>
    ''' Parses the <paramref name="data">byte data</paramref> into a protocol message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messageType"> The message type. </param>
    ''' <param name="data">        The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Public MustOverride Function ParseStream(ByVal messageType As MessageType, ByVal data As IEnumerable(Of Byte)) As StatusCode Implements IProtocolMessage.ParseStream

    ''' <summary>
    ''' Parses the <paramref name="data">byte data</paramref> into a protocol message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messengerRole"> The messenger role. </param>
    ''' <param name="data">          The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Public Function ParseStream(ByVal messengerRole As MessengerRole, ByVal data As IEnumerable(Of Byte)) As StatusCode Implements IProtocolMessage.ParseStream
        Return Me.ParseStream(Me.ToMessageType(messengerRole), data)
    End Function

    ''' <summary>
    ''' Parses the <paramref name="data">byte data</paramref> into a protocol message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data"> The byte data. </param>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Public Function ParseStream(ByVal data As IEnumerable(Of Byte)) As StatusCode Implements IProtocolMessage.ParseStream
        Return Me.ParseStream(Me.MessageType, data)
    End Function

    ''' <summary> Parses the stream into a protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns>
    ''' <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
    ''' </returns>
    Public Function ParseStream() As StatusCode Implements IProtocolMessage.ParseStream
        Return Me.ParseStream(Me.MessageType, Me.Stream)
    End Function

    ''' <summary> Validates the protocol message relative to this protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> The validated protocol message. </param>
    ''' <returns> An Integer. </returns>
    Public MustOverride Function Validate(ByVal protocolMessage As IProtocolMessage) As StatusCode Implements IProtocolMessage.Validate

#End Region

#Region " BUILDERS "

    ''' <summary> Calculates the checksum hexadecimal. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The calculated checksum hexadecimal. </returns>
    Public MustOverride Function CalculateChecksumHex() As String Implements IProtocolMessage.CalculateChecksumHex

    ''' <summary> Calculates the checksum. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The calculated checksum. </returns>
    Public Function CalculateChecksum() As IEnumerable(Of Byte) Implements IProtocolMessage.CalculateChecksum
        Return Me.CalculateChecksumHex.ToBytes
    End Function

    ''' <summary> Calculates and sets the checksum value. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub UpdateChecksum() Implements IProtocolMessage.UpdateChecksum
        Me.Checksum = Me.CalculateChecksum
    End Sub

    ''' <summary> Enumerates build message in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messageType"> The message type. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Public MustOverride Function BuildStream(ByVal messageType As MessageType) As IEnumerable(Of Byte) Implements IProtocolMessage.BuildStream

    ''' <summary> Enumerates build message in this collection. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messengerRole"> The messenger role. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build message in this collection.
    ''' </returns>
    Public Function BuildStream(ByVal messengerRole As MessengerRole) As IEnumerable(Of Byte) Implements IProtocolMessage.BuildStream
        Return Me.BuildStream(Me.ToMessageType(messengerRole))
    End Function

    ''' <summary> Converts a value to a message type. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a MessageType. </returns>
    Public Function ToMessageType(ByVal value As MessengerRole) As MessageType Implements IProtocolMessage.ToMessageType
        Return If(value = MessengerRole.Collector, MessageType.Response, MessageType.Command)
    End Function

#End Region

End Class

