﻿Imports isr.Core.EnumExtensions

''' <summary> A protocol command. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-06-19 </para>
''' </remarks>
Public Class ProtocolCommand

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode">  The command code. </param>
    ''' <param name="commandAscii"> The command. </param>
    ''' <param name="description">  The description. </param>
    Public Sub New(ByVal commandCode As Integer, ByVal commandAscii As String, ByVal description As String)
        MyBase.New()
        Me.CommandCode = commandCode
        Me.CommandAscii = commandAscii
        Me.Description = description
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandCode"> The command code. </param>
    Public Sub New(ByVal commandCode As System.Enum)
        Me.New(CInt(CObj(commandCode)), commandCode.ExtractBetween(), commandCode.Description)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="command"> The command. </param>
    Public Sub New(ByVal command As ProtocolCommand)
        MyBase.New
        If command IsNot Nothing Then
            Me._CommandAscii = command.CommandAscii
            Me._CommandCode = command.CommandCode
            Me._Description = command.Description
            Me._TurnaroundTime = command.TurnaroundTime
        End If
    End Sub

    ''' <summary> Gets or sets the command code. </summary>
    ''' <value> The command code. </value>
    Public ReadOnly Property CommandCode As Integer

    ''' <summary> Gets or sets the command ASCII value. </summary>
    ''' <value> The command. </value>
    Public ReadOnly Property CommandAscii As String

    ''' <summary> Gets or sets the description. </summary>
    ''' <value> The description. </value>
    Public ReadOnly Property Description As String

    ''' <summary>
    ''' Gets or sets the time it takes from the receipt of the command to when the module starts to
    ''' transmit a response.
    ''' </summary>
    ''' <value> The command timeout. </value>
    Public Overridable Property TurnaroundTime As TimeSpan

End Class

''' <summary> Dictionary of Protocol commands. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-17 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class ProtocolCommandDictionary
    Inherits Dictionary(Of String, Integer)

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="collection"> The collection. </param>
    Public Sub New(ByVal collection As ProtocolCommandDictionary)
        MyBase.New
        If collection IsNot Nothing Then
            For Each item As KeyValuePair(Of String, Integer) In collection
                Me.Add(item.Key, item.Value)
            Next
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="type"> The type. </param>
    Public Sub New(ByVal type As Type)
        MyBase.New
        For Each enumValue As System.Enum In System.Enum.GetValues(type)
            Me.Add(enumValue.ExtractBetween, CInt(CObj(enumValue)))
        Next enumValue
    End Sub

#End Region

End Class

''' <summary> Collection of Protocol commands. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-17 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class ProtocolCommandCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, ProtocolCommand)

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub New()
        MyBase.New
        Me.CommandDictionary = New ProtocolCommandDictionary()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="collection"> The collection. </param>
    Public Sub New(ByVal collection As ProtocolCommandCollection)
        MyBase.New
        If collection IsNot Nothing Then
            For Each item As ProtocolCommand In collection
                Me.Add(New ProtocolCommand(item))
            Next
            Me.CommandDictionary = New ProtocolCommandDictionary(collection.CommandDictionary)
        End If
    End Sub

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="type"> The type. </param>
    Public Sub New(ByVal type As Type)
        MyBase.New
        For Each enumValue As System.Enum In System.Enum.GetValues(type)
            Me.Add(New ProtocolCommand(enumValue))
        Next enumValue
        Me.CommandDictionary = New ProtocolCommandDictionary(type)
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ProtocolCommand) As Integer
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.CommandCode
    End Function

    ''' <summary> Gets or sets a dictionary of commands. </summary>
    ''' <value> A Dictionary of commands. </value>
    Public ReadOnly Property CommandDictionary As ProtocolCommandDictionary

    ''' <summary> Contains command. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ContainsCommand(ByVal commandAscii As String) As Boolean
        Return Me.CommandDictionary.ContainsKey(commandAscii)
    End Function

    ''' <summary> Commands. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command ASCII. </param>
    ''' <returns> A ProtocolCommand. </returns>
    Public Function Command(ByVal commandAscii As String) As ProtocolCommand
        Return Me(Me.CommandDictionary(commandAscii))
    End Function

#End Region

End Class

