Imports System.IO.Ports
Imports isr.Core
Imports isr.Ports.Teleport.ExceptionExtensions
Imports isr.Ports.Serial

''' <summary> The Scribe class writes and reads information to and from the module. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Scribe
    Inherits isr.Core.Models.ViewModelTalkerBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub New()
        MyBase.New
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Scribe" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="role">                    The role. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    Public Sub New(ByVal moduleAddress As IEnumerable(Of Byte), ByVal role As MessengerRole, ByVal templateProtocolMessage As IProtocolMessage)
        Me.New(If(role = MessengerRole.Emitter, Teleport.Emitter.Create(moduleAddress, templateProtocolMessage), Teleport.Collector.Create(moduleAddress, templateProtocolMessage)))
        Me.IsMessengerOwner = True
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Scribe" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="messenger"> The Messenger. </param>
    Public Sub New(ByVal messenger As IMessenger)
        MyBase.New()
        Me.AssignMessengerThis(messenger)
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If disposing Then
                    Me._Messenger?.Dispose()
                End If
                Me.Talker?.Listeners.Clear()
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Gets the is port open. </summary>
    ''' <value> The is port open. </value>
    Public Overridable ReadOnly Property IsPortOpen As Boolean
        Get
            Return Me.Messenger IsNot Nothing AndAlso Me.Messenger.IsPortOpen
        End Get
    End Property

#End Region

#Region " MESSENGER MANAGEMENT "

    ''' <summary> The messenger. </summary>
    Private _Messenger As IMessenger

    ''' <summary> Gets or sets the Messenger. </summary>
    ''' <value> The Messenger. </value>
    Public Property Messenger As IMessenger
        Get
            Return Me._Messenger
        End Get
        Set(value As IMessenger)
            Me.AssignMessengerThis(value)
        End Set
    End Property

    ''' <summary> Assigns the Messenger. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignMessengerThis(ByVal value As IMessenger)
        If Me._Messenger IsNot Nothing Then
            RemoveHandler Me._Messenger.ConnectionChanged, AddressOf Me.MessengerConnectionChanged
            RemoveHandler Me._Messenger.DataReceived, AddressOf Me.MessengerDataReceived
            RemoveHandler Me._Messenger.DataSent, AddressOf Me.MessengerDataSent
            RemoveHandler Me._Messenger.MessageReceived, AddressOf Me.MessangeMessageReceived
            RemoveHandler Me._Messenger.MessageSent, AddressOf Me.MessengerMessageSent
            RemoveHandler Me._Messenger.SerialPortDisposed, AddressOf Me.MessengerSerialPortDisposed
            RemoveHandler Me._Messenger.SerialPortErrorReceived, AddressOf Me.MessengerSerialPortErrorReceived
            RemoveHandler Me._Messenger.Timeout, AddressOf Me.MessengerTimeout
            ' this also closes the session. 

            If value Is Nothing AndAlso Me.IsMessengerOwner Then Me._Messenger.Dispose()
        End If
        Me._Messenger = value
        If Me._Messenger IsNot Nothing Then
            AddHandler Me._Messenger.ConnectionChanged, AddressOf Me.MessengerConnectionChanged
            AddHandler Me._Messenger.DataReceived, AddressOf Me.MessengerDataReceived
            AddHandler Me._Messenger.DataSent, AddressOf Me.MessengerDataSent
            AddHandler Me._Messenger.MessageReceived, AddressOf Me.MessangeMessageReceived
            AddHandler Me._Messenger.MessageSent, AddressOf Me.MessengerMessageSent
            AddHandler Me._Messenger.SerialPortDisposed, AddressOf Me.MessengerSerialPortDisposed
            AddHandler Me._Messenger.SerialPortErrorReceived, AddressOf Me.MessengerSerialPortErrorReceived
            AddHandler Me._Messenger.Timeout, AddressOf Me.MessengerTimeout
        End If
    End Sub

    ''' <summary> Gets the is Messenger that owns this item. </summary>
    ''' <value> The is Messenger owner. </value>
    Public ReadOnly Property IsMessengerOwner As Boolean

    ''' <summary> Assigns a Messenger. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overridable Sub AssignMessenger(ByVal value As IMessenger)
        Me._IsMessengerOwner = False
        Me.Messenger = value
    End Sub

    ''' <summary> Releases the Messenger. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overridable Sub ReleaseMessenger()
        Me._Messenger = Nothing
    End Sub

#End Region

#Region " TRANSPORT "

    ''' <summary> Gets the <see cref="Transport">Transport</see>. </summary>
    ''' <value> The transport. </value>
    Public ReadOnly Property Transport As ITransport
        Get
            Return Me.Messenger.Transport
        End Get
    End Property

    ''' <summary> Sends an hexadecimal message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexMessage"> The message hex characters. </param>
    Public Sub SendMessage(ByVal hexMessage As String)
        If String.IsNullOrEmpty(hexMessage) Then
            Me.TryNotifyDataSent(New PortEventArgs(False))
        Else
            Me.SendMessage(hexMessage.ToHexBytes)
        End If
    End Sub

    ''' <summary> Sends the data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub SendMessage(ByVal message As IEnumerable(Of Byte))
        If message Is Nothing Then
            Me.TryNotifyDataSent(New PortEventArgs(False))
        Else
            Me.Messenger.SendData(message)
        End If
    End Sub

#End Region

#Region " TRANSMIT/RECEIVE LOG "

    ''' <summary> Gets or sets a value indicating whether [log events]. </summary>
    ''' <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
    Public Property IsLogEvents As Boolean
        Get
            Return Me._Messenger.IsLogEvents
        End Get
        Set(value As Boolean)
            Me._Messenger.IsLogEvents = value
        End Set
    End Property

    ''' <summary> Gets or sets the name of the event log file. </summary>
    ''' <value> The name of the event log file. </value>
    Public Property EventLogFileName As String
        Get
            Return Me._Messenger.EventLogFileName
        End Get
        Set(value As String)
            Me._Messenger.EventLogFileName = value
        End Set
    End Property

#End Region

#Region " QUERY COMMANDS: GENERIC "

    ''' <summary> Issues a command. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="message">     The message. </param>
    ''' <param name="readTimeout"> The read timeout. </param>
    ''' <param name="e">           Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryQuery(ByVal message As IProtocolMessage, ByVal readTimeout As TimeSpan, ByVal e As isr.Core.ActionEventArgs) As ITransport
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If message Is Nothing Then Throw New ArgumentNullException(NameOf(message))
        Dim activity As String = "issuing command"
        Dim t As ITransport = Nothing
        Try
            activity = $"issuing {message.CommandAscii}"
            t = Me.Query(message, readTimeout)
            If t.IsSuccess Then
                e.Clear()
            Else
                e.RegisterFailure($"failed {activity}: {t.FailureMessage}")
            End If
        Catch ex As Exception
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return t
    End Function

    ''' <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="hexMessage">     The hex message. </param>
    ''' <param name="turnaroundTime"> The turnaround time. </param>
    ''' <param name="readTimeout">    The read timeout. </param>
    ''' <returns> The <see cref="ITransport">transport</see> </returns>
    Public Function Query(ByVal hexMessage As String, ByVal turnaroundTime As TimeSpan, ByVal readTimeout As TimeSpan) As ITransport
        If hexMessage Is Nothing Then Throw New ArgumentNullException(NameOf(hexMessage))
        Return Me.Query(hexMessage.ToHexBytes, turnaroundTime, readTimeout)
    End Function

    ''' <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data">           The data. </param>
    ''' <param name="turnaroundTime"> The turnaround time. </param>
    ''' <param name="readTimeout">    The read timeout. </param>
    ''' <returns> The <see cref="ITransport">transport</see> </returns>
    Public Function Query(ByVal data As IEnumerable(Of Byte), ByVal turnaroundTime As TimeSpan, ByVal readTimeout As TimeSpan) As ITransport
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        Return Me.Messenger.Query(data, turnaroundTime, readTimeout)
    End Function

    ''' <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message">     The message. </param>
    ''' <param name="readTimeout"> The read timeout. </param>
    ''' <returns> The <see cref="ITransport">transport</see> </returns>
    Public Function Query(ByVal message As IProtocolMessage, ByVal readTimeout As TimeSpan) As ITransport
        Return Me.Messenger.Query(message, readTimeout)
    End Function

#End Region

#Region " RETRY COMMANDS "

    ''' <summary> Gets or sets the retry count. </summary>
    ''' <value> The retry count. </value>
    Public Property RetryCount As Integer

    ''' <summary> Echo command. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="message">     The message. </param>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <param name="pauseTime">   The pause time. </param>
    ''' <param name="readTimeout"> The read timeout. </param>
    ''' <param name="e">           Cancel details event information. </param>
    ''' <returns> An ITransport. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function EchoCommand(ByVal message As IProtocolMessage, ByVal repeatCount As Integer, ByVal pauseTime As TimeSpan, ByVal readTimeout As TimeSpan, ByVal e As isr.Core.ActionEventArgs) As ITransport
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If message Is Nothing Then Throw New ArgumentNullException(NameOf(message))
        Dim activity As String = "Echoing command"
        Dim t As ITransport = Nothing
        Try
            Me.RetryCount = 0
            If Not Me.IsPortOpen Then
                activity = $"Open port {Me.Messenger.Port.PortParameters.PortName}"
                Me.Messenger.Port.TryOpen(e)
            End If
            If Me.IsPortOpen Then
                Dim countDown As Integer = repeatCount
                Do
                    countDown -= 1
                    activity = "applying address mode"
                    activity = $"querying {message.ModuleAddress} w/ {message.CommandAscii}"
                    t = Me.Messenger.Query(message, readTimeout)
                    If t.IsSuccess Then
                        e.Clear()
                    Else
                        e.RegisterFailure($"failed {Me.RetryCount} {activity}: {t.FailureMessage}")
                    End If
                    If e.Failed Then
                        Me.RetryCount += 1
                        isr.Core.ApplianceBase.DoEventsWait(pauseTime)
                    End If
                Loop Until countDown = 0 OrElse Not e.Failed
                If Not e.Failed Then
                    activity = $"comparing TX:{t.SentHexMessage} to RX:{t.ReceivedHexMessage} of {message.ModuleAddress} w/ {message.CommandAscii}"
                    If Not t.SentHexMessage.Equals(t.ReceivedHexMessage) Then
                        e.RegisterFailure($"failed {activity}")
                    End If
                End If
            Else

                e.RegisterFailure($"failed {activity}")
            End If
        Catch ex As Exception
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return t
    End Function

    ''' <summary> Queries until success or count down. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="message">     The message. </param>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <param name="pauseTime">   The pause time. </param>
    ''' <param name="readTimeout"> The read timeout. </param>
    ''' <param name="e">           Action event information. </param>
    ''' <returns> An ITransport. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Query(ByVal message As IProtocolMessage, ByVal repeatCount As Integer, ByVal pauseTime As TimeSpan, ByVal readTimeout As TimeSpan, ByVal e As isr.Core.ActionEventArgs) As ITransport
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If message Is Nothing Then Throw New ArgumentNullException(NameOf(message))
        Dim activity As String = "Issuing command"
        Dim t As ITransport = Nothing
        Try
            Me.RetryCount = 0
            If Not Me.IsPortOpen Then
                activity = $"Opening port {Me.Messenger.Port.PortParameters.PortName}"
                Me.Messenger.Port.TryOpen(e)
            End If
            If Me.IsPortOpen Then
                Dim countDown As Integer = repeatCount
                Do
                    countDown -= 1
                    activity = "applying address mode"
                    activity = $"querying {message.ModuleAddress} w/ {message.CommandAscii}"
                    t = Me.Messenger.Query(message, readTimeout)
                    If t.IsSuccess Then
                        e.Clear()
                    Else
                        e.RegisterFailure($"failed {Me.RetryCount} {activity}: {t.FailureMessage}")
                    End If
                    If e.Failed Then
                        Me.RetryCount += 1
                        isr.Core.ApplianceBase.DoEventsWait(pauseTime)
                    End If
                Loop Until countDown = 0 OrElse Not e.Failed
            Else
                e.RegisterFailure($"failed {activity}")

            End If
        Catch ex As Exception
            e.RegisterFailure($"Exception {activity};. {ex.ToFullBlownString}")
        End Try

        Return t
    End Function

#End Region

#Region " EVENT MANAGEMENT "

#Region " CONNECTION CHANGED "

    ''' <summary> Raises the connection event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub NotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Me.Publish(TraceEventType.Information, $"port {If(Me.IsPortOpen, "open", "closed")}")
        Me.SyncNotifyConnectionChanged(e)
    End Sub

    ''' <summary> Removes the Connection Changed event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveConnectionChangedEventHandlers()
        Me._ConnectionChangedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Connection Changed event handlers. </summary>
    Private ReadOnly _ConnectionChangedEventHandlers As New EventHandlerContextCollection(Of ConnectionEventArgs)

    ''' <summary> Event queue for all listeners interested in Connection Changed events. 
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see> 
    ''' </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ConnectionChanged As EventHandler(Of ConnectionEventArgs)
        AddHandler(value As EventHandler(Of ConnectionEventArgs))
            Me._ConnectionChangedEventHandlers.Add(New EventHandlerContext(Of ConnectionEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ConnectionEventArgs))
            Me._ConnectionChangedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ConnectionEventArgs)
            Me._ConnectionChangedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="ConnectionChanged">Connection Changed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ConnectionEventArgs"/> instance containing the event
    '''                  Connection. </param>
    Protected Sub SyncNotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Me._ConnectionChangedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Raises the connection event within a try catch clause. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Dim activity As String = "notifying serial port connection changed"
        Try
            Me.NotifyConnectionChanged(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Messenger connection changed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Connection event information. </param>
    Private Sub MessengerConnectionChanged(sender As Object, e As ConnectionEventArgs)
        Me.TryNotifyConnectionChanged(e)
    End Sub


#End Region

#Region " DATA RECEIVED "

    ''' <summary> Notifies a data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Protected Overridable Sub NotifyDataReceived(ByVal e As PortEventArgs)
        Me.SyncNotifyDataReceived(e)
    End Sub

    ''' <summary> Try notify data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyDataReceived(ByVal e As PortEventArgs)
        Dim activity As String = "notifying data received"
        Try
            Me.NotifyDataReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Removes the <see cref="DataReceived">data received</see> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveDataReceivedEventHandlers()
        Me._DataReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="DataReceived">data received</see> event handlers. </summary>
    Private ReadOnly _DataReceivedEventHandlers As New EventHandlerContextCollection(Of PortEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DataReceived">data received</see>/> events
    ''' Receipt status is reported along with the received data in the receive buffer
    ''' using the <see cref="PortEventArgs">port event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DataReceived As EventHandler(Of PortEventArgs)
        AddHandler(value As EventHandler(Of PortEventArgs))
            Me._DataReceivedEventHandlers.Add(New EventHandlerContext(Of PortEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of PortEventArgs))
            Me._DataReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As PortEventArgs)
            Me._DataReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DataReceived">Data Received Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
    Protected Sub SyncNotifyDataReceived(ByVal e As PortEventArgs)
        Me._DataReceivedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Messenger data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Port event information. </param>
    Private Sub MessengerDataReceived(sender As Object, e As PortEventArgs)
        Me.TryNotifyDataReceived(e)
    End Sub

#End Region

#Region " DATA SENT "

    ''' <summary> Notifies a data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Private Sub NotifyDataSent(ByVal e As PortEventArgs)
        Me.SyncNotifyDataSent(e)
    End Sub

    ''' <summary> Try notify data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyDataSent(ByVal e As PortEventArgs)
        Dim activity As String = "notifying data sent"
        Try
            Me.NotifyDataSent(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Removes the <see cref="DataSent">Data Sent event</see> handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveDataSentEventHandlers()
        Me._DataSentEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="DataSent">Data Sent event</see> handlers. </summary>
    Private ReadOnly _DataSentEventHandlers As New EventHandlerContextCollection(Of PortEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
    ''' Receipt status is reported along with the Sent data in the receive buffer
    ''' using the <see cref="PortEventArgs">port event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DataSent As EventHandler(Of PortEventArgs)
        AddHandler(value As EventHandler(Of PortEventArgs))
            Me._DataSentEventHandlers.Add(New EventHandlerContext(Of PortEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of PortEventArgs))
            Me._DataSentEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As PortEventArgs)
            Me._DataSentEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DataSent">Data Sent Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
    Protected Sub SyncNotifyDataSent(ByVal e As PortEventArgs)
        Me._DataSentEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Messenger data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Port event information. </param>
    Private Sub MessengerDataSent(sender As Object, e As PortEventArgs)
        Me.TryNotifyDataSent(e)
    End Sub

#End Region

#Region " MESSAGE RECEIVED "

    ''' <summary> Notifies a Message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Protocol event information. </param>
    Protected Overridable Sub NotifyMessageReceived(ByVal e As ProtocolEventArgs)
        Me.SyncNotifyMessageReceived(e)
    End Sub

    ''' <summary> Try notify Message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyMessageReceived(ByVal e As ProtocolEventArgs)
        Dim activity As String = "notifying message received"
        Try
            Me.NotifyMessageReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Removes the <see cref="MessageReceived">Message Received</see> event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveMessageReceivedEventHandlers()
        Me._MessageReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="MessageReceived">Message Received</see> event handlers. </summary>
    Private ReadOnly _MessageReceivedEventHandlers As New EventHandlerContextCollection(Of ProtocolEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="MessageReceived">Message Received</see> events.
    ''' The Received message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event MessageReceived As EventHandler(Of ProtocolEventArgs)
        AddHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageReceivedEventHandlers.Add(New EventHandlerContext(Of ProtocolEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ProtocolEventArgs)
            Me._MessageReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="MessageReceived">MessageReceived Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyMessageReceived(ByVal e As ProtocolEventArgs)
        Me._MessageReceivedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Messange message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Protocol event information. </param>
    Private Sub MessangeMessageReceived(sender As Object, e As ProtocolEventArgs)
        Me.TryNotifyMessageReceived(e)
    End Sub

#End Region

#Region " MESSAGE SENT "

    ''' <summary> Notifies a Message Sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Protocol event information. </param>
    Private Sub NotifyMessageSent(ByVal e As ProtocolEventArgs)
        Me.SyncNotifyMessageSent(e)
    End Sub

    ''' <summary> Try notify Message Sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyMessageSent(ByVal e As ProtocolEventArgs)
        Dim activity As String = "notifying message sent"
        Try
            Me.NotifyMessageSent(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Removes the <see cref="MessageSent">Message Sent</see> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveMessageSentEventHandlers()
        Me._MessageSentEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="MessageSent">Message Sent</see> event handlers. </summary>
    Private ReadOnly _MessageSentEventHandlers As New EventHandlerContextCollection(Of ProtocolEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="MessageSent">Message Sent</see>events.
    ''' The Sent message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event MessageSent As EventHandler(Of ProtocolEventArgs)
        AddHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageSentEventHandlers.Add(New EventHandlerContext(Of ProtocolEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageSentEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ProtocolEventArgs)
            Me._MessageSentEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="MessageSent">Message Sent Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyMessageSent(ByVal e As ProtocolEventArgs)
        Me._MessageSentEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Messenger message sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Protocol event information. </param>
    Private Sub MessengerMessageSent(sender As Object, e As ProtocolEventArgs)
        Me.TryNotifyMessageSent(e)
    End Sub

#End Region

#Region " SERIAL PORT DISPOSED "

    ''' <summary>
    ''' Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    Private Sub NotifySerialPortDisposed(ByVal e As EventArgs)
        Me.SyncNotifySerialPortDisposed(e)
    End Sub

    ''' <summary> Try notify serial port disposed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifySerialPortDisposed(ByVal e As EventArgs)
        Dim activity As String = "notifying serial port disposed"
        Try
            Me.Publish(TraceEventType.Information, activity)
            Me.NotifySerialPortDisposed(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Removes the <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveSerialPortDisposedEventHandlers()
        Me._SerialPortDisposedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers. </summary>
    Private ReadOnly _SerialPortDisposedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port Disposed</see> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SerialPortDisposed As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._SerialPortDisposedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._SerialPortDisposedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._SerialPortDisposedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="SerialPortDisposed">Serial Port Disposed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifySerialPortDisposed(ByVal e As System.EventArgs)
        Me._SerialPortDisposedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary>
    ''' Handles the Disposed event of the _serialPort control. Propagates the events to the calling
    ''' controls.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub MessengerSerialPortDisposed(sender As Object, e As System.EventArgs)
        Me.TryNotifySerialPortDisposed(e)
    End Sub

#End Region

#Region " SERIAL PORT ERROR RECEIVED "

    ''' <summary> Raises the serial port error received event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub NotifySerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Me.Publish(TraceEventType.Error, "Serial port error type {0} occurred", e.EventType)
        Me.SyncNotifySerialPortErrorReceived(e)
    End Sub

    ''' <summary> Try notify serial port error received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Serial error received event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifySerialPortErrorReceived(e As System.IO.Ports.SerialErrorReceivedEventArgs)
        Dim activity As String = "notifying serial port error received"
        Try
            Me.NotifySerialPortErrorReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the ErrorReceived event of the talker. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.IO.Ports.SerialErrorReceivedEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub MessengerSerialPortErrorReceived(sender As Object, e As System.IO.Ports.SerialErrorReceivedEventArgs)
        Me.TryNotifySerialPortErrorReceived(e)
    End Sub

    ''' <summary>
    ''' Removes the <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event of
    ''' the <see cref="IPort"/> Port.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveSerialPortErrorReceivedEventHandlers()
        Me._SerialPortErrorReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The SerialPortErrorReceived event handlers. </summary>
    Private ReadOnly _SerialPortErrorReceivedEventHandlers As New EventHandlerContextCollection(Of SerialErrorReceivedEventArgs)

    ''' <summary> Event queue for all listeners interested in the
    ''' <see cref="SerialPortErrorReceived">Serial port error receivd </see>> event of the <see cref="IPort"/> Port.
    ''' Serial Port Error Received status is reported with the
    ''' <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
    ''' </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SerialPortErrorReceived As EventHandler(Of SerialErrorReceivedEventArgs)
        AddHandler(value As EventHandler(Of SerialErrorReceivedEventArgs))
            Me._SerialPortErrorReceivedEventHandlers.Add(New EventHandlerContext(Of SerialErrorReceivedEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of SerialErrorReceivedEventArgs))
            Me._SerialPortErrorReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As SerialErrorReceivedEventArgs)
            Me._SerialPortErrorReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="SerialPortErrorReceived">SerialPortErrorReceived Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="SerialErrorReceivedEventArgs" /> instance containing the event
    '''                  data. </param>
    Protected Sub SyncNotifySerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        Me._SerialPortErrorReceivedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " TIMEOUT "

    ''' <summary> Removes the <see cref="Timeout"/> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveTimeoutEventHandlers()
        Me._TimeoutEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Timeout"/> event handlers. </summary>
    Private ReadOnly _TimeoutEventHandlers As New EventHandlerContextCollection(Of ProtocolEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Timeout As EventHandler(Of ProtocolEventArgs)
        AddHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._TimeoutEventHandlers.Add(New EventHandlerContext(Of ProtocolEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._TimeoutEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ProtocolEventArgs)
            Me._TimeoutEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Timeout">Timeout Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyTimeout(ByVal e As ProtocolEventArgs)
        Me._TimeoutEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Notifies a timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    Protected Overridable Sub NotifyTimeout(ByVal e As ProtocolEventArgs)
        If e IsNot Nothing AndAlso Me.Transport IsNot Nothing Then Me.Transport.TransportStatus = StatusCode.ReceiveTimeout
        Me.SyncNotifyTimeout(e)
    End Sub

    ''' <summary> Try notify data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryNotifyTimeout(ByVal e As ProtocolEventArgs)
        Dim activity As String = "notifying serial port timeout"
        Try
            Me.NotifyTimeout(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Talker timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Protocol event information. </param>
    Private Sub MessengerTimeout(sender As Object, e As ProtocolEventArgs)
        Me.TryNotifyTimeout(e)
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

