Partial Public Class MessengerBase

#Region " RECEIVE BUFFER MANAGER "

    ''' <summary> The stream initiation value. </summary>
    Private _StreamInitiationValue As Byte

    ''' <summary> Gets or sets the stream Initiation value. </summary>
    ''' <value> The stream Initiation value. </value>
    Public Property StreamInitiationValue As Byte
        Get
            Return Me._StreamInitiationValue
        End Get
        Set(value As Byte)
            If value <> Me.StreamInitiationValue Then
                Me._StreamInitiationValue = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The stream termination value. </summary>
    Private _StreamTerminationValue As Byte

    ''' <summary> Gets or sets the stream termination value. </summary>
    ''' <value> The stream termination value. </value>
    Public Property StreamTerminationValue As Byte
        Get
            Return Me._StreamTerminationValue
        End Get
        Set(value As Byte)
            If value <> Me.StreamTerminationValue Then
                Me._StreamTerminationValue = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The read pointer. </summary>
    Private _ReadPointer As Integer

    ''' <summary> Buffer for received data. </summary>
    Private _ReceivedBuffer As List(Of Byte)

    ''' <summary> Reads the next byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> The next. </returns>
    Private Function ReadNext() As Byte
        If Me.InputBufferingOption = isr.Ports.Serial.DataBufferingOption.CircularBuffer Then
            Return Me._Port.ReadNext
        Else
            If Me.HasData Then
                Me._ReadPointer += 1
                Return Me._ReceivedBuffer(Me._ReadPointer)
            Else
                Return 255
            End If
        End If
    End Function

    ''' <summary> Returns the number of bytes left to read. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> An Integer. </returns>
    Private Function RemainingDataCount() As Integer
        Return If(Me.InputBufferingOption = isr.Ports.Serial.DataBufferingOption.CircularBuffer,
                    Me._Port.DataCount,
                    If(Me._ReceivedBuffer Is Nothing, 0, Me._ReceivedBuffer.Count - Me._ReadPointer))
    End Function

    ''' <summary> Determines whether this instance has data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> <c>true</c> if this instance has data; otherwise, <c>false</c>. </returns>
    Private Function HasData() As Boolean
        Return Me.RemainingDataCount > 0
    End Function

    ''' <summary> Encapsulates the result of a read. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Structure ReadResult

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="readResult"> The read result. </param>
        Public Sub New(ByVal readResult As ReadResult)
            Me.New(readResult.Reading, readResult.Timeout)
            Me.Reading = readResult.Reading
            Me.Timeout = readResult.Timeout
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="reading"> The reading. </param>
        ''' <param name="timeout"> The timeout. </param>
        Public Sub New(ByVal reading As Byte?, timeout As Boolean)
            Me.Reading = reading
            Me.Timeout = timeout
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="timeout"> The timeout. </param>
        Public Sub New(timeout As Boolean)
            Me.Reading = New Byte?
            Me.Timeout = timeout
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="reading"> The reading. </param>
        Public Sub New(reading As Byte)
            Me.Reading = reading
            Me.Timeout = False
        End Sub

        ''' <summary> Gets or sets the reading. </summary>
        ''' <value> The reading. </value>
        Public Property Reading As Byte?

        ''' <summary> Gets or sets the timeout. </summary>
        ''' <value> The timeout. </value>
        Public Property Timeout As Boolean

        ''' <summary> Stream started. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="streamInitiationValue"> The stream initiation value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function StreamStarted(ByVal streamInitiationValue As Byte) As Boolean
            Return Me.Reading.HasValue AndAlso ((streamInitiationValue = 0) OrElse (Me.Reading.Value = streamInitiationValue))
        End Function

        ''' <summary> Stream ended. </summary>
        ''' <remarks> David, 2020-10-22. </remarks>
        ''' <param name="streamTerminationValue"> The stream termination value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function StreamEnded(ByVal streamTerminationValue As Byte) As Boolean
            Return Me.Reading.HasValue AndAlso Me.Reading.Value = streamTerminationValue
        End Function
    End Structure

    ''' <summary> Reads the next byte. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="loopDelay">        The loop delay. </param>
    ''' <param name="timeoutStopwatch"> The timeout stopwatch. </param>
    ''' <returns> The next. </returns>
    Private Function ReadNext(ByVal loopDelay As TimeSpan, ByVal timeoutStopwatch As isr.Core.Constructs.TimeoutStopwatch) As ReadResult
        Dim result As New ReadResult(False)
        Do
            If Me.HasData Then
                result = New ReadResult(Me.ReadNext)
            ElseIf timeoutStopwatch.IsTimeout Then
                result = New ReadResult(True)
            Else
                ' wait a bit for the next character
                isr.Core.ApplianceBase.DoEventsWait(loopDelay)
            End If
        Loop Until result.Reading.HasValue OrElse result.Timeout
        Return result
    End Function


#End Region

#Region " RECEIVE SERVICES: - BYTE-BY-BYTE READ AND SERVICE "

    ''' <summary> Await start stream. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Private Function AwaitStartStream() As StatusCode
        Dim result As StatusCode = StatusCode.Okay

        ' loop waiting for data
        Dim delay As TimeSpan = TimeSpan.FromMilliseconds(0.75)

        ' set timeout for message turn around time. 
        Dim timeoutStopwatch As New isr.Core.Constructs.TimeoutStopwatch(Me.Transport.SelectCommand(Me.InputMessage.CommandAscii).TurnaroundTime)
        Dim readResult As ReadResult = New ReadResult(Me.ReadNext(delay, timeoutStopwatch))

        If readResult.StreamEnded(Me.StreamTerminationValue) OrElse readResult.Timeout OrElse Not readResult.Reading.HasValue Then
            result = StatusCode.MessageIncomplete
        ElseIf readResult.StreamStarted(Me.StreamInitiationValue) Then
            Me.InputMessage.StreamValues.Add(readResult.Reading.Value)
        Else
            result = StatusCode.InvalidStreamStart
        End If
        Return result
    End Function

    ''' <summary> Builds input message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Private Function BuildInputMessage() As StatusCode

        ' clear the existing message.
        Me.InputMessage.Clear()

        Dim startCountDown As Integer = 10
        Dim result As StatusCode
        Do
            startCountDown -= 1
            result = Me.AwaitStartStream
        Loop Until result = StatusCode.Okay OrElse startCountDown = 0

        Dim delay As TimeSpan = TimeSpan.FromMilliseconds(0.75)
        Dim timeoutStopwatch As New isr.Core.Constructs.TimeoutStopwatch(TimeSpan.FromMilliseconds(10))

        Dim readResult As ReadResult = New ReadResult(Me.ReadNext(delay, timeoutStopwatch))
        Do Until readResult.StreamEnded(Me.StreamTerminationValue) OrElse readResult.Timeout
            readResult = New ReadResult(Me.ReadNext(delay, timeoutStopwatch))
            If readResult.Reading.HasValue Then Me.InputMessage.StreamValues.Add(readResult.Reading.Value)
        Loop
        result = If(readResult.Timeout, StatusCode.ReceiveTimeout, Me.InputMessage.ParseStream())
        Return result
    End Function

    ''' <summary> Receives and services the protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Protected Function ReceiveProtocolMessage() As StatusCode

        Dim result As StatusCode

        ' read and parse the data from the interface
        result = Me.BuildInputMessage()

        ' validate status of message
        If MessengerBase.IsErrorCode(result) Then

            result = Me.ServiceError(result)

        ElseIf (result = StatusCode.Okay) OrElse (result = StatusCode.ServiceMessageAvailable) Then

            ' Build and send back the reply.
            result = Me.ServiceRequest()

        End If

        ' save last status code
        Me._LastProcessStatus = result

        ' Return with the status code
        Return result

    End Function

#End Region

#Region " RECEIVE SERVICES: - BUFFER RECEIPT SERVICE "


#End Region

#Region " REPLY RESPONSE "

    ''' <summary> Services the response. Responds to the received message after processing. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Protected Function ServiceResponse() As StatusCode

        isr.Core.ApplianceBase.DoEventsWait(Me.OutputMessage.CommandTimeout)

        ' transmit error status
        Dim sentStatusCode As StatusCode = Me.SendProtocolMessage()


        ' validate status of the message
        If MessengerBase.IsErrorCode(sentStatusCode) Then
            Me.Publish(TraceEventType.Warning, "Failed sending reply message '{0}'", sentStatusCode)
        Else

            sentStatusCode = StatusCode.Okay
        End If

        ' return status
        Return sentStatusCode

    End Function

#End Region

End Class

