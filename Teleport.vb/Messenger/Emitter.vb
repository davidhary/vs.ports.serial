﻿''' <summary> A communicator emulating the System controller. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Emitter
    Inherits MessengerBase
    Implements IMessenger

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="Emitter" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    Public Sub New(ByVal moduleAddress As IEnumerable(Of Byte), ByVal templateProtocolMessage As IProtocolMessage)
        MyBase.New(moduleAddress, MessengerRole.Emitter, templateProtocolMessage)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Emitter" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    ''' <param name="port">                    Specifies the
    '''                                        <see cref="isr.Ports.Serial.IPort">port</see>. </param>
    Public Sub New(ByVal moduleAddress As IEnumerable(Of Byte), ByVal templateProtocolMessage As IProtocolMessage, ByVal port As isr.Ports.Serial.IPort)
        MyBase.New(moduleAddress, MessengerRole.Collector, templateProtocolMessage, port)
    End Sub

    ''' <summary> Creates a new emitter. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    ''' <returns> An emitter. </returns>
    Public Shared Function Create(ByVal moduleAddress As IEnumerable(Of Byte), ByVal templateProtocolMessage As IProtocolMessage) As IMessenger
        Dim result As Emitter = Nothing
        Try
            result = New Emitter(moduleAddress, templateProtocolMessage)
        Catch
            If result IsNot Nothing Then result.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Creates a new emitter. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    ''' <param name="port">                    Specifies the
    '''                                        <see cref="isr.Ports.Serial.IPort">port</see>. </param>
    ''' <returns> An Emitter. </returns>
    Public Shared Function Create(ByVal moduleAddress As IEnumerable(Of Byte), ByVal templateProtocolMessage As IProtocolMessage, ByVal port As isr.Ports.Serial.IPort) As Emitter
        Dim result As Emitter = Nothing
        Try
            result = New Emitter(moduleAddress, templateProtocolMessage, port)
        Catch
            If result IsNot Nothing Then result.Dispose()
            Throw
        End Try
        Return result
    End Function

#End Region

#Region " RECEIVE MANAGMENT "

    ''' <summary>
    ''' Services an error. This is a strictly listener mode.  It sends back an error message to the
    ''' emitter.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns> A StatusCode. </returns>
    Public Overrides Function ServiceError(ByVal status As StatusCode) As StatusCode
        Return MyBase.ServiceError(status)
    End Function

    ''' <summary> Services the request. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Overrides Function ServiceRequest() As StatusCode


        ' prepare for a new output message
        Me.OutputMessage.PayloadValues.Clear()


        Dim result As StatusCode
        Select Case Me.InputMessage.AddressMode

            Case AddressModes.None

                Me.OutputMessage.MessageType = MessageType.Response
                Me.OutputMessage.ModuleAddress = Me.InputMessage.ModuleAddress
                Me.OutputMessage.Command = Me.InputMessage.Command
                Me.OutputMessage.Payload = Me.InputMessage.Payload
                Me.OutputMessage.UpdateChecksum()

                result = StatusCode.ServiceMessageReadyToSend

            Case AddressModes.Broadcast

                ' Broadcast Message
                ' illegal for commands returning value (for now)
                ' set flag that commands validate against
                '
                result = StatusCode.CommandNotAvailable

            Case AddressModes.Echo
                ' Echo Message
                ' valid all commands
                ' call echo message routine - will send msg
                result = Me.SendEchoResponse()

            Case AddressModes.SpecialFunction
                ' Special Function
                ' Reserved / Undefined - will ignore message
                result = StatusCode.CommandNotAvailable

            Case Else
                ' ERROR - more than 1 bit set
                ' invalid all commands
                result = StatusCode.InvalidCommandData

        End Select

        ' return status
        Return result

    End Function

#End Region

End Class


