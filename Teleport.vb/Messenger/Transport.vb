Imports System.Drawing
Imports System.Windows.Threading
Imports System.IO
Imports isr.Ports.Serial
Imports isr.Core.EnumExtensions

''' <summary>
''' Includes <see cref="IProtocolMessage">protocol messages</see> that are sent and received from
''' the module and their status values.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Transport
    Implements ITransport

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    Public Sub New(ByVal templateProtocolMessage As IProtocolMessage)
        MyBase.New()
        ProtocolMessageBase.ValidateInstance(templateProtocolMessage)
        Me._TransportProtocolMessage = CType(templateProtocolMessage.Clone(), IProtocolMessage)
        Me._SentProtocolMessage = CType(templateProtocolMessage.Clone(), IProtocolMessage)
        Me._ReceivedProtocolMessage = CType(templateProtocolMessage.Clone(), IProtocolMessage)
        Me.ClearThis()
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transportProtocolMessage"> A message describing the transport protocol. </param>
    ''' <param name="sentProtocolMessage">      A message describing the sent protocol. </param>
    ''' <param name="receivedProtocolMessage">  A message describing the received protocol. </param>
    Public Sub New(ByVal transportProtocolMessage As IProtocolMessage, sentProtocolMessage As IProtocolMessage, receivedProtocolMessage As IProtocolMessage)
        MyBase.New()
        Me._TransportProtocolMessage = transportProtocolMessage
        Me._SentProtocolMessage = sentProtocolMessage
        Me._ReceivedProtocolMessage = receivedProtocolMessage
        Me.ClearThis()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transport"> The transport message. </param>
    Public Sub New(ByVal transport As ITransport)
        MyBase.New()
        Me.CopyFromThis(transport)
    End Sub

    ''' <summary> Validated the given transport. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="transport"> The transport. </param>
    ''' <returns> An ITransport. </returns>
    Public Shared Function Validated(ByVal transport As ITransport) As ITransport
        If transport Is Nothing Then Throw New ArgumentNullException(NameOf(transport))
        Return transport
    End Function

    ''' <summary> Clears the transport protocol message t his. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ClearTransportProtocolMessageTHis()
        Me._TransportProtocolMessage.Clear()
        Me.TransportStatus = StatusCode.ValueNotSet
    End Sub

    ''' <summary> Clears the sent protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ClearSentProtocolMessageThis()
        Me._SentProtocolMessage.Clear()
        Me._SendStatus = StatusCode.ValueNotSet
    End Sub

    ''' <summary> Clears the received protocol message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ClearReceivedProtocolMessageThis()
        Me._ReceivedProtocolMessage.Clear()
        Me._ReceiveStatus = StatusCode.ValueNotSet
    End Sub

    ''' <summary> Clears this instance. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub ClearThis()
        Me._ItemNumber = 0
        Me.ClearTransportProtocolMessageTHis()
        Me.ClearSentProtocolMessageThis()
        Me.ClearReceivedProtocolMessageThis()
    End Sub

    ''' <summary> Clears this instance. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub Clear() Implements ITransport.Clear
        Me.ClearThis()
    End Sub

#Region " CLONE "

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return Transport.Clone(Me)
    End Function

    ''' <summary>
    ''' Creates a new <see cref="Transport">transport</see> that is a copy of the specified instance.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Shared Function Clone(ByVal value As ITransport) As ITransport
        Return New Transport(value)
    End Function

    ''' <summary> Copies the contents of an existing <see cref="ITransport" />. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transport"> The transport. </param>
    Private Sub CopyFromThis(ByVal transport As ITransport)
        Me.ItemNumber = transport.ItemNumber
        Me._TransportProtocolMessage = CType(transport.TransportProtocolMessage.Clone, IProtocolMessage)
        Me.TransportStatus = transport.TransportStatus
        Me._SentProtocolMessage = CType(transport.SentProtocolMessage.Clone, IProtocolMessage)
        Me._SendStatus = transport.SendStatus
        Me._ReceivedProtocolMessage = CType(transport.ReceivedProtocolMessage, IProtocolMessage)
        Me._ReceiveStatus = transport.ReceiveStatus
        Me._Commands = New ProtocolCommandCollection(transport.Commands)
    End Sub

    ''' <summary> Copies the contents of an existing <see cref="ITransport" />. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="transport"> The transport. </param>
    Public Sub CopyFrom(ByVal transport As ITransport) Implements ITransport.CopyFrom
        If transport Is Nothing Then Throw New ArgumentNullException(NameOf(transport))
        Me.CopyFromThis(transport)
    End Sub

#End Region

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Public Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._TransportProtocolMessage?.Dispose()
                Me._SentProtocolMessage?.Dispose()
                Me._ReceivedProtocolMessage?.Dispose()
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " COMMANDS "

    ''' <summary> Populate the protocol commands. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="collection"> The collection. </param>
    Public Sub PopulateCommands(ByVal collection As ProtocolCommandCollection) Implements ITransport.PopulateCommands
        Me._Commands = New ProtocolCommandCollection(collection)
    End Sub

    ''' <summary> Populate commands. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="type"> The type. </param>
    Public Sub PopulateCommands(ByVal type As Type) Implements ITransport.PopulateCommands
        Me._Commands = New ProtocolCommandCollection(type)
    End Sub

    ''' <summary> Gets the commands. </summary>
    ''' <value> The commands. </value>
    Public ReadOnly Property Commands As ProtocolCommandCollection Implements ITransport.Commands

    ''' <summary> Gets the command ASCII. </summary>
    ''' <value> The command ASCII. </value>
    Public ReadOnly Property CommandAscii As String Implements ITransport.CommandAscii
        Get
            Return Me.TransportProtocolMessage.CommandAscii
        End Get
    End Property

    ''' <summary> Gets the command description. </summary>
    ''' <value> The command description. </value>
    Public ReadOnly Property CommandDescription As String
        Get
            Return Me.TransportMessageCommand.Description
        End Get
    End Property

    ''' <summary>
    ''' Gets the sentinel indicating if the protocol message is a known transport message command.
    ''' </summary>
    ''' <value>
    ''' The sentinel indicating if the protocol message is known transport message command.
    ''' </value>
    Public ReadOnly Property IsKnownTransportMessageCommand As Boolean Implements ITransport.IsKnownTransportMessageCommand
        Get
            Return Me.Commands.CommandDictionary.ContainsKey(Me.CommandAscii)
        End Get
    End Property

    ''' <summary> Select command. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="commandAscii"> The command Ascii. </param>
    ''' <returns> A ProtocolCommand. </returns>
    Public Function SelectCommand(ByVal commandAscii As String) As ProtocolCommand Implements ITransport.SelectCommand
        Return Me.Commands.Command(commandAscii)
    End Function

    ''' <summary>
    ''' Gets the transport message command.  This could be the command HEX characters.
    ''' </summary>
    ''' <value> The name of the transport message command. </value>
    Public ReadOnly Property TransportMessageCommand As ProtocolCommand Implements ITransport.TransportMessageCommand
        Get
            Return Me.Commands.Command(Me.CommandAscii)
        End Get
    End Property

#End Region

#Region " MEMBERS "

    ''' <summary> Gets the item number. </summary>
    ''' <value> The item number. </value>
    Public Property ItemNumber As Integer Implements ITransport.ItemNumber

    ''' <summary>
    ''' Gets the Transport status. This status includes timeout status as well as status informing of
    ''' mismatch between the received and sent messages.
    ''' </summary>
    ''' <value> The Transport status. </value>
    Public Property TransportStatus As StatusCode Implements ITransport.TransportStatus

    ''' <summary> Gets the <see cref="IProtocolMessage">Transport Protocol Message.</see> </summary>
    ''' <value> A message describing the transport protocol. </value>
    Public Property TransportProtocolMessage As IProtocolMessage Implements ITransport.TransportProtocolMessage

    ''' <summary>
    ''' Gets the <see cref="IProtocolMessage">Protocol Message</see> that was sent.
    ''' </summary>
    ''' <value> A message describing the sent protocol. </value>
    Public Property SentProtocolMessage As IProtocolMessage Implements ITransport.SentProtocolMessage

    ''' <summary> Gets the status of the sent message. </summary>
    ''' <value> The Send status. </value>
    Public Property SendStatus As StatusCode Implements ITransport.SendStatus

    ''' <summary>
    ''' Gets the <see cref="IProtocolMessage">Protocol Message</see> that was Received.
    ''' </summary>
    ''' <value> A message describing the received protocol. </value>
    Public Property ReceivedProtocolMessage As IProtocolMessage Implements ITransport.ReceivedProtocolMessage

    ''' <summary> Gets the receive status. </summary>
    ''' <value> The receive status. </value>
    Public Property ReceiveStatus As StatusCode Implements ITransport.ReceiveStatus

    ''' <summary> Clears the send/receive status. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ClearSendReceiveStatus() Implements ITransport.ClearSendReceiveStatus
        Me.ReceiveStatus = StatusCode.ValueNotSet
        Me.SendStatus = StatusCode.ValueNotSet
        Me.TransportStatus = StatusCode.ValueNotSet
        Me.TransportProtocolMessage.Status = StatusCode.ValueNotSet
        Me.SentProtocolMessage.Status = StatusCode.ValueNotSet
        Me.ReceivedProtocolMessage.Status = StatusCode.ValueNotSet
    End Sub

    ''' <summary>
    ''' Determines whether transport was a success. For success, all status values must have a value
    ''' of Okay.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> <c>true</c> if this instance is success; otherwise, <c>false</c>. </returns>
    Public Function IsSuccess() As Boolean Implements ITransport.IsSuccess
        Return Me.TransportStatus = StatusCode.Okay AndAlso
            Me.SendStatus = StatusCode.Okay AndAlso
            Me.ReceiveStatus = StatusCode.Okay
    End Function

    ''' <summary> The failure message format. </summary>
    Private Const _FailureMessageFormat As String = "{0} failure occurred with status {1}:{2}."

    ''' <summary> Interprets all status values to return a proper message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A String. </returns>
    Public Function FailureMessage() As String Implements ITransport.FailureMessage
        If Me.IsSuccess Then
            Return StatusCode.Okay.Description
        ElseIf Me.ReceiveStatus = StatusCode.ValueNotSet Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat,
                                 "Transport", CInt(Me.TransportStatus), Me.TransportStatus.Description)
        ElseIf Me.ReceiveStatus <> StatusCode.Okay Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat,
                                 "Receive", CInt(Me.ReceiveStatus), Me.ReceiveStatus.Description)
        ElseIf Me.SendStatus = StatusCode.ValueNotSet Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat,
                                 "Transport", CInt(Me.TransportStatus), Me.TransportStatus.Description)
        ElseIf Me.SendStatus <> StatusCode.Okay Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat,
                                 "Send", CInt(Me.SendStatus), Me.SendStatus.Description)
        ElseIf Me.TransportStatus <> StatusCode.Okay Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat,
                                 "Transport", CInt(Me.TransportStatus), Me.TransportStatus.Description)
        Else
            Return "No status was set"
        End If
    End Function

#End Region

#Region " LOG "

    ''' <summary> Gets the transport hex message. This is the message that needs to be sent. </summary>
    ''' <value> The transport message. </value>
    Public ReadOnly Property TransportHexMessage As String Implements ITransport.TransportHexMessage
        Get
            Return If(Me.TransportProtocolMessage Is Nothing, String.Empty, Me.TransportProtocolMessage.HexMessage)
        End Get
    End Property

    ''' <summary>
    ''' Returns the status code obtained after parsing the transport protocol message.
    ''' </summary>
    ''' <value> The transport message status. </value>
    Public ReadOnly Property TransportMessageStatus As StatusCode Implements ITransport.TransportMessageStatus
        Get
            Return If(Me.TransportProtocolMessage Is Nothing, StatusCode.ValueNotSet, Me.TransportProtocolMessage.Status)
        End Get
    End Property

    ''' <summary>
    ''' Gets the hex message that was sent. This is the message that needs to be sent.
    ''' </summary>
    ''' <value> The Sent message. </value>
    Public ReadOnly Property SentHexMessage As String Implements ITransport.SentHexMessage
        Get
            Return If(Me.SentProtocolMessage Is Nothing, String.Empty, Me.SentProtocolMessage.HexMessage)
        End Get
    End Property

    ''' <summary> Returns the status code obtained after parsing the Sent protocol message. </summary>
    ''' <value> The sent message status. </value>
    Public ReadOnly Property SentMessageStatus As StatusCode Implements ITransport.SentMessageStatus
        Get
            Return If(Me.SentProtocolMessage Is Nothing, StatusCode.ValueNotSet, Me.SentProtocolMessage.Status)
        End Get
    End Property

    ''' <summary>
    ''' Returns the status code obtained after parsing the Received protocol message.
    ''' </summary>
    ''' <value> The received message status. </value>
    Public ReadOnly Property ReceivedMessageStatus As StatusCode Implements ITransport.ReceivedMessageStatus
        Get
            Return If(Me.ReceivedProtocolMessage Is Nothing, StatusCode.ValueNotSet, Me.ReceivedProtocolMessage.Status)
        End Get
    End Property

    ''' <summary>
    ''' Gets the hex message that was Received. This is the message that needs to be Received.
    ''' </summary>
    ''' <value> The Received message. </value>
    Public ReadOnly Property ReceivedHexMessage As String Implements ITransport.ReceivedHexMessage
        Get
            Return If(Me.ReceivedProtocolMessage Is Nothing, String.Empty, Me.ReceivedProtocolMessage.HexMessage)
        End Get
    End Property

    ''' <summary> Builds the log header. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A String. </returns>
    Public Function BuildLogHeader() As String Implements ITransport.BuildLogHeader
        Dim delimeter As String = ","
        Dim logEntery As New System.Text.StringBuilder
        logEntery.Append(NameOf(Transport.ItemNumber))
        logEntery.Append(delimeter)
        logEntery.Append(NameOf(Transport.TransportStatus))
        logEntery.Append(delimeter)
        logEntery.Append(NameOf(Transport.TransportHexMessage))
        logEntery.Append(delimeter)
        logEntery.Append(NameOf(Transport.SentHexMessage))
        logEntery.Append(delimeter)
        logEntery.Append(NameOf(Transport.SendStatus))
        logEntery.Append(delimeter)
        logEntery.Append(NameOf(Transport.ReceivedHexMessage))
        logEntery.Append(delimeter)
        logEntery.Append(NameOf(Transport.ReceiveStatus))
        Return logEntery.ToString
    End Function

    ''' <summary> The log delimeter. </summary>
    Private Const _LogDelimeter As Char = ","c

    ''' <summary> Builds the log line. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A String. </returns>
    Public Function BuildLogline() As String Implements ITransport.BuildLogline
        Dim logEntery As New System.Text.StringBuilder
        logEntery.Append(Me.ItemNumber.ToString)
        logEntery.Append(_LogDelimeter)
        If Me.TransportStatus = StatusCode.ValueNotSet Then
            logEntery.Append("")
        Else
            logEntery.Append(CInt(Me.TransportStatus).ToString)
        End If
        logEntery.Append(_LogDelimeter)
        If String.IsNullOrEmpty(Me.TransportHexMessage) Then
            logEntery.Append("")
        Else
            logEntery.Append(Me.TransportHexMessage)
        End If
        logEntery.Append(_LogDelimeter)
        If String.IsNullOrEmpty(Me.SentHexMessage) Then
            logEntery.Append("")
        Else
            logEntery.Append(Me.SentHexMessage)
        End If
        logEntery.Append(_LogDelimeter)
        If Me.SendStatus = StatusCode.ValueNotSet Then
            logEntery.Append("")
        Else
            logEntery.Append(CInt(Me.SendStatus).ToString)
        End If
        logEntery.Append(_LogDelimeter)
        If String.IsNullOrEmpty(Me.ReceivedHexMessage) Then
            logEntery.Append("")
        Else
            logEntery.Append(Me.ReceivedHexMessage)
        End If
        logEntery.Append(_LogDelimeter)
        If Me.ReceiveStatus = StatusCode.ValueNotSet Then
            logEntery.Append("")
        Else
            logEntery.Append(CInt(Me.ReceiveStatus).ToString)
        End If
        Return logEntery.ToString
    End Function

    ''' <summary> Parses the log line. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ParseLogline(ByVal value As String) As Boolean Implements ITransport.ParseLogline
        If String.IsNullOrEmpty(value) Then Return False
        Me.Clear()
        Dim values As String() = value.Split(_LogDelimeter)
        If values.Length < 7 Then
            Return False
        End If
        For i As Integer = 0 To values.Length - 1
            If String.IsNullOrEmpty(values(i)) Then
                values(i) = String.Empty
            End If
            Select Case i
                Case 0
                    If Not Integer.TryParse(values(i), Me.ItemNumber) Then
                        Return False
                    End If
                Case 1
                    If Not String.IsNullOrEmpty(values(i)) Then
                        If Not [Enum].TryParse(Of StatusCode)(values(i), Me.TransportStatus) Then
                            Return False
                        End If
                    End If
                Case 2
                    Me.TransportProtocolMessage.ParseHexMessage(values(i))
                Case 3
                    Me.SentProtocolMessage.ParseHexMessage(values(i))
                Case 4
                    If Not String.IsNullOrEmpty(values(i)) Then
                        If Not [Enum].TryParse(Of StatusCode)(values(i), Me.SendStatus) Then
                            Return False
                        End If
                    End If
                Case 5
                    Me.ReceivedProtocolMessage.ParseHexMessage(values(i))
                Case 6
                    If Not String.IsNullOrEmpty(values(i)) Then
                        If Not [Enum].TryParse(Of StatusCode)(values(i), Me.ReceiveStatus) Then
                            Return False
                        End If
                    End If
            End Select
        Next
        Return True
    End Function

    ''' <summary> The delimiter. </summary>
    Public Const Delimiter As String = " "

#End Region

End Class

''' <summary> Implements a collection of <see cref="Transport">Transport</see> entities. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Class TransportCollection
    Inherits Collections.ObjectModel.Collection(Of ITransport)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets or sets the transport template. </summary>
    ''' <value> The transport template. </value>
    Private ReadOnly Property TransportTemplate As ITransport

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TransportCollection" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transport"> The transport. </param>
    Public Sub New(ByVal transport As ITransport)
        MyBase.New()
        Me.TransportTemplate = Teleport.Transport.Clone(transport)
    End Sub

    ''' <summary> Adds the specified transport message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transportMessage"> The transport message. </param>
    Public Overloads Sub Add(ByVal transportMessage As String)
        If Not String.IsNullOrEmpty(transportMessage) Then
            Dim ci As ITransport = Teleport.Transport.Clone(Me.TransportTemplate)
            ci.TransportProtocolMessage.ParseHexMessage(transportMessage)
            ci.ItemNumber = Me.Count
            MyBase.Add(ci)
        End If
    End Sub

    ''' <summary> Adds the specified transport record. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transportRecord"> The transport record. </param>
    Public Overloads Sub AddRecord(ByVal transportRecord As String)
        If Not String.IsNullOrEmpty(transportRecord) Then
            Dim ci As ITransport = Teleport.Transport.Clone(Me.TransportTemplate)
            ci.ParseLogline(transportRecord)
            Me.Add(ci)
        End If
    End Sub

    ''' <summary> Clears the send receive status. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Public Sub ClearSendReceiveStatus()
        For Each ti As Transport In Me
            ti.ClearSendReceiveStatus()
        Next
    End Sub

    ''' <summary> Updates the transport info. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="transportInfo"> The Transport Info. </param>
    Public Sub UpdateTransportInfo(ByVal transportInfo As ITransport)
        If transportInfo Is Nothing Then
            Debug.Assert(False, "Transport info is nothing")
        ElseIf Me.Count = 0 Then
            ' ignore if we have no configuration loaded.
        ElseIf Me.Count > transportInfo.ItemNumber AndAlso transportInfo.ItemNumber >= 0 Then
            Dim transport As ITransport = Me.Item(transportInfo.ItemNumber)
            transport.ReceiveStatus = transportInfo.ReceiveStatus
            transport.SendStatus = transportInfo.SendStatus
            transport.TransportStatus = transportInfo.TransportStatus
            ' transport.SentHexMessage = transportInfo.SentHexMessage
            ' transport.ReceivedHexMessage = transportInfo.ReceivedHexMessage
        Else
            Debug.Assert(False, "Incorrect item number ", "Incorrect item number ", transportInfo.ItemNumber)
        End If
    End Sub

#End Region

#Region " FILE MANAGER "

    ''' <summary> Reads all items from the specified file name. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="fileName"> Name of the file. </param>
    Public Sub Read(ByVal fileName As String)
        Using sr As New StreamReader(fileName)
            Do Until sr.EndOfStream
                Me.Add(sr.ReadLine)
            Loop
        End Using
    End Sub

    ''' <summary> Reads all items from the log. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="fileName"> Name of the file. </param>
    Public Sub ReadLog(ByVal fileName As String)
        Me.Clear()
        Using sr As New StreamReader(fileName)
            sr.ReadLine()
            Do Until sr.EndOfStream
                Me.AddRecord(sr.ReadLine)
            Loop
        End Using
    End Sub

    ''' <summary> Save all items to the log. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="fileName"> Name of the file. </param>
    Public Sub SaveLog(ByVal fileName As String)
        Using sw As New StreamWriter(fileName)
            Dim wroteHeader As Boolean = False
            For Each item As Transport In Me
                If Not wroteHeader Then
                    sw.WriteLine(item.BuildLogHeader)
                    wroteHeader = True
                End If
                sw.WriteLine(item.BuildLogline)
            Next
        End Using
    End Sub

#End Region

End Class

