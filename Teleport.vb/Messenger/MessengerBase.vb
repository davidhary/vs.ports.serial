Imports System.Threading
Imports System.Windows.Threading
Imports System.IO.Ports
Imports isr.Core
Imports isr.Ports.Teleport.ExceptionExtensions
Imports isr.Ports.Serial
Imports isr.Ports.Serial.Methods

''' <summary>
''' A communicator capable of controlling or emulating a Rooster module. The communicator could
''' be a system controller also called <see cref="Emitter"/> following the SCPI/GPIB standard.
''' The communicator could also be the Rooster module, or a <see cref="Collector"/> sending
''' messages back to the controller.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-08 </para>
''' </remarks>
Public MustInherit Class MessengerBase
    Inherits isr.Core.Models.ViewModelTalkerBase
    Implements IMessenger

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MessengerBase" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="messengerRole">           The messenger role. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    Protected Sub New(ByVal moduleAddress As IEnumerable(Of Byte), ByVal messengerRole As MessengerRole, ByVal templateProtocolMessage As IProtocolMessage)
        Me.New(moduleAddress, messengerRole, templateProtocolMessage, isr.Ports.Serial.Port.Create)
        Me.IsPortOwner = True
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="MessengerBase" /> class. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="moduleAddress">           The module address. </param>
    ''' <param name="messengerRole">           The messenger role. </param>
    ''' <param name="templateProtocolMessage"> Message describing the template protocol. </param>
    ''' <param name="port">                    Specifies the
    '''                                        <see cref="isr.Ports.Serial.IPort">port</see>. </param>
    Protected Sub New(ByVal moduleAddress As IEnumerable(Of Byte), ByVal messengerRole As MessengerRole,
                      ByVal templateProtocolMessage As IProtocolMessage, ByVal port As isr.Ports.Serial.IPort)
        MyBase.New()

        ' tag this as implementing the message parser. 
        ' This instance will be assigned to the port as the message parser
        Me._ParseEnabled = True
        Me._MessengerRole = messengerRole
        Me._ModuleAddress = moduleAddress
        Me._Transport = New Transport(templateProtocolMessage)
        Me.AssignPort_(port)
        Me.StreamTerminationValue = 13
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._Port?.Dispose()
                Me._StreamWriter?.Dispose()
                Me._OutputMessage?.Dispose()
                Me._InputMessage?.Dispose()
                Me._Transport?.Dispose()
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " STATUS CODE MANAGEMENT "

    ''' <summary> Parses the status. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Indicator. </returns>
    Public Shared Function ParseStatus(ByVal value As StatusCode) As Indicator
        If [Enum].IsDefined(GetType(StatusCode), value) Then
            Select Case value
                Case StatusCode.ValueNotSet
                    Return Indicator.None
                Case StatusCode.Okay
                    Return Indicator.Okay
                Case Is < StatusCode.InvalidBaud
                    Return Indicator.Warning
                Case Is < StatusCode.ValueNotSet
                    Return Indicator.Error
                Case Else
                    Return Indicator.Warning
            End Select
        Else
            Return Indicator.None
        End If
    End Function

    ''' <summary> Query if 'value' represents an error code. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if error; otherwise <c>false</c> </returns>
    Public Shared Function IsErrorCode(ByVal value As StatusCode) As Boolean
        Return MessengerBase.ParseStatus(value) = Indicator.Error
    End Function

#End Region

#Region " TRANSMIT/RECEIVE LOG "

    ''' <summary> True to log events. </summary>
    Private _LogEvents As Boolean

    ''' <summary> Gets or sets a value indicating whether [log events]. </summary>
    ''' <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
    Public Property IsLogEvents As Boolean Implements IMessenger.IsLogEvents
        Get
            Return Me._LogEvents
        End Get
        Set(value As Boolean)
            Me._LogEvents = value
            If Me._StreamWriter Is Nothing AndAlso value AndAlso Not String.IsNullOrEmpty(Me.EventLogFileName) Then
                Me.OpenEventLogFile()
            End If
        End Set
    End Property

    ''' <summary> Gets the name of the event log file. </summary>
    ''' <value> The name of the event log file. </value>
    Public Property EventLogFileName As String Implements IMessenger.EventLogFileName

    ''' <summary> The stream writer. </summary>
    Private _StreamWriter As IO.StreamWriter

    ''' <summary> Opens the event log file. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Private Sub OpenEventLogFile()
        Me._StreamWriter = New IO.StreamWriter(Me.EventLogFileName)
    End Sub

    ''' <summary> Logs the message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="message"> The message. </param>
    ''' <param name="isSent">  if set to <c>true</c> [is sent]. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LogMessage(ByVal message As String, ByVal isSent As Boolean)
        Dim activity As String = "logging message"
        Try
            If Me._StreamWriter IsNot Nothing Then
                Me._StreamWriter.WriteLine("{0:HH:mm:ss:ffff},{1},{2}", DateTimeOffset.Now, IIf(isSent, "Tx", "Rx"), message)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MESSENGER MODULE AND SERVICING IMPLEMENTATON "

    ''' <summary> Gets the module address for this messenger. </summary>
    ''' <value> The module address. </value>
    Public Property ModuleAddress As IEnumerable(Of Byte) Implements IMessenger.ModuleAddress

    ''' <summary>
    ''' Services an error. This is a strictly listener mode.  It sends back an error message to the
    ''' talker.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns> A StatusCode. </returns>
    Public Overridable Function ServiceError(ByVal status As StatusCode) As StatusCode Implements IMessenger.ServiceError
        Return StatusCode.Okay
    End Function

    ''' <summary> Services the request. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Public Overridable Function ServiceRequest() As StatusCode Implements IMessenger.ServiceRequest
        Return StatusCode.Okay
    End Function

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Gets a value indicating whether this instance is open. </summary>
    ''' <value> The is port open. </value>
    Public ReadOnly Property IsPortOpen As Boolean Implements IMessenger.IsPortOpen
        Get
            Return Me.Port IsNot Nothing AndAlso Me.Port.IsOpen
        End Get
    End Property

#End Region

#Region " PORT MANAGEMENT "

    ''' <summary> Gets or sets the data buffering option. </summary>
    ''' <value> The data buffering option. </value>
    Public Property InputBufferingOption As isr.Ports.Serial.DataBufferingOption Implements IMessenger.InputBufferingOption
        Get
            Return Me._Port.InputBufferingOption
        End Get
        Set(value As isr.Ports.Serial.DataBufferingOption)
            Me._Port.InputBufferingOption = value
        End Set
    End Property

    ''' <summary> The port. </summary>
    Private _Port As isr.Ports.Serial.IPort

    ''' <summary> Gets or sets the port. </summary>
    ''' <value> The port. </value>
    Public Property Port As isr.Ports.Serial.IPort Implements IMessenger.Port
        Get
            Return Me._Port
        End Get
        Set(value As isr.Ports.Serial.IPort)
            Me.AssignPort_(value)
        End Set
    End Property

    ''' <summary> Gets the elapsed time stop watch. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The elapsed time stop watch. </value>
    Protected ReadOnly Property ElapsedTimeStopwatch As Stopwatch Implements IMessenger.ElapsedTimeStopwatch

    ''' <summary> Assigns the port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignPort_(ByVal value As IPort)
        If Me._Port IsNot Nothing Then
            RemoveHandler Me._Port.ConnectionChanged, AddressOf Me.PortConnectionChanged
            RemoveHandler Me._Port.DataReceived, AddressOf Me.PortDataReceived
            RemoveHandler Me._Port.DataSent, AddressOf Me.PortDataSent
            RemoveHandler Me._Port.SerialPortDisposed, AddressOf Me.PortSerialPortDisposed
            RemoveHandler Me._Port.SerialPortErrorReceived, AddressOf Me.PortSerialPortErrorReceived
            RemoveHandler Me._Port.Timeout, AddressOf Me.PortTimeout
            ' this also closes the session. 
            If value Is Nothing AndAlso Me.IsPortOwner Then Me._Port.Dispose()
        End If

        Me._ElapsedTimeStopwatch = New Stopwatch
        Me._Port = value
        If Me._Port IsNot Nothing Then
            AddHandler Me._Port.ConnectionChanged, AddressOf Me.PortConnectionChanged
            AddHandler Me._Port.DataReceived, AddressOf Me.PortDataReceived
            AddHandler Me._Port.DataSent, AddressOf Me.PortDataSent
            AddHandler Me._Port.SerialPortDisposed, AddressOf Me.PortSerialPortDisposed
            AddHandler Me._Port.SerialPortErrorReceived, AddressOf Me.PortSerialPortErrorReceived
            AddHandler Me._Port.Timeout, AddressOf Me.PortTimeout
        End If
    End Sub

    ''' <summary> Gets the is port that owns this item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The is port owner. </value>
    Public ReadOnly Property IsPortOwner As Boolean Implements IMessenger.IsPortOwner

    ''' <summary> Assigns a Port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overridable Sub AssignPort(ByVal value As IPort) Implements IMessenger.AssignPort
        Me._IsPortOwner = False
        Me.Port = value
        Me.TryNotifyConnectionChanged()
    End Sub

    ''' <summary> Releases the Port. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Overridable Sub ReleasePort() Implements IMessenger.ReleasePort
        Me._Port = Nothing
    End Sub

#End Region

#Region " RECEIVE MANAGMENT "

    ''' <summary> The received message. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> A message describing the input. </value>
    Public Property InputMessage As IProtocolMessage Implements IMessenger.InputMessage

    ''' <summary> Gets the last process status. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The last process status. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private ReadOnly Property LastProcessStatus As StatusCode

    ''' <summary>
    ''' Gets the sentinel indicating if the messenger is implementing the message parser interface
    ''' and has applied it to the <see cref="isr.Ports.Serial.Port">port.</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The parse enabled. </value>
    Public Property ParseEnabled As Boolean Implements IMessageParser.ParseEnabled

    ''' <summary> Parses the received byte values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The byte values received from the port. </param>
    ''' <returns> A StatusCode. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function ParseReceivedMesssage(ByVal values As IEnumerable(Of Byte)) As StatusCode

        Dim activity As String = "parsing"
        If values Is Nothing Then values = Array.Empty(Of Byte)()
        If Me.IsLogEvents Then Me.LogMessage(values.ToHex, False)
        Me.InputMessage.Clear()
        Dim transportStatus As StatusCode

        Try
            activity = "populating receive buffer"
            Me._ReadPointer = 0
            Me._ReceivedBuffer = New List(Of Byte)(values)
            activity = "parsing receive buffer into the input message"
            transportStatus = Me.InputMessage.ParseStream(Me._ReceivedBuffer)

            activity = "validating input against the output message"
            ' if the message is okay, validate it against the output message. 
            If transportStatus = StatusCode.Okay Then transportStatus = Me.OutputMessage.Validate(Me.InputMessage)

            If transportStatus <> StatusCode.Okay Then
                Me.Publish(TraceEventType.Error, $"failed @{activity};. status ={transportStatus}")
            End If

            activity = "updating the transport information"
            Me.UpdateTransportInfo(Me.InputMessage, transportStatus, False)

        Catch ex As Exception
            transportStatus = StatusCode.ExceptionParsing
            Me.UpdateTransportInfo(transportStatus)
            Me.PublishException(activity, ex)
        End Try
        Me._LastProcessStatus = transportStatus
        Return transportStatus

    End Function

    ''' <summary>
    ''' Parses byte values received from the port. This can be called automatically at the port when
    ''' the messenger is set to be <see cref="ParseEnabled"/>
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The byte values received from the port. </param>
    ''' <returns> An isr.Ports.Serial.MessageParserOutcome. </returns>
    Public Function Parse(ByVal values As IEnumerable(Of Byte)) As isr.Ports.Serial.MessageParserOutcome Implements IMessageParser.Parse
        Dim outcome As isr.Ports.Serial.MessageParserOutcome = MessageParserOutcome.None
        If Me.ParseEnabled Then
            Dim transportStatus As StatusCode = Me.ParseReceivedMesssage(values)
            If transportStatus = StatusCode.Okay Then
                outcome = MessageParserOutcome.Complete
            ElseIf Me.OutputMessage.IsIncomplete Then
                outcome = MessageParserOutcome.Incomplete
            ElseIf Me.OutputMessage.IsInvalid Then
                outcome = MessageParserOutcome.Invalid
            Else
                ' if the message is not defined as incomplete or invalid, it is complete even if broken
                ' for any reason. 
                outcome = MessageParserOutcome.Complete
            End If
            ' notify the top level of the received message to allow handling responses as necessary
            Me.TryNotifyMessageReceived(New ProtocolEventArgs(transportStatus, Me.InputMessage))
        End If

        Return outcome
    End Function

    ''' <summary> Process the received message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="receivedStatus"> The received status. </param>
    ''' <param name="receivedData">   Information describing the received. </param>
    ''' <returns> A StatusCode. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Function ProcessReceivedMessage(ByVal receivedStatus As Indicator, ByVal receivedData As IEnumerable(Of Byte)) As StatusCode Implements IMessenger.ProcessReceivedMessage

        Dim activity As String = "processing message received"
        Me.InputMessage.Clear()
        Dim transportStatus As StatusCode = StatusCode.Okay
        Try
            If Me.IsPortOpen Then
                If receivedStatus = Indicator.Okay Then
                    If Me.InputBufferingOption = isr.Ports.Serial.DataBufferingOption.CircularBuffer Then
                    Else
                        activity = "building receive buffer"
                        Me._ReadPointer = 0
                        Me._ReceivedBuffer = New List(Of Byte)(receivedData)
                    End If
                Else
                    Me.Publish(TraceEventType.Error, $"failed @{activity};. status ={receivedStatus}")
                End If
            Else
                transportStatus = StatusCode.Closed
            End If
        Catch ex As Exception
            transportStatus = StatusCode.ExceptionParsing
            Me.PublishException(activity, ex)
        Finally
            Me.TryNotifyDataReceived(New PortEventArgs(receivedStatus = Indicator.Okay, receivedData))
        End Try

        Try
            If transportStatus = StatusCode.Okay Then
                activity = "parsing receive buffer"
                transportStatus = Me.ParseReceivedMesssage(Me._ReceivedBuffer)
            Else
                Me.InputMessage.Clear()
                Me.InputMessage.Status = transportStatus
                Me.Publish(TraceEventType.Error, $"Receive failed;. status='{transportStatus}'")
            End If
        Catch ex As Exception
            transportStatus = StatusCode.ExceptionParsing
            Me.PublishException(activity, ex)
        Finally
            Me.UpdateTransportInfo(transportStatus)
            ' notify the top level of the received message to allow handling responses as necessary
            Me.TryNotifyMessageReceived(New ProtocolEventArgs(transportStatus, Me.InputMessage))
        End Try
        Me._LastProcessStatus = transportStatus
        Return transportStatus

    End Function

    ''' <summary>
    ''' Handles the DataReceived event of the _serialPort control. Updates the data boxes and the
    ''' received status.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the
    '''                       event data. </param>
    Private Sub PortDataReceived(sender As Object, e As isr.Ports.Serial.PortEventArgs)
        If Me.ParseEnabled Then
            ' if parse enabled, the 
            Me.TryNotifyDataReceived(New PortEventArgs(e))
        Else
            Me.ProcessReceivedMessage(If(e.StatusOkay, Indicator.Okay, Indicator.Error), e.DataBuffer)
        End If
        If Me.MessengerRole = MessengerRole.Collector AndAlso MessengerBase.IsErrorCode(Me.InputMessage.Status) Then
            Me.ServiceError(Me.InputMessage.Status)
        End If
    End Sub

#End Region

#Region " TRANSMIT MANAGEMENT "

    ''' <summary> The sent message. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> A message describing the output. </value>
    Public Property OutputMessage As IProtocolMessage Implements IMessenger.OutputMessage

    ''' <summary>
    ''' Holds a copy of the transmitted data.
    ''' </summary>
    Private _TransmittedBuffer As List(Of Byte)

    ''' <summary> Sends byte data. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="data"> The Byte data. </param>
    Public Sub SendData(ByVal data As IEnumerable(Of Byte)) Implements IMessenger.SendData
        ' clear the input message.
        Me.InputMessage.Clear()
        ' clear the transport send and receive status.
        Me.Transport.ClearSendReceiveStatus()
        If data?.Any Then
            Me.Port.SendData(data)
        End If
    End Sub

    ''' <summary> Sends the specified message and returns a reply. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="hexMessage">     The hex message. </param>
    ''' <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
    '''                               module starts to transmit a response. </param>
    ''' <param name="readTimeout">    The read timeout. </param>
    ''' <returns> An ITransport. </returns>
    Public Function Query(ByVal hexMessage As String, ByVal turnaroundTime As TimeSpan, ByVal readTimeout As TimeSpan) As ITransport Implements IMessenger.Query
        Return Me.Query(hexMessage.ToHexBytes, turnaroundTime, readTimeout)
    End Function

    ''' <summary> Sends the specified data and returns a reply. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data">           The data. </param>
    ''' <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
    '''                               module starts to transmit a response. </param>
    ''' <param name="readTimeout">    The read timeout. </param>
    ''' <returns> An ITransport. </returns>
    Public Function Query(ByVal data As IEnumerable(Of Byte), ByVal turnaroundTime As TimeSpan, ByVal readTimeout As TimeSpan) As ITransport Implements IMessenger.Query
        If data?.Any Then
            Me.Port.Resync()
            Me.SendData(data)

            ' TO_DO: Change to task; See VI Session Base wait for service request.
            isr.Core.ApplianceBase.DoEventsWait(turnaroundTime)

            Dim sw As Stopwatch = Stopwatch.StartNew
            Do Until (Me.Transport.ReceiveStatus <> StatusCode.ValueNotSet) OrElse sw.Elapsed > readTimeout
                isr.Core.ApplianceBase.DoEvents()
                Threading.Thread.SpinWait(10)
                isr.Core.ApplianceBase.DoEvents()
                Threading.Thread.SpinWait(10)
            Loop
        Else
            Throw New ArgumentNullException(NameOf(data))
        End If

        Return New Transport(Me.Transport)

    End Function

    ''' <summary> Sends the specified message and returns a reply. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="message">     The protocol message. </param>
    ''' <param name="readTimeout"> The read timeout. </param>
    ''' <returns> An ITransport. </returns>
    Public Function Query(ByVal message As IProtocolMessage, ByVal readTimeout As TimeSpan) As ITransport Implements IMessenger.Query
        If message Is Nothing Then Throw New ArgumentNullException(NameOf(message))
        Return Me.Query(message.BuildStream(Me.MessengerRole), Me.Transport.SelectCommand(message.CommandAscii).TurnaroundTime, readTimeout)
    End Function

    ''' <summary> Sends the protocol message. </summary>
    ''' <remarks>
    ''' Assumes data buffer/structure for appropriate channel has been loaded with data for transmit.
    ''' </remarks>
    ''' <returns> <see cref="StatusCode">global system status value</see> </returns>
    Protected Function SendProtocolMessage() As StatusCode
        Me._Port.SendData(Me.OutputMessage.BuildStream(Me.MessengerRole))
        Return StatusCode.Okay
    End Function

    ''' <summary> Echo back the received message as is. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <returns> A StatusCode. </returns>
    Protected Overridable Function SendEchoResponse() As StatusCode
        Me.OutputMessage.CopyFrom(Me.InputMessage)
        Return Me.SendProtocolMessage
    End Function

    ''' <summary>
    ''' Gets the messenger role. The messenger could be a talker (emitter or controller), controlling
    ''' a module or a listener (collector or receiver) emulating a module.
    ''' </summary>
    ''' <value> The messenger role. </value>
    Protected Property MessengerRole As MessengerRole Implements IMessenger.MessengerRole

#End Region

#Region " PORT DATA SENT MANAGEMENT "

    ''' <summary> Parses the sent byte values. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="values"> The byte values sent from the port. </param>
    ''' <returns> A StatusCode. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function ParseSentMesssage(ByVal values As IEnumerable(Of Byte)) As StatusCode

        Dim activity As String = "parsing"
        If values Is Nothing Then values = Array.Empty(Of Byte)()
        If Me.IsLogEvents Then Me.LogMessage(values.ToHex, False)
        Me.InputMessage.Clear()
        Dim transportStatus As StatusCode

        Try
            activity = "populating transmitter buffer"
            Me._TransmittedBuffer = New List(Of Byte)(values)

            activity = "parsing output message"
            transportStatus = Me.OutputMessage.ParseStream(Me._TransmittedBuffer)

            If transportStatus <> StatusCode.Okay Then
                Me.Publish(TraceEventType.Error, $"failed @{activity};. status={transportStatus}")
            End If

            activity = "updating the transport information"
            Me.UpdateTransportInfo(Me.OutputMessage, transportStatus, True)

        Catch ex As Exception
            transportStatus = StatusCode.ExceptionParsing
            Me.UpdateTransportInfo(transportStatus)
            Me.PublishException(activity, ex)
        End Try
        Me._LastProcessStatus = transportStatus
        Return transportStatus

    End Function

    ''' <summary> Process the sent message. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sentStatus"> The sent status. </param>
    ''' <param name="sentData">   Information describing the sent. </param>
    ''' <returns> A StatusCode. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Function ProcessSentMessage(ByVal sentStatus As Indicator, ByVal sentData As IEnumerable(Of Byte)) As StatusCode Implements IMessenger.ProcessSentMessage
        Dim activity As String = "processing message sent"
        Me.OutputMessage.Clear()
        Dim transportStatus As StatusCode = StatusCode.Okay
        Try
            If Me.IsPortOpen Then
                If Me.IsLogEvents Then
                    activity = "logging message"
                    Me.LogMessage(sentData.ToHex, True)
                End If
            Else
                transportStatus = StatusCode.Closed
            End If
            If sentStatus <> Indicator.Okay Then
                Me.Publish(TraceEventType.Error, $"failed @{activity};. status={sentStatus}")
            End If
        Catch ex As Exception
            transportStatus = StatusCode.ExceptionParsing
            Me.PublishException(activity, ex)
        Finally
            Me.TryNotifyDataSent(New PortEventArgs(sentStatus = Indicator.Okay, sentData))
        End Try

        Try
            If transportStatus = StatusCode.Okay Then
                transportStatus = Me.ParseSentMesssage(sentData)
            Else
                Me.OutputMessage.Clear()
                Me.OutputMessage.Status = transportStatus
                Me.Publish(TraceEventType.Error, $"Transmit failed;. status='{transportStatus}'")
            End If
        Catch ex As Exception
            transportStatus = StatusCode.ExceptionParsing
            Me.PublishException(activity, ex)
        Finally
            Me.UpdateTransportInfo(transportStatus)
            ' notify the top level of the sent message to allow handling responses as necessary
            Me.TryNotifyMessageSent(New ProtocolEventArgs(transportStatus, Me.OutputMessage))
        End Try
        Me._LastProcessStatus = transportStatus
        Return transportStatus
    End Function

    ''' <summary>
    ''' Handles the DataSent event of the _serialPort control. Updates controls and data.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the
    '''                       event data. </param>
    Private Sub PortDataSent(sender As Object, e As isr.Ports.Serial.PortEventArgs)
        Me.ProcessSentMessage(If(e.StatusOkay, Indicator.Okay, Indicator.Error), e.DataBuffer)
    End Sub

    ''' <summary> Handles the Connection Changed event of the _serialPort control. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="isr.Ports.Serial.ConnectionEventArgs" /> instance
    '''                       containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PortConnectionChanged(sender As Object, e As isr.Ports.Serial.ConnectionEventArgs)
        Dim activity As String = "notifying connection changed"
        Try
            Me.TryNotifyConnectionChanged()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    #End Region

    #Region " TRANSPORT "

    ''' <summary> The transport. </summary>
    Private ReadOnly _Transport As ITransport

    ''' <summary> Gets the <see cref="Transport">Transport</see>. </summary>
    ''' <value> The transport. </value>
    Public ReadOnly Property Transport As ITransport Implements IMessenger.Transport
        Get
            Return Me._Transport
        End Get
    End Property

    ''' <summary> Updates the <see cref="Transport">Transport</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="status"> The <see cref="StatusCode">status code</see> </param>
    Private Sub UpdateTransportInfo(ByVal status As StatusCode)
        If Me._Transport IsNot Nothing Then Me._Transport.TransportStatus = status
    End Sub

    ''' <summary> Updates the <see cref="Transport">Transport</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="protocolMessage"> Message describing the protocol. </param>
    ''' <param name="transportStatus"> The transport status. </param>
    ''' <param name="isSent">          if set to <c>true</c> [is sent]. </param>
    Private Sub UpdateTransportInfo(ByVal protocolMessage As IProtocolMessage, ByVal transportStatus As StatusCode, ByVal isSent As Boolean)
        If protocolMessage IsNot Nothing AndAlso Me.Transport IsNot Nothing Then
            If isSent Then
                protocolMessage.ParseStream()
                Me.Transport.SentProtocolMessage = protocolMessage
                If protocolMessage.HasStatusValue Then Me.Transport.SendStatus = CType(protocolMessage.Status, StatusCode)
                If transportStatus <> StatusCode.Okay Then Me.Transport.TransportStatus = transportStatus
            Else
                protocolMessage.ParseStream()
                Me.Transport.ReceivedProtocolMessage = protocolMessage
                If protocolMessage.HasStatusValue Then Me.Transport.ReceiveStatus = CType(protocolMessage.Status, StatusCode)
                Me.Transport.TransportStatus = transportStatus
            End If
        End If
    End Sub

#End Region

#Region " EVENT MANAGEMENT "

#Region " CONNECTION CHANGED "

    ''' <summary> Raises the connection event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub NotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Me.Port.MessageParser = If(Me.Port.IsOpen, Me, Nothing)
        Dim activity As String = $"notifying {Me.Port.SerialPort.PortName} port {If(Me.IsPortOpen, "open", "closed")}"
        Me.Publish(TraceEventType.Information, activity)
        Me.SyncNotifyConnectionChanged(e)
    End Sub

    ''' <summary> Raises the connection event within a try catch clause. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Dim activity As String = "notifying connection changed"
        Try
            Me.NotifyConnectionChanged(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Safely Invokes the <see cref="ConnectionChanged">connection changed event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub TryNotifyConnectionChanged()
        Me.TryNotifyConnectionChanged(New ConnectionEventArgs(Me.IsPortOpen))
    End Sub

    ''' <summary> Raises the connection event within a try catch clause. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="isOpen"> True if is open, false if not. </param>
    Protected Sub TryNotifyConnectionChanged(ByVal isOpen As Boolean)
        ' clear the i/o messages
        Me.InputMessage.Clear()
        Me.OutputMessage.Clear()
        Me.TryNotifyConnectionChanged(New ConnectionEventArgs(isOpen))
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="ConnectionChanged">connection changed event</see>. Must be called with
    ''' the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="obj"> The obj. </param>
    Private Sub UnsafeInvokeConnectionChanged(ByVal obj As Object)
        Me.UnsafeInvokeConnectionChanged(CType(obj, ConnectionEventArgs))
    End Sub

    ''' <summary> Removes the Connection Changed event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveConnectionChangedEventHandlers()
        Me._ConnectionChangedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Connection Changed event handlers. </summary>
    Private ReadOnly _ConnectionChangedEventHandlers As New EventHandlerContextCollection(Of ConnectionEventArgs)

    ''' <summary> Event queue for all listeners interested in Connection Changed events. 
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see> 
    ''' </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ConnectionChanged As EventHandler(Of ConnectionEventArgs) Implements IMessenger.ConnectionChanged
        AddHandler(value As EventHandler(Of ConnectionEventArgs))
            Me._ConnectionChangedEventHandlers.Add(New EventHandlerContext(Of ConnectionEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ConnectionEventArgs))
            Me._ConnectionChangedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ConnectionEventArgs)
            Me._ConnectionChangedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="ConnectionChanged">Connection Changed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ConnectionEventArgs"/> instance containing the event
    '''                  Connection. </param>
    Protected Sub SyncNotifyConnectionChanged(ByVal e As ConnectionEventArgs)
        Me._ConnectionChangedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " DATA RECEIVED "

    ''' <summary> Notifies a data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Private Sub NotifyDataReceived(ByVal e As PortEventArgs)
        Me.SyncNotifyDataReceived(e)
    End Sub

    ''' <summary> Try notify data received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyDataReceived(ByVal e As PortEventArgs)
        Dim activity As String = "notifying data received"
        Try
            Me.NotifyDataReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Removes the <see cref="DataReceived">data received</see> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveDataReceivedEventHandlers()
        Me._DataReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="DataReceived">data received</see> event handlers. </summary>
    Private ReadOnly _DataReceivedEventHandlers As New EventHandlerContextCollection(Of PortEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DataReceived">data received</see>/> events
    ''' Receipt status is reported along with the received data in the receive buffer
    ''' using the <see cref="PortEventArgs">port event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DataReceived As EventHandler(Of PortEventArgs) Implements IMessenger.DataReceived
        AddHandler(value As EventHandler(Of PortEventArgs))
            Me._DataReceivedEventHandlers.Add(New EventHandlerContext(Of PortEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of PortEventArgs))
            Me._DataReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As PortEventArgs)
            Me._DataReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DataReceived">Data Received Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
    Protected Sub SyncNotifyDataReceived(ByVal e As PortEventArgs)
        Me._DataReceivedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " DATA SENT "

    ''' <summary> Notifies a data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    Private Sub NotifyDataSent(ByVal e As PortEventArgs)
        Me.SyncNotifyDataSent(e)
    End Sub

    ''' <summary> Try notify data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyDataSent(ByVal e As PortEventArgs)
        Dim activity As String = "notifying data sent"
        Try
            Me.NotifyDataSent(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Removes the <see cref="DataSent">Data Sent event</see> handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveDataSentEventHandlers()
        Me._DataSentEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="DataSent">Data Sent event</see> handlers. </summary>
    Private ReadOnly _DataSentEventHandlers As New EventHandlerContextCollection(Of PortEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
    ''' Receipt status is reported along with the Sent data in the receive buffer
    ''' using the <see cref="PortEventArgs">port event arguments.</see> </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DataSent As EventHandler(Of PortEventArgs) Implements IMessenger.DataSent
        AddHandler(value As EventHandler(Of PortEventArgs))
            Me._DataSentEventHandlers.Add(New EventHandlerContext(Of PortEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of PortEventArgs))
            Me._DataSentEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As PortEventArgs)
            Me._DataSentEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DataSent">Data Sent Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="PortEventArgs"/> instance containing the event data. </param>
    Protected Sub SyncNotifyDataSent(ByVal e As PortEventArgs)
        Me._DataSentEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " MESSAGE RECEIVED "

    ''' <summary> Notifies a Message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Protocol event information. </param>
    Private Sub NotifyMessageReceived(ByVal e As ProtocolEventArgs)
        Me.SyncNotifyMessageReceived(e)
    End Sub

    ''' <summary> Try notify Message received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyMessageReceived(ByVal e As ProtocolEventArgs)
        Dim activity As String = "notifying message received"
        Try
            Me.NotifyMessageReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Removes the <see cref="MessageReceived">Message Received</see> event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveMessageReceivedEventHandlers()
        Me._MessageReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="MessageReceived">Message Received</see> event handlers. </summary>
    Private ReadOnly _MessageReceivedEventHandlers As New EventHandlerContextCollection(Of ProtocolEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="MessageReceived">Message Received</see> events.
    ''' The Received message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event MessageReceived As EventHandler(Of ProtocolEventArgs) Implements IMessenger.MessageReceived
        AddHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageReceivedEventHandlers.Add(New EventHandlerContext(Of ProtocolEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ProtocolEventArgs)
            Me._MessageReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="MessageReceived">MessageReceived Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyMessageReceived(ByVal e As ProtocolEventArgs)
        Me._MessageReceivedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " MESSAGE SENT "

    ''' <summary> Notifies a Message Sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Protocol event information. </param>
    Private Sub NotifyMessageSent(ByVal e As ProtocolEventArgs)
        Me.SyncNotifyMessageSent(e)
    End Sub

    ''' <summary> Try notify Message Sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyMessageSent(ByVal e As ProtocolEventArgs)
        Dim activity As String = "notifying message sent"
        Try
            Me.NotifyMessageSent(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Removes the <see cref="MessageSent">Message Sent</see> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveMessageSentEventHandlers()
        Me._MessageSentEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="MessageSent">Message Sent</see> event handlers. </summary>
    Private ReadOnly _MessageSentEventHandlers As New EventHandlerContextCollection(Of ProtocolEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="MessageSent">Message Sent</see>events.
    ''' The Sent message is provided in <see cref="ProtocolEventArgs">protocol event arguments.</see></summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event MessageSent As EventHandler(Of ProtocolEventArgs) Implements IMessenger.MessageSent
        AddHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageSentEventHandlers.Add(New EventHandlerContext(Of ProtocolEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._MessageSentEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ProtocolEventArgs)
            Me._MessageSentEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="MessageSent">Message Sent Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyMessageSent(ByVal e As ProtocolEventArgs)
        Me._MessageSentEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " SERIAL PORT DISPOSED "

    ''' <summary>
    ''' Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    Private Sub NotifySerialPortDisposed(ByVal e As EventArgs)
        Me.SyncNotifySerialPortDisposed(e)
    End Sub

    ''' <summary> Try notify serial port disposed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifySerialPortDisposed(ByVal e As EventArgs)
        Dim activity As String = "notifying serial port disposed"
        Try
            Me.Publish(TraceEventType.Information, activity)
            Me.NotifySerialPortDisposed(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Port disposed. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PortSerialPortDisposed(sender As Object, e As System.EventArgs)
        Me.TryNotifySerialPortDisposed(e)
    End Sub

    ''' <summary>
    ''' Removes the <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveSerialPortDisposedEventHandlers()
        Me._SerialPortDisposedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="SerialPortDisposed">Serial Port Disposed</see> event handlers. </summary>
    Private ReadOnly _SerialPortDisposedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port Disposed</see> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SerialPortDisposed As EventHandler(Of System.EventArgs) Implements IMessenger.SerialPortDisposed
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._SerialPortDisposedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._SerialPortDisposedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._SerialPortDisposedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="SerialPortDisposed">Serial Port Disposed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifySerialPortDisposed(ByVal e As System.EventArgs)
        Me._SerialPortDisposedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " SERIAL PORT ERROR RECEIVED "

    ''' <summary> Raises the serial port error received event. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub NotifySerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        Me.Publish(TraceEventType.Warning, $"Error {e.EventType} occurred in serial port {Me.Port.SerialPort.PortName}")
        Select Case e.EventType
            Case IO.Ports.SerialError.Frame
                Me.UpdateTransportInfo(StatusCode.FrameError)
            Case IO.Ports.SerialError.Overrun
                Me.UpdateTransportInfo(StatusCode.ReceiveOverrun)
            Case IO.Ports.SerialError.RXOver
                Me.UpdateTransportInfo(StatusCode.ReceiveOverrun)
            Case IO.Ports.SerialError.RXParity
                Me.UpdateTransportInfo(StatusCode.ParityError)
            Case IO.Ports.SerialError.TXFull
                Me.UpdateTransportInfo(StatusCode.TransmitOverrun)
            Case Else
                Me.UpdateTransportInfo(StatusCode.TransmitFailed)
        End Select
        Me.SyncNotifySerialPortErrorReceived(e)
    End Sub

    ''' <summary> Try notify serial port error received. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Serial error received event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifySerialPortErrorReceived(e As System.IO.Ports.SerialErrorReceivedEventArgs)
        Dim activity As String = "notifying serial port error received"
        Try
            Me.NotifySerialPortErrorReceived(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event of
    ''' the <see cref="IPort"/> Port.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.IO.Ports.SerialErrorReceivedEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub PortSerialPortErrorReceived(sender As Object, e As System.IO.Ports.SerialErrorReceivedEventArgs)
        Me.TryNotifySerialPortErrorReceived(e)
    End Sub

    ''' <summary>
    ''' Removes the <see cref="SerialPortErrorReceived">Serial port error receivd </see>&gt; event of
    ''' the <see cref="IPort"/> Port.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveSerialPortErrorReceivedEventHandlers()
        Me._SerialPortErrorReceivedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The SerialPortErrorReceived event handlers. </summary>
    Private ReadOnly _SerialPortErrorReceivedEventHandlers As New EventHandlerContextCollection(Of SerialErrorReceivedEventArgs)

    ''' <summary> Event queue for all listeners interested in the
    ''' <see cref="SerialPortErrorReceived">Serial port error receivd </see>> event of the <see cref="IPort"/> Port.
    ''' Serial Port Error Received status is reported with the
    ''' <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
    ''' </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event SerialPortErrorReceived As EventHandler(Of SerialErrorReceivedEventArgs) Implements IMessenger.SerialPortErrorReceived
        AddHandler(value As EventHandler(Of SerialErrorReceivedEventArgs))
            Me._SerialPortErrorReceivedEventHandlers.Add(New EventHandlerContext(Of SerialErrorReceivedEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of SerialErrorReceivedEventArgs))
            Me._SerialPortErrorReceivedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As SerialErrorReceivedEventArgs)
            Me._SerialPortErrorReceivedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="SerialPortErrorReceived">SerialPortErrorReceived Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="SerialErrorReceivedEventArgs" /> instance containing the event
    '''                  data. </param>
    Protected Sub SyncNotifySerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        Me._SerialPortErrorReceivedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " TIMEOUT MANAGER "

    ''' <summary> Notifies a timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Event information. </param>
    Private Sub NotifyTimeout(ByVal e As ProtocolEventArgs)
        If e IsNot Nothing AndAlso Me.Transport IsNot Nothing Then Me.Transport.TransportStatus = StatusCode.ReceiveTimeout
        Me.SyncNotifyTimeout(e)
    End Sub

    ''' <summary> Try notify data sent. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> Port event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub TryNotifyTimeout(ByVal e As ProtocolEventArgs)
        Dim activity As String = "notifying serial port timeout"
        Try
            Me.NotifyTimeout(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Safely posts the <see cref="Timeout">Timeout event</see>. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub TryNotifyTimeout()
        Me.TryNotifyTimeout(New ProtocolEventArgs(StatusCode.ReceiveTimeout, Me.InputMessage))
    End Sub

    ''' <summary> Handles the Timeout event of the _port control. Propagates the timeout. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub PortTimeout(sender As Object, e As System.EventArgs)
        Me.TryNotifyTimeout()
    End Sub

    ''' <summary> Removes the <see cref="Timeout"/> event handlers. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    Protected Sub RemoveTimeoutEventHandlers()
        Me._TimeoutEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="Timeout"/> event handlers. </summary>
    Private ReadOnly _TimeoutEventHandlers As New EventHandlerContextCollection(Of ProtocolEventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Timeout As EventHandler(Of ProtocolEventArgs) Implements IMessenger.Timeout
        AddHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._TimeoutEventHandlers.Add(New EventHandlerContext(Of ProtocolEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of ProtocolEventArgs))
            Me._TimeoutEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As ProtocolEventArgs)
            Me._TimeoutEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Timeout">Timeout Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="e"> The <see cref="ProtocolEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyTimeout(ByVal e As ProtocolEventArgs)
        Me._TimeoutEventHandlers.Send(Me, e)
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-22. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
