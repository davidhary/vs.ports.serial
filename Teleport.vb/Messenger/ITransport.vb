﻿''' <summary> Interface defining the <see cref="T:ITransport">transport</see>class. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Interface ITransport
    Inherits ICloneable, IDisposable

#Region " COMMANDS "

    ''' <summary> Populate the protocol commands. </summary>
    ''' <param name="collection"> The collection. </param>
    Sub PopulateCommands(ByVal collection As ProtocolCommandCollection)

    ''' <summary> Populate the protocol commands. </summary>
    ''' <param name="type"> The type. </param>
    Sub PopulateCommands(ByVal type As Type)

    ''' <summary> Gets or sets the commands. </summary>
    ''' <value> The commands. </value>
    ReadOnly Property Commands As ProtocolCommandCollection

    ''' <summary>
    ''' Gets or sets the sentinel indicating if the protocol message is a known transport message
    ''' command.
    ''' </summary>
    ''' <value>
    ''' The sentinel indicating if the protocol message is known transport message command.
    ''' </value>
    ReadOnly Property IsKnownTransportMessageCommand As Boolean

    ''' <summary> Select command. </summary>
    ''' <param name="commandAscii"> The command Ascii. </param>
    ''' <returns> A ProtocolCommand. </returns>
    Function SelectCommand(ByVal commandAscii As String) As ProtocolCommand

    ''' <summary> Gets or sets the transport message command. </summary>
    ''' <value> The 'transport message' command. </value>
    ReadOnly Property TransportMessageCommand As ProtocolCommand

    ''' <summary> Gets or sets the command ASCII. </summary>
    ''' <value> The command ASCII. </value>
    ReadOnly Property CommandAscii As String

#End Region

#Region " MEMBERS "

    ''' <summary> Gets or sets the item number. </summary>
    ''' <value> The item number. </value>
    Property ItemNumber As Integer

    ''' <summary>
    ''' Gets or sets the Transport status. This status includes timeout status as well as status
    ''' informing of mismatch between the received and sent messages.
    ''' </summary>
    ''' <value> The Transport status. </value>
    Property TransportStatus As StatusCode

    ''' <summary>
    ''' Gets or sets the <see cref="IProtocolMessage">Transport Protocol Message.</see>
    ''' </summary>
    ''' <value> A message describing the transport protocol. </value>
    Property TransportProtocolMessage As IProtocolMessage

    ''' <summary>
    ''' Gets or sets the <see cref="IProtocolMessage">Protocol Message</see> that was sent.
    ''' </summary>
    ''' <value> A message describing the sent protocol. </value>
    Property SentProtocolMessage As IProtocolMessage

    ''' <summary> Gets or sets the status of the sent message. </summary>
    ''' <value> The Send status. </value>
    Property SendStatus As StatusCode

    ''' <summary>
    ''' Gets or sets the <see cref="IProtocolMessage">Protocol Message</see> that was Received.
    ''' </summary>
    ''' <value> A message describing the received protocol. </value>
    Property ReceivedProtocolMessage As IProtocolMessage

    ''' <summary> Gets or sets the receive status. </summary>
    ''' <value> The receive status. </value>
    Property ReceiveStatus As StatusCode

    ''' <summary> Clears this instance. </summary>
    Sub Clear()

    ''' <summary> Clears the send/receive status. </summary>
    Sub ClearSendReceiveStatus()

    ''' <summary>
    ''' Determines whether transport was a success. For success, all status values must have a value
    ''' of Okay.
    ''' </summary>
    ''' <returns> <c>true</c> if this instance is success; otherwise, <c>false</c>. </returns>
    Function IsSuccess() As Boolean

    ''' <summary> Interprets all status values to return a proper message. </summary>
    ''' <returns> A String. </returns>
    Function FailureMessage() As String

#End Region

#Region " CLONE "

    ''' <summary> Copies the contents of an existing <see cref="ITransport" />. </summary>
    ''' <param name="transport"> The transport. </param>
    Sub CopyFrom(ByVal transport As ITransport)

#End Region

#Region " LOG "

    ''' <summary>
    ''' Gets or sets the transport hex message. This is the message that needs to be sent.
    ''' </summary>
    ''' <value> The transport message. </value>
    ReadOnly Property TransportHexMessage As String

    ''' <summary>
    ''' Returns the status code obtained after parsing the transport protocol message.
    ''' </summary>
    ''' <value> The transport message status. </value>
    ReadOnly Property TransportMessageStatus As StatusCode

    ''' <summary>
    ''' Gets or sets the hex message that was sent. This is the message that needs to be sent.
    ''' </summary>
    ''' <value> The Sent message. </value>
    ReadOnly Property SentHexMessage As String

    ''' <summary> Returns the status code obtained after parsing the Sent protocol message. </summary>
    ''' <value> The sent message status. </value>
    ReadOnly Property SentMessageStatus As StatusCode

    ''' <summary>
    ''' Gets or sets the hex message that was Received. This is the message that needs to be Received.
    ''' </summary>
    ''' <value> The Received message. </value>
    ReadOnly Property ReceivedHexMessage As String

    ''' <summary>
    ''' Returns the status code obtained after parsing the Received protocol message.
    ''' </summary>
    ''' <value> The received message status. </value>
    ReadOnly Property ReceivedMessageStatus As StatusCode

    ''' <summary> Builds the log header. </summary>
    ''' <returns> A String. </returns>
    Function BuildLogHeader() As String

    ''' <summary> Builds the log line. </summary>
    ''' <returns> A String. </returns>
    Function BuildLogline() As String

    ''' <summary> Parses the log line. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Function ParseLogline(ByVal value As String) As Boolean

#End Region

End Interface

