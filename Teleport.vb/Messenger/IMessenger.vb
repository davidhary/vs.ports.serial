Imports System.ComponentModel

Imports isr.Ports.Serial

''' <summary>
''' Defines the interface for the <see cref="MessengerBase">Teleport Messenger</see>
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Interface IMessenger
    Inherits IDisposable, INotifyPropertyChanged, Core.ITalker, isr.Ports.Serial.IMessageParser

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Gets or sets a value indicating whether the port is open. </summary>
    ''' <value> The is port open. </value>
    ReadOnly Property IsPortOpen As Boolean

#End Region

#Region " COMMUNICATION "

    ''' <summary> Sends byte data. </summary>
    ''' <param name="data"> The Byte data. </param>
    Sub SendData(ByVal data As IEnumerable(Of Byte))

    ''' <summary> Sends the specified message and returns a reply. </summary>
    ''' <param name="hexMessage">     The hex message. </param>
    ''' <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
    '''                               module starts to transmit a response. </param>
    ''' <param name="readTimeout">    The read timeout. </param>
    ''' <returns> An ITransport. </returns>
    Function Query(ByVal hexMessage As String, ByVal turnaroundTime As TimeSpan, ByVal readTimeout As TimeSpan) As ITransport

    ''' <summary> Sends the specified data and returns a reply. </summary>
    ''' <param name="data">           The data. </param>
    ''' <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
    '''                               module starts to transmit a response. </param>
    ''' <param name="readTimeout">    The read timeout. </param>
    ''' <returns> An ITransport. </returns>
    Function Query(ByVal data As IEnumerable(Of Byte), ByVal turnaroundTime As TimeSpan, ByVal readTimeout As TimeSpan) As ITransport

    ''' <summary> Sends the specified message and returns a reply. </summary>
    ''' <param name="message">     The protocol message. </param>
    ''' <param name="readTimeout"> The read timeout. </param>
    ''' <returns> An ITransport. </returns>
    Function Query(ByVal message As IProtocolMessage, ByVal readTimeout As TimeSpan) As ITransport

#End Region

#Region " TRANSMIT/RECEIVE LOG "

    ''' <summary> Gets or sets a value indicating whether [log events]. </summary>
    ''' <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
    Property IsLogEvents As Boolean

    ''' <summary> Gets or sets the name of the event log file. </summary>
    ''' <value> The name of the event log file. </value>
    Property EventLogFileName As String

#End Region

#Region " DATA MANAGEMENT "

    ''' <summary> Gets or sets the input data buffering option. </summary>
    ''' <value> The data buffering option. </value>
    Property InputBufferingOption As isr.Ports.Serial.DataBufferingOption

    ''' <summary> The received message. </summary>
    ''' <value> A message describing the input. </value>
    Property InputMessage As IProtocolMessage

    ''' <summary> The sent message. </summary>
    ''' <value> A message describing the output. </value>
    Property OutputMessage As IProtocolMessage

    ''' <summary> Gets or sets the <see cref="ITransport">Transport</see>. </summary>
    ''' <value> The transport. </value>
    ReadOnly Property Transport As ITransport

#End Region

#Region " PORT MANAGEMENT "

    ''' <summary> Gets or sets the elapsed time stopwatch. </summary>
    ''' <value> The elapsed time stopwatch. </value>
    ReadOnly Property ElapsedTimeStopwatch As Stopwatch

    ''' <summary> Gets or sets the port. </summary>
    ''' <value> The port. </value>
    Property Port As isr.Ports.Serial.IPort

    ''' <summary> Gets or sets the is port that owns this item. </summary>
    ''' <value> The is port owner. </value>
    ReadOnly Property IsPortOwner As Boolean

    ''' <summary> Assigns a Port. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Sub AssignPort(ByVal value As IPort)

    ''' <summary> Releases the Port. </summary>
    Sub ReleasePort()

    ''' <summary> Process the received message. </summary>
    ''' <param name="receivedStatus"> The received status. </param>
    ''' <param name="receivedData">   Information describing the received. </param>
    ''' <returns> A StatusCode. </returns>
    Function ProcessReceivedMessage(ByVal receivedStatus As Indicator, ByVal receivedData As IEnumerable(Of Byte)) As StatusCode

    ''' <summary> Process the sent message. </summary>
    ''' <param name="sentStatus"> The sent status. </param>
    ''' <param name="sentData">   Information describing the sent. </param>
    ''' <returns> A StatusCode. </returns>
    Function ProcessSentMessage(ByVal sentStatus As Indicator, ByVal sentData As IEnumerable(Of Byte)) As StatusCode

#End Region

#Region " MESSENGER MODULE MANAGEMENT "

    ''' <summary> Gets or sets the module address for this messenger. </summary>
    ''' <value> The module address. </value>
    Property ModuleAddress As IEnumerable(Of Byte)

    ''' <summary> Services an error. </summary>
    ''' <param name="status"> The status. </param>
    ''' <returns> A StatusCode. </returns>
    Function ServiceError(ByVal status As StatusCode) As StatusCode

    ''' <summary> Services the request. </summary>
    ''' <remarks>
    ''' This is the message handler routine for the RS485 interface which is a default communication
    ''' channel for Teleport modules.
    ''' </remarks>
    ''' <returns> A StatusCode. </returns>
    Function ServiceRequest() As StatusCode

    ''' <summary>
    ''' Gets or sets the messenger role. The messenger could be a talker (emitter or controller),
    ''' controlling a module or a listener (collector or receiver) emulating a module.
    ''' </summary>
    ''' <value> The messenger role. </value>
    Property MessengerRole As MessengerRole

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Occurs when connection changed.
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
    ''' </summary>
    Event ConnectionChanged As EventHandler(Of ConnectionEventArgs)

    ''' <summary>
    ''' Occurs when data is received.
    ''' Receipt status is report along with the received data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataReceived As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when data is Sent.
    ''' Send status is reported along with the Sent data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataSent As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when Message is received.
    ''' The received message is included in the <see cref="ProtocolEventArgs">protocol event arguments.</see>
    ''' </summary>
    Event MessageReceived As EventHandler(Of ProtocolEventArgs)

    ''' <summary>
    ''' Occurs when Message is sent.
    ''' The sent message is included in the <see cref="ProtocolEventArgs">protocol event arguments.</see>
    ''' </summary>
    Event MessageSent As EventHandler(Of ProtocolEventArgs)

    ''' <summary>
    ''' Occurs when Serial Port Error Received.
    ''' SerialPortErrorReceived status is reported with the 
    ''' <see cref="System.IO.Ports.SerialErrorReceivedEventArgs">SerialPortErrorReceived event arguments.</see>
    ''' </summary>
    Event SerialPortErrorReceived As EventHandler(Of System.IO.Ports.SerialErrorReceivedEventArgs)

    ''' <summary>
    ''' Occurs when Serial Port Disposed.
    ''' </summary>
    Event SerialPortDisposed As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Occurs when timeout.
    ''' </summary>
    Event Timeout As EventHandler(Of ProtocolEventArgs)

#End Region

End Interface

''' <summary> Values that represent messenger roles. </summary>
''' <remarks> David, 2020-10-22. </remarks>
Public Enum MessengerRole

    ''' <summary> An enum constant representing the emitter (controller/talker) option. </summary>
    <ComponentModel.Description("Emitter")>
    Emitter

    ''' <summary> An enum constant representing the collector (receiver/listener) option. </summary>
    <ComponentModel.Description("Collector")>
    Collector
End Enum

