# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.2.8138] - 2022-04-13
* Remove the Constructs project and add the Primitives, Concurrent and Binding Lists projects.
* Use project file package reference.

## [2.2.7645] - 2020-12-05
* Adds a serial port using Win32 Safe Handle class.

## [2.2.7624] - 2020-11-16
* Split off D1000 and Love unit test projects. Add Win32 unit tests.

## [2.1.6667] - 2018-04-03
* 2018 release.

## [2.0.6611] - 2018-02-06
* Uses core library elements.

## [1.0.4710] - 2012-11-23
* Removes .VB tags from assemblies.

## [1.0.4657] - 2012-10-01
* Hide bias console properties.

## [1.0.4655] - 2012-09-29
* Fixes a crush due to handling of the port disposed event.

## [1.0.4651] - 2012-09-25
* Adds a message parser interface. Rearranges Port code. Removes history comments. Handles the data sent event to display the event data.

## [1.0.4650] - 2012-09-24
* Validates Sync Context. Restore unsafe sync if sync context is nothing. Clear the input buffer on a resync. Assign receive delay.

## [0.5.4645] - 2012-09-19
* Defaults to hex display and enter hex. Handles the device error and dispose commands.

## [0.4.4643] - 2012-09-17
* Implements safe invokes and view message events.

## [0.3.4640] - 2012-09-14
* Fixes and adds extensions. Adds try-catch clauses to event handlers. Adds construction to message events arguments.

## [0.2.4635] - 2012-09-09
* Updates connection parameters on 'Connected'. Exposes Serial Port to the calling application, Removes threshold parameters. Documents the event methods. Changes connect and disconnect to functions. Renames panel to Port Terminal. Improve handling of characters. Makes some control properties hidden from the designer. Updates panel based on current port parameters. Fixes To Hex. Adds circular buffer. Applies code analysis changes.

## [0.1.4626] - 2012-08-31
* Version 0.1

\(C\) 2012 Integrated Scientific Resources, Inc. All rights reserved.


```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
